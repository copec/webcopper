using System;
using System.Collections;
using System.Globalization;
using System.util;
using iTextSharp.text;
using iTextSharp.text.pdf;
/*
 * Copyright 2004 Paulo Soares
 *
 * The contents of this file are subject to the Mozilla Public License Version 1.1
 * (the "License"); you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the License.
 *
 * The Original Code is 'iText, a free JAVA-PDF library'.
 *
 * The Initial Developer of the Original Code is Bruno Lowagie. Portions created by
 * the Initial Developer are Copyright (C) 1999, 2000, 2001, 2002 by Bruno Lowagie.
 * All Rights Reserved.
 * Co-Developer of the code is Paulo Soares. Portions created by the Co-Developer
 * are Copyright (C) 2000, 2001, 2002 by Paulo Soares. All Rights Reserved.
 *
 * Contributor(s): all the names of the contributors are added in the source code
 * where applicable.
 *
 * Alternatively, the contents of this file may be used under the terms of the
 * LGPL license (the "GNU LIBRARY GENERAL PUBLIC LICENSE"), in which case the
 * provisions of LGPL are applicable instead of those above.  If you wish to
 * allow use of your version of this file only under the terms of the LGPL
 * License and not to allow others to use your version of this file under
 * the MPL, indicate your decision by deleting the provisions above and
 * replace them with the notice and other provisions required by the LGPL.
 * If you do not delete the provisions above, a recipient may use your version
 * of this file under either the MPL or the GNU LIBRARY GENERAL PUBLIC LICENSE.
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the MPL as stated above or under the terms of the GNU
 * Library General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Library general Public License for more
 * details.
 *
 * If you didn't download this code from the following link, you should check if
 * you aren't using an obsolete version:
 * http://www.lowagie.com/iText/
 */

namespace iTextSharp.text.html.simpleparser {

    /**
    *
    * @author  psoares
    */
    public class FactoryProperties {
        
        private FontFactoryImp fontImp;

        /** Creates a new instance of FactoryProperties */
        public FactoryProperties() {
        }
        
        public Chunk CreateChunk(String text, ChainedProperties props) {
            Chunk ck = new Chunk(text, GetFont(props));
            if (props.HasProperty("sub"))
                ck.SetTextRise(-6);
            else if (props.HasProperty("sup"))
                ck.SetTextRise(6);
            return ck;
        }
        
        private static void SetParagraphLeading(Paragraph p, String leading) {
            if (leading == null) {
                p.SetLeading(0, 1.5f);
                return;
            }
            try {
                StringTokenizer tk = new StringTokenizer(leading, " ,");
                String v = tk.NextToken();
                float v1 = float.Parse(v, System.Globalization.NumberFormatInfo.InvariantInfo);
                if (!tk.HasMoreTokens()) {
                    p.SetLeading(v1, 0);
                    return;
                }
                v = tk.NextToken();
                float v2 = float.Parse(v, System.Globalization.NumberFormatInfo.InvariantInfo);
                p.SetLeading(v1, v2);
            }
            catch {
                p.SetLeading(0, 1.5f);
            }

        }

        public static Paragraph CreateParagraph(Hashtable props) {
            Paragraph p = new Paragraph();
            String value = (String)props["align"];
            if (value != null) {
                if (Util.EqualsIgnoreCase(value, "center"))
                    p.Alignment = Element.ALIGN_CENTER;
                else if (Util.EqualsIgnoreCase(value, "right"))
                    p.Alignment = Element.ALIGN_RIGHT;
                else if (Util.EqualsIgnoreCase(value, "justify"))
                    p.Alignment = Element.ALIGN_JUSTIFIED;
            }
            SetParagraphLeading(p, (String)props["leading"]);
            return p;
        }

        public static void CreateParagraph(Paragraph p, ChainedProperties props) {
            String value = props["align"];
            if (value != null) {
                if (Util.EqualsIgnoreCase(value, "center"))
                    p.Alignment = Element.ALIGN_CENTER;
                else if (Util.EqualsIgnoreCase(value, "right"))
                    p.Alignment = Element.ALIGN_RIGHT;
                else if (Util.EqualsIgnoreCase(value, "justify"))
                    p.Alignment = Element.ALIGN_JUSTIFIED;
            }
            SetParagraphLeading(p, props["leading"]);
            value = props["before"];
            if (value != null) {
                try {
                    p.SpacingBefore = float.Parse(value, System.Globalization.NumberFormatInfo.InvariantInfo);
                }
                catch {}
            }
            value = props["after"];
            if (value != null) {
                try {
                    p.SpacingAfter = float.Parse(value, System.Globalization.NumberFormatInfo.InvariantInfo);
                }
                catch {}
            }
            value = props["extraparaspace"];
            if (value != null) {
                try {
                    p.ExtraParagraphSpace = float.Parse(value, System.Globalization.NumberFormatInfo.InvariantInfo);
                }
                catch {}
            }
        }

        public static Paragraph CreateParagraph(ChainedProperties props) {
            Paragraph p = new Paragraph();
            CreateParagraph(p, props);
            return p;
        }

        public static ListItem CreateListItem(ChainedProperties props) {
            ListItem p = new ListItem();
            CreateParagraph(p, props);
            return p;
        }

        public Font GetFont(ChainedProperties props) {
            String face = props["face"];
            if (face != null) {
                StringTokenizer tok = new StringTokenizer(face, ",");
                while (tok.HasMoreTokens()) {
                    face = tok.NextToken().Trim();
                    if (FontFactory.IsRegistered(face))
                        break;
                }
            }
            int style = 0;
            if (props.HasProperty("i"))
                style |= Font.ITALIC;
            if (props.HasProperty("b"))
                style |= Font.BOLD;
            if (props.HasProperty("u"))
                style |= Font.UNDERLINE;
            String value = props["size"];
            float size = 12;
            if (value != null)
                size = float.Parse(value, System.Globalization.NumberFormatInfo.InvariantInfo);
            Color color = DecodeColor(props["color"]);
            String encoding = props["encoding"];
            if (encoding == null)
                encoding = BaseFont.WINANSI;
            FontFactoryImp ff = fontImp;
            if (ff == null)
                ff = FontFactory.FontImp;
            return ff.GetFont(face, encoding, true, size, style, color);
        }
        
        public static Color DecodeColor(String s) {
            if (s == null)
                return null;
            s = s.ToLower().Trim();
            Color c = (Color)colorTable[s];
            if (c != null)
                return c;
            try {
                if (s.StartsWith("#"))
                    return new Color(int.Parse(s.Substring(1), NumberStyles.HexNumber));
            }
            catch {
            }
            return null;
        }
        
        public FontFactoryImp FontImp {
            get {
                return fontImp;
            }
            set {
                fontImp = value;
            }
        }

        public static Hashtable colorTable = new Hashtable();
        public static Hashtable followTags = new Hashtable();
        static FactoryProperties() {
            followTags["i"] = "i";
            followTags["b"] = "b";
            followTags["u"] = "u";
            followTags["sub"] = "sub";
            followTags["sup"] = "sup";
            followTags["em"] = "i";
            followTags["strong"] = "b";
            
            colorTable["black"] = new Color(0x000000);
            colorTable["green"] = new Color(0x008000);
            colorTable["silver"] = new Color(0xC0C0C0);
            colorTable["lime"] = new Color(0x00FF00);
            colorTable["gray"] = new Color(0x808080);
            colorTable["olive"] = new Color(0x808000);
            colorTable["white"] = new Color(0xFFFFFF);
            colorTable["yellow"] = new Color(0xFFFF00);
            colorTable["maroon"] = new Color(0x800000);
            colorTable["navy"] = new Color(0x000080);
            colorTable["red"] = new Color(0xFF0000);
            colorTable["blue"] = new Color(0x0000FF);
            colorTable["purple"] = new Color(0x800080);
            colorTable["teal"] = new Color(0x008080);
            colorTable["fuchsia"] = new Color(0xFF00FF);
            colorTable["aqua"] = new Color(0x00FFFF);
        }
    }
}
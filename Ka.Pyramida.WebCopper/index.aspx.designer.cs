﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.4200
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------



public partial class _Index {
    
    /// <summary>
    /// MCHeader control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Controls_Header MCHeader;
    
    /// <summary>
    /// MCPanelProtokol control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Ka.Garance.Web.Panel.Panel MCPanelProtokol;
    
    /// <summary>
    /// PanelItem1 control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Ka.Garance.Web.Panel.PanelItem PanelItem1;
    
    /// <summary>
    /// PanelItem2 control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Ka.Garance.Web.Panel.PanelItem PanelItem2;
    
    /// <summary>
    /// MCPanelKatalog control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Ka.Garance.Web.Panel.Panel MCPanelKatalog;
    
    /// <summary>
    /// PanelItem3 control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Ka.Garance.Web.Panel.PanelItem PanelItem3;
    
    /// <summary>
    /// PanelItem4 control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Ka.Garance.Web.Panel.PanelItem PanelItem4;
    
    /// <summary>
    /// MCPanelFaktury control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Ka.Garance.Web.Panel.Panel MCPanelFaktury;
    
    /// <summary>
    /// PanelItem5 control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Ka.Garance.Web.Panel.PanelItem PanelItem5;
    
    /// <summary>
    /// PanelItem6 control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Ka.Garance.Web.Panel.PanelItem PanelItem6;
    
    /// <summary>
    /// MCPanelClaim control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Ka.Garance.Web.Panel.Panel MCPanelClaim;
    
    /// <summary>
    /// PanelItem7 control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Ka.Garance.Web.Panel.PanelItem PanelItem7;
    
    /// <summary>
    /// PanelItem8 control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Ka.Garance.Web.Panel.PanelItem PanelItem8;
}

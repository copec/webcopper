﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="_Protocol" Codebehind="protocols.aspx.cs" %>
<%@ Register TagPrefix="yuki" TagName="header" Src="Controls/Header.ascx" %>
<%@ Register TagPrefix="yuki" Namespace="Ka.Garance.Web.List" Assembly="garance" %>
<%@ Register TagPrefix="yuki" Namespace="Ka.Garance.Web.TabList" Assembly="garance" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Language.Code%>" dir="ltr" lang="<%=Language.Code%>">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><%=Language["APP_TITLE"]%></title>
<link rel="stylesheet" type="text/css" href="css/yuki.css" />
<link rel="stylesheet" type="text/css" href="css/combo.css" />
</head>

<body>

<yuki:header Id="MCHeader" LocationID="LOCATION_PROTOCOL" runat="server" />

<div align="center" style="padding:0px 16px 16px 16px"> 
<table border="0" cellpadding="0" cellspacing="0" align="center" style="width:1%">
<tr>
 <td align="left">
 
<div style="width:800px"> 

<img src="images/mod.protocol.png" alt="" style="vertical-align:text-bottom" />
<span class="mod"><%=Language["HEADER_PROTOCOL"]%></span><br /><br />


<form action="protocols.aspx" method="post">
<table border="0" cellpadding="4" cellspacing="1" class="list">
<tr class="category">
 <td colspan="2"><%=Language["PARTS_FILTER"]%></td>
</tr>
<tr class="even">
 <td width="15%"><label for="c101"><%=Language["PROTO_NUMBER"]%></label></td>
 <td class="odd"><input id="c101" type="text" name="pnum" value="<%=Server.HtmlEncode(FilterNum)%>" size="20" maxlength="20" class="sw-input" /></td>
</tr>
<tr class="even">
 <td width="15%"><label for="c102"><%=Language["BIKES_LIST_CHASSIS"]%></label></td>
 <td class="odd"><input id="c102" type="text" name="pvin" value="<%=Server.HtmlEncode(FilterVin)%>" size="20" maxlength="20" class="sw-input" /></td>
</tr>
<tr class="footer">
 <td colspan="2">
  <input type="submit" value="<%=Language["BUTTON_FILTER"]%>" style="font-weight:bold" />
  <input type="button" value="<%=Language["BUTTON_FILTER_RESET"]%>" onclick="location='protocols.aspx?filter=';" />
 </td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
</form>
<br />


<yuki:TabList ID="MCFilterTabs" runat="server">
 <yuki:TabItem ID="TabItem1" LangID="PROTO_STATE_ALL" Link="protocols.aspx?state=-1" runat="server" />
 <yuki:TabItem ID="TabItem8" LangID="PROTO_STATE_NEW" Link="protocols.aspx?state=0" runat="server" />
 <yuki:TabItem ID="TabItem9" LangID="PROTO_STATE_SENT" Link="protocols.aspx?state=1" runat="server" /> 
 <yuki:TabItem ID="TabItem2" LangID="PROTO_STATE_ERROR" Link="protocols.aspx?state=2" runat="server" />
 <yuki:TabItem ID="TabItem3" LangID="PROTO_STATE_NOINVOICE" Link="protocols.aspx?state=3" runat="server" />
 <yuki:TabItem ID="TabItem4" LangID="PROTO_STATE_INVOICE" Link="protocols.aspx?state=4" runat="server" /> 
 <yuki:TabItem ID="TabItem5" LangID="PROTO_STATE_AUTHORIZED" Link="protocols.aspx?state=5" runat="server" /> 
 <yuki:TabItem ID="TabItem6" LangID="PROTO_STATE_CANCELED" Link="protocols.aspx?state=6" runat="server" />
 <yuki:TabItem ID="TabItem7" LangID="PROTO_STATE_REMOVED" Link="protocols.aspx?state=7" runat="server" />
</yuki:TabList>
<yuki:List ID="MCProtoList" Identifier="cislo" Link="protocol.detail.aspx?id={0}" Name="ProtoList" runat="server">
 <yuki:ListItem ID="ListItem1" LangID="PROTO_LIST_NUMBER" ColumnName="cislo" Default="true" runat="server" /> 
 <yuki:ListItem ID="ListItem2" LangID="PROTO_LIST_CHASSIS" ColumnName="karoserie" runat="server" /> 
 <yuki:ListItem ID="ListItem5" LangID="PROTO_LIST_NAME" ColumnName="jmeno" runat="server" /> 
 <yuki:ListItem ID="ListItem6" LangID="PROTO_LIST_CITY" ColumnName="obec" runat="server" />  
 <yuki:ListItem ID="ListItem4" LangID="PROTO_LIST_DATE" ColumnName="datum_opravy" Format="{0:d'. 'M'. 'yyyy}" runat="server" /> 
 <yuki:ListItem ID="ListItem3" LangID="PROTO_LIST_STATE" ColumnName="tcstav" td_style="text-transform:lowercase" runat="server" /> 
</yuki:List>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>

</div>
 </td>
</tr>
</table>
</div>

</body>

</html>
﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="_Protocol_Detail" Codebehind="protocol.detail.aspx.cs" %>
<%@ Register TagPrefix="yuki" Namespace="Ka.Garance.Web.List" Assembly="garance" %>
<%@ Register TagPrefix="yuki" TagName="header" Src="Controls/Header.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Language.Code%>" dir="ltr" lang="<%=Language.Code%>">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><%=Language["APP_TITLE"]%></title>
<link rel="stylesheet" type="text/css" href="css/yuki.css" />
<script language="javascript" type="text/javascript" src="js/ajax.js"></script>
<script language="javascript" type="text/javascript" src="js/protocol.detail.js"></script>
</head>

<body>

<yuki:header Id="MCHeader" LocationID="LOCATION_PROTOCOL_DETAIL" runat="server" />

<div align="center" style="padding:0px 16px 16px 16px"> 
<table border="0" cellpadding="0" cellspacing="0" align="center" style="width:1%">
<tr>
 <td align="left">
 
<div style="width:600px"> 


<table border="0" cellpadding="0" cellspacing="0" style="width:100%">
<tr>
 <td align="left">
  <img src="images/mod.protocol.png" alt="" style="vertical-align:text-bottom" />
  <span class="mod"><%=Language["HEADER_PROTOCOL_DETAIL"]%> - <%=(Deleted ? Request.QueryString["deleid"] : Protocol.Cislo)%></span>
 </td><% if (!Deleted && !Deleting) { %>
 <td align="right" valign="bottom">
   <button type="button" class="main" onclick="location='protocol.detail.aspx?id=<%=Request.QueryString["id"]%>\x26pdf=';" style="padding:4px">
    <img src="images/print.pdf.png" alt="" />
    <%=Language["BUTTON_PRINT"]%>
   </button>
 </td><% } %>
</table>&nbsp;
<br />

<table border="0" cellpadding="0" cellspacing="0" style="width:100%"> 
<tr>
 <td align="left" width="99%">
<table border="0" cellpadding="4" cellspacing="1" class="list">
<tr class="header">
 <th align="left">
<table border="0" cellpadding="0" cellspacing="0" style="width:100%"> 
<tr>
 <td align="left" style="color:#fff" width="33%"><%=Language["HEADER_PROTOCOL_DETAIL"]%></td>
 <td align="center" style="color:#fff" width="34%"><%=ProtocolStavText%></td>
 <td align="right" style="color:#fff" width="33%"><%=Language["PROTO_NUMBER"]%>: <%=(Deleting ? Request.QueryString["delid"] : Protocol.Cislo)%></td>
</tr> 
</table>
 </th>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
 </td><% if (ProtocolHasZprava && !Deleting) { %>
 <td align="right">
  <div style="padding-left:4px;cursor:pointer" onclick="displayMessage(this,'<%=JsProtocolZprava%>');"><img id="imgMessage001" src="images/message.png" width="24" height="24" alt="" /></div>
  <script language="javascript" type="text/javascript" src="js/message.js"></script>
  <script language="javascript" type="text/javascript">
  //<![CDATA[
  MSG_BUTTON_CLOSE = '<%=Language["BUTTON_CLOSE"]%>';
  MSG_MESSAGE = '<%=Language["PROTO_MESSAGE"]%>';
  MSG_DisplayStartup = '<%=Protocol.Stav==2?"imgMessage001":""%>';
  //]]>
  </script>
 </td><% } %>
</tr>
</table>
<br />

<% if (Deleting) { %>

<table border="0" cellpadding="4" cellspacing="1" class="list">
<tr class="category">
 <td><%=Language["PROTO_DELETE_REVIEW"]%></td>
</tr>
<tr class="odd">
 <td align="center">
  <div style="padding:32px 8px 32px 8px"><%=Language["PROTO_DELETE_INFOA"]%></div>
 </td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
<br />

<table border="0" cellpadding="2" cellspacing="1" class="list">
<tr class="footer">
 <td align="right">
<form action="protocol.detail.aspx?id=<%=Request.QueryString["delid"]%>" method="post"><input type="hidden" name="pcheck" value="" />
<table border="0" cellpadding="0" cellspacing="0" style="width:100%">
<tr>
 <td align="left"> 
  <button type="submit" name="p_btn_delete" class="main" onclick="this.form.pcheck.value='<%=PCHECK_DELETE%>';return true;">
   <img src="images/delete.png" alt="" />
   <%=Language["BUTTON_DELETE"]%>
  </button> 
 </td>
 <td align="right">
  <button type="button" class="main" onclick="location='protocol.detail.aspx?id=<%=Request.QueryString["delid"]%>';">
   <img src="images/cancel.png" alt="" />
   <%=Language["BUTTON_CANCEL"]%>
  </button>
 </td> 
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
</form>
 </td>
</tr>
</table>

<% } else if (Deleted) { %>

<table border="0" cellpadding="4" cellspacing="1" class="list">
<tr class="category">
 <td><%=Language["PROTO_DELETE_REVIEW"]%></td>
</tr>
<tr class="odd">
 <td align="center">
  <div style="padding:32px 8px 32px 8px"><%=Language["PROTO_DELETE_INFO"]%></div>
 </td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
<br />


<table border="0" cellpadding="2" cellspacing="1" class="list">
<tr class="footer">
 <td align="left">
  <button type="button" class="main" onclick="location='protocols.aspx';">
   <img src="images/cancel.png" alt="" />
   <%=Language["BUTTON_CLOSE"]%>
  </button>
 </td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>


<% } else { %>

<table border="0" cellpadding="4" cellspacing="1" class="list">
<tr class="category">
 <td colspan="4"><%=Language["PROTO_ADD_BASICS"]%></td>
</tr>
<tr class="even">
 <td width="20%"><%=Language["PROTO_NUMBER"]%></td>
 <td nowrap="nowrap" class="odd"><%=Protocol.Cislo%></td>
 <td width="20%"><%=Language["PROTO_DATE_CREATED"]%></td>
 <td width="20%" nowrap="nowrap" class="odd"><%=Protocol.DateCreated.ToString("d'. 'M'. 'yyyy' 'HH':'mm")%></td>
</tr>
<tr class="even">
 <td width="20%"><%=Language["PROTO_GROUP_OWNER"]%></td>
 <td colspan="3" class="odd">
<table border="0" cellpadding="0" cellspacing="0"> 
<tr>
 <td valign="top" align="left"><%=Protocol.Jmeno%><br /><%=(Protocol.Adresa != null && Protocol.Adresa.Trim() != "" ? string.Format("{0}<br />", Protocol.Adresa) : "")%><%=Protocol.Obec%> <%=Protocol.PSC+GetICOString()%></td>
 <td width="50">&nbsp;</td>
 <td valign="top" align="left">Tel: <%=Protocol.Telefon%></td> 
</tr> 
</table> 
 </td>
</tr>
<tr class="even">
 <td width="20%"><%=Language["BIKES_LIST_MODEL"]%></td>
 <td class="odd"><%=Protocol.Model%></td>
 <td width="20%"><%=Language["PROTO_DATE_REPAIR"]%></td>
 <td width="20%" class="odd"><%=Protocol.DateRepair.ToString("d'. 'M'. 'yyyy")%></td> 
</tr>
<tr class="even">
 <td width="20%"><%=Language["BIKES_LIST_CHASSIS"]%></td>
 <td class="odd"><%=Protocol.VIN%></td>
 <td width="20%"><%=Language["PROTO_KM"]%></td>
 <td width="20%" class="odd"><%=Protocol.Km.ToString()%></td> 
</tr>
<tr class="even">
 <td width="20%"><%=Language["BIKES_LIST_DATE"]%></td>
 <td colspan="3" class="odd"><%=Protocol.DateSold.ToString("d'. 'M'. 'yyyy")%></td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
<br />

<div id="MP_ProtoDiv_Issue">
<table border="0" cellpadding="4" cellspacing="1" class="list">
<tr class="category">
 <td colspan="4"><%=Language["PROTO_ADD_EXTEND"]%></td>
</tr>
<tr class="even">
 <td width="20%"><%=Language["PROTO_KO"]%></td>
 <td width="30%" class="odd"><%=Protocol.KOTitle%></td>
 <td width="20%"><%=Language["PROTO_GT"]%></td>
 <td width="30%" class="odd"><%=Protocol.DZTitle%></td>
</tr>
<tr class="even">
 <td width="20%"><%=Language["PROTO_WORKER"]%></td>
 <td colspan="3" width="80%" class="odd"><%=Server.HtmlEncode(Protocol.Resitel)%></td>
</tr>
<tr class="even">
 <td width="20%"><%=Language["PROTO_PHONE"]%></td>
 <td width="30%" class="odd"><%=Server.HtmlEncode(Protocol.ResitelTel)%></td>
 <td width="20%"><%=Language["PROTO_FAX"]%></td>
 <td width="30%" class="odd"><%=Server.HtmlEncode(Protocol.ResitelFax)%></td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
<br />

<yuki:List ID="MCProtocolDetailList" Identifier="idzavadadealer" Name="MLProtocolDetail" RowNumbers="false" DisplayFooter="false" runat="server">
 <yuki:ListItem ID="ListItem0" Orderable="false" ColumnName="poradi" align="center" td_align="center" Title="&nbsp;" Format="{0}." width="1%" runat="server" />
 <yuki:ListItem ID="ListItem1" Orderable="false" ColumnName="kod" LangID="PROTO_ISSUE_CODE" align="center" td_align="center" width="1%" runat="server" />
 <yuki:ListItem ID="ListItem2" Orderable="false" ColumnName="druh" LangID="PROTO_ISSUE_DZ" align="center" td_align="center" width="1%" runat="server" />
 <yuki:ListItem ID="ListItem3" Orderable="false" ColumnName="vyrobce" LangID="PROTO_ISSUE_MAN" align="center" width="1%" td_align="center" runat="server" />
 <yuki:ListItem ID="ListItem4" Orderable="false" ColumnName="zavada" LangID="PROTO_ISSUE_TITLE" align="left" runat="server" />
 <yuki:ListItem ID="ListItem5" Orderable="false" ColumnName="popis" LangID="PROTO_ISSUE_TEXT" align="left" runat="server" />
 <yuki:ListItem ID="ListItem6" Orderable="false" ColumnName="prace" LangID="PROTO_ISSUE_WORK" Format="{0:F2}" align="right" width="1%" td_align="right" runat="server" />
 <yuki:ListItem ID="ListItem7" Orderable="false" ColumnName="material" LangID="PROTO_ISSUE_MAT" Format="{0:F2}" align="right" width="1%" td_align="right" runat="server" />
 <yuki:ListItem ID="ListItem8" Orderable="false" ColumnName="stav" LangID="PROTO_ISSUE_STATE" align="center" td_align="center" width="1%" runat="server" />
</yuki:List>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
<br />

<table border="0" cellpadding="2" cellspacing="1" class="list">
<tr class="footer">
 <td>
  <table border="0" cellpadding="0" cellspacing="0" style="width:100%">
  <tr><%=GetEditButton()%>
   <td align="right">
    <button type="button" class="main" onclick="location='protocols.aspx';">
     <img src="images/cancel.png" alt="" />
     <%=Language["BUTTON_CLOSE"]%>
    </button>
   </td>
  </tr>
  </table>
 </td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>

</div>

<% } %>

</div>

 </td>
</tr>
</table>

</body>

</html>
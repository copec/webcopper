﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class _Claim : Ka.Garance.Web.GarancePage
{
	protected void Page_Load(object sender, EventArgs e)
	{
		MCClaimList.Filter = string.Format("A.iddealer={0} GROUP BY B.idlist", User.Id);
		MCClaimList.SQLCount = string.Format("SELECT COUNT(*) FROM tlisty WHERE iddealer={0}", User.Id);
		MCClaimList.SQL = @"
			SELECT A.id AS id,A.nazev AS nazev,A.datum AS datum, COUNT(B.idlist) AS pocet
			FROM tlisty A
			INNER JOIN tlistydil B ON B.idlist=A.id
		";
	}
}

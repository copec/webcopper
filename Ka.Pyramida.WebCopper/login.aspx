﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="_Login" Codebehind="login.aspx.cs" %>

<DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Language.Code%>" dir="ltr" lang="<%=Language.Code%>">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><%=Language["APP_TITLE"]%></title>
<link rel="stylesheet" type="text/css" href="css/login.css" />
</head>

<body>

<table border="0" cellpadding="0" cellspacing="0" style="width:100%">
<tr>
 <td colspan="2" style="background:url(images/bk.top.png) repeat-x 0% 0% #8BA8C7"><img src="images/<%=Language["APP_ICON"]%>" alt="<%=Language["APP_TITLE_ICON"]%>" style="margin:2px 7px 2px 7px" /></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" style="width:100%">
<tr>
 <td align="center">
 
<div align="center" style="height:290px">

<div align="center" style="padding:48px 0px 48px 0px;white-space:nowrap">
 <%=Language["LOGIN_INFO"]%> &nbsp; | &nbsp; <%=DateTime.Now.ToString("D", System.Globalization.CultureInfo.GetCultureInfo(Language.Code))%>
</div>

<% if (LoginFailed) { %>
<table border="0" cellpadding="4" cellspacing="2" style="width:1%">
<tr>
 <td nowrap="nowrap" align="center">
<b style="color:red"><%=Language["ERROR"]%></b><br /><br /><%=Language["LOGIN_ERROR"]%><br /><br />
<a href="login.aspx" class="login">&raquo;</a>
 </td>
</tr>
</table>
<% } else { %>
<table border="0" cellpadding="0" cellspacing="0" style="width:220px;background:url(images/form.bk.gif) repeat-x 0% 0% #7E9FC1">
<tr>
 <td align="left" valign="top" width="1%"><div style="height:16px"><img src="images/form.l.gif" alt="" width="11" height="14" /></div></td>
 <td align="left" width="98%" style="color:white;font-family:Arial,Verdana;font-size:10pt;font-weight:bold;padding:1px 0px 1px 0px"><%=Language["LOGIN_TITLE"]%></td>
 <td align="right" valign="top" width="1%"><div style="height:16px"><img src="images/form.r.gif" alt="" width="11" height="14" /></div></td>
</tr>
<tr bgcolor="white">
 <td colspan="3" align="center" valign="top">
 
<form action="login.aspx" method="post">
<table border="0" cellpadding="3" cellspacing="1" style="width:100%;border:1px solid #95B1CC;border-bottom:0px">
<tr>
 <td class="active" align="center" nowrap="nowrap"><div style="width:60px"><label for="c100"><%=Language["LOGIN_LANG"]%>:</label></div></td>
 <td class="odd" align="center">
  <select id="c100" name="language" class="login" onchange="form.submit();">
<%=GetLangOptions()%>
  </select>
 </td>
</tr>
<tr>
 <td class="active" align="center" nowrap="nowrap"><div style="width:60px"><label for="c101"><%=Language["LOGIN_USER"]%>:</label></div></td>
 <td class="odd" align="center"><input id="c101" type="text" name="username" value="<%=Server.HtmlEncode(LogUser)%>" size="20" class="login" /></td>
</tr>
<tr>
 <td class="active" align="center" nowrap="nowrap"><label for="c102"><%=Language["LOGIN_PASS"]%>:</label></td>
 <td class="odd" align="center"><input id="c102" type="password" name="password" size="20" class="login" /></td>
</tr>
<tr>
 <td class="active" colspan="2">
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tr>
 <td><input id="c103" accesskey="p" type="checkbox" name="persistent" value="1" <%=(Request.Form["persistent"]=="1"?"checked=\"checked\" ":"")%>/></td>
 <td nowrap="nowrap" width="99%"><label for="c103"><%=Language["LOGIN_PERSISTENT"]%></label></td>
 <td align="right" style="padding-left:16px"><input type="submit" name="login" value="<%=Language["LOGIN_LOGIN"]%>" /></td>
</tr>
</table>
 </td>
</tr>
</table>
</form>

 </td>
</tr>
<tr>
 <td colspan="3"><img src="images/form.bk.gif" alt="" width="100%" height="3" /></td>
</tr>
</table>
<br />
<a href="mailto:ka@ka.cz" class="copyright">Yuki WebGarance &copy 2006 ka.soft sk, s.r.o.</a>

<% } %>

</div>

 </td>
</tr>
</table>
	
</body>

</html>

﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.xml;
using iTextSharp.text.html;
using Ka.Garance.Util;
using Ka.Garance.Invoice.Yuki;

public partial class _Invoice_Detail : Ka.Garance.Web.GarancePage
{
	private YukiFaktura m_Faktura;
	public YukiFaktura Faktura
	{
		get { return m_Faktura; }
	}

	private bool m_PruvodniList = false;
	public bool PruvodniList
	{
		get { return m_PruvodniList; }
	}

    protected void Page_Load(object sender, EventArgs e)
    {		
		m_Faktura = new YukiFaktura(Request.QueryString["fa"], User);

		if (Request.QueryString["pdf"] != null)
		{
			m_PruvodniList = (Request.QueryString["pdf"] == "letter");
			m_Faktura = new YukiFaktura(Request.QueryString["fa"], User);
			MemoryStream ms = RenderPdf();
			try
			{
				GaranceUtil.ForceDownload(Context, ms, string.Format("Yuki-Invoice-{0}.pdf", Faktura.Cislo));
			}
			finally
			{
				ms.Dispose();
			}
		}

    }

	public string GetSeznamProtokoluHTML()
	{
		StringBuilder sb = new StringBuilder();
		sb.AppendFormat(@"<table border=""0"" cellpadding=""4"" cellspacing=""1"" class=""list"">
<tr class=""category"">
 <td colspan=""6"">{0}</td> 
</tr>", Language["LOCATION_PROTOCOL"]);
		sb.AppendFormat(@"
<tr class=""header"">
 <th align=""left"">{0}</th>
 <th align=""right"" width=""15%"">{1}</th>
 <th align=""right"" width=""15%"">{2}</th>
<th align=""right"" width=""15%"">{3}</th>
<th align=""right"" width=""15%"">{4}</th>
<th>Datum</th>
</tr>", Language["INVOICE_ORDER_MARK"], Language["INVOICE_MATERIAL"], Language["INVOICE_WORK"], Language["PARTS_PRICE"], Language["INVOICE_TAX"]);
		int idx = 0;
		foreach (YukiFakturaProtokol p in Faktura.Protokoly)
		{
			sb.AppendFormat(@"
<tr class=""{0}"">
 <td align=""left"">{1}</td>
 <td align=""right"" width=""15%"">{2:F2}</td>
 <td align=""right"" width=""15%"">{3:F2}</td>
 <td align=""right"" width=""15%"">{4:F2}</td>
 <td align=""right"" width=""15%"">{5:F2}</td>
<td>{6:d'.'M'.'yyyy}</td>
</tr>", (idx++ % 2 == 0 ? "even" : "odd"), Language["INVOICE_ORDER_PROTO"].Replace("%s", p.Cislo), p.CenaMaterial, p.CenaPrace, p.CenaCelkem, p.CenaCelkemDPH, p.Datum);
		}

		sb.AppendFormat(@"
<tr class=""footer""><td colspan=""6"" style=""padding:0px""><div style=""height:1px;overflow:hidden"">&nbsp;</div></td></tr>
<tr class=""even"">
 <td align=""right"" colspan=""4""><b>{0}</b></td>
 <td align=""right""><b>{1:F2} {2}</b></td>
<td>&nbsp;</td>
</tr>", Language["INVOICE_TOTAL_CURRENCY"].Replace("%s", Faktura.Mena), Faktura.CenaCelkemDPH, Faktura.Mena);

		sb.Append("</table>");


		return sb.ToString();
	}

	private MemoryStream RenderPdf()
	{
		PdfMemoryStream ms = new PdfMemoryStream();
		ms.LeaveOpen = true;
		Document document = new Document(PageSize.A4, 45, 45, 45, 45);
		try
		{
			PdfWriter pf = PdfWriter.GetInstance(document, ms);
			document.Open();
			if (PruvodniList)
			{
				Faktura.RenderPruvodniList(document);
			}
			else
			{
				Faktura.RenderFaktura(document);
				PdfPTable tblInfo = new PdfPTable(1);
				tblInfo.DefaultCell.Border = Rectangle.NO_BORDER;
				tblInfo.DefaultCell.BorderWidth = 0;
				tblInfo.DefaultCell.HorizontalAlignment = Rectangle.ALIGN_CENTER;
				tblInfo.AddCell(new Phrase(new Chunk(Language["INVOICE_MSG_FEE"], FontFactory.GetFont(FontFactory.TIMES_ROMAN, "cp1250", true, 8.0F))));
				tblInfo.TotalWidth = 470;
				tblInfo.WriteSelectedRows(0, -1, 60, 60, pf.DirectContent);
			}
		}
		finally
		{
			try { document.Close(); }
			catch { }
		}
		ms.LeaveOpen = false;
		ms.Position = 0;
		return ms;
	}

}

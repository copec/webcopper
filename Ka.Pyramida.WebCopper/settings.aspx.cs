﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Ka.Garance.Data;
using Ka.Garance.User;

public partial class _Settings : Ka.Garance.Web.GarancePage
{
	private bool m_FormSaved = false;
	private NameValueCollection m_Errors = new NameValueCollection();
	private Uzivatel m_Uzivatel;

	public Uzivatel Uzivatel
	{
		get { return m_Uzivatel; }
	}

	public bool FormSaved
	{
		get { return m_FormSaved; }
	}

	public bool ZmenaOsobnichUdaju
	{
		get { return TabItem1.Selected; }
	}

	public bool ZmenaHesla
	{
		get { return TabItem2.Selected; }
	}

	public bool ZmenaFakturace
	{
		get { return TabItem3.Selected; }
	}

	public bool JePlatceDPH
	{
		get
		{
			if (Request.HttpMethod == "POST")
				return (Request.Form["pdph"] == "1");
			return Uzivatel.PlatceDPH;
		}
	}

	public string MsgSettingsSaved
	{
		get
		{
			if (ZmenaOsobnichUdaju) return Language["SETTINGS_MSG_USAVED"];
			if (ZmenaHesla) return Language["SETTINGS_MSG_PSAVED"];
			if (ZmenaFakturace) return Language["SETTINGS_MSG_ISAVED"];
			return "";
		}
	}

	public string GetFormValue(string name, string value)
	{
		if (Request.Form[name] != null) value = Request.Form[name];

		return Server.HtmlEncode(value);
	}
	
	protected void Page_Load(object sender, EventArgs e)
	{
		m_Uzivatel = new Uzivatel(User);

		if (Request.QueryString["state"] == "0")
		{
			TabItem1.Select();
			User.Cache["settings.state"] = "0";
		}
		else if (Request.QueryString["state"] == "1")
		{
			TabItem2.Select();
			User.Cache["settings.state"] = "1";
		}
		else if (Request.QueryString["state"] == "2")
		{
			TabItem3.Select();
			User.Cache["settings.state"] = "2";
		}
		else if (Request.QueryString["state"] == "3")
		{
			TabItem4.Select();
			User.Cache["settings.state"] = "3";
		}
		else if (User.Cache["settings.state"] == "3")
			TabItem4.Select();
		else if (User.Cache["settings.state"] == "2")
			TabItem3.Select();
		else if (User.Cache["settings.state"] == "1")
			TabItem2.Select();
		else
			TabItem1.Select();

		if (Request.HttpMethod == "POST")
		{
			if (ValidateForm())
				SaveFormData();
		}
	}

	private bool ValidateForm()
	{
		m_Errors.Clear();

		// overenie platnosti zadanych udajov
		if (ZmenaOsobnichUdaju)
		{

		}
		else if (ZmenaHesla)
		{
			string newPass = Request.Form["pnpass"];
			string confirmPass = Request.Form["pcpass"];
			if (newPass == null || newPass.Trim() == "")
				m_Errors.Add("pass", Language["SETTINGS_ERR_NONEWPASS"]);
			else if (newPass != confirmPass)
				m_Errors.Add("pass", Language["SETTINGS_ERR_NOPASSMATCH"]);
		}
		else if (ZmenaFakturace)
		{
			/*
			string cislofa = string.Format("{0}", Request.Form["pfa"]).Trim();
			if (!Regex.IsMatch(cislofa, "^[0-9]+$"))
				m_Errors.Add("fa", Language["SETTINGS_ERR_BADINVOICE"]);

			// ak uz cis. rada existuje, vypis chybu
			else if (Uzivatel.PocetFaktur(cislofa) > 0 || Convert.ToInt32(cislofa) < Uzivatel.CisloFa)
				m_Errors.Add("fa", Language["SETTINGS_ERR_USEDINVOICE"]);
			*/
		}

		return (m_Errors.Count == 0);
	}

	protected string GenerateErrorHTML()
	{
		if (m_Errors.Count == 0) return "";

		StringBuilder sb = new StringBuilder();
		sb.Append("<ul>\n<li>");
		for (int i = 0; i < m_Errors.Count; i++)
		{
			if (i > 0) sb.Append("</li>\n<li>");
			sb.Append(m_Errors[i]);
		}
		sb.Append("</li>\n</ul>");

		return string.Format(@"<table border=""0"" cellpadding=""4"" cellspacing=""1"" class=""list"">
<tr class=""category""><td><b style=""color:red"">{0}</b></td></tr>
<tr>
 <td style=""background-color:#FFFFE2"">
{1}
 </td>
</tr>
</table>
<br />", Language["ERROR_PROCESSING_DATA"], sb.ToString());
	}

	private void SaveFormData()
	{
		if (ZmenaOsobnichUdaju)
		{
			Uzivatel.Ulice = Request.Form["pstreet"];
			Uzivatel.Mesto = Request.Form["pcity"];
			Uzivatel.PSC = Request.Form["pzip"];
			Uzivatel.Tel = Request.Form["ptel"];
			Uzivatel.Fax = Request.Form["pfax"];
			Uzivatel.Mobil = Request.Form["pmobile"];
			Uzivatel.Email = Request.Form["pemail"];

			Uzivatel.Save();
		}
		else if (ZmenaHesla)
		{
			Uzivatel.NastavHeslo(Request.Form["pnpass"].Trim());
		}
		else if (ZmenaFakturace)
		{
			// Uzivatel.CisloFa = Convert.ToInt32(Request.Form["pfa"].Trim());
			// Uzivatel.NastavFakturace();

			Uzivatel.Ico = Request.Form["pico"];
			Uzivatel.Dic = Request.Form["pdic"];
			Uzivatel.Ucet = Request.Form["paccount"];
			Uzivatel.PlatceDPH = JePlatceDPH;

			Uzivatel.Save();
		}

		m_FormSaved = true;
	}
}

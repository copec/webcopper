﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Ka.Garance.Data;

public partial class _Bikes : Ka.Garance.Web.GarancePage
{
	private string m_ModelFilter = ""; // filter
	private string m_VINFilter = "";

	protected string VINFilter
	{
		get { return m_VINFilter; }
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		if (Request.QueryString["state"] != null)
			User.Cache["bikes.state"] = Request.QueryString["state"];

		if (Request.HttpMethod == "POST")
		{
			User.Cache["bikes.filter.model"] = Request.Form["pmodel"];
			User.Cache["bikes.filter.vin"] = Request.Form["pvin"];
		}
		else if (Request.QueryString["filter"] != null)
		{
			User.Cache["bikes.filter.model"] = null;
			User.Cache["bikes.filter.vin"] = null;
		}

		m_VINFilter = User.Cache["bikes.filter.vin"];
		m_ModelFilter = User.Cache["bikes.filter.model"];

		MCFilterTabs.SelectedIndex = 0;
		try
		{
			MCFilterTabs.SelectedIndex = Convert.ToInt32(User.Cache["bikes.state"]);
		}
		catch { }

		if (MCFilterTabs.SelectedIndex == 3)
		{
			MCBikesList.Visible = false;
			MCBikesVIN.Visible = true;

			MCBikesVIN.Filter = string.Format("iddealer={0} AND stav=0", User.Id);

			/*
			if (m_VINFilter != null && m_VINFilter.Trim() != "")
				MCBikesVIN.Filter += string.Format(" AND ciskar LIKE '%{0}%'", MySQLDatabase.Escape(m_VINFilter.Trim(), true));
			*/

			MCBikesVIN.SQL = "SELECT ciskar FROM tmotocykl";
		}
		else
		{
			MCBikesVIN.Visible = false;
			MCBikesList.Visible = true;
			MCBikesList.Filter = string.Format("iddealer={0} AND tcmodelklic.state<>9", User.Id);

			if (m_ModelFilter != null && m_ModelFilter.Trim() != "")
				MCBikesList.Filter += string.Format(" AND tcmodelklic.id='{0}'", MySQLDatabase.Escape(m_ModelFilter.Trim()));
			if (m_VINFilter != null && m_VINFilter.Trim() != "")
				MCBikesList.Filter += string.Format(" AND tmotocykl.ciskar LIKE '%{0}%'", MySQLDatabase.Escape(m_VINFilter.Trim(), true));

			if (MCFilterTabs.SelectedIndex == 1) MCBikesList.Filter += " AND tmotocykl.stav=0";
			else if (MCFilterTabs.SelectedIndex == 2) MCBikesList.Filter += " AND tmotocykl.stav=2";
			MCBikesList.SQL = string.Format(@"
			SELECT
				CONCAT(ciskar,CAST(idzeme AS CHAR)) AS id,
				CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(REPLACE(tcmodelklic.jmeno, '  ', ' '), ' ', 2), ' ', -1) AS SIGNED) AS kubatura,
				CAST(IF(IFNULL(prodej_datum,'')='',NULL,CONCAT('20',MID(prodej_datum,5,2),'-',MID(prodej_datum,3,2),'-',LEFT(prodej_datum,2))) AS DATETIME) AS datum_prodeje,
				IF(stav=0,'{0}','{1}') AS stav,
				IFNULL(CONCAT(jmeno,' (',model,')'),model) AS model,
				ciskar
			FROM tmotocykl
				INNER JOIN tcmodelklic ON tcmodelklic.id=tmotocykl.model
			",
				Language["BIKES_STATE_STOCK"],
				Language["BIKES_STATE_SOLD"]
			);

			ListItem1.Validate += new Ka.Garance.Web.List.ValidateListItem(ListItem1_Validate);
		}
	}

	private void ListItem1_Validate(object sender, Ka.Garance.Web.List.ValidateEventArgs e)
	{
		e.Validated = true;
		if (e.Value == null || e.Value is DBNull)
			e.Value = "";
		else
			e.Value = Convert.ToDateTime(e.Value).ToString("d'. 'M'. 'yyyy");
	}

	protected string GetModelOptions()
	{
		StringBuilder sb = new StringBuilder();

		string cmodel = User.Cache["bikes.filter.model"];

		MySQLDataReader dr = Database.ExecuteReader(@"
			SELECT
				CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(REPLACE(tcmodelklic.jmeno, '  ', ' '), ' ', 2), ' ', -1) AS SIGNED) AS kubatura,
				IFNULL(CONCAT(jmeno,' (',model,')'),model) AS nazev,
				model
			FROM tmotocykl
				INNER JOIN tcmodelklic ON tcmodelklic.id=tmotocykl.model
			WHERE iddealer={0} AND tcmodelklic.state<>9
			GROUP BY model
			ORDER BY kubatura,nazev", User.Id);
		while (dr.Read())
		{
			string model = Convert.ToString(dr.GetValue("model"));
			bool selected = (cmodel == model);
			sb.AppendFormat("<option value=\"{0}\"{1}>{2}</option>\n", model, (selected?" selected=\"selected\"":""), dr.GetValue("nazev"));
		}

		return sb.ToString();
	}
}

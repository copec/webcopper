﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="_Invoice" Codebehind="invoice.aspx.cs" %>
<%@ Register TagPrefix="yuki" TagName="header" Src="Controls/Header.ascx" %>
<%@ Register TagPrefix="yuki" Namespace="Ka.Garance.Web.List" Assembly="garance" %>
<%@ Register TagPrefix="yuki" Namespace="Ka.Garance.Web.TabList" Assembly="garance" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Language.Code%>" dir="ltr" lang="<%=Language.Code%>">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><%=Language["APP_TITLE"]%></title>
<link rel="stylesheet" type="text/css" href="css/yuki.css" />
</head>

<body>

<yuki:header Id="MCHeader" LocationID="LOCATION_INVOICES" runat="server" />

<div align="center" style="padding:0px 16px 16px 16px"> 
<table border="0" cellpadding="0" cellspacing="0" align="center" style="width:1%">
<tr>
 <td align="left">
 
<div style="width:800px"> 

<img src="images/mod.invoice.png" alt="" style="vertical-align:text-bottom" />
<span class="mod"><%=Language["HEADER_INVOICES"]%></span><br /><br />

<table border="0" cellpadding="4" cellspacing="1" class="list">
<tr class="category">
 <td colspan="2"><%=Language["INVOICES_STATE_SUM"]%></td>
</tr>
<tr class="even">
 <td width="25%"><%=Language["INVOICES_STATE_SUM_MSG1"]%></td>
 <td class="odd"><asp:Literal ID="msg1" runat="server" /></td>
</tr>
<tr class="even">
 <td width="25%" valign="top"><%=Language["INVOICES_STATE_SUM_MSG2"]%></td>
 <td class="odd"><asp:Literal ID="msg2" runat="server" /></td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
<br />

<yuki:TabList ID="MCFilterTabs" runat="server">
 <yuki:TabItem ID="TabItem1" LangID="PROTO_STATE_ALL" Link="invoice.aspx?state=1" runat="server" />
 <yuki:TabItem ID="TabItem2" LangID="PROTO_STATE_INVOICE" Link="invoice.aspx?state=2" runat="server" />
 <yuki:TabItem ID="TabItem3" LangID="PROTO_STATE_CANCELED" Link="invoice.aspx?state=3" runat="server" />
</yuki:TabList>
<yuki:List ID="MCInvoiceList" Name="InvoiceList" Identifier="cislofa" Link="invoice.detail.aspx?fa={0}" runat="server">
 <yuki:ListItem ID="ListItem1" LangID="INVOICE_NO" ColumnName="cislofa" align="left" width="50%" runat="server" />
 <yuki:ListItem ID="ListItem2" LangID="INVOICE_DATE" ColumnName="datum" Default="true" Format="{0:d'. 'M'. 'yyyy}" align="left" width="20%" runat="server" />
 <yuki:ListItem ID="ListItem3" LangID="INVOICE_PRICE" ColumnName="cena" align="right" td_align="right" width="20%" runat="server" />
</yuki:List>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>

</div>
 </td>
</tr>
</table>
</div>

</body>

</html>

﻿using System;
using System.Collections;
using System.Text;
using Ka.Garance.Data;
using Ka.Garance.User;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Ka.Garance.Protocol.Yuki;

namespace Ka.Garance.Invoice.Yuki
{

	public sealed class YukiFakturaDodavatel
	{
		private string m_Jmeno = "Autocentrum Seat s r.o";
		private string m_Adresa = "U pyramidy 721";
		private string m_PSC = "25243";
		private string m_Obec = "Průhonice";
		private string m_Tel = "+420 000 000 000";
		private string m_Fax = "+420 000 000 000";
		private string m_Mobil = "+420 000 000 000";
		private string m_Email = "prodej@seatovapyramida.cz";
		private string m_ICO = "25113429";
		private string m_DIC = "012-25113429";
		private string m_Ucet = "000000000 / 0000";

		public YukiFakturaDodavatel(YukiFaktura faktura)
		{
			MySQLDataReader dr = faktura.User.Database.ExecuteReader("SELECT nazev,mesto,tel,fax,mobil,email,psc,ulice,ico,dic,ucet FROM tdealer WHERE id={0}", faktura.User.Id);
			if (!dr.Read())
				throw new Exception("Could not load user details");
			m_Jmeno = Convert.ToString(dr.GetValue("nazev"));
			m_Adresa = Convert.ToString(dr.GetValue("ulice"));
			m_PSC = Convert.ToString(dr.GetValue("psc"));
			m_Obec = Convert.ToString(dr.GetValue("mesto"));
			m_ICO = Convert.ToString(dr.GetValue("ico"));
			m_DIC = Convert.ToString(dr.GetValue("dic"));
			m_Tel = Convert.ToString(dr.GetValue("tel"));
			m_Fax = Convert.ToString(dr.GetValue("fax"));
			m_Mobil = Convert.ToString(dr.GetValue("mobil"));
			m_Email = Convert.ToString(dr.GetValue("email"));
			m_Ucet = Convert.ToString(dr.GetValue("ucet"));
		}

		#region properties
		public string Jmeno
		{
			get { return m_Jmeno; }
		}
		public string Adresa
		{
			get { return m_Adresa; }
		}
		public string PSC
		{
			get { return m_PSC; }
		}
		public string Obec
		{
			get { return m_Obec; }
		}
		public string Tel
		{
			get { return m_Tel; }
		}
		public string Fax
		{
			get { return m_Fax; }
		}
		public string Mobil
		{
			get { return m_Mobil; }
		}
		public string Email
		{
			get { return m_Email; }
		}
		public string ICO
		{
			get { return m_ICO; }
		}
		public string DIC
		{
			get { return m_DIC; }
		}
		public string Ucet
		{
			get { return m_Ucet; }
		}
		#endregion
	}

	public sealed class YukiFakturaOdberatel
	{
		private string m_Jmeno;
		private string m_Adresa;
		private string m_PSC;
		private string m_Obec;
		private string m_ICO;
		private string m_DIC;

		public YukiFakturaOdberatel(YukiFaktura faktura)
		{
			MySQLDataReader dr = faktura.User.Database.ExecuteReader("SELECT firma,obec,psc,ulice,ico,dic FROM tcimportexport WHERE idzeme={0}", faktura.User.IdZeme);
			if (!dr.Read())
				throw new Exception("Could not load user details");
			m_Jmeno = Convert.ToString(dr.GetValue("firma"));
			m_Adresa = Convert.ToString(dr.GetValue("ulice"));
			m_PSC = Convert.ToString(dr.GetValue("psc"));
			m_Obec = Convert.ToString(dr.GetValue("obec"));
			m_ICO = Convert.ToString(dr.GetValue("ico"));
			m_DIC = Convert.ToString(dr.GetValue("dic"));
		}

		#region properties
		public string Jmeno
		{
			get { return m_Jmeno; }
		}
		public string Adresa
		{
			get { return m_Adresa; }
		}
		public string PSC
		{
			get { return m_PSC; }
		}
		public string Obec
		{
			get { return m_Obec; }
		}
		public string ICO
		{
			get { return m_ICO; }
		}
		public string DIC
		{
			get { return m_DIC; }
		}
		#endregion
	}

	public sealed class YukiFakturaProtokol
	{
		private string m_Cislo;
		private decimal m_CenaMaterial;
		private decimal m_CenaPrace;
		private decimal m_CenaMaterialOrig;
		private decimal m_CenaPraceOrig;
		private DateTime m_Date;
		private YukiFaktura m_Faktura;

		public YukiFakturaProtokol(YukiFaktura faktura, string cislo, decimal cenam, decimal cenap, decimal cenamo, decimal cenapo, DateTime date)
		{
			m_Faktura = faktura;
			m_Cislo = cislo;
			m_CenaMaterial = cenam;
			m_CenaPrace = cenap;
			m_CenaMaterialOrig = cenamo;
			m_CenaPraceOrig = cenapo;
			m_Date = date;
		}

		public string Cislo
		{
			get { return m_Cislo; }
		}
		public decimal CenaPrace
		{
			get { return m_CenaPraceOrig; }
		}


		public DateTime Datum
		{
			get { return this.m_Date; }
		}

		/// <summary>
		/// Toto je cena - rabat
		/// </summary>
		public decimal CenaMaterial
		{
			get
			{
				// old
				if (m_Faktura.DatumVystaveni < Ka.Garance.Protocol.Yuki.YukiProtocol.DATE_NEW_RECOUNT)
					return Math.Round(m_CenaMaterialOrig * ((100 - m_Faktura.Rabat) / 100), 1);

				// new
                if (m_Faktura.DatumVystaveni < Ka.Garance.Protocol.Yuki.YukiProtocol.DATE_NEW_RECOUNT_2012)
				    return Math.Round(m_CenaMaterialOrig * 0.75m * ((100 + m_Faktura.Rabat) / 100), m_Faktura.RecalcEUR ? 2 : 1);

                // new 2012
                if (m_Faktura.DatumVystaveni < Ka.Garance.Protocol.Yuki.YukiProtocol.DATE_NEW_RECOUNT_2012_10)
                    return Math.Round(m_CenaMaterialOrig * 0.35m * ((100 + m_Faktura.Rabat) / 100), m_Faktura.RecalcEUR ? 2 : 1);

                // new 2012/10
                if (m_Faktura.DatumVystaveni < Ka.Garance.Protocol.Yuki.YukiProtocol.DATE_NEW_RECOUNT_2013)
                    return Math.Round(m_CenaMaterialOrig * 0.605m * ((100 + m_Faktura.Rabat) / 100), m_Faktura.RecalcEUR ? 2 : 1);

                // new 2013
                return Math.Round(m_CenaMaterialOrig * 0.7m * ((100 + m_Faktura.Rabat) / 100), m_Faktura.RecalcEUR ? 2 : 1);
			}
		}

		public decimal CenaCelkem
		{
			get { return CenaPrace + CenaMaterial; } 
		}

		public decimal CenaPraceDPH
		{
			get { return Math.Round(m_CenaPrace * (100 + m_Faktura.DPH) / 100, m_Faktura.RecalcEUR ? 2 : 1); }
		}

		public decimal CenaMaterialDPH
		{
			get
			{
				// old
				if (m_Faktura.DatumVystaveni < Ka.Garance.Protocol.Yuki.YukiProtocol.DATE_NEW_RECOUNT)
					return Math.Round(m_CenaMaterialOrig * ((100 - m_Faktura.Rabat) / 100) * ((100 + m_Faktura.DPH) / 100), 1);

                // new
                if (m_Faktura.DatumVystaveni < Ka.Garance.Protocol.Yuki.YukiProtocol.DATE_NEW_RECOUNT_2012)
				    return Math.Round(m_CenaMaterial * 0.75m * ((100 + m_Faktura.Rabat) / 100) * ((100 + m_Faktura.DPH) / 100), m_Faktura.RecalcEUR ? 2 : 1);

                // new 2012
                if (m_Faktura.DatumVystaveni < Ka.Garance.Protocol.Yuki.YukiProtocol.DATE_NEW_RECOUNT_2012_10)
                    return Math.Round(m_CenaMaterial * 0.35m * ((100 + m_Faktura.Rabat) / 100) * ((100 + m_Faktura.DPH) / 100), m_Faktura.RecalcEUR ? 2 : 1);

                // new 2012/10
                if (m_Faktura.DatumVystaveni < Ka.Garance.Protocol.Yuki.YukiProtocol.DATE_NEW_RECOUNT_2013)
                    return Math.Round(m_CenaMaterial * 0.605m * ((100 + m_Faktura.Rabat) / 100) * ((100 + m_Faktura.DPH) / 100), m_Faktura.RecalcEUR ? 2 : 1);

                // new 2013
                return Math.Round(m_CenaMaterial * 0.7m * ((100 + m_Faktura.Rabat) / 100) * ((100 + m_Faktura.DPH) / 100), m_Faktura.RecalcEUR ? 2 : 1);
			}
		}

		public decimal CenaCelkemDPH
		{
			get { return CenaPraceDPH + CenaMaterialDPH; }
		}

		internal void Set(decimal cenam, decimal cenap, decimal cenamo, decimal cenapo, DateTime date)
		{
			m_CenaMaterial = cenam;
			m_CenaPrace = cenap;
			m_CenaMaterialOrig = cenamo;
			m_CenaPraceOrig = cenapo;
			m_Date = date;
		}
	}

	public sealed class YukiFakturaProtokolList : IEnumerable
	{
		private ArrayList m_Items = new ArrayList();
		private YukiFaktura m_Faktura;

		public YukiFakturaProtokolList(YukiFaktura faktura)
		{
			m_Faktura = faktura;
			MySQLDataReader dr = faktura.User.Database.ExecuteReader("SELECT cislo FROM tprotocol WHERE cislofa={0} AND iddealer={1} AND stav>=4", faktura.Cislo, faktura.User.Id);
			while (dr.Read())
			{
				string cisloprotokol = Convert.ToString(dr.GetValue("cislo"));
				Ka.Garance.Protocol.Yuki.YukiProtocol.UpdateZavadaPrice(faktura.User.Database, cisloprotokol);
				this.Add(cisloprotokol);
			}
		}

		public int Count
		{
			get { return m_Items.Count; }
		}

		public YukiFakturaProtokol this[int index]
		{
			get { return m_Items[index] as YukiFakturaProtokol; }
		}

		public void Clear()
		{
			m_Items.Clear();
		}

		public YukiFakturaProtokol Add(string cislo, decimal prace, decimal material, decimal prace_orig, decimal material_orig, DateTime datum)
		{
			foreach (YukiFakturaProtokol p in m_Items)
			{
				if (p.Cislo == cislo)
				{
					p.Set(material, prace, material_orig, prace_orig, datum);
					return p;
				}
			}

			YukiFakturaProtokol pp = new YukiFakturaProtokol(m_Faktura, cislo, material, prace, material_orig, prace_orig, datum);
			m_Items.Add(pp);
			return pp;
		}

		public YukiFakturaProtokol Add(string cislo)
		{
			/*
			MySQLDataReader dr = m_Faktura.User.Database.ExecuteReader(@"
				SELECT
					IFNULL(SUM(A.material),0) AS cenan,
					IFNULL(SUM(A.prace),0) AS cenap,
					IF(B.datumfa<'" + Ka.Garance.Protocol.Yuki.YukiProtocol.DATE_NEW_RECOUNT.ToString("yyyy'-'MM'-'dd") + @"',
						IFNULL(SUM(ROUND(A.material * (1 + (B.dphfa / 100)) * (1 - (B.rabatfa / 100)),1) * 1.00),0),
						IFNULL(SUM(ROUND(A.material * 0.75 * (1 + (B.dphfa / 100)) * (1 + (B.rabatfa / 100)),1) * 1.00),0)						
					) AS cenam_dph,
					IFNULL(SUM(ROUND(A.prace * (1 + (B.dphfa / 100)),1) * 1.00),0) AS cenap_dph
				FROM tzavada A
					INNER JOIN tprotocol B ON B.cislo=A.cisloprotokol AND B.iddealer={0}
				WHERE B.stav>=4 AND B.cislo={1} AND A.stav=1", m_Faktura.User.Id, cislo);
			 */

			int rn = m_Faktura.RecalcEUR ? 2 : 1;

			MySQLDataReader dr = m_Faktura.User.Database.ExecuteReader(@"
				SELECT
					IFNULL(SUM(A.material),0) AS cenan,
					IFNULL(SUM(A.prace),0) AS cenap,
					IF(B.datumfa<'" + Ka.Garance.Protocol.Yuki.YukiProtocol.DATE_NEW_RECOUNT.ToString("yyyy'-'MM'-'dd") + @"',
						IFNULL(SUM(ROUND(A.material * (1 + (B.dphfa / 100)) * (1 - (B.rabatfa / 100))," + rn + @") * 1.00),0),
						IF(B.datumfa<'" + Ka.Garance.Protocol.Yuki.YukiProtocol.DATE_NEW_RECOUNT_2012.ToString("yyyy'-'MM'-'dd") + @"',
                            IFNULL(SUM(ROUND(A.material * 0.75 * (1 + (B.dphfa / 100)) * (1 + (B.rabatfa / 100))," + rn + @") * 1.00),0),
                            IF(B.datumfa<'" + Ka.Garance.Protocol.Yuki.YukiProtocol.DATE_NEW_RECOUNT_2012_10.ToString("yyyy'-'MM'-'dd") + @"',
                                IFNULL(SUM(ROUND(A.material * 0.35 * (1 + (B.dphfa / 100)) * (1 + (B.rabatfa / 100))," + rn + @") * 1.00),0),
                                IF(B.datumfa<'" + Ka.Garance.Protocol.Yuki.YukiProtocol.DATE_NEW_RECOUNT_2013.ToString("yyyy'-'MM'-'dd") + @"',
                                    IFNULL(SUM(ROUND(A.material * 0.605 * (1 + (B.dphfa / 100)) * (1 + (B.rabatfa / 100))," + rn + @") * 1.00),0),
                                    IFNULL(SUM(ROUND(A.material * 0.7 * (1 + (B.dphfa / 100)) * (1 + (B.rabatfa / 100))," + rn + @") * 1.00),0)                            
                                )                            
                            )
                        )
					) AS cenam_dph,
					IFNULL(SUM(ROUND(A.prace * (1 + (B.dphfa / 100))," + rn + @") * 1.00),0) AS cenap_dph,
					MAX(CAST(IF(IFNULL(B.datumoprava,'')='',NULL,CONCAT('20',MID(B.datumoprava,5,2),'-',MID(B.datumoprava,3,2),'-',LEFT(B.datumoprava,2))) AS DATE)) AS timestamp
				FROM tzavada A
					INNER JOIN tprotocol B ON B.cislo=A.cisloprotokol AND B.iddealer={0}
				WHERE B.stav>=4 AND B.cislo=" + cislo+" AND A.stav=1", m_Faktura.User.Id);

			if (dr.Read())
			{
				decimal cenaND = Convert.ToDecimal(dr.GetValue("cenan"));
				decimal cenaP = Convert.ToDecimal(dr.GetValue("cenap"));
				decimal cenam_dph = Convert.ToDecimal(dr.GetValue("cenam_dph"));
				decimal cenap_dph = Convert.ToDecimal(dr.GetValue("cenap_dph"));
				DateTime datum = DateTime.MinValue;
				try { datum = Convert.ToDateTime(dr.GetValue("timestamp")); }
				catch { }

				if (m_Faktura.DatumVystaveni < Ka.Garance.Protocol.Yuki.YukiProtocol.DATE_NEW_RECOUNT)
					cenam_dph = cenam_dph / (1 + (m_Faktura.DPH / 100)) / (1 - (m_Faktura.Rabat / 100));
                else if (m_Faktura.DatumVystaveni < Ka.Garance.Protocol.Yuki.YukiProtocol.DATE_NEW_RECOUNT_2012)
					cenam_dph = cenam_dph / (1 + (m_Faktura.DPH / 100)) / (1 + (m_Faktura.Rabat / 100)) / 0.75m;
                else if (m_Faktura.DatumVystaveni < Ka.Garance.Protocol.Yuki.YukiProtocol.DATE_NEW_RECOUNT_2012_10)
                    cenam_dph = cenam_dph / (1 + (m_Faktura.DPH / 100)) / (1 + (m_Faktura.Rabat / 100)) / 0.35m;
                else if (m_Faktura.DatumVystaveni < Ka.Garance.Protocol.Yuki.YukiProtocol.DATE_NEW_RECOUNT_2013)
                    cenam_dph = cenam_dph / (1 + (m_Faktura.DPH / 100)) / (1 + (m_Faktura.Rabat / 100)) / 0.605m;
                else
                    cenam_dph = cenam_dph / (1 + (m_Faktura.DPH / 100)) / (1 + (m_Faktura.Rabat / 100)) / 0.7m;
				cenap_dph = cenap_dph / (1 + (m_Faktura.DPH / 100));

				return Add(cislo, cenap_dph, cenam_dph, cenaP, cenaND, datum);
			}

			return null;		
		}

		public void Delete(int index)		
		{
			m_Items.RemoveAt(index);
		}

		public IEnumerator GetEnumerator()
		{
			return m_Items.GetEnumerator();
		}
	}

	public sealed class YukiFaktura
	{
		private string m_Cislo;
		private GaranceUser m_User;

		private YukiFakturaDodavatel m_Dodavatel;
		private YukiFakturaOdberatel m_Odberatel;
		private YukiFakturaProtokolList m_Protokoly;

		private bool m_DatumLoaded;
		private DateTime m_DatumVystaveni;
		private DateTime m_DatumSplatnosti;
		private decimal m_DPH;
		private decimal m_DPHLow = 10.00m;
		private decimal m_Rabat;
		private string m_Mena;

		internal bool RecalcEUR
		{
			get { return this.User.IdZeme == 2 && this.DatumVystaveni >= YukiProtocol.DATE_SK_EURO; }
		}

		public YukiFaktura(string cislo, GaranceUser user)
		{
			m_Cislo = cislo;
			m_User = user;

			// nahraj detaily faktury
			MySQLDataReader dr = user.Database.ExecuteReader("SELECT datumfa,dphfa,rabatfa,mena,splatnostfa FROM tprotocol WHERE iddealer={0} AND cislofa={1} LIMIT 1", user.Id, cislo);
			if (!dr.Read())
				throw new Exception("Could not load invoice details");

			m_DatumLoaded = true;
			try
			{
				object df = dr.GetValue("datumfa");
				if (df == null || df is DBNull)
					m_DatumLoaded = false;
				else
					m_DatumVystaveni = Convert.ToDateTime(df);
			}
			catch
			{
				m_DatumLoaded = false;
			}

			if (m_DatumLoaded)
			{
				try
				{
					m_DatumSplatnosti = Convert.ToDateTime(dr.GetValue("splatnostfa"));
				}
				catch
				{
					m_DatumSplatnosti = DateTime.MinValue;
				}
				// toto je postarom !!!
				// musi byt 15 dni
				if (m_DatumSplatnosti < m_DatumVystaveni)
					m_DatumSplatnosti = m_DatumVystaveni.AddDays(15);
			}

			
			m_DPH = Convert.ToDecimal(dr.GetValue("dphfa"));
			m_Rabat = Convert.ToDecimal(dr.GetValue("rabatfa"));
			m_Mena = Convert.ToString(dr.GetValue("mena"));

			// WCP-4 - je potrebne pri vytvoreni DPH pouzit aktualnu hodnotu DPH
			// napr. ak bol protokol zalozeny v r 2009 ale fakturovany 2010 a sadzba DPH sa zmenila
			if (user.PlatceDPH)
				try
				{
					string sql = string.Format("SELECT sazbazakladni FROM `tcsazbadph` WHERE idzeme={0} AND datumOd<='{1:yyyy'-'MM'-'dd}' ORDER BY datumOd DESC LIMIT 1", user.IdZeme, m_DatumVystaveni);
					try { m_DPH = Convert.ToDecimal(user.Database.ExecuteScalar(sql)); }
					catch { m_DPH = Convert.ToDecimal(user.Database.ExecuteScalar("SELECT sazbadph FROM ndzeme WHERE ID={0}", user.IdZeme)); }

					sql = string.Format("SELECT sazbasnizena FROM `tcsazbadph` WHERE idzeme={0} AND datumOd<='{1:yyyy'-'MM'-'dd}' ORDER BY datumOd DESC LIMIT 1", user.IdZeme, m_DatumVystaveni);
					try { m_DPHLow = Convert.ToDecimal(user.Database.ExecuteScalar(sql)); }
					catch { }
				}
				catch { }

			m_Dodavatel = new YukiFakturaDodavatel(this);
			m_Odberatel = new YukiFakturaOdberatel(this);
			m_Protokoly = new YukiFakturaProtokolList(this);
		}

		public string Cislo
		{
			get { return m_Cislo; }
		}

		public string KonstantniSymbol
		{
			get { return "000"; }
		}

		public string VariabilniSymbol
		{
			get { return m_Cislo; }
		}

		public string SpecifickySymbol
		{
			get { return "000"; }
		}

		public bool DatumLoaded
		{
			get { return m_DatumLoaded; }
		}
		public DateTime DatumVystaveni
		{
			get { return m_DatumVystaveni; }
		}

		public DateTime DatumSplatnosti
		{
			get { return m_DatumSplatnosti; }
		}

		public decimal DPH
		{
			get { return m_DPH; }
		}

		public decimal Rabat
		{
			get { return m_Rabat; }
		}

		public string Mena
		{
			get { return m_Mena; }
		}

		public decimal CenaMaterialCelkem
		{
			get
			{
				decimal celkem = 0;
				foreach (YukiFakturaProtokol p in m_Protokoly)
				{
					celkem += p.CenaMaterial;
				}
				return celkem;
			}
		}

		public decimal CenaMaterialCelkemDPH
		{
			get
			{
				decimal celkem = 0;
				foreach (YukiFakturaProtokol p in m_Protokoly)
				{
					celkem += p.CenaMaterialDPH;
				}
				return celkem;
			}
		}

		public decimal CenaPraceCelkem
		{
			get
			{
				decimal celkem = 0;
				foreach (YukiFakturaProtokol p in m_Protokoly)
				{
					celkem += p.CenaPrace;
				}
				return celkem;
			}
		}

		public decimal CenaPraceCelkemDPH
		{
			get
			{
				decimal celkem = 0;
				foreach (YukiFakturaProtokol p in m_Protokoly)
				{
					celkem += p.CenaPraceDPH;
				}
				return celkem;
			}
		}

		public decimal CenaCelkem
		{
			get
			{
				decimal celkem = 0;
				foreach (YukiFakturaProtokol p in m_Protokoly)
				{
					celkem += p.CenaMaterial + p.CenaPrace;
				}
				return celkem;
			}
		}

		public decimal CenaCelkemDPH
		{
			get
			{
				decimal celkem = 0;
				foreach (YukiFakturaProtokol p in m_Protokoly)
				{
					celkem += (p.CenaMaterialDPH + p.CenaPraceDPH);
				}
				return celkem;
			}
		}

		public decimal VyseDPH
		{
			get { return CenaCelkemDPH - CenaCelkem; }
		}

		public GaranceUser User
		{
			get { return m_User; }
		}

		public bool PlatceDPH
		{
			get { return (m_DPH > 0); }
		}

		public YukiFakturaDodavatel Dodavatel
		{
			get { return m_Dodavatel; }
		}

		public YukiFakturaOdberatel Odberatel
		{
			get { return m_Odberatel; }
		}

		public YukiFakturaProtokolList Protokoly
		{
			get { return m_Protokoly; }
		}

		#region renderPDF

		private static Font m_PdfFont9 = FontFactory.GetFont(FontFactory.TIMES_ROMAN, "cp1250", true, 10.0F);
		private static Font m_PdfFont9B = FontFactory.GetFont(FontFactory.TIMES_ROMAN, "cp1250", true, 10.0F, Font.BOLD);
		private static Font m_PdfFont11B = FontFactory.GetFont(FontFactory.TIMES_ROMAN, "cp1250", true, 12.0F, Font.BOLD);
		private static Font m_PdfFont13 = FontFactory.GetFont(FontFactory.TIMES_ROMAN, "cp1250", true, 14.0F);
		private static Font m_PdfFont13B = FontFactory.GetFont(FontFactory.TIMES_ROMAN, "cp1250", true, 14.0F, Font.BOLD);

		private Chunk GetBaseChunk(string format, params object[] args) {
			return new Chunk(string.Format(format, args), m_PdfFont9);
		}
		private Chunk GetBaseChunkB(string format, params object[] args)
		{
			return new Chunk(string.Format(format, args), m_PdfFont9B);
		}

		private Cell GetBaseCell(string format, params object[] args)
		{
			Cell cell = new Cell(GetBaseChunk(format, args));
			cell.BorderWidth = 0;
			cell.Leading = 9;
			return cell;
		}

		private void AddPropertyValue(PdfPTable tbl, string ID, string value, float paddingBottom)
		{
			PdfPCell cellProperty = new PdfPCell(GetBaseChunk("{0}:", User.Language[ID]));
			cellProperty.Padding = 2;
			cellProperty.PaddingLeft = 10;
			if (paddingBottom > 0)
				cellProperty.PaddingBottom = paddingBottom;
			else if (paddingBottom < 0)
				cellProperty.PaddingTop = -paddingBottom;
			cellProperty.BorderWidth = 0;
			cellProperty.Border = Rectangle.NO_BORDER;
			tbl.AddCell(cellProperty);

			PdfPCell cellValue = new PdfPCell(GetBaseChunk(value));
			cellValue.Padding = 2;
			cellValue.PaddingRight = 10;
			if (paddingBottom > 0)
				cellValue.PaddingBottom = paddingBottom;
			else if (paddingBottom < 0)
				cellValue.PaddingTop = -paddingBottom;
			cellValue.BorderWidth = 0;
			cellValue.Border = Rectangle.NO_BORDER;
			tbl.AddCell(cellValue);
		}

		private void AddPropertyValue(PdfPTable tbl, string ID, string value)
		{
			AddPropertyValue(tbl, ID, value, 2);
		}

		private PdfPCell AddItemHeader(PdfPTable tbl, string ID, int align)
		{
			PdfPCell cellHeader = new PdfPCell(GetBaseChunk(User.Language[ID]));
			cellHeader.Padding = 2;
			cellHeader.PaddingBottom = 4;
			cellHeader.PaddingTop = 10;
			cellHeader.Border = Rectangle.BOTTOM_BORDER;
			cellHeader.BorderWidth = 0.5F;
			cellHeader.HorizontalAlignment = align;
			cellHeader.NoWrap = true;
			tbl.AddCell(cellHeader);
			return cellHeader;
		}
		private PdfPCell AddItemHeader(PdfPTable tbl, string ID)
		{
			return AddItemHeader(tbl, ID, Rectangle.ALIGN_LEFT);
		}

		private PdfPCell AddItemData(PdfPTable tbl, string value, int align)
		{
			PdfPCell cellData = new PdfPCell(GetBaseChunk(value));
			cellData.Padding = 2;
			cellData.Border = Rectangle.NO_BORDER;
			cellData.BorderWidth = 0;
			cellData.HorizontalAlignment = align;
			cellData.NoWrap = true;
			tbl.AddCell(cellData);
			return cellData;
		}
		private PdfPCell AddItemData(PdfPTable tbl, string value)
		{
			return AddItemData(tbl, value, Rectangle.ALIGN_LEFT);
		}

		private PdfPTable GetTableDodavatel()
		{
			PdfPTable tbl = new PdfPTable(2);
			tbl.WidthPercentage = 100;
			tbl.SetWidths(new float[2] { 20, 80 });

			// dodavatel
			PdfPCell cellDodavatel = new PdfPCell(GetBaseChunk("{0}:", User.Language["INVOICE_SUPPLIER"]));
			cellDodavatel.Colspan = 2;
			cellDodavatel.PaddingTop = 2;
			cellDodavatel.PaddingLeft = 5;
			cellDodavatel.PaddingRight = 10;
			cellDodavatel.PaddingBottom = 0;
			cellDodavatel.BorderWidth = 0;
			tbl.AddCell(cellDodavatel);

			PdfPCell cellAdresa = new PdfPCell(new Chunk(string.Format("{0}\n{1}\n{2}        {3}", Dodavatel.Jmeno, Dodavatel.Adresa, Dodavatel.PSC, Dodavatel.Obec), m_PdfFont11B));
			cellAdresa.Colspan = 2;
			cellAdresa.Padding = 10;
			cellAdresa.PaddingTop = 2;
			cellAdresa.Leading = 18;
			cellAdresa.BorderWidth = 0;
			tbl.AddCell(cellAdresa);

			AddPropertyValue(tbl, "PROTO_PHONE", Dodavatel.Tel);
			AddPropertyValue(tbl, "PROTO_FAX", Dodavatel.Fax);
			AddPropertyValue(tbl, "INVOICE_MOBILE", Dodavatel.Mobil);
			AddPropertyValue(tbl, "INVOICE_EMAIL", Dodavatel.Email, 10);
			AddPropertyValue(tbl, "INVOICE_ICO", Dodavatel.ICO);
			AddPropertyValue(tbl, "INVOICE_DIC", Dodavatel.DIC, 10);
			AddPropertyValue(tbl, "INVOICE_ACCOUNT", Dodavatel.Ucet, 10);

			return tbl;
		}

		private PdfPTable GetTableFaktura()
		{
			PdfPTable tbl = new PdfPTable(2);
			tbl.WidthPercentage = 100;
			tbl.SetWidths(new float[2] { 45, 55 });

			PdfPCell cf = new PdfPCell(new Chunk(string.Format("{0}:", User.Language["INVOICE_NR"]), m_PdfFont11B));
			cf.Padding = 2;
			cf.PaddingTop = 18;
			cf.PaddingLeft = 10;
			cf.BorderWidth = 0;
			tbl.AddCell(cf);

			cf = new PdfPCell(new Chunk(this.Cislo, m_PdfFont11B));
			cf.Padding = 2;
			cf.PaddingTop = 18;
			cf.PaddingRight = 10;
			cf.BorderWidth = 0;
			cf.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
			tbl.AddCell(cf);

			AddPropertyValue(tbl, "INVOICE_ORDER", " ");
			AddPropertyValue(tbl, "INVOICE_CSYM", this.KonstantniSymbol);
			AddPropertyValue(tbl, "INVOICE_VSYM", this.VariabilniSymbol);
			AddPropertyValue(tbl, "INVOICE_SSYM", this.SpecifickySymbol, 10);

			AddPropertyValue(tbl, "INVOICE_ICO_CUSTOMER", this.Odberatel.ICO);
			AddPropertyValue(tbl, "INVOICE_DIC_CUSTOMER", this.Odberatel.DIC, 12);

			// odberatel
			PdfPTable tblOdberatel = new PdfPTable(1);
			tblOdberatel.WidthPercentage = 100;
			tblOdberatel.DefaultCell.Border = Rectangle.NO_BORDER;

			cf = new PdfPCell(GetBaseChunk("{0}:", User.Language["INVOICE_CUSTOMER"]));
			cf.Padding = 4;
			cf.PaddingTop = 2;
			cf.PaddingBottom = 0;
			cf.Border = Rectangle.TOP_BORDER | Rectangle.RIGHT_BORDER | Rectangle.LEFT_BORDER;
			cf.BorderWidth = 0.5F;
			tblOdberatel.AddCell(cf);

			PdfPCell cellAdresa = new PdfPCell(new Chunk(string.Format("{0}\n{1}\n{2}        {3}", Odberatel.Jmeno, Odberatel.Adresa, Odberatel.PSC, Odberatel.Obec), m_PdfFont11B));
			cellAdresa.Padding = 4;
			cellAdresa.PaddingTop = 0;
			cellAdresa.PaddingBottom = 10;
			cellAdresa.Leading = 18;
			cellAdresa.Border = Rectangle.BOTTOM_BORDER | Rectangle.RIGHT_BORDER | Rectangle.LEFT_BORDER;
			cellAdresa.BorderWidth = 0.5F;
			tblOdberatel.AddCell(cellAdresa);

			cf = new PdfPCell(tblOdberatel);
			cf.Colspan = 2;
			cf.PaddingTop = 0;
			cf.PaddingLeft = 6;
			cf.PaddingRight = 6;
			cf.PaddingBottom = 6;
			cf.Border = Rectangle.NO_BORDER;

			tbl.AddCell(cf);

			return tbl;
		}

		private PdfPTable GetTablePrijemce()
		{
			// konecny prijemce	  tabulka
			PdfPTable tbl = new PdfPTable(1);
			tbl.WidthPercentage = 100;
			tbl.DefaultCell.Border = Rectangle.NO_BORDER;
			tbl.DefaultCell.BorderWidth = 0;

			// konecny prijemce
			PdfPCell cellDodavatel = new PdfPCell(GetBaseChunk("{0}:", User.Language["INVOICE_END_CUSTOMER"]));
			cellDodavatel.PaddingTop = 2;
			cellDodavatel.PaddingLeft = 5;
			cellDodavatel.PaddingRight = 10;
			cellDodavatel.PaddingBottom = 4;
			cellDodavatel.BorderWidth = 0;
			tbl.AddCell(cellDodavatel);

			PdfPCell cellAdresa = new PdfPCell(new Chunk(string.Format("{0}\n{1}\n{2}        {3}", Odberatel.Jmeno, Odberatel.Adresa, Odberatel.PSC, Odberatel.Obec), m_PdfFont9));
			cellAdresa.Padding = 10;
			cellAdresa.PaddingTop = 2;
			cellAdresa.PaddingBottom = 12;
			cellAdresa.Leading = 16;
			cellAdresa.BorderWidth = 0;
			tbl.AddCell(cellAdresa);

			return tbl;
		}

		private PdfPTable GetTablePlatba()
		{
			// zpusob platby
			PdfPTable tbl = new PdfPTable(2);
			tbl.WidthPercentage = 100;
			tbl.DefaultCell.Border = Rectangle.NO_BORDER;
			tbl.DefaultCell.BorderWidth = 0;

			AddPropertyValue(tbl, "INVOICE_PAYMENT_METHOD", User.Language["INVOICE_PAYMENT_PREVOD"], -10);
			AddPropertyValue(tbl, "INVOICE_DATE", (this.DatumLoaded ? this.DatumVystaveni.ToString("d'. 'M'. 'yyyy") : ""));
			AddPropertyValue(tbl, "INVOICE_PAYDATE", (this.DatumLoaded ? this.DatumSplatnosti.ToString("d'. 'M'. 'yyyy") : ""));
			AddPropertyValue(tbl, "INVOICE_DUZP", (this.DatumLoaded ? this.DatumVystaveni.ToString("d'. 'M'. 'yyyy") : ""));

			return tbl;
		}

		private PdfPTable GetTableItems(int index, int count)
		{
			PdfPTable tbl = new PdfPTable((this.PlatceDPH ? 9 : 7));
			tbl.WidthPercentage = 100;
			tbl.DefaultCell.Border = Rectangle.NO_BORDER;
			tbl.DefaultCell.BorderWidth = 0;
			if (this.PlatceDPH)			
				tbl.SetWidths(new float[9] { 22.5F, 10, 10, 10, 4, 11, 8.5F, 12.5F, 12.5F });
			else
				tbl.SetWidths(new float[7] { 40, 10, 10, 10, 5, 12.5F, 12.5F });

			// hlavicky
			AddItemHeader(tbl, "INVOICE_ORDER_MARK").PaddingLeft = 0;
			AddItemHeader(tbl, "INVOICE_MATERIAL", Rectangle.ALIGN_RIGHT);
			AddItemHeader(tbl, "INVOICE_WORK", Rectangle.ALIGN_RIGHT);
			AddItemHeader(tbl, "INVOICE_NUM_QTY", Rectangle.ALIGN_RIGHT);
			AddItemHeader(tbl, "INVOICE_VAL_QTY", Rectangle.ALIGN_RIGHT);
			AddItemHeader(tbl, "INVOICE_PRICE_QTY", Rectangle.ALIGN_RIGHT);
			if (this.PlatceDPH)
			{
				AddItemHeader(tbl, "INVOICE_TAXPERCENT", Rectangle.ALIGN_RIGHT);
				AddItemHeader(tbl, "INVOICE_NO_TAX", Rectangle.ALIGN_RIGHT);
			}
			AddItemHeader(tbl, "INVOICE_TAX", Rectangle.ALIGN_RIGHT);

			// polozky
			count += index;
			if (count > Protokoly.Count) count = Protokoly.Count;

			for (int i = index; i < count; i++)
			{
				AddItemData(tbl, User.Language["INVOICE_ORDER_PROTO"].Replace("%s", Protokoly[i].Cislo));
				AddItemData(tbl, User.FormatPrice(Protokoly[i].CenaMaterial), Rectangle.ALIGN_RIGHT);
				AddItemData(tbl, User.FormatPrice(Protokoly[i].CenaPrace), Rectangle.ALIGN_RIGHT);
				AddItemData(tbl, "1", Rectangle.ALIGN_RIGHT);
				AddItemData(tbl, User.Language["INVOICE_NAME_QTY"], Rectangle.ALIGN_RIGHT);
				AddItemData(tbl, User.FormatPrice(Protokoly[i].CenaCelkem), Rectangle.ALIGN_RIGHT);
				if (this.PlatceDPH)
				{
					AddItemData(tbl, string.Format("{0}", (int)this.DPH), Rectangle.ALIGN_RIGHT);
					AddItemData(tbl, User.FormatPrice(Protokoly[i].CenaCelkem), Rectangle.ALIGN_RIGHT);
				}
				AddItemData(tbl, User.FormatPrice(Protokoly[i].CenaCelkemDPH), Rectangle.ALIGN_RIGHT);
			}

			return tbl;
		}

		private PdfPTable GetTableSumInfo()
		{
			PdfPTable tbl = new PdfPTable((this.PlatceDPH ? 3 : 2));
			tbl.WidthPercentage = (this.PlatceDPH ? 50 : 40);
			tbl.DefaultCell.Border = Rectangle.BOX;
			tbl.DefaultCell.BorderWidth = 0.5F;
			tbl.DefaultCell.Padding = 2;
			tbl.DefaultCell.PaddingTop = 0;
			tbl.DefaultCell.PaddingBottom = 4;
			tbl.HorizontalAlignment = Rectangle.ALIGN_LEFT;
			tbl.SetWidths((this.PlatceDPH ? new float[3] { 40, 20, 20 } : new float[2] { 65, 35 }));


			tbl.AddCell(new PdfPCell(GetBaseChunk(" ")));
			if (this.PlatceDPH)
			{
				PdfPCell cellBezDPH = new PdfPCell(GetBaseChunk(User.Language["INVOICE_NO_TAX"]));
				cellBezDPH.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
				cellBezDPH.PaddingLeft = 10;
				cellBezDPH.PaddingTop = 0;
				cellBezDPH.PaddingBottom = 4;
				tbl.AddCell(cellBezDPH);
			}
			PdfPCell cellDPH = new PdfPCell(GetBaseChunk(User.Language["INVOICE_TAX"]));
			cellDPH.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
			cellDPH.PaddingLeft = 10;
			cellDPH.PaddingTop = 0;
			cellDPH.PaddingBottom = 4;
			tbl.AddCell(cellDPH);

			PdfPCell cf = new PdfPCell(GetBaseChunk(User.Language["INVOICE_TOTAL_MATERIAL"]));
			cf.PaddingRight = 6;
			cf.PaddingTop = 0;
			cf.PaddingBottom = 4;
			cf.NoWrap = true;
			tbl.AddCell(cf);

			if (this.PlatceDPH)
			{
				cf = new PdfPCell(GetBaseChunk(User.FormatPrice(this.CenaMaterialCelkem)));
				cf.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
				cf.PaddingLeft = 10;
				cf.PaddingTop = 0;
				cf.PaddingBottom = 4;
				tbl.AddCell(cf);
			}

			cf = new PdfPCell(GetBaseChunk(User.FormatPrice(this.CenaMaterialCelkemDPH)));
			cf.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
			cf.PaddingLeft = 10;
			cf.PaddingTop = 0;
			cf.PaddingBottom = 4;
			tbl.AddCell(cf);

			cf = new PdfPCell(GetBaseChunk(User.Language["INVOICE_TOTAL_WORK"]));
			cf.PaddingRight = 6;
			cf.PaddingTop = 0;
			cf.PaddingBottom = 4;
			cf.NoWrap = true;
			tbl.AddCell(cf);

			if (this.PlatceDPH)
			{
				cf = new PdfPCell(GetBaseChunk(User.FormatPrice(this.CenaPraceCelkem)));
				cf.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
				cf.PaddingLeft = 10;
				cf.PaddingTop = 0;
				cf.PaddingBottom = 4;
				tbl.AddCell(cf);
			}

			cf = new PdfPCell(GetBaseChunk(User.FormatPrice(this.CenaPraceCelkemDPH)));
			cf.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
			cf.PaddingLeft = 10;
			cf.PaddingTop = 0;
			cf.PaddingBottom = 4;
			tbl.AddCell(cf);

			return tbl;
		}

		// len pre platca DPH
		private PdfPTable GetTableCenaDane()
		{
			PdfPTable tbl = new PdfPTable(5);
			tbl.WidthPercentage = 75;
			tbl.DefaultCell.Padding = 2;
			tbl.DefaultCell.BorderWidth = 0.5F;
			tbl.HorizontalAlignment = Rectangle.ALIGN_RIGHT;

			PdfPCell cf = new PdfPCell(GetBaseChunk(" "));
			cf.Border = Rectangle.LEFT_BORDER | Rectangle.TOP_BORDER;
			cf.BorderWidth = 0.5F;
			cf.Colspan = 2;
			tbl.AddCell(cf);

			cf = new PdfPCell(GetBaseChunk(User.Language["INVOICE_TAX_BASE"]));
			cf.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
			cf.Border = Rectangle.TOP_BORDER;
			tbl.AddCell(cf);

			cf = new PdfPCell(GetBaseChunk(User.Language["INVOICE_TAX_AMOUNT"]));
			cf.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
			cf.Border = Rectangle.TOP_BORDER;
			tbl.AddCell(cf);

			cf = new PdfPCell(GetBaseChunk(User.Language["INVOICE_TAX_INCL"]));
			cf.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
			cf.Border = Rectangle.TOP_BORDER | Rectangle.RIGHT_BORDER;
			cf.PaddingRight = 4;
			tbl.AddCell(cf);

			cf = new PdfPCell(GetBaseChunk(" "));
			cf.Border = Rectangle.LEFT_BORDER;
			tbl.AddCell(cf);

			cf = new PdfPCell(GetBaseChunkB("0 %"));
			cf.Border = Rectangle.NO_BORDER;
			cf.BorderWidth = 0;
			cf.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
			tbl.AddCell(cf);

			cf = new PdfPCell(GetBaseChunkB(User.FormatPrice(0.0F)));
			cf.Border = Rectangle.NO_BORDER;
			cf.BorderWidth = 0;
			cf.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
			tbl.AddCell(cf);

			cf = new PdfPCell(GetBaseChunkB(User.FormatPrice(0.0F)));
			cf.Border = Rectangle.NO_BORDER;
			cf.BorderWidth = 0;
			cf.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
			tbl.AddCell(cf);

			cf = new PdfPCell(GetBaseChunkB(User.FormatPrice(0.0F)));
			cf.Border = Rectangle.RIGHT_BORDER;
			cf.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
			cf.PaddingRight = 4;
			tbl.AddCell(cf);

			cf = new PdfPCell(GetBaseChunk(User.Language["INVOICE_TAX_LOW"]));
			cf.Border = Rectangle.LEFT_BORDER;
			cf.PaddingLeft = 4;
			tbl.AddCell(cf);

			cf = new PdfPCell(GetBaseChunkB("{0} %", (int)m_DPHLow));
			cf.Border = Rectangle.NO_BORDER;
			cf.BorderWidth = 0;
			cf.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
			tbl.AddCell(cf);

			cf = new PdfPCell(GetBaseChunkB(User.FormatPrice(0.0F)));
			cf.Border = Rectangle.NO_BORDER;
			cf.BorderWidth = 0;
			cf.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
			tbl.AddCell(cf);

			cf = new PdfPCell(GetBaseChunkB(User.FormatPrice(0.0F)));
			cf.Border = Rectangle.NO_BORDER;
			cf.BorderWidth = 0;
			cf.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
			tbl.AddCell(cf);

			cf = new PdfPCell(GetBaseChunkB(User.FormatPrice(0.0F)));
			cf.Border = Rectangle.RIGHT_BORDER;
			cf.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
			cf.PaddingRight = 4;
			tbl.AddCell(cf);

			cf = new PdfPCell(GetBaseChunk(User.Language["INVOICE_TAX_NOR"]));
			cf.Border = Rectangle.LEFT_BORDER | Rectangle.BOTTOM_BORDER;
			cf.PaddingLeft = 4;
			cf.PaddingBottom = 4;
			tbl.AddCell(cf);

			cf = new PdfPCell(GetBaseChunkB("{0} %", (int)this.DPH));
			cf.Border = Rectangle.BOTTOM_BORDER;
			cf.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
			cf.PaddingBottom = 4;
			tbl.AddCell(cf);

			cf = new PdfPCell(GetBaseChunkB(User.FormatPrice(this.CenaCelkem)));
			cf.Border = Rectangle.BOTTOM_BORDER;
			cf.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
			cf.PaddingBottom = 4;
			tbl.AddCell(cf);

			cf = new PdfPCell(GetBaseChunkB(User.FormatPrice(this.CenaCelkemDPH - this.CenaCelkem)));
			cf.Border = Rectangle.BOTTOM_BORDER;
			cf.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
			cf.PaddingBottom = 4;
			tbl.AddCell(cf);

			cf = new PdfPCell(GetBaseChunkB(User.FormatPrice(this.CenaCelkemDPH)));
			cf.Border = Rectangle.BOTTOM_BORDER | Rectangle.RIGHT_BORDER;
			cf.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
			cf.PaddingBottom = 4;
			cf.PaddingRight = 4;
			tbl.AddCell(cf);

			return tbl;
		}

		private PdfPTable GetTableCenaTotal()
		{
			PdfPTable tbl = new PdfPTable(2);
			tbl.SetWidths(new float[2] { 80, 20 });
			tbl.WidthPercentage = 100;
			tbl.DefaultCell.Border = Rectangle.NO_BORDER;
			tbl.DefaultCell.BorderWidth = 0;
			tbl.DefaultCell.Padding = 2;

			if (!this.PlatceDPH)
			{
				PdfPCell cellCenaTxt = new PdfPCell(GetBaseChunk("{0}:", User.Language["INVOICE_PRICE"]));
				cellCenaTxt.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
				cellCenaTxt.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER;
				cellCenaTxt.PaddingBottom = 6;
				tbl.AddCell(cellCenaTxt);

				PdfPCell cellCena = new PdfPCell(GetBaseChunk(User.FormatPrice(this.CenaCelkemDPH)));
				cellCena.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
				cellCena.Border = Rectangle.TOP_BORDER | Rectangle.BOTTOM_BORDER;
				cellCena.PaddingBottom = 6;
				tbl.AddCell(cellCena);
			}

			PdfPCell cf = new PdfPCell(new Chunk(string.Format("{0} :", User.Language["INVOICE_TOTAL_CURRENCY"].Replace("%s", User.Currency)), m_PdfFont11B));
			cf.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
			cf.Border = Rectangle.NO_BORDER;
			tbl.AddCell(cf);

			cf = new PdfPCell(new Chunk(User.FormatPrice(this.CenaCelkemDPH), m_PdfFont11B));
			cf.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
			cf.Border = Rectangle.NO_BORDER;
			tbl.AddCell(cf);

			cf = new PdfPCell(new Chunk(string.Format("{0} :", User.Language["INVOICE_TOTAL_PRE"]), m_PdfFont11B));
			cf.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
			cf.Border = Rectangle.NO_BORDER;
			tbl.AddCell(cf);

			cf = new PdfPCell(new Chunk(User.FormatPrice(0.0F), m_PdfFont11B));
			cf.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
			cf.Border = Rectangle.NO_BORDER;
			tbl.AddCell(cf);

			cf = new PdfPCell(new Chunk(string.Format("{0} :", User.Language["INVOICE_TOTAL_PAY"]), m_PdfFont11B));
			cf.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
			cf.Border = Rectangle.NO_BORDER;
			tbl.AddCell(cf);

			cf = new PdfPCell(new Chunk(User.FormatPrice(this.CenaCelkemDPH), m_PdfFont11B));
			cf.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
			cf.Border = Rectangle.NO_BORDER;
			tbl.AddCell(cf);


			return tbl;
		}

		private void RenderHeader(Document document, bool pruvodniList)
		{
			PdfPTable tbl = new PdfPTable((pruvodniList ? 2 : 1));
			tbl.DefaultCell.Border = Rectangle.BOTTOM_BORDER;
			tbl.DefaultCell.BorderWidth = 0.5F;
			tbl.DefaultCell.Padding = 0;
			tbl.DefaultCell.PaddingBottom = 2;
			tbl.WidthPercentage = 100;

			if (pruvodniList)
			{
				tbl.SetWidths(new float[2] { 80, 20 });

				PdfPCell cf = new PdfPCell(new Phrase(User.Language["INVOICE_LETTER"], m_PdfFont13B));
				cf.Padding = 0;
				cf.PaddingBottom = 5;
				cf.Border = Rectangle.BOTTOM_BORDER;
				cf.VerticalAlignment = Rectangle.ALIGN_BOTTOM;
				cf.BorderWidth = 0.5F;
				tbl.AddCell(cf);

				cf = new PdfPCell(new Phrase(DateTime.Now.ToString("d'. 'M'. 'yyyy"), m_PdfFont9B));
				cf.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
				cf.VerticalAlignment = Rectangle.ALIGN_BOTTOM;
				cf.Padding = 0;
				cf.PaddingBottom = 2;
				cf.PaddingBottom = 5;
				cf.Border = Rectangle.BOTTOM_BORDER;
				cf.BorderWidth = 0.5F;
				tbl.AddCell(cf);
			}
			else
			{
				Phrase p = new Phrase(User.Language["HEADER_INVOICE"], m_PdfFont13);
				p.Leading = 10;
				tbl.AddCell(p);
			}

			document.Add(tbl);
			document.Add(new Paragraph(9, " "));
		}

		private void RenderInvoice(Document document)
		{
			PdfPTable tbl = new PdfPTable(2);
			tbl.DefaultCell.Padding = 0;
			tbl.WidthPercentage = 100;

			// dodavatel
			tbl.AddCell(GetTableDodavatel());
			tbl.AddCell(GetTableFaktura());
			tbl.AddCell(GetTablePrijemce());
			tbl.AddCell(GetTablePlatba());

			document.Add(tbl);
		}

		// konstanty
		private void RenderItems(Document document, bool pruvodniList)
		{
			int ITEMS_FIRST_OVERHANG = (pruvodniList ? 20 : 10);
			int ITEMS_NEXT_OVERHANG = 20;
			int ITEMS_FIRST_PAGE = (pruvodniList ? 25 : 15);
			int ITEMS_NEXT_PAGE = 35;

			// hlavicka
			document.Add(new Paragraph(12, " "));
			if (!pruvodniList)
				document.Add(new Paragraph(9, GetBaseChunk("{0}:", User.Language["INVOICE_MSG_ITEMS"])));

			// jednotlive polozky
			// max 10/20 poloziek na prvu stranku, na dalsie max 30/40
			bool jePrvaStranka = true;
			int startPos = 0;
			while (startPos < Protokoly.Count)
			{
				int maxNaStranku = (jePrvaStranka ? ITEMS_FIRST_PAGE : ITEMS_NEXT_PAGE);
				int nextCount = Protokoly.Count - startPos;
				bool addNewPage = false;

				if (nextCount > maxNaStranku)
				{
					maxNaStranku += (jePrvaStranka ? ITEMS_FIRST_OVERHANG : ITEMS_NEXT_OVERHANG);
					addNewPage = true;
				}

				document.Add(GetTableItems(startPos, maxNaStranku));

				startPos += maxNaStranku;
				if (addNewPage || startPos <= Protokoly.Count)
				{
					document.NewPage();
				}
				jePrvaStranka = false;
			}
		}

		private void RenderPriceInfo(Document document)
		{
			// tabulka s celkovym poctom cien
			document.Add(new Paragraph(9, " "));
			document.Add(GetTableSumInfo());
			document.Add(new Paragraph(9, " "));

			if (this.PlatceDPH)
			{
				document.Add(GetTableCenaDane());
				document.Add(new Paragraph(9, " "));
			}
			document.Add(GetTableCenaTotal());
		}

		public void RenderFaktura(Document document)
		{
			RenderHeader(document, false);
			RenderInvoice(document);
			RenderItems(document, false);
			RenderPriceInfo(document);
		}

		public void RenderPruvodniList(Document document)
		{
			RenderHeader(document, true);
			RenderItems(document, true);
			RenderPriceInfo(document);
		}

		#endregion

	}


	public class PdfMemoryStream : System.IO.MemoryStream
	{
		private bool m_LeaveOpen = false;
		public bool LeaveOpen
		{
			get { return m_LeaveOpen; }
			set { m_LeaveOpen = value; }
		}

		public override void Close()
		{
			if (!m_LeaveOpen)
				base.Close();
		}

		protected override void Dispose(bool disposing)
		{
			m_LeaveOpen = false;
			base.Dispose(disposing);
		}
	}
}
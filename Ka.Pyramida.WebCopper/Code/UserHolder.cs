﻿using System;
using System.Data;
using System.Configuration;
using Ka.Garance.Data;
using Ka.Garance.User;

namespace Ka.Garance.User
{

	public sealed class Uzivatel
	{
		private GaranceUser m_User;

		private string m_Nazev;
		private string m_Kod;
		private string m_Stat;
		private string m_Mesto;
		private string m_PSC;
		private string m_Ulice;
		private string m_Email;
		private string m_Tel;
		private string m_Fax;
		private string m_Mobil;
		private bool m_PlatceDPH;
		private string m_Ico;
		private string m_Dic;
		private string m_Ucet;
		private int m_CisloFa;

		public Uzivatel(GaranceUser user)
		{
			m_User = user;
			Load();
		}

		#region properties
		public string Nazev
		{
			get { return m_Nazev; }
		}
		public string Kod
		{
			get { return m_Kod; }
		}
		public string Stat
		{
			get { return m_Stat; }
		}
		public string Mesto
		{
			get { return m_Mesto; }
			set { m_Mesto = value; }
		}
		public string PSC
		{
			get { return m_PSC; }
			set { m_PSC = value; }
		}
		public string Ulice
		{
			get { return m_Ulice; }
			set { m_Ulice = value; }
		}
		public string Email
		{
			get { return m_Email; }
			set { m_Email = value; }
		}
		public string Tel
		{
			get { return m_Tel; }
			set { m_Tel = value; }
		}
		public string Fax
		{
			get { return m_Fax; }
			set { m_Fax = value; }
		}
		public string Mobil
		{
			get { return m_Mobil; }
			set { m_Mobil = value; }
		}
		public bool PlatceDPH
		{
			get { return m_PlatceDPH; }
			set { m_PlatceDPH = value; }
		}	
		public string Ico
		{
			get { return m_Ico; }
			set { m_Ico = value; }
		}
		public string Dic
		{
			get { return m_Dic; }
			set { m_Dic = value; }
		}
		public string Ucet
		{
			get { return m_Ucet; }
			set { m_Ucet = value; }
		}
		public int CisloFa
		{
			get { return m_CisloFa; }
			set { m_CisloFa = value; }
		}
		#endregion

		public void Load()
		{
			MySQLDataReader dr = m_User.Database.ExecuteReader("SELECT nazev, kod, stat, mesto, psc, ulice, email, tel, fax, mobil, ico, dic, ucet, sdph, IFNULL(cislofa, "+string.Format("{0}0000", DateTime.Now.Year.ToString())+") AS cislofa FROM tdealer WHERE id={0}", m_User.Id);
			if (!dr.Read())
				throw new Exception("Could not load user details!");

			m_Nazev = Convert.ToString(dr.GetValue("nazev"));
			m_Kod = Convert.ToString(dr.GetValue("kod"));
			m_Stat = Convert.ToString(dr.GetValue("stat"));
			m_Mesto = Convert.ToString(dr.GetValue("mesto"));
			m_PSC = Convert.ToString(dr.GetValue("psc"));
			m_Ulice = Convert.ToString(dr.GetValue("ulice"));
			m_Email = Convert.ToString(dr.GetValue("email"));
			m_Tel = Convert.ToString(dr.GetValue("tel"));
			m_Fax = Convert.ToString(dr.GetValue("fax"));
			m_Mobil = Convert.ToString(dr.GetValue("mobil"));
			m_Ico = Convert.ToString(dr.GetValue("ico"));
			m_Dic = Convert.ToString(dr.GetValue("dic"));
			m_Ucet = Convert.ToString(dr.GetValue("ucet"));
			m_PlatceDPH = (Convert.ToInt32(dr.GetValue("sdph")) == 1);
			m_CisloFa = Convert.ToInt32(dr.GetValue("cislofa"));
		}

		public void Save()
		{
			m_User.Database.Execute(
				"UPDATE tdealer SET mesto={0},psc={1},ulice={2},email={3},tel={4},fax={5},mobil={6},ico={7},dic={8},ucet={9},sdph={10} WHERE id={11}",
				m_Mesto, m_PSC, m_Ulice, m_Email, m_Tel, m_Fax, m_Mobil, m_Ico, m_Dic, m_Ucet, (m_PlatceDPH ? 1 : 0), m_User.Id
			);
		}

		public void NastavHeslo(string noveHeslo)
		{
			noveHeslo = Ka.Garance.Util.UnixCrypt.Crypt(m_User.UserName.Substring(0, 2), noveHeslo);
			m_User.Database.Execute(
				"UPDATE tdealer SET password={0} WHERE id={1}",
				noveHeslo, m_User.Id
			);
		}

		/*
		public void NastavFakturace()
		{
			m_User.Database.Execute("UPDATE tdealer SET cislofa={0} WHERE id={1}", m_CisloFa, m_User.Id);
		}
		*/

		public int PocetFaktur(string cislofa)
		{
			return Convert.ToInt32(m_User.Database.ExecuteScalar("SELECT COUNT(*) FROM tprotocol WHERE iddealer={0} AND cislofa={1}", m_User.Id, cislofa));
		}
	}

}
﻿using System;
using System.Data;
using System.Collections;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Ka.Garance.Data;
using Ka.Garance.User;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Ka.Garance.Invoice.Yuki;

namespace Ka.Garance.Protocol.Yuki
{

	public class NovaZavadaPozice
	{
		private decimal m_CenaCjJednotka;
		private int m_Id = -1;
		private NovaZavada m_NovaZavada;
		private MySQLDataReader m_DrData = null;

		public NovaZavadaPozice(NovaZavada zav, int id)
		{
			m_NovaZavada = zav;
			m_Id = id;
			if (m_Id > -1)
			{
				m_DrData = zav.Database.ExecuteReader("SELECT skodnicislo,pozicecislo,nazev,operace,cj FROM tcpozice WHERE id={0}", m_Id);
				m_DrData.Read();
			}

			object cena = zav.Database.ExecuteScalar("SELECT cenacj_nm FROM ndcenikprace WHERE idzeme={0} ORDER BY cenacj_nm DESC", zav.User.IdZeme);
			if (cena == null || cena is DBNull) m_CenaCjJednotka = 0.0m;
			else m_CenaCjJednotka = Convert.ToDecimal(cena) / 100;
		}

		public NovaZavada Zavada
		{
			get { return m_NovaZavada; }
		}

		public int Id
		{
			get { return m_Id; }
		}

		public virtual string Kod
		{
			get { return Convert.ToString(m_DrData.GetValue("skodnicislo")); }
		}

		public virtual string Pozice
		{
			get { return Convert.ToString(m_DrData.GetValue("pozicecislo")); }
		}

		public virtual string Nazev
		{
			get { return Convert.ToString(m_DrData.GetValue("nazev")); }
		}

		public virtual string Operace
		{
			get { return Convert.ToString(m_DrData.GetValue("operace")); }
		}

		public virtual int Cj
		{
			get { return Convert.ToInt32(m_DrData.GetValue("cj")); }
		}

		public decimal CenaCjJednotka
		{
			get { return m_CenaCjJednotka; }
		}

		public decimal Cena
		{
			get { return (decimal)Math.Round(CenaCjJednotka * (decimal)Cj, m_NovaZavada.RecalcEUR ? 2 : 1); }
		}

		public decimal CenaDPH
		{
			get { return (decimal)Math.Round(Cena * (Zavada.User.DPH + 100) / 100, m_NovaZavada.RecalcEUR ? 2 : 1); }
		}
	}

	public class NovaZavadaPoziceVolna : NovaZavadaPozice
	{
		private string m_Nazev;
		private string m_Popis;
		private int m_Cj;

		public NovaZavadaPoziceVolna(NovaZavada zav, string nazev, string popis, int cj)
			: base(zav, -1)
		{
			m_Nazev = nazev;
			m_Popis = popis;
			m_Cj = cj;
		}

		public override string Kod
		{
			get { return "99"; }
		}

		public override string Pozice
		{
			get { return "99"; }
		}

		public override int Cj
		{
			get { return m_Cj; }
		}

		public override string Nazev
		{
			get { return m_Nazev; }
		}

		public override string Operace
		{
			get { return m_Popis; }
		}
	}

	public class NovaZavadaPoziceList : IEnumerable
	{
		private ArrayList m_Items = new ArrayList();
		private NovaZavada m_NovaZavada;

		public NovaZavadaPoziceList(NovaZavada zav)
		{
			m_NovaZavada = zav;
		}

		public NovaZavadaPozice this[int index]
		{
			get { return m_Items[index] as NovaZavadaPozice; }
		}

		public int Count
		{
			get { return m_Items.Count; }
		}

		public NovaZavadaPozice Add(int id)
		{
			int idx = -1, idxCounter = 0;
			foreach (NovaZavadaPozice p in m_Items)
			{
				if (p.Id == id) return p;
				else if (p.Id == -1)
				{
					idx = idxCounter;
					break;
				}
				else
					idxCounter++;
			}

			NovaZavadaPozice pp = new NovaZavadaPozice(m_NovaZavada, id);
			if (idx != -1)
				m_Items.Insert(idx, pp);
			else
				m_Items.Add(pp);
			return pp;
		}

		public NovaZavadaPozice Add(string nazev, string popis, int cj)
		{
			m_Items.Add(new NovaZavadaPoziceVolna(m_NovaZavada, nazev, popis, cj));
			return m_Items[m_Items.Count - 1] as NovaZavadaPozice;
		}

		public void Delete(int index)
		{
			m_Items.RemoveAt(index);
		}

		public void Clear()
		{
			m_Items.Clear();
		}

		public IEnumerator GetEnumerator()
		{
			return m_Items.GetEnumerator();
		}
		
	}

	public class NovaZavadaNd
	{
		private NovaZavada m_Zavada;
		private MySQLDataReader m_Data;
		private int m_Id;
		private int m_Mnozstvi;
		private int m_Objednat;
		private decimal m_CenaJednotka = 0.0m;
		private string m_Kod;

		public NovaZavadaNd(NovaZavada zav, int id, int mnozstvi, int objednat)
		{
			m_Zavada = zav;
			m_Id = id;
			m_Mnozstvi = mnozstvi;
			m_Objednat = objednat;

			// TODO: nazev cez objcislo
			m_Data = Zavada.Database.ExecuteReader(@"
				SELECT DISTINCT
					A.id AS id,
					IF(D.sklcislo IS NULL,A.nazevdilu,D.nazevdilu) AS nazevdilu,
					IF(D.sklcislo IS NULL,A.nazevdilu_en,D.nazevdilu_en) AS nazevdilu_en,
					IF(D.sklcislo IS NULL,A.cena,D.cena) AS cena,
					CONCAT(A.skupina,'.',A.pozice) AS kod,
					B.BarvaEn AS barva_en,
					B.BarvaCz AS barva
				FROM tcskodnicisla A
					LEFT JOIN ndbarvy B ON B.Identifikator=SUBSTRING(A.sklcislo, 8)
					LEFT JOIN tcskodnicisla D ON A.objcislo=D.sklcislo AND D.state<>9
				WHERE A.id={0}", id);
			m_Data.Read();
			m_Kod = Convert.ToString(m_Data.GetValue("kod"));

            object cena = Zavada.Database.ExecuteScalar("SELECT A.NM AS cena FROM ndcenik A LEFT JOIN tcskodnicisla B ON A.sklcislo = IF(B.objcislo IS NULL OR LENGTH(B.objcislo) = 0, B.sklcislo, B.objcislo) WHERE A.idzeme={0} AND B.id={1} ORDER BY A.NM DESC", Zavada.User.IdZeme, this.Id);
			if (cena == null || cena is DBNull) m_CenaJednotka = 0.0m;
			else m_CenaJednotka = Convert.ToDecimal(cena);
		}

		public NovaZavada Zavada
		{
			get { return m_Zavada; }
		}

		public int Id
		{
			get { return m_Id; }
		}

		public string Kod
		{
			get { return m_Kod; }
		}

		public string Skupina
		{
			get
			{
				try
				{
					string[] s = m_Kod.Split(".".ToCharArray());
					if (s[0].Length != 2) return m_Kod.Substring(0, 2);
					return s[0];
				}
				catch { }
				return "01";
			}
		}

		public string Pozice
		{
			get
			{
				try
				{
					string[] s = m_Kod.Split(".".ToCharArray());
					if (s[0].Length != 2) return m_Kod.Substring(2);
					return s[1];
				}
				catch { }
				return "01";
			}
		}

		public string Nazev
		{
			get { return Convert.ToString(m_Data.GetValue("nazevdilu")); }
		}

		public string NazevEn
		{
			get { return Convert.ToString(m_Data.GetValue("nazevdilu_en")); }
		}

		public string Barva
		{
			get { return Convert.ToString(m_Data.GetValue("barva")); }
		}

		public string BarvaEn
		{
			get { return Convert.ToString(m_Data.GetValue("barva_en")); }
		}

		public decimal Cena
		{
			get { return (decimal)Math.Round(m_CenaJednotka * Mnozstvi, m_Zavada.RecalcEUR ? 2 : 1); }
		}

		private decimal UserDPHRabatConst
		{
			get
			{
				return (0.7m * ((100 + (decimal)Zavada.User.Rabat) / 100) * ((100 + (decimal)Zavada.User.DPH) / 100));
			}
		}

		public decimal CenaDPH
		{
			get
			{
				decimal unitPrice = Math.Round((decimal)m_CenaJednotka, 2);
				return (decimal)Math.Round(unitPrice * m_Mnozstvi * UserDPHRabatConst, m_Zavada.RecalcEUR ? 2 : 1);
			}
		}	

		public int Mnozstvi
		{
			get { return m_Mnozstvi; }
			set { m_Mnozstvi = value; }
		}

		public int Objednat
		{
			get { return m_Objednat; }
			set { m_Objednat = value; }
		}

	}

	public class NovaZavadaNdList : IEnumerable
	{
		private ArrayList m_Items = new ArrayList();
		private NovaZavada m_Zavada;

		public NovaZavadaNdList(NovaZavada zav)
		{
			m_Zavada = zav;
		}

		public NovaZavadaNd this[int index]
		{
			get { return m_Items[index] as NovaZavadaNd; }
		}

		public int Count
		{
			get { return m_Items.Count; }
		}

		public NovaZavadaNd Add(int id, int mnozstvi, int objednat)
		{
			foreach (NovaZavadaNd nd in m_Items)
			{
				if (nd.Id == id)
				{
					nd.Mnozstvi = mnozstvi;
					nd.Objednat = objednat;
					return nd;
				}
			}

			NovaZavadaNd ndd = new NovaZavadaNd(m_Zavada, id, mnozstvi, objednat);
			m_Items.Add(ndd);
			return ndd;
		}

		public void Delete(int index)
		{
			m_Items.RemoveAt(index);
		}

		public void Clear()
		{
			m_Items.Clear();
		}

		public IEnumerator GetEnumerator()
		{
			return m_Items.GetEnumerator();
		}
	}

	public class NovaZavada : IDisposable
	{
		private int m_SavedIndex = -1;
		private string m_VIN;
		private string m_Kod;
		private string m_DZ;
		private string m_Vyrobce = "Y00";
		private string m_Nazev;
		private string m_Popis;
		private GaranceUser m_User;

		private NovaZavadaPoziceList m_Pozice;
		private NovaZavadaNdList m_Nd;

		public NovaZavada(string vin, GaranceUser user)
		{
			m_Pozice = new NovaZavadaPoziceList(this);
			m_Nd = new NovaZavadaNdList(this);
			m_VIN = vin;
			m_User = user;
			Clear();
		}

		internal bool RecalcEUR
		{
			get { return m_User.IdZeme == 2; }
		}

		public GaranceUser User
		{
			get { return m_User; }
		}

		public MySQLDatabase Database
		{
			get { return m_User.Database; }
		}

		public NovaZavadaPoziceList Pozice
		{
			get { return m_Pozice; }
		}

		public NovaZavadaNdList Nd
		{
			get { return m_Nd; }
		}

		public string VIN
		{
			get { return m_VIN; }
		}

		public string Model
		{
			get { return Convert.ToString(Database.ExecuteScalar("SELECT model FROM tmotocykl WHERE ciskar={0}", m_VIN)); }
		}

		public string ModelVzor
		{
			get
			{
				string model = this.Model;
				Ka.Garance.Util.GaranceUtil.GetPartsVzor(model, Database);
				string vzor = Convert.ToString(Database.ExecuteScalar("SELECT vzor FROM tcmodelklic WHERE id={0}", model));
				if (vzor != null && vzor.Trim() != "")
				{
					return vzor;
				}
				return model;
			}
		}

        public string ModelMotor
        {
            get
            {
                string model = this.Model;
                string vzor = Ka.Garance.Util.GaranceUtil.GetVzorMotor(model, Database);
                
                return vzor;
            }
        }

        public string ModelRam
        {
            get
            {
                string model = this.Model;
                string vzor = Ka.Garance.Util.GaranceUtil.GetVzorRam(model, Database);

                return vzor;
            }
        }

		public string ModelNazev
		{
			get
			{
				return Convert.ToString(Database.ExecuteScalar("SELECT CONCAT(tmotocykl.model,' - ',tcmodelklic.jmeno) AS jmeno FROM tmotocykl LEFT JOIN tcmodelklic ON tcmodelklic.id=tmotocykl.model WHERE ciskar={0}", m_VIN));
			}
		}

		public string Kod
		{
			get { return m_Kod; }
			set
			{
				if (m_Kod != value)
				{
					m_Kod = value;
					ResetPoziceND();
				}
			}
		}

		public string KodSkupina
		{
			get
			{
				try
				{
					return m_Kod.Substring(0, 2);
				}
				catch { }
				return Convert.ToString(Database.ExecuteScalar("SELECT IFNULL(skupina,'01') AS id FROM tcskodnicisla WHERE skupina IS NOT NULL AND typmoto={0} GROUP BY skupina ORDER BY id", Model));
			}
			set
			{
				if (value == "" || value == null || value.Length != 2) return;
				try
				{
					Kod = string.Format("{0}.{1}", value, KodPozice);
				}
				catch { }
			}
		}

		public string KodPozice
		{
			get
			{
				try
				{
					string[] s = m_Kod.Split(".".ToCharArray());
					if (s.Length == 1) return m_Kod.Substring(2);
					return s[1];
				}
				catch { }
				return "01";
			}
			set
			{
				try
				{
					Kod = string.Format("{0}.{1}", KodSkupina, value);
				}
				catch { }
			}
		}

		public string SkodniCislo
		{
			get
			{
				/*if (m_Kod == null || m_Kod.Length < 3)
					return m_Kod;
				else if (m_Kod[2] == '.')
					return string.Format("{0}{1}", m_Kod.Substring(0, 2), m_Kod.Substring(3));
				else*/
					return m_Kod;
			}
		}

		public string DZ
		{
			get { return m_DZ; }
			set { m_DZ = value; }
		}

		public string Vyrobce
		{
			get { return m_Vyrobce; }
			set { m_Vyrobce = value; }
		}

		public string Nazev
		{
			get { return m_Nazev; }
			set { m_Nazev = value; }
		}

		public string Popis
		{
			get { return m_Popis; }
			set { m_Popis = value; }
		}

		private void ResetPoziceND()
		{
			m_Pozice.Clear();
			m_Nd.Clear();
		}

		public void Clear()
		{
			m_Kod = "";
			m_DZ = "";
			m_Vyrobce = "Y00";
			m_Nazev = "";
			m_Popis = "";
			ResetPoziceND();
		}

		public string[] GetSubmitErrors(Garance.Lang.LanguageStrings Language)
		{
			int sCount = 0;
			if (m_Kod == null || m_Kod.Trim() == "") sCount++;
			if (m_Pozice.Count == 0) sCount++;

			string[] errors = new string[sCount];
			sCount = 0;
			if (m_Kod == null || m_Kod.Trim() == "") errors[sCount++] = Language["PROTO_ERR_ISSUE_CODE"];
			if (m_Pozice.Count == 0) errors[sCount++] = Language["PROTO_ERR_ISSUE_POS"];

			return errors;
		}

		public void AddToProtocol(YukiProtocol protocol)
		{
			YukiProtocolZavada z = null;
			if (m_SavedIndex != -1)
			{
				try
				{
					z = protocol.Zavady[m_SavedIndex];
				}
				catch { return; }				
				z.DZ = m_DZ;
				z.Nazev = m_Nazev;
				z.Popis = m_Popis;
				z.Pozice.Clear();
				z.Nd.Clear();
			}
			else
				z = protocol.Zavady.Add(-1, m_Kod, m_DZ, m_Vyrobce, m_Nazev, m_Popis, true);

			foreach (NovaZavadaPozice p in this.Pozice)
			{
				z.Pozice.Add(p.Id, p.Nazev, p.Operace, p.Cj, 0, true);
			}
			foreach (NovaZavadaNd n in this.Nd)
			{
				z.Nd.Add(n.Id, n.Mnozstvi, n.Objednat, 0, true);
			}

			protocol.SaveToCache();
		}

		private string m_CacheId;
		private void AddToCache(string name, string value)
		{
			User.Cache.SetString(string.Format("IssueHolder_{0}.[{1}]", m_CacheId, name), value);
		}
		private void AddToCache(string name, int value)
		{
			User.Cache.SetInt32(string.Format("IssueHolder_{0}.[{1}]", m_CacheId, name), value);
		}
		private string GetFromCache(string name)
		{
			return User.Cache.GetString(string.Format("IssueHolder_{0}.[{1}]", m_CacheId, name));
		}
		private int GetFromCache(string name, bool isint)
		{
			return User.Cache.GetInt32(string.Format("IssueHolder_{0}.[{1}]", m_CacheId, name));
		}

		public void SaveToCache()
		{
			User.Cache.BeginUpdate();
			try
			{
				AddToCache("Z.Saved", 1);
				AddToCache("DZ", m_DZ);
				AddToCache("Kod", m_Kod);
				AddToCache("Nazev", m_Nazev);
				AddToCache("Popis", m_Popis);
				AddToCache("SavedIndex", m_SavedIndex);
				AddToCache("Vyrobce", m_Vyrobce);
				AddToCache("Nd", m_Nd.Count);
				for (int i = 0; i < m_Nd.Count; i++)
				{
					AddToCache(string.Format("Nd[{0}].Id", i), m_Nd[i].Id);
					AddToCache(string.Format("Nd[{0}].M", i), m_Nd[i].Mnozstvi);
					AddToCache(string.Format("Nd[{0}].O", i), m_Nd[i].Objednat);
				}
				AddToCache("Pos", m_Pozice.Count);
				for (int i = 0; i < m_Pozice.Count; i++)
				{
					AddToCache(string.Format("Pos[{0}].Id",i), m_Pozice[i].Id);
					if (m_Pozice[i].Id == -1)
					{
						AddToCache(string.Format("Pos[{0}].N", i), m_Pozice[i].Nazev);
						AddToCache(string.Format("Pos[{0}].P", i), m_Pozice[i].Pozice);
						AddToCache(string.Format("Pos[{0}].Cj", i), m_Pozice[i].Cj);
					}
				}
			}
			finally
			{
				User.Cache.EndUpdate();
			}
		}

		public void LoadFromCache()
		{
			if (GetFromCache("Z.Saved", true) != 1) return;
			m_DZ = GetFromCache("DZ");
			m_Kod = GetFromCache("Kod");
			m_Nazev = GetFromCache("Nazev");
			m_Popis = GetFromCache("Popis");
			m_SavedIndex = GetFromCache("SavedIndex", true);
			m_Vyrobce = GetFromCache("Vyrobce");
			m_Nd.Clear();
			int nc = GetFromCache("Nd", true);
			for (int i = 0; i < nc; i++)
			{
				m_Nd.Add(
					GetFromCache(string.Format("Nd[{0}].Id", i), true),
					GetFromCache(string.Format("Nd[{0}].M", i), true),
					GetFromCache(string.Format("Nd[{0}].O", i), true)
				);
			}
			m_Pozice.Clear();
			nc = GetFromCache("Pos", true);
			for (int i = 0; i < nc; i++)
			{
				int id = GetFromCache(string.Format("Pos[{0}].Id", nc), true);
				if (id == -1)
				{
					m_Pozice.Add(
						GetFromCache(string.Format("Pos[{0}].N", nc)),
						GetFromCache(string.Format("Pos[{0}].P", nc)),
						GetFromCache(string.Format("Pos[{0}].Cj", nc), true)
					);
				}
				else
					m_Pozice.Add(id);
			}
		}

		private bool m_Disposed = false;
		public void Dispose()
		{
			if (!m_Disposed)
			{
				try { /*SaveToCache();*/ }
				catch { }
				m_Disposed = true;
			}
		}

		
		~NovaZavada()
		{
			Dispose();
		}

		public string TmpSkupina = "";
		public string TmpPozice = "";

		#region static
		private static Hashtable m_CachedZavada = new Hashtable();
		public static NovaZavada Create(string vin, GaranceUser user, string id)
		{
			id = string.Format("{0}.{1}.{2}", user.Id, user.CacheKey, id);
			NovaZavada nz = m_CachedZavada[id] as NovaZavada;
			bool cached = true;

			if (nz == null || nz.VIN != vin)
			{
				nz = new NovaZavada(vin, user);
				cached = false;
			}
			
			nz.m_User = user;
			nz.m_CacheId = id;

			if (!cached)
				try { nz.LoadFromCache(); }
				catch { }

			m_CachedZavada[id] = nz;

			return nz;
		}
		public static NovaZavada Create(YukiProtocol protocol)
		{
			if (protocol.Zavady.SelectedIndex != -1)
				return Create(protocol.VIN, protocol.User, "editcode");
			return Create(protocol.VIN, protocol.User, "code");
		}
		public static NovaZavada Create(YukiProtocol protocol, int idx)
		{
			NovaZavada nz = Create(protocol.VIN, protocol.User, "editcode");

			YukiProtocolZavada z = protocol.Zavady[idx];

			nz.m_SavedIndex = idx;

			nz.Kod = z.Kod;
			nz.Vyrobce = z.Vyrobce;
			nz.Popis = z.Popis;
			nz.Nazev = z.Nazev;

			nz.Nd.Clear();
			nz.Pozice.Clear();

			for (int i = 0; i < z.Nd.Count; i++)
			{
				nz.Nd.Add(z.Nd[i].Id, z.Nd[i].Mnozstvi, z.Nd[i].Objednat);
			}

			for (int i = 0; i < z.Pozice.Count; i++)
			{
				if (z.Pozice[i].Id == -1)
					nz.Pozice.Add(z.Pozice[i].Nazev, z.Pozice[i].Operace, z.Pozice[i].Cj);
				else
					nz.Pozice.Add(z.Pozice[i].Id);
			}

			return nz;
		}
		#endregion

	}

/*
	public class YukiProtocolPdfRenderer
	{
		private YukiProtocol m_Protocol;

		public YukiProtocolPdfRenderer(YukiProtocol protocol)
		{
			m_Protocol = protocol;
		}

		protected YukiProtocol Protocol
		{
			get { return m_Protocol; }
		}

		protected GaranceUser User
		{
			get { return m_Protocol.User; }
		}

		private static Font m_PdfFont9 = FontFactory.GetFont(FontFactory.TIMES_ROMAN, "cp1250", true, 10.0F);
		private static Font m_PdfFont9B = FontFactory.GetFont(FontFactory.TIMES_ROMAN, "cp1250", true, 10.0F, Font.BOLD);
		private static Font m_PdfFont11B = FontFactory.GetFont(FontFactory.TIMES_ROMAN, "cp1250", true, 12.0F, Font.BOLD);
		private static Font m_PdfFont13 = FontFactory.GetFont(FontFactory.TIMES_ROMAN, "cp1250", true, 14.0F);
		private static Font m_PdfFont13B = FontFactory.GetFont(FontFactory.TIMES_ROMAN, "cp1250", true, 14.0F, Font.BOLD);

		private Chunk GetBaseChunk(string format, params object[] args)
		{
			return new Chunk(string.Format(format, args), m_PdfFont9);
		}

		private void AddPropertyValue(PdfPTable tbl, string ID, string value, float paddingBottom, bool useLangId)
		{
			PdfPCell cellProperty = new PdfPCell(GetBaseChunk("{0}:", (useLangId ? User.Language[ID] : ID)));
			cellProperty.Padding = 2;
			cellProperty.PaddingLeft = 10;
			if (paddingBottom > 0)
				cellProperty.PaddingBottom = paddingBottom;
			else if (paddingBottom < 0)
				cellProperty.PaddingTop = -paddingBottom;
			cellProperty.BorderWidth = 0;
			cellProperty.Border = Rectangle.NO_BORDER;
			tbl.AddCell(cellProperty);

			PdfPCell cellValue = new PdfPCell(GetBaseChunk(value));
			cellValue.Padding = 2;
			cellValue.PaddingRight = 10;
			if (paddingBottom > 0)
				cellValue.PaddingBottom = paddingBottom;
			else if (paddingBottom < 0)
				cellValue.PaddingTop = -paddingBottom;
			cellValue.BorderWidth = 0;
			cellValue.Border = Rectangle.NO_BORDER;
			tbl.AddCell(cellValue);
		}

		private void AddPropertyValue(PdfPTable tbl, string ID, string value)
		{
			AddPropertyValue(tbl, ID, value, 2, true);
		}

		private PdfPTable GetTableUserInfo()
		{
			PdfPTable tbl = new PdfPTable(2);
			tbl.WidthPercentage = 100;
			tbl.SetWidths(new float[2] { 20, 80 });

			// majitel
			PdfPCell cellMajitel = new PdfPCell(GetBaseChunk("{0}:", User.Language["PROTO_GROUP_OWNER"]));
			cellMajitel.Colspan = 2;
			cellMajitel.PaddingTop = 2;
			cellMajitel.PaddingLeft = 5;
			cellMajitel.PaddingRight = 10;
			cellMajitel.PaddingBottom = 0;
			cellMajitel.BorderWidth = 0;
			tbl.AddCell(cellMajitel);

			// adresa
			PdfPCell cellAdresa = new PdfPCell(new Chunk(string.Format("{0}\n{1}\n{2}        {3}", Protocol.Jmeno, Protocol.Adresa, Protocol.PSC, Protocol.Obec), m_PdfFont11B));
			cellAdresa.Colspan = 2;
			cellAdresa.Padding = 10;
			cellAdresa.PaddingTop = 2;
			cellAdresa.Leading = 18;
			cellAdresa.BorderWidth = 0;
			tbl.AddCell(cellAdresa);

			// telefon
			AddPropertyValue(tbl, "PROTO_PHONE", Protocol.Telefon, 10, true);

			return tbl;
		}

		private PdfPTable GetTableBikeInfo()
		{
			PdfPTable tbl = new PdfPTable(2);
			tbl.WidthPercentage = 100;
			tbl.SetWidths(new float[2] { 35, 65 });

			// motocykl
			PdfPCell cellMotocykl = new PdfPCell(GetBaseChunk("{0}:", User.Language["PROTO_BIKE"]));
			cellMotocykl.Colspan = 2;
			cellMotocykl.PaddingTop = 2;
			cellMotocykl.PaddingLeft = 5;
			cellMotocykl.PaddingRight = 10;
			cellMotocykl.PaddingBottom = 5;
			cellMotocykl.BorderWidth = 0;
			tbl.AddCell(cellMotocykl);

			AddPropertyValue(tbl, "VIN", Protocol.VIN, 2, false);
			AddPropertyValue(tbl, "BIKES_LIST_MODEL", Protocol.Model);
			AddPropertyValue(tbl, "BIKES_STATE_SOLD", Protocol.DateSold.ToString("d'. 'M'. 'yyyy"), 10, true);

			// protokol
			PdfPCell cellProtokol = new PdfPCell(GetBaseChunk("{0}:", User.Language["HEADER_PROTOCOL"]));
			cellProtokol.Colspan = 2;
			cellProtokol.PaddingTop = 2;
			cellProtokol.PaddingLeft = 5;
			cellProtokol.PaddingRight = 10;
			cellProtokol.PaddingBottom = 5;
			cellProtokol.BorderWidth = 0;
			tbl.AddCell(cellProtokol);

			AddPropertyValue(tbl, "PROTO_LIST_DATE_CREATED", Protocol.DateCreated.ToString("d'. 'M'. 'yyyy"));
			AddPropertyValue(tbl, "PROTO_LIST_DATE", Protocol.DateRepair.ToString("d'. 'M'. 'yyyy"));
			AddPropertyValue(tbl, "PROTO_KM", Protocol.Km.ToString());

			AddPropertyValue(tbl, "PROTO_KO", Protocol.KOTitle, -10, true);
			AddPropertyValue(tbl, "PROTO_GT", Protocol.DZTitle, 10, true);

			return tbl;
		}

		private PdfPTable GetTableResitel()
		{
			PdfPTable tbl = new PdfPTable(2);
			tbl.WidthPercentage = 100;
			tbl.SetWidths(new float[2] { 30, 70 });

			AddPropertyValue(tbl, "PROTO_WORKER", Protocol.Resitel, -10, true);
			AddPropertyValue(tbl, "PROTO_PHONE", Protocol.ResitelTel);
			AddPropertyValue(tbl, "PROTO_FAX", Protocol.ResitelFax, 10, true);

			return tbl;
		}

		private void RenderHeader(Document doc)
		{
			PdfPTable tbl = new PdfPTable(2);
			tbl.DefaultCell.Border = Rectangle.BOTTOM_BORDER;
			tbl.DefaultCell.BorderWidth = 0.5F;
			tbl.DefaultCell.Padding = 0;
			tbl.DefaultCell.PaddingBottom = 2;
			tbl.WidthPercentage = 100;

			tbl.SetWidths(new float[2] { 80, 20 });

			PdfPCell cf = new PdfPCell(new Phrase(User.Language["INVOICE_ORDER_PROTO"].Replace("%s", Protocol.Cislo), m_PdfFont13B));
			cf.Padding = 0;
			cf.PaddingBottom = 5;
			cf.Border = Rectangle.BOTTOM_BORDER;
			cf.VerticalAlignment = Rectangle.ALIGN_BOTTOM;
			cf.BorderWidth = 0.5F;
			tbl.AddCell(cf);

			cf = new PdfPCell(new Phrase(Protocol.DateCreated.ToString("d'. 'M'. 'yyyy"), m_PdfFont9B));
			cf.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
			cf.VerticalAlignment = Rectangle.ALIGN_BOTTOM;
			cf.Padding = 0;
			cf.PaddingBottom = 2;
			cf.PaddingBottom = 5;
			cf.Border = Rectangle.BOTTOM_BORDER;
			cf.BorderWidth = 0.5F;
			tbl.AddCell(cf);

			doc.Add(tbl);
			doc.Add(new Paragraph(" ", m_PdfFont9));
		}

		private void RenderBasicInfo(Document doc)
		{
			PdfPTable tbl = new PdfPTable(2);
			tbl.WidthPercentage = 100;
			tbl.DefaultCell.Padding = 0;
			tbl.SetWidths(new float[2] { 45, 55 });

			PdfPTable tblUB = new PdfPTable(1);
			tblUB.WidthPercentage = 100;
			tblUB.AddCell(GetTableUserInfo());
			tblUB.AddCell(GetTableResitel());
			tbl.AddCell(tblUB);

			PdfPCell cf = new PdfPCell(GetTableBikeInfo());
			cf.Padding = 2;

			tbl.AddCell(cf);

			doc.Add(tbl);
		}

		private void RenderZavadaPozice(YukiProtocolZavada zavada, Document doc)
		{
			PdfPTable tblM = new PdfPTable(1);
			tblM.WidthPercentage = 100;
			PdfPCell cf = new PdfPCell(GetBaseChunk("{0}:", User.Language["PROTO_WORK_POS"]));
			cf.BorderWidth = 0;
			cf.Padding = 0;
			cf.PaddingTop = 5;
			cf.PaddingBottom = 3;
			tblM.AddCell(cf);

			PdfPTable tbl = new PdfPTable(6);
			tbl.DefaultCell.BorderWidth = 0;
			tbl.SetWidths(new float[6] { 10, 10, 37.5F, 25, 5, 12.5F });

			cf = new PdfPCell(GetBaseChunk(User.Language["PROTO_POS_BPART"]));
			cf.HorizontalAlignment = Rectangle.ALIGN_CENTER;
			cf.BorderWidth = 0;
			tbl.AddCell(cf);

			cf = new PdfPCell(GetBaseChunk(User.Language["PROTO_POS_POS"]));
			cf.HorizontalAlignment = Rectangle.ALIGN_CENTER;
			cf.BorderWidth = 0;
			tbl.AddCell(cf);

			tbl.AddCell(new Phrase(GetBaseChunk(User.Language["PROTO_POS_NAME"])));
			tbl.AddCell(new Phrase(GetBaseChunk(User.Language["PROTO_POS_OP"])));

			cf = new PdfPCell(GetBaseChunk(User.Language["PROTO_POS_TIME"]));
			cf.HorizontalAlignment = Rectangle.ALIGN_CENTER;
			cf.BorderWidth = 0;
			tbl.AddCell(cf);

			cf = new PdfPCell(GetBaseChunk(User.Language["PARTS_PRICE"]));
			cf.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
			cf.BorderWidth = 0;
			tbl.AddCell(cf);

			for (int i = 0; i < zavada.Pozice.Count; i++)
			{
				cf = new PdfPCell(GetBaseChunk(zavada.Pozice[i].Kod));
				cf.HorizontalAlignment = Rectangle.ALIGN_CENTER;
				cf.BorderWidth = 0;
				tbl.AddCell(cf);

				cf = new PdfPCell(GetBaseChunk(zavada.Pozice[i].KodPozice));
				cf.HorizontalAlignment = Rectangle.ALIGN_CENTER;
				cf.BorderWidth = 0;
				tbl.AddCell(cf);

				tbl.AddCell(new Phrase(GetBaseChunk(zavada.Pozice[i].Nazev)));
				tbl.AddCell(new Phrase(GetBaseChunk(zavada.Pozice[i].Operace)));

				cf = new PdfPCell(GetBaseChunk(zavada.Pozice[i].Cj.ToString()));
				cf.HorizontalAlignment = Rectangle.ALIGN_CENTER;
				cf.BorderWidth = 0;
				tbl.AddCell(cf);

				cf = new PdfPCell(GetBaseChunk(User.FormatPrice(zavada.Pozice[i].CjCenaDPH)));
				cf.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
				cf.BorderWidth = 0;
				tbl.AddCell(cf);
			}

			cf = new PdfPCell(tbl);
			cf.Padding = 1;
			cf.Border = Rectangle.BOX;
			cf.BorderWidth = 0.5F;

			tblM.AddCell(cf);

			doc.Add(tblM);
		}

		private void RenderZavadaND(YukiProtocolZavada zavada, Document doc)
		{
			PdfPTable tblM = new PdfPTable(1);
			tblM.WidthPercentage = 100;
			PdfPCell cf = new PdfPCell(GetBaseChunk("{0}:", User.Language["PROTO_WORK_ND"]));
			cf.BorderWidth = 0;
			cf.Padding = 0;
			cf.PaddingTop = 5;
			cf.PaddingBottom = 3;
			tblM.AddCell(cf);

			PdfPTable tbl = new PdfPTable(6);
			tbl.DefaultCell.BorderWidth = 0;
			tbl.SetWidths(new float[6] { 10, 10, 57.5F, 5, 5, 12.5F });

			cf = new PdfPCell(GetBaseChunk(User.Language["PARTS_GROUP"]));
			cf.HorizontalAlignment = Rectangle.ALIGN_CENTER;
			cf.BorderWidth = 0;
			tbl.AddCell(cf);

			cf = new PdfPCell(GetBaseChunk(User.Language["PROTO_POS_POS"]));
			cf.HorizontalAlignment = Rectangle.ALIGN_CENTER;
			cf.BorderWidth = 0;
			tbl.AddCell(cf);

			tbl.AddCell(new Phrase(GetBaseChunk(User.Language["PROTO_POS_NAME"])));

			cf = new PdfPCell(GetBaseChunk(User.Language["PROTO_QTY"]));
			cf.HorizontalAlignment = Rectangle.ALIGN_CENTER;
			cf.BorderWidth = 0;
			tbl.AddCell(cf);

			cf = new PdfPCell(GetBaseChunk(User.Language["PROTO_ORDER"]));
			cf.HorizontalAlignment = Rectangle.ALIGN_CENTER;
			cf.BorderWidth = 0;
			tbl.AddCell(cf);

			cf = new PdfPCell(GetBaseChunk(User.Language["PARTS_PRICE"]));
			cf.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
			cf.BorderWidth = 0;
			tbl.AddCell(cf);

			for (int i = 0; i < zavada.Nd.Count; i++)
			{
				cf = new PdfPCell(GetBaseChunk(zavada.Nd[i].KodSkupina));
				cf.HorizontalAlignment = Rectangle.ALIGN_CENTER;
				cf.BorderWidth = 0;
				tbl.AddCell(cf);

				cf = new PdfPCell(GetBaseChunk(zavada.Nd[i].KodPozice));
				cf.HorizontalAlignment = Rectangle.ALIGN_CENTER;
				cf.BorderWidth = 0;
				tbl.AddCell(cf);

				tbl.AddCell(new Phrase(GetBaseChunk(zavada.Nd[i].Nazev)));

				cf = new PdfPCell(GetBaseChunk(zavada.Nd[i].Mnozstvi.ToString()));
				cf.HorizontalAlignment = Rectangle.ALIGN_CENTER;
				cf.BorderWidth = 0;
				tbl.AddCell(cf);

				cf = new PdfPCell(GetBaseChunk(zavada.Nd[i].Objednat.ToString()));
				cf.HorizontalAlignment = Rectangle.ALIGN_CENTER;
				cf.BorderWidth = 0;
				tbl.AddCell(cf);

				cf = new PdfPCell(GetBaseChunk(User.FormatPrice(zavada.Nd[i].CenaDPH)));
				cf.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
				cf.BorderWidth = 0;
				tbl.AddCell(cf);
			}

			cf = new PdfPCell(tbl);
			cf.Padding = 1;
			cf.Border = Rectangle.BOX;
			cf.BorderWidth = 0.5F;

			tblM.AddCell(cf);

			doc.Add(tblM);
		}

		private void RenderZavada(YukiProtocolZavada zavada, int idx, Document doc)
		{
			PdfPTable tblM = new PdfPTable(1);
			tblM.WidthPercentage = 100;

			PdfPCell cf = new PdfPCell(new Chunk(string.Format("{0}: {1}", User.Language["PROTO_ISSUE_NO"].Replace("%d", idx.ToString()), zavada.Nazev), m_PdfFont11B));
			cf.Padding = 0;
			cf.PaddingBottom = 4;
			cf.PaddingTop = 16;
			cf.BorderWidth = 0;
			cf.Border = Rectangle.BOTTOM_BORDER;
			cf.BorderWidthBottom = 1;
			tblM.AddCell(cf);

			PdfPTable tbl = new PdfPTable(6);
			tbl.DefaultCell.BorderWidth = 0;
			tbl.WidthPercentage = 100;
			tbl.SetWidths(new float[6] { 10, 7.5F, 7.5F, 50, 12.5F, 12.5F });

			tbl.AddCell(new Phrase(GetBaseChunk(User.Language["PROTO_ISSUE_CODE"])));
			tbl.AddCell(new Phrase(GetBaseChunk(User.Language["PROTO_ISSUE_DZ"])));
			tbl.AddCell(new Phrase(GetBaseChunk(User.Language["PROTO_ISSUE_MAN"])));
			tbl.AddCell(new Phrase(GetBaseChunk(User.Language["PROTO_ISSUE_TEXT"])));

			cf = new PdfPCell(GetBaseChunk(User.Language["PROTO_ISSUE_WORK"]));
			cf.BorderWidth = 0;
			cf.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
			tbl.AddCell(cf);

			cf = new PdfPCell(GetBaseChunk(User.Language["PROTO_ISSUE_MAT"]));
			cf.BorderWidth = 0;
			cf.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
			tbl.AddCell(cf);

			tbl.AddCell(new Phrase(GetBaseChunk(zavada.Kod)));
			tbl.AddCell(new Phrase(GetBaseChunk(zavada.DZ)));
			tbl.AddCell(new Phrase(GetBaseChunk(zavada.Vyrobce)));
			tbl.AddCell(new Phrase(GetBaseChunk(zavada.Popis)));

			cf = new PdfPCell(GetBaseChunk(User.FormatPrice(zavada.CelkemCjCenaDPH)));
			cf.BorderWidth = 0;
			cf.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
			tbl.AddCell(cf);

			cf = new PdfPCell(GetBaseChunk(User.FormatPrice(zavada.CelkemCenaDPH)));
			cf.BorderWidth = 0;
			cf.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
			tbl.AddCell(cf);

			cf = new PdfPCell(tbl);
			cf.Padding = 1;
			cf.Border = Rectangle.BOX;
			cf.BorderWidth = 0.5F;

			tblM.AddCell(cf);

			doc.Add(tblM);

			RenderZavadaPozice(zavada, doc);
			if (zavada.Nd.Count > 0)
				RenderZavadaND(zavada, doc);
		}

		public void Render(Document doc)
		{
			RenderHeader(doc);
			RenderBasicInfo(doc);

			for (int i = 0; i < Protocol.Zavady.Count; i++)
			{
				RenderZavada(Protocol.Zavady[i], i+1, doc);
			}
		}
	}
 */


	public class YukiProtocolPdfRenderer
	{
		private YukiProtocol m_Protocol;

		public YukiProtocolPdfRenderer(YukiProtocol protocol)
		{
			m_Protocol = protocol;
		}

		protected YukiProtocol Protocol
		{
			get { return m_Protocol; }
		}

		protected GaranceUser User
		{
			get { return m_Protocol.User; }
		}

		private static Font m_PdfFont9 = FontFactory.GetFont(FontFactory.TIMES_ROMAN, "cp1250", true, 10.0F);
		private static Font m_PdfFont9B = FontFactory.GetFont(FontFactory.TIMES_ROMAN, "cp1250", true, 10.0F, Font.BOLD);
		private static Font m_PdfFont11B = FontFactory.GetFont(FontFactory.TIMES_ROMAN, "cp1250", true, 12.0F, Font.BOLD);
		private static Font m_PdfFont13 = FontFactory.GetFont(FontFactory.TIMES_ROMAN, "cp1250", true, 14.0F);
		private static Font m_PdfFont13B = FontFactory.GetFont(FontFactory.TIMES_ROMAN, "cp1250", true, 14.0F, Font.BOLD);
		private static Font m_PdfFont16 = FontFactory.GetFont(FontFactory.TIMES_ROMAN, "cp1250", true, 16.0F);
		private static Font m_PdfFont16B = FontFactory.GetFont(FontFactory.TIMES_ROMAN, "cp1250", true, 16.0F, Font.BOLD);

		private void RenderHeader(Document doc)
		{
			PdfPTable tbl = new PdfPTable(2);
			tbl.WidthPercentage = 100;

			tbl.SetWidths(new float[2] { 50, 50 });

			PdfPCell cf = new PdfPCell(new Phrase(User.Language["INVOICE_ORDER_PROTO2"].Replace("%s", Protocol.Cislo), m_PdfFont16B));
			cf.Padding = 0;
			cf.PaddingBottom = 15;
			cf.Border = Rectangle.NO_BORDER;
			cf.VerticalAlignment = Rectangle.ALIGN_BOTTOM;
			tbl.AddCell(cf);

			cf = new PdfPCell(new Phrase(User.Language["INVOICE_ORDER_DEALER2"].Replace("%s", User.Code), m_PdfFont16B));
			cf.Padding = 0;
			cf.PaddingBottom = 15;
			cf.Border = Rectangle.NO_BORDER;
			cf.VerticalAlignment = Rectangle.ALIGN_BOTTOM;
			cf.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
			tbl.AddCell(cf);

			doc.Add(tbl);

			string pHeaderData = string.Format(
				"{0}: {1}\n{2}: {3}      VIN: {4}      {5}: {6}\n{7}: {8}      {9}: {10}",
				User.Language["INVOICE_PROTO_CLIENT2"], string.Format("{0}, {1}, {2} {3}", Protocol.Jmeno, Protocol.Adresa, Protocol.PSC, Protocol.Obec),
				User.Language["PROTO_BIKE"], Protocol.ModelNazev, Protocol.VIN,
				User.Language["BIKES_LIST_MODEL"], Protocol.ModelId,
				User.Language["PROTO_LIST_DATE"], Protocol.DateRepair.ToString("d'. 'M'. 'yyyy"),
				User.Language["PROTO_KM"], Protocol.Km
			);

			tbl = new PdfPTable(1);
			tbl.WidthPercentage = 100;

			cf = new PdfPCell(new Phrase(pHeaderData, m_PdfFont9B));
			cf.Padding = 0;
			cf.PaddingBottom = 10;
			cf.Border = Rectangle.BOTTOM_BORDER;
			cf.BorderWidth = 0.5F;
			cf.Leading = 13.5F;
			tbl.AddCell(cf);

			doc.Add(tbl);
		}


		private void RenderZavada(YukiProtocolZavada zavada, int idx, Document doc)
		{
			PdfPTable tbl = new PdfPTable(1);
			tbl.WidthPercentage = 100;
			tbl.SetWidths(new float[] { 100 });

			Paragraph p = new Paragraph();
			p.ExtraParagraphSpace = 0;
			p.FirstLineIndent = 0;
			p.IndentationLeft = 2;
			p.IndentationRight = 2;
			p.SpacingAfter = 0;
			p.SpacingBefore = 0;

			p.Add(new Phrase(User.Language["PROTO_ISSUE_NO"].Replace("%d", idx.ToString()) + "\n", m_PdfFont13B));

			// kod
			p.Add(new Phrase(User.Language["PROTO_ISSUE_CODE"] + ": ", m_PdfFont11B));
			p.Add(new Phrase(string.Format("{0}-{1}            ", zavada.Kod, zavada.KodNazev), m_PdfFont9));
			// dz
			p.Add(new Phrase(User.Language["PROTO_ISSUE_DZ"] + ": " + zavada.DZ + "-", m_PdfFont11B));
			p.Add(new Phrase(zavada.DZTitle + "\n", m_PdfFont9));
			// zavada
			p.Add(new Phrase(User.Language["PROTO_ISSUE_TITLE"] + ": ", m_PdfFont11B));
			p.Add(new Phrase(zavada.Nazev + "\n", m_PdfFont9));
			// popis
			p.Add(new Phrase(User.Language["PROTO_ISSUE_TEXT"] + ": ", m_PdfFont11B));
			p.Add(new Phrase(zavada.Popis + "\n", m_PdfFont9));
			// práce
			p.Add(new Phrase(User.Language["PROTO_ISSUE_WORK"] + ": ", m_PdfFont11B));

			PdfPCell cf = new PdfPCell();
			cf.Padding = 0;
			cf.PaddingTop = 20;
			cf.Border = Rectangle.NO_BORDER;
			cf.AddElement(p);

			tbl.AddCell(cf);
			doc.Add(tbl);

			// pracovni pozice
			PdfPTable tblPP = new PdfPTable(new float[] { 70, 15, 15 });
			tblPP.WidthPercentage = 100;

			for (int i = 0; i < zavada.Pozice.Count; i++)
			{
				if (!zavada.Pozice[i].Schvaleno) continue;

				string ppTitle = string.Format("{0}{1} - {2}", zavada.Pozice[i].Kod, zavada.Pozice[i].KodPozice, zavada.Pozice[i].Operace);
				string ppCj = string.Format("{0}{1}", zavada.Pozice[i].Cj, User.Language["PROTO_POS_TIME"]);
				string ppCena = User.FormatPrice(zavada.Pozice[i].SavedCenaDPH, true);

				cf = new PdfPCell(new Phrase(ppTitle, m_PdfFont9));
				cf.Border = Rectangle.NO_BORDER;
				cf.Padding = 2;
				cf.PaddingLeft = 0;
				tblPP.AddCell(cf);

				cf = new PdfPCell(new Phrase(ppCj, m_PdfFont9));
				cf.Border = Rectangle.NO_BORDER;
				cf.Padding = 2;
				cf.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
				tblPP.AddCell(cf);

				cf = new PdfPCell(new Phrase(ppCena, m_PdfFont9));
				cf.Border = Rectangle.NO_BORDER;
				cf.Padding = 2;
				cf.PaddingRight = 0;
				cf.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
				tblPP.AddCell(cf);
			}

			tbl = new PdfPTable(1);
			tbl.WidthPercentage = 100;
			cf = new PdfPCell();
			cf.Border = Rectangle.BOTTOM_BORDER;
			cf.BorderWidth = 0.5F;
			cf.AddElement(tblPP);
			tbl.AddCell(cf);

			doc.Add(tbl);

			// material
			p = new Paragraph();
			p.ExtraParagraphSpace = 0;
			p.FirstLineIndent = 0;
			p.IndentationLeft = 2;
			p.IndentationRight = 3;
			p.SpacingAfter = 0;
			p.SpacingBefore = 0;
			p.Add(new Phrase(User.Language["INVOICE_MATERIAL"] + ": ", m_PdfFont11B));
			doc.Add(p);

			tblPP = new PdfPTable(new float[] { 80, 20 });
			tblPP.WidthPercentage = 100;

			for (int i = 0; i < zavada.Nd.Count; i++)
			{
				if (!zavada.Nd[i].Schvaleno) continue;

				string ndTitle = string.Format("{0} - {1}", zavada.Nd[i].SklCislo, zavada.Nd[i].Nazev);
				string ndPrice = User.FormatPrice(zavada.Nd[i].SavedCenaDPH, true);

				cf = new PdfPCell(new Phrase(ndTitle, m_PdfFont9));
				cf.Border = Rectangle.NO_BORDER;
				cf.Padding = 2;
				cf.PaddingLeft = 0;
				tblPP.AddCell(cf);

				cf = new PdfPCell(new Phrase(ndPrice, m_PdfFont9));
				cf.Border = Rectangle.NO_BORDER;
				cf.Padding = 2;
				cf.PaddingRight = 0;
				cf.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
				tblPP.AddCell(cf);
			}

			tbl = new PdfPTable(1);
			tbl.WidthPercentage = 100;
			cf = new PdfPCell();
			cf.Border = Rectangle.BOTTOM_BORDER;
			cf.BorderWidth = 0.5F;
			cf.AddElement(tblPP);
			tbl.AddCell(cf);

			doc.Add(tbl);

			// celkem
			tblPP = new PdfPTable(new float[] { 60, 40 });
			tblPP.HorizontalAlignment = Rectangle.ALIGN_LEFT;
			tblPP.WidthPercentage = 50;

			cf = new PdfPCell(new Phrase(User.Language["PROTO_WORK_TOTAL2"] + ":", m_PdfFont9));
			cf.Border = Rectangle.NO_BORDER;
			cf.Padding = 2;
			cf.PaddingLeft = 0;
			tblPP.AddCell(cf);

			cf = new PdfPCell(new Phrase(User.FormatPrice(zavada.CelkemCjSavedCenaDPH, true), m_PdfFont9));
			cf.Border = Rectangle.NO_BORDER;
			cf.Padding = 2;
			cf.PaddingRight = 0;
			cf.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
			tblPP.AddCell(cf);

			cf = new PdfPCell(new Phrase(User.Language["PROTO_MAT_TOTAL2"] + ":", m_PdfFont9));
			cf.Border = Rectangle.NO_BORDER;
			cf.Padding = 2;
			cf.PaddingLeft = 0;
			tblPP.AddCell(cf);

			cf = new PdfPCell(new Phrase(User.FormatPrice(zavada.CelkemSavedCenaDPH, true), m_PdfFont9));
			cf.Border = Rectangle.NO_BORDER;
			cf.Padding = 2;
			cf.PaddingRight = 0;
			cf.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
			tblPP.AddCell(cf);

			cf = new PdfPCell(new Phrase(User.Language["PROTO_ISS_TOTAL2"] + ":", m_PdfFont11B));
			cf.Border = Rectangle.NO_BORDER;
			cf.Padding = 2;
			cf.PaddingLeft = 0;
			tblPP.AddCell(cf);

			cf = new PdfPCell(new Phrase(User.FormatPrice(zavada.CelkemCjSavedCenaDPH + zavada.CelkemSavedCenaDPH, true), m_PdfFont11B));
			cf.Border = Rectangle.NO_BORDER;
			cf.Padding = 2;
			cf.PaddingRight = 0;
			cf.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
			tblPP.AddCell(cf);

			tbl = new PdfPTable(1);
			tbl.WidthPercentage = 100;
			cf = new PdfPCell();
			cf.Border = Rectangle.BOTTOM_BORDER;
			cf.BorderWidth = 0.5F;
			cf.AddElement(tblPP);
			tbl.AddCell(cf);

			doc.Add(tbl);
		}

		private void RenderTotal(Document doc)
		{
			Paragraph p = new Paragraph();
			p.ExtraParagraphSpace = 0;
			p.FirstLineIndent = 0;
			p.IndentationLeft = 2;
			p.IndentationRight = 2;
			p.SpacingAfter = 0;
			p.SpacingBefore = 0;			

			p.Add(new Phrase("\n"+User.Language["PROTO_TOTAL2"]+"\n", m_PdfFont9B));
			p.Add(new Phrase(User.Language["PROTO_WORK_TOTAL2"] + ": ", m_PdfFont9B));
			p.Add(new Phrase(User.FormatPrice(Protocol.CelkemCjSavedCenaDPH, true) + "      ", m_PdfFont9));
			p.Add(new Phrase(User.Language["PROTO_MAT_TOTAL2"] + ": ", m_PdfFont9));
			p.Add(new Phrase(User.FormatPrice(Protocol.CelkemSavedCenaDPH, true) + "\n", m_PdfFont9));
			p.Add(new Phrase(User.Language["PROTO_TOTALA2"] + ": ", m_PdfFont9B));
			p.Add(new Phrase(User.FormatPrice(Protocol.CelkemCjSavedCenaDPH + Protocol.CelkemSavedCenaDPH, true), m_PdfFont9));

			PdfPTable tbl = new PdfPTable(1);
			tbl.WidthPercentage = 100;

			PdfPCell cf = new PdfPCell();
			cf.Padding = 0;
			cf.Border = Rectangle.NO_BORDER;
			cf.AddElement(p);
			tbl.AddCell(cf);

			doc.Add(tbl);
		}


		public void Render(Document doc)
		{
			RenderHeader(doc);

			for (int i = 0; i < Protocol.Zavady.Count; i++)
				if (Protocol.Zavady[i].Schvaleno)
					RenderZavada(Protocol.Zavady[i], i + 1, doc);

			RenderTotal(doc);
		}

	}
}

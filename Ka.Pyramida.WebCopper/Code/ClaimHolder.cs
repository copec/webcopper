﻿using System;
using System.Collections;
using System.Text;
using Ka.Garance.Data;
using Ka.Garance.User;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Ka.Garance.Invoice.Yuki;

namespace Ka.Garance.Claim.Yuki
{

	public sealed class ListDil
	{
		private string m_Protokol;
		private string m_Model;
		private string m_SklCislo;
		private string m_Nazev;
		private int m_Pocet;
		private string m_Poznamka;


		public ListDil(MySQLDataReader dr)
		{
			m_Protokol = Convert.ToString(dr.GetValue("protokol"));
			m_Model = Convert.ToString(dr.GetValue("model"));
			m_SklCislo = Convert.ToString(dr.GetValue("sklcislo"));
			m_Nazev = Convert.ToString(dr.GetValue("nazev"));
			m_Pocet = Convert.ToInt32(dr.GetValue("mnozstvi"));
			m_Poznamka = Convert.ToString(dr.GetValue("poznamka"));
		}

		public string Protokol
		{
			get { return m_Protokol; }
		}
		public string Model
		{
			get { return m_Model; }
		}
		public string SklCislo
		{
			get { return m_SklCislo; }
		}
		public string Nazev
		{
			get { return m_Nazev; }
		}
		public int Pocet
		{
			get { return m_Pocet; }
		}
		public string Poznamka
		{
			get { return m_Poznamka; }
		}

	}

	public sealed class Listy : IEnumerable
	{
		private ArrayList m_Items = new ArrayList();
		private GaranceUser m_User;

		private int m_Id;
		private string m_Nazev;
		private DateTime m_Datum;

		public Listy(GaranceUser user, int id)
		{
			m_User = user;

			Load(id);
		}

		public int Id
		{
			get { return m_Id; }
		}
		public string Nazev
		{
			get { return m_Nazev; }
		}
		public DateTime Datum
		{
			get { return m_Datum; }
		}
		public int Count
		{
			get { return m_Items.Count; }
		}
		public ListDil this[int index]
		{
			get { return m_Items[index] as ListDil; }
		}

		public void Load(int id)
		{
			m_Id = id;

			MySQLDataReader dr = m_User.Database.ExecuteReader("SELECT nazev,datum FROM tlisty WHERE iddealer={0} AND id={1}", m_User.Id, m_Id);
			if (!dr.Read())
				throw new Exception("Could not load letter details");

			m_Nazev = Convert.ToString(dr.GetValue("nazev"));
			m_Datum = Convert.ToDateTime(dr.GetValue("datum"));

			m_Items.Clear();

			dr = m_User.Database.ExecuteReader(
				string.Format(@"
					SELECT
						A.id AS id,
						A.cisloprotokol AS protokol,
						C.typmoto AS model,
						C.sklcislo AS sklcislo,
						C.nazevdilu{0} AS nazev,
						IFNULL(B.mnozstvi,0) AS mnozstvi,
						A.poznamka AS poznamka
					FROM tlistydil A
						LEFT JOIN tzdil B ON B.id=A.iddil
						LEFT JOIN tcskodnicisla C ON B.iddil=C.sklcislo
					WHERE idlist={{0}} ORDER BY A.id", (m_User.Language.UseEnglishNames ? "_en" : "")), m_Id);
			while (dr.Read())
			{
				m_Items.Add(new ListDil(dr));
			}
		}

		public IEnumerator GetEnumerator()
		{
			return m_Items.GetEnumerator();
		}

		private static Font m_PdfFont9 = FontFactory.GetFont(FontFactory.TIMES_ROMAN, "cp1250", true, 10.0F);
		private static Font m_PdfFont9B = FontFactory.GetFont(FontFactory.TIMES_ROMAN, "cp1250", true, 10.0F, Font.BOLD);
		private static Font m_PdfFont13B = FontFactory.GetFont(FontFactory.TIMES_ROMAN, "cp1250", true, 14.0F, Font.BOLD);

		private PdfPCell GetItemHeader(string title)
		{
			PdfPCell cellHeader = new PdfPCell(new Chunk(title, m_PdfFont9B));
			cellHeader.Border = Rectangle.BOTTOM_BORDER;
			cellHeader.Padding = 0;
			cellHeader.PaddingBottom = 4;
			cellHeader.BorderWidth = 0.5F;

			return cellHeader;
		}

		private PdfPCell GetItemContent(string data)
		{
			PdfPCell cf = new PdfPCell(new Chunk(data, m_PdfFont9));
			cf.Border = Rectangle.NO_BORDER;
			cf.Padding = 0;
			cf.PaddingTop = 4;

			return cf;
		}

		private PdfPTable GetTableItems(int idx, int cnt)
		{
			PdfPTable tbl = new PdfPTable(6);
			tbl.WidthPercentage = 100;
			tbl.SetWidths(new float[6] { 7.5F, 7.5F, 10, 25, 5, 30 });

			tbl.AddCell(GetItemHeader(m_User.Language["HEADER_PROTOCOL"]));
			tbl.AddCell(GetItemHeader(m_User.Language["BIKES_LIST_MODEL"]));
			tbl.AddCell(GetItemHeader(m_User.Language["CLAIM_LIST_STOCK"]));
			tbl.AddCell(GetItemHeader(m_User.Language["CLAIM_LIST_TITLE"]));
			tbl.AddCell(GetItemHeader(m_User.Language["CLAIM_LIST_COUNT"]));
			tbl.AddCell(GetItemHeader(m_User.Language["CLAIM_LIST_NOTE"]));

			for (int i = idx; i < Math.Min(Count, cnt); i++)
			{
				tbl.AddCell(GetItemContent(this[i].Protokol));
				tbl.AddCell(GetItemContent(this[i].Model));
				tbl.AddCell(GetItemContent(this[i].SklCislo));
				tbl.AddCell(GetItemContent(this[i].Nazev));
				tbl.AddCell(GetItemContent(this[i].Pocet.ToString()));
				tbl.AddCell(GetItemContent(this[i].Poznamka));
			}

			return tbl;
		}

		private void RenderHeader(Document document)
		{
			PdfPTable tbl = new PdfPTable(2);
			tbl.DefaultCell.Border = Rectangle.BOTTOM_BORDER;
			tbl.DefaultCell.BorderWidth = 0.5F;
			tbl.DefaultCell.Padding = 0;
			tbl.DefaultCell.PaddingBottom = 2;
			tbl.DefaultCell.VerticalAlignment = Rectangle.ALIGN_BOTTOM;
			tbl.WidthPercentage = 100;

			tbl.SetWidths(new float[2] { 80, 20 });

			PdfPCell cf = new PdfPCell(new Phrase(m_User.Language["CLAIM_PDF_TITLE"], m_PdfFont13B));
			cf.Padding = 0;
			cf.PaddingBottom = 5;
			cf.Border = Rectangle.BOTTOM_BORDER;
			cf.VerticalAlignment = Rectangle.ALIGN_BOTTOM;
			cf.BorderWidth = 0.5F;
			tbl.AddCell(cf);

			cf = new PdfPCell(new Phrase(this.Datum.ToString("d'. 'M'. 'yyyy"), m_PdfFont9B));
			cf.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
			cf.VerticalAlignment = Rectangle.ALIGN_BOTTOM;
			cf.Padding = 0;
			cf.PaddingBottom = 2;
			cf.PaddingBottom = 5;
			cf.Border = Rectangle.BOTTOM_BORDER;
			cf.BorderWidth = 0.5F;
			tbl.AddCell(cf);

			document.Add(tbl);
			document.Add(new Paragraph(" ", m_PdfFont9));
		}

		private void RenderItems(Document document)
		{
			int ITEMS_FIRST_PAGE = 25;
			int ITEMS_NEXT_PAGE = 30;

			int idx = 0;
			while (idx < Count)
			{
				int nItems = (idx == 0 ? ITEMS_FIRST_PAGE : ITEMS_NEXT_PAGE);

				document.Add(GetTableItems(idx, nItems));

				idx += nItems;
				if (idx < Count) document.NewPage();
			}
		}

		public void Render(Document document)
		{
			RenderHeader(document);
			RenderItems(document);
		}

	}

}



﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="_Settings" Codebehind="settings.aspx.cs" %>
<%@ Register TagPrefix="yuki" Namespace="Ka.Garance.Web.TabList" Assembly="garance" %>
<%@ Register TagPrefix="yuki" TagName="header" Src="Controls/Header.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Language.Code%>" dir="ltr" lang="<%=Language.Code%>">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><%=Language["APP_TITLE"]%></title>
<link rel="stylesheet" type="text/css" href="css/yuki.css" />
<link rel="stylesheet" type="text/css" href="css/combo.css" />
</head>

<body>

<yuki:header Id="MCHeader" LocationID="LOCATION_SETTINGS" runat="server" />

<div align="center" style="padding:0px 16px 16px 16px"> 
<table border="0" cellpadding="0" cellspacing="0" align="center" style="width:1%">
<tr>
 <td align="left">
 
<div style="width:600px"> 

<img src="images/mod.settings.png" alt="" style="vertical-align:text-bottom" />
<span class="mod"><%=Language["HEADER_SETTINGS_USER"]%></span><br /><br />

<table border="0" cellpadding="4" cellspacing="1" class="list">
<tr class="header">
 <th>
<table border="0" cellpadding="0" cellspacing="0" style="width:100%">
<tr>
 <td align="left" style="color:#fff"><%=Uzivatel.Nazev%>, <%=Uzivatel.Stat%></td>
 <td align="right" style="color:#fff"><%=Uzivatel.Kod%></td>
</tr>
</table> 
 </th>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
<br />

<% if (FormSaved) { %>

<table border="0" cellpadding="4" cellspacing="1" class="list">
<tr class="category">
 <td><%=Language["SETTINGS_SAVED"]%></td>
</tr>
<tr class="odd">
 <td align="center">
  <div style="padding:32px 8px 32px 8px"><%=MsgSettingsSaved%></div>
 </td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
<br />

<table border="0" cellpadding="2" cellspacing="1" class="list">
<tr class="footer">
 <td align="left">
<table border="0" cellpadding="0" cellspacing="0" style="width:100%"> 
<tr>
 <td>
  <button type="button" class="main" onclick="location='settings.aspx';">
   <img src="images/icon.rarr.png" alt="" />
   <%=Language["SETTINGS_BUTTON_CONTINUE"]%>
  </button>
 </td>
 <td align="right">
  <button type="button" class="main" onclick="location='index.aspx';">
   <img src="images/cancel.png" />
   <%=Language["BUTTON_CLOSE"]%>
  </button> 
 </td>
</tr> 
</table>
 </td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>

<% } else { %>

<%=GenerateErrorHTML()%>

<yuki:TabList ID="MCSettingsTabs" runat="server">
 <yuki:TabItem ID="TabItem1" LangID="SETTINGS_PERSONAL" Link="settings.aspx?state=0" Selected="true" runat="server" />
 <yuki:TabItem ID="TabItem2" LangID="SETTINGS_PASSWORD" Link="settings.aspx?state=1" runat="server" />
 <yuki:TabItem ID="TabItem3" LangID="SETTINGS_INVOICE" Link="settings.aspx?state=2" runat="server" />
 <yuki:TabItem ID="TabItem4" LangID="SETTINGS_SYSTEM" Link="settings.aspx?state=3" runat="server" />
</yuki:TabList>
<form action="settings.aspx" method="post">
<% if (ZmenaOsobnichUdaju) { %>
<table border="0" cellpadding="4" cellspacing="1" class="list">
<tr class="category">
 <td colspan="2" align="left"><%=Language["SETTINGS_ADDRESS"]%></td>
</tr>
<tr class="even">
 <td width="15%"><label for="c101"><%=Language["SETTINGS_STREET"]%></label></td>
 <td class="odd"><input id="c101" type="text" name="pstreet" value="<%=GetFormValue("pstreet", Uzivatel.Ulice)%>" maxlength="128" class="sw-input" style="width:98.5%" /></td>
</tr>
<tr class="even">
 <td width="15%"><label for="c102"><%=Language["SETTINGS_CITY"]%></label></td>
 <td class="odd"><input id="c102" type="text" name="pcity" value="<%=GetFormValue("pcity", Uzivatel.Mesto)%>" maxlength="64" size="32" class="sw-input" /></td>
</tr>
<tr class="even">
 <td width="15%"><label for="c103"><%=Language["SETTINGS_ZIPCODE"]%></label></td>
 <td class="odd"><input id="c103" type="text" name="pzip" value="<%=GetFormValue("pzip", Uzivatel.PSC)%>" maxlength="16" size="5" class="sw-input" /></td>
</tr>
<tr class="category">
 <td colspan="2" align="left"><%=Language["SETTINGS_CONTACT"]%></td>
</tr>
<tr class="even">
 <td width="15%"><label for="c104"><%=Language["SETTINGS_TELEPHONE"]%></label></td>
 <td class="odd"><input id="c104" type="text" name="ptel" value="<%=GetFormValue("ptel", Uzivatel.Tel)%>" maxlength="64" size="32" class="sw-input" /></td>
</tr>
<tr class="even">
 <td width="15%"><label for="c105"><%=Language["SETTINGS_FAX"]%></label></td>
 <td class="odd"><input id="c105" type="text" name="pfax" value="<%=GetFormValue("pfax", Uzivatel.Fax)%>" maxlength="64" size="32" class="sw-input" /></td>
</tr>
<tr class="even">
 <td width="15%"><label for="c106"><%=Language["SETTINGS_MOBILE"]%></label></td>
 <td class="odd"><input id="c106" type="text" name="pmobile" value="<%=GetFormValue("pmobile", Uzivatel.Mobil)%>" maxlength="64" size="32" class="sw-input" /></td>
</tr>
<tr class="even">
 <td width="15%"><label for="c107"><%=Language["SETTINGS_EMAIL"]%></label></td>
 <td class="odd"><input id="c107" type="text" name="pemail" value="<%=GetFormValue("pemail", Uzivatel.Email)%>" maxlength="128" size="64" class="sw-input" /></td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
<br />

<table border="0" cellpadding="2" cellspacing="1" class="list">
<tr class="footer">
 <td>
<table border="0" cellpadding="0" cellspacing="0" style="width:100%"> 
<tr>
 <td align="left">
  <button type="submit" class="main">
   <img src="images/save.png" alt="" />
   <%=Language["SETTINGS_BUTTON_SAVE"]%>
  </button>
 </td>
 <td align="right">
  <button type="button" class="main" onclick="location='index.aspx';">
   <img src="images/cancel.png" alt="" />
   <%=Language["BUTTON_CANCEL"]%>
  </button>
 </td> 
</tr>
</table>
 </td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>

<% } else if (ZmenaHesla) { %>
<table border="0" cellpadding="4" cellspacing="1" class="list">
<tr class="category"><td colspan="2"><%=Language["SETTINGS_PASSWORD"]%></td></tr>
<tr class="even">
 <td width="15%"><label for="c108"><%=Language["SETTINGS_NEWPASS"]%></label></td>
 <td class="odd"><input id="c108" type="password" name="pnpass" size="16" maxlength="64" class="sw-input" /></td>
</tr>
<tr class="even">
 <td width="15%"><label for="c109"><%=Language["SETTINGS_CONFIRMPASS"]%></label></td>
 <td class="odd"><input id="c109" type="password" name="pcpass" size="16" maxlength="64" class="sw-input" /></td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
<br />

<table border="0" cellpadding="2" cellspacing="1" class="list">
<tr class="footer">
 <td>
<table border="0" cellpadding="0" cellspacing="0" style="width:100%"> 
<tr>
 <td align="left">
  <button type="submit" class="main">
   <img src="images/save.png" alt="" />
   <%=Language["SETTINGS_BUTTON_NEWPASS"]%>
  </button>
 </td>
 <td align="right">
  <button type="button" class="main" onclick="location='index.aspx';">
   <img src="images/cancel.png" alt="" />
   <%=Language["BUTTON_CANCEL"]%>
  </button>
 </td> 
</tr>
</table>
 </td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>

<% } else if (ZmenaFakturace) { %>
<table border="0" cellpadding="4" cellspacing="1" class="list">
<tr class="category">
 <td colspan="2"><%=Language["SETTINGS_INVOICE"]%></td>
</tr>
<tr class="even">
 <td width="15%" align="left"><label for="c110"><%=Language["SETTINGS_INVOICE_LAST"]%></label></td>
 <td align="left" class="odd"><input id="c110" type="text" name="pfa" readonly="readonly" value="<%=GetFormValue("pfa", User.GenerateCisloFa())%>" size="20" class="sw-input" /></td>
</tr>
<tr class="category">
 <td colspan="2" align="left"><%=Language["SETTINGS_PAYMENT"]%></td>
</tr>
<tr class="even">
 <td width="15%"><%=Language["SETTINGS_PDPH"]%></td>
 <td class="odd">
  <input id="rb201" type="radio" name="pdph" value="1"<%=(JePlatceDPH?" checked=\"checked\"":"")%> style="height:18px;width:16px;margin:0px;padding:0px;vertical-align:middle" /><label for="rb201"> <%=Language["SETTINGS_PDPH_YES"]%> &nbsp;</label> &nbsp; &nbsp;
  <input id="rb202" type="radio" name="pdph" value="0"<%=(!JePlatceDPH?" checked=\"checked\"":"")%> style="height:18px;width:16px;margin:0px;padding:0px;vertical-align:middle" /><label for="rb202"> <%=Language["SETTINGS_PDPH_NO"]%> &nbsp;</label>
 </td>
</tr>
<tr class="even">
 <td width="15%"><label for="c111"><%=Language["INVOICE_ICO"]%></label></td>
 <td class="odd"><input id="c111" type="text" name="pico" value="<%=GetFormValue("pico", Uzivatel.Ico)%>" maxlength="20" size="20" class="sw-input" /></td>
</tr>
<tr class="even">
 <td width="15%"><label for="c112"><%=Language["INVOICE_DIC"]%></label></td>
 <td class="odd"><input id="c112" type="text" name="pdic" value="<%=GetFormValue("pdic", Uzivatel.Dic)%>" maxlength="20" size="20" class="sw-input" /></td>
</tr>
<tr class="even">
 <td width="15%"><label for="c113"><%=Language["INVOICE_ACCOUNT"]%></label></td>
 <td class="odd"><input id="c113" type="text" name="paccount" value="<%=GetFormValue("paccount", Uzivatel.Ucet)%>" maxlength="64" size="20" class="sw-input" /></td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
<br />

<table border="0" cellpadding="2" cellspacing="1" class="list">
<tr class="footer">
 <td>
<table border="0" cellpadding="0" cellspacing="0" style="width:100%"> 
<tr>
 <td align="left">
  <button type="submit" class="main">
   <img src="images/save.png" alt="" />
   <%=Language["SETTINGS_BUTTON_SAVE"]%>
  </button>
 </td>
 <td align="right">
  <button type="button" class="main" onclick="location='index.aspx';">
   <img src="images/cancel.png" alt="" />
   <%=Language["BUTTON_CANCEL"]%>
  </button>
 </td> 
</tr>
</table>
 </td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>

<% } else { %>

<table border="0" cellpadding="4" cellspacing="1" class="list">
<tr class="category">
 <td><%=Language["SETTINGS_SYSTEM"]%></td>
</tr>
<tr class="odd">
 <td align="left">
  <%=Language["SETTINGS_SYSMSG_1"]%><br /><br />
  <div align="center"><a href="http://www.adobe.com/products/acrobat/readstep2_allversions.html" target="_blank"><img src="images/getacro.gif" alt="<%=Language["GET_ACROBAT"]%>" /></a></div>&nbsp;
 </td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
<br />

<table border="0" cellpadding="2" cellspacing="1" class="list">
<tr class="footer">
 <td align="right">
  <button type="button" class="main" onclick="location='index.aspx';">
   <img src="images/cancel.png" alt="" />
   <%=Language["BUTTON_CANCEL"]%>
  </button>
 </td> 
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>

<% } %>

</form>

<% } %>

</div>

 </td>
</tr>
</table>
</div>

</body>

</html>

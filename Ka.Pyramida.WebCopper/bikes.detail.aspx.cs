﻿using System;
using System.Collections.Generic;
using System.Text;

using Ka.Garance.Data;

public partial class _BikesDetail : Ka.Garance.Web.GarancePage
{
	private string m_SoldDate = "";
	private string m_Status = "";
	private string m_Model = "";
	private string m_ChassisNum = "";
	private bool m_BikeAssigned = false;
	private bool m_BikeSold = false;
	private DateTime dtCheck;

	public string SoldDate
	{
		get { return m_SoldDate; }
	}

	public bool BikeSold
	{
		get { return m_BikeSold; }
	}

	public string Status
	{
		get { return m_Status; }
	}

	public string Model
	{
		get { return m_Model; }
	}

	public string ChassisNum
	{
		get { return m_ChassisNum; }
	}

	public bool BikeAssigned
	{
		get { return m_BikeAssigned; }
	}

	private int TabState
	{
		get
		{
			try { return (int)Session["bikes.detail.state"]; }
			catch { return 1; }
		}
		set { Session["bikes.detail.state"] = value; }
	}

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);

		if (Request.QueryString["state"] == "2")
			TabState = 2;
		else
			TabState = 1;
	}

	protected bool IsProdejSubmitted
	{
		get
		{
			return (!m_BikeSold && Request.HttpMethod == "POST" && Request.Form[owner_type.UniqueID] != null);
		}
	}

    protected void Page_Load(object sender, EventArgs e)
    {
		string laelkID = Request.QueryString["id"];

		string queryV = string.Format(@"
			SELECT
				CAST(IF(IFNULL(prodej_datum,'')='',NULL,CONCAT('20',MID(prodej_datum,5,2),'-',MID(prodej_datum,3,2),'-',LEFT(prodej_datum,2))) AS DATE) AS datum_prodeje,
				IF(stav=0,'{0}','{1}') AS stav,
				IFNULL(CONCAT(model,' - ',jmeno),model) AS model,
				ciskar,t_datv,stav AS stavN,
				majitel_jmeno,majitel_fo,majitel_ico,majitel_ulice,majitel_psc,majitel_obec
			FROM tmotocykl
			LEFT JOIN tcmodelklic ON tcmodelklic.id=tmotocykl.model
			WHERE iddealer={{0}} AND CONCAT(ciskar,CAST(idzeme AS CHAR))={{1}}
		", Language["BIKES_STATE_STOCK"], Language["BIKES_STATE_SOLD"]);

		MySQLDataReader m_Data = Database.ExecuteReader(queryV, User.Id, laelkID);

		if (!m_Data.Read())
			Response.Redirect("bikes.aspx");

		m_SoldDate = string.Format(System.Globalization.CultureInfo.GetCultureInfo(Language.Code), "{0:D}", m_Data.GetValue(0));
		m_Status = m_Data.GetStringValue(1);
		m_Model = m_Data.GetStringValue(2);
		m_ChassisNum = m_Data.GetStringValue(3);

		dtCheck = m_Data.GetDateValue(4);
		bool bProdano = (m_Data.GetIntValue(5) == 2);

		// pridaj tlacitko na zaplatenie
		m_BikeSold = (m_SoldDate != null && m_SoldDate.Trim() != "");

		// process postback before processing the data
		if (Request.HttpMethod == "POST")
		{
			owner_name.Value = Request.Form[owner_name.UniqueID];
			try { owner_type.SelectedIndex = int.Parse(Request.Form[owner_type.UniqueID]); }
			catch { }
			owner_ico.Value = Request.Form[owner_ico.UniqueID];
			owner_address.Value = Request.Form[owner_address.UniqueID];
			owner_zip.Value = Request.Form[owner_zip.UniqueID];
			owner_city.Value = Request.Form[owner_city.UniqueID];
		}
		else
		{
			owner_name.Value = m_Data.GetStringValue(6);
			owner_type.SelectedIndex = m_Data.GetIntValue(7);
			owner_ico.Value = m_Data.GetStringValue(8);
			owner_address.Value = m_Data.GetStringValue(9);
			owner_zip.Value = m_Data.GetStringValue(10);
			owner_city.Value = m_Data.GetStringValue(11);

			if (m_BikeSold)
			{
				owner_type.Disabled = true;
				owner_name.Attributes["readonly"] = "readonly";
				owner_ico.Attributes["readonly"] = "readonly";
				owner_address.Attributes["readonly"] = "readonly";
				owner_zip.Attributes["readonly"] = "readonly";
				owner_city.Attributes["readonly"] = "readonly";
			}
		}

		// zisti, ci bolo prevedenie inemu dealerovi zadane
		if (Request.Form["assignDealer"] != null)
		{
			PrevestDealera();
		}

		if (IsProdejSubmitted)
		{
			ProdejMotocyklu();
		}
    }

	private void PrevestDealera()
	{
		Database.Execute(
			"UPDATE tmotocykl SET prevod=1,iddealer={2} WHERE iddealer={0} AND CONCAT(ciskar,idzeme)={1}",
			User.Id, Request.QueryString["id"],
			Convert.ToInt32(Request.Form["dealerID"])
		);

		Database.Execute(
			"INSERT INTO tmotocyklprevod (ciskar,iddealer_src,iddealer_dst,datum) VALUES({0},{1},{2},NOW())",
			ChassisNum, User.Id, Convert.ToInt32(Request.Form["dealerID"])
		);

		m_BikeAssigned = true;
	}

	private void ProdejMotocyklu()
	{
		// kontrola vstupnich dat
		List<string> m_Errors = new List<string>();

		if (owner_name.Value.Trim() == "")
			m_Errors.Add(Language["PROTO_ERR_NAME"]);
		if (owner_type.SelectedIndex > 0 && owner_ico.Value.Trim() == "")
			m_Errors.Add(Language["PROTO_ERR_ICO"]);
		if (owner_address.Value.Trim() == "")
			m_Errors.Add(Language["PROTO_ERR_ADDRESS"]);
		if (owner_zip.Value.Trim() == "")
			m_Errors.Add(Language["PROTO_ERR_ZIP"]);
		if (owner_city.Value.Trim() == "")
			m_Errors.Add(Language["PROTO_ERR_CITY"]);

		DateTime sellDate = DateTime.Now;
		bool bSellDone = false;
		try
		{
			string[] sd = Request.Form["sell_calTxt"].Split(".".ToCharArray());
			sellDate = new DateTime(
				Convert.ToInt32(sd[2].Trim()),
				Convert.ToInt32(sd[1].Trim()),
				Convert.ToInt32(sd[0].Trim())
			);
			if (sellDate >= dtCheck && sellDate <= DateTime.Now)
				bSellDone = true;
		}
		catch { }

		if (!bSellDone)
			m_Errors.Add(Language["ERROR_BAD_DATE"]);

		// invalid data
		if (m_Errors.Count > 0)
		{
			ShowErrorSell(m_Errors);
			return;
		}

		// save the data to the database
		try
		{
			string query = Database.FormatQuery(
				@"UPDATE tmotocykl SET
					stav=2,prodej_datum={8},prodej_datum_d={9},
					majitel_jmeno={0},
					majitel_fo={1},
					majitel_ico={2},
					majitel_ulice={3},
					majitel_psc={4},
					majitel_obec={5}
				WHERE iddealer={6} AND CONCAT(ciskar,idzeme)={7}",
				owner_name.Value.Trim(),
				owner_type.SelectedIndex,
				owner_ico.Value.Trim(),
				owner_address.Value.Trim(),
				owner_zip.Value.Trim(),
				owner_city.Value.Trim(),
				User.Id,
				Request.QueryString["id"],
				string.Format("{0:D2}{1:D2}{2}", sellDate.Day, sellDate.Month, sellDate.Year.ToString().Substring(2, 2)),
				sellDate
			);

			Database.Execute(query);

			m_SoldDate = string.Format(System.Globalization.CultureInfo.GetCultureInfo(Language.Code), "{0:D}", sellDate);

			// log to database
			Database.Execute(
				"INSERT INTO `tprodejlog` (`ip`,`iddealer`,`operace`) VALUES ({0},{1},{2})",
				Request.UserHostAddress,
				User.Id,
				query
			);
		}
		catch (Exception ex)
		{
			m_Errors.Add(ex.Message);
			ShowErrorSell(m_Errors);
			return;
		}

		// all ok
		Response.Redirect("bikes.detail.aspx?id=" + Request.QueryString["id"]);
	}

	protected string GetSoldDate()
	{
		if (BikeSold)
			return SoldDate;

		StringBuilder sb = new StringBuilder();
		sb.Append("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>");
		sb.AppendFormat(
			"<input id=\"sell_calTxt\" type=\"text\" name=\"sell_calTxt\" value=\"{0}\" size=\"10\" class=\"sw-input\" /></td>",
			Request.Form["sell_calTxt"]
		);
		sb.Append("<td>&nbsp;</td>");
		sb.Append("<td><a href=\"javascript:void(0);\" onclick=\"displayCalendar(calTxt);return false;\"><img src=\"images/calendar.png\" alt=\"\" /></a></td>");
		sb.Append("</tr></table>");
		return sb.ToString();
	}

	private void ShowErrorSell(List<string> m_Errors)
	{
		if (m_Errors.Count == 0) return;

		StringBuilder sb = new StringBuilder();
		sb.Append("<ul>\n<li>");
		for (int i = 0; i < m_Errors.Count; i++)
		{
			if (i > 0) sb.Append("</li>\n<li>");
			sb.Append(m_Errors[i]);
		}
		sb.Append("</li>\n</ul>");

		ASPL_ErrorSell.Text = string.Format(@"<table border=""0"" cellpadding=""4"" cellspacing=""1"" class=""list"">
<tr class=""category""><td><b style=""color:red"">{0}</b></td></tr>
<tr>
 <td style=""background-color:#FFFFE2"">
{1}
 </td>
</tr>
</table>
<div style=""padding:0px 3px 0px 3px""><img src=""images/bk.shadow.png"" width=""100%"" height=""1"" /></div>
<br />", Language["ERROR_PROCESSING_DATA"], sb.ToString());
	}

	protected override void OnPreRender(EventArgs e)
	{
		base.OnPreRender(e);

		TabItem1.Link = string.Format("bikes.detail.aspx?id={0}&amp;state=1", Request.QueryString["id"]);
		TabItem2.Link = string.Format("bikes.detail.aspx?id={0}&amp;state=2", Request.QueryString["id"]);

		TabItem1.Selected = (TabState == 1);
		TabItem2.Selected = (TabState == 2);

		MySQLDataReader m_Data = Database.ExecuteReader(@"
SELECT
	id,
	nazev,
	ulice, psc, mesto
FROM tdealer
WHERE id<>{0}
ORDER BY nazev
		", this.User.Id);

		StringBuilder sb = new StringBuilder();
		sb.Append("<select name=\"dealerID\" class=\"sw-input\" onchange=\"this.title=this.options[this.selectedIndex].title;\">\n");
		while (m_Data.Read())
		{
			int dealerID = m_Data.GetIntValue(0);
			bool selected = false;
			try { selected = (Convert.ToInt32(Request.Form["dealerID"]) == dealerID); }
			catch { }
			sb.AppendFormat(
				" <option value=\"{0}\"{1} title=\"{2}, {3}\">{2}</option>\n",
				dealerID,
				(selected ? " selected=\"selected\"" : ""),
				Server.HtmlEncode(m_Data.GetStringValue(1)),
				Server.HtmlEncode(m_Data.GetStringValue(2) + ", " + m_Data.GetStringValue(3) + " " + m_Data.GetStringValue(4))
			);
		}

		sb.Append("</select>");

		cbDealers.Text = sb.ToString();

		owner_type.Items[0].Text = Language["PROTO_OWNER_FO"];
		owner_type.Items[1].Text = Language["PROTO_OWNER_FO_ICO"];
		owner_type.Items[2].Text = Language["PROTO_OWNER_PO"];
	}

	public string PrevedenDealer()
	{
		try
		{
			MySQLDataReader m_Data = Database.ExecuteReader(@"
SELECT
  id,
  nazev,
  ulice,psc,mesto
FROM tdealer
WHERE id={0}
ORDER BY nazev
		", Convert.ToInt32(Request.Form["dealerID"]));

			m_Data.Read();

			return string.Format(
				"<br /><br /><b>{0}</b><br /><br />{1}<br />{2} {3}",
				Server.HtmlEncode(m_Data.GetStringValue(1)),
				Server.HtmlEncode(m_Data.GetStringValue(2)),
				Server.HtmlEncode(m_Data.GetStringValue(3)),
				Server.HtmlEncode(m_Data.GetStringValue(4))
			);
		}
		catch
		{
			return null;
		}
	}
}

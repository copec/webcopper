using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

public partial class _stats : Ka.Garance.Web.GarancePage
{
	protected void Page_Load(object sender, EventArgs e)
	{
		// only 01-000 has access to this area
		if (User.UserName != "01-000")
			Response.Redirect("index.aspx");

		string state = Request.QueryString["state"];
		if (state != null)
			Session["webcopper.stats"] = state;

		state = string.Format("{0}", Session["webcopper.stats"]);
		if (state == "1")
		{
			TabItem1.Selected = true;
			WSModels.Visible = true;
			WSVin.Visible = false;
			WSBikes.Visible = false;
		}
		else if (state == "2")
		{
			TabItem2.Selected = true;
			WSModels.Visible = false;
			WSVin.Visible = true;
			WSBikes.Visible = false;
		}
		else
		{
			TabItem0.Selected = true;
			WSModels.Visible = false;
			WSVin.Visible = false;
			WSBikes.Visible = true;
		}		
	}
}

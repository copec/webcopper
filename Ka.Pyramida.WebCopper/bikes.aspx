﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="_Bikes" Codebehind="bikes.aspx.cs" %>
<%@ Register TagPrefix="yuki" TagName="header" Src="Controls/Header.ascx" %>
<%@ Register TagPrefix="yuki" Namespace="Ka.Garance.Web.List" Assembly="garance" %>
<%@ Register TagPrefix="yuki" Namespace="Ka.Garance.Web.TabList" Assembly="garance" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Language.Code%>" dir="ltr" lang="<%=Language.Code%>">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><%=Language["APP_TITLE"]%></title>
<link rel="stylesheet" type="text/css" href="css/yuki.css" />
<link rel="stylesheet" type="text/css" href="css/combo.css" />
</head>

<body>

<yuki:header Id="MCHeader" LocationID="LOCATION_BIKES" runat="server" />

<div align="center" style="padding:0px 16px 16px 16px"> 
<table border="0" cellpadding="0" cellspacing="0" align="center" style="width:1%">
<tr>
 <td align="left">
 
<div style="width:800px"> 

<img src="images/mod.moto.png" alt="" style="vertical-align:text-bottom" />
<span class="mod"><%=Language["HEADER_BIKES"]%></span><br /><br />

<form action="bikes.aspx" method="post">
<table border="0" cellpadding="4" cellspacing="1" class="list">
<tr class="category">
 <td colspan="2"><%=Language["PARTS_FILTER"]%></td>
</tr>
<tr class="even">
 <td width="15%"><label for="c101_wg_02"><%=Language["BIKES_LIST_MODEL"]%></label></td>
 <td class="odd">
<select id="c101_wg_02" name="pmodel"> 
 <option value=""><%=Language["PROTO_STATE_ALL"]%></option>
 <option value="" style="color:#C0C0C0" disabled="disabled">- - - - - - - - - - - - - - - - -</option>
 <%=GetModelOptions()%>
</select> 
 </td>
</tr>
<tr class="even">
 <td width="15%"><label for="c101_wg_01"><%=Language["BIKES_LIST_CHASSIS"]%></label></td>
 <td class="odd"><input id="c101_wg_01" type="text" name="pvin" value="<%=Server.HtmlEncode(VINFilter)%>" size="20" maxlength="20" class="sw-input" /></td>
</tr>
<tr class="footer">
 <td colspan="2">
  <input type="submit" value="<%=Language["BUTTON_FILTER"]%>" style="font-weight:bold" />
  <input type="button" value="<%=Language["BUTTON_FILTER_RESET"]%>" onclick="location='bikes.aspx?filter=';" />
 </td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
</form>
<br />

<yuki:TabList ID="MCFilterTabs" runat="server">
 <yuki:TabItem ID="TabItem1" LangID="BIKES_STATE_ALL" Link="bikes.aspx?state=0" runat="server" />
 <yuki:TabItem ID="TabItem2" LangID="BIKES_STATE_STOCK" Link="bikes.aspx?state=1" runat="server" />
 <yuki:TabItem ID="TabItem3" LangID="BIKES_STATE_SOLD" Link="bikes.aspx?state=2" runat="server" />
 <yuki:TabItem ID="TabItem4" LangID="BIKES_STATE_VIN" Link="bikes.aspx?state=3" runat="server" />
</yuki:TabList>
<yuki:List ID="MCBikesList" Name="BikesList" Identifier="id" Link="bikes.detail.aspx?id={0}" runat="server">
 <yuki:ListItem ID="ListItem3" LangID="BIKES_LIST_MODEL" ColumnName="model" align="left" width="50%" runat="server" /> 
 <yuki:ListItem ID="ListItem5" Title="Ccm" ColumnName="kubatura" align="left" td_align="left" runat="server" />
 <yuki:ListItem ID="ListItem4" LangID="BIKES_LIST_CHASSIS" ColumnName="ciskar" align="left" runat="server" />
 <yuki:ListItem ID="ListItem2" LangID="BIKES_LIST_STATE" ColumnName="stav" align="left" runat="server" />
 <yuki:ListItem ID="ListItem1" LangID="BIKES_LIST_DATE" ColumnName="datum_prodeje" align="left" Default="true" Format="{0:d'. 'M'. 'yyyy}" runat="server" />
</yuki:List>
<yuki:List ID="MCBikesVIN" Name="BikesVIN" Identifier="id" runat="server" Visible="false">
  <yuki:ListItem ID="liVIN" LangID="BIKES_LIST_CHASSIS" ColumnName="ciskar" align="left" width="100%" runat="server" />
</yuki:List>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>

</div>
 </td>
</tr>
</table>
</div>

</body>

</html>
﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="_Protocol_Edit" Codebehind="protocol.edit.aspx.cs" %>
<%@ Register TagPrefix="yuki" TagName="header" Src="Controls/Header.ascx" %>
<%@ Register TagPrefix="yuki" TagName="protocol" Src="Controls/ProtocolAdd.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Language.Code%>" dir="ltr" lang="<%=Language.Code%>">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><%=Language["APP_TITLE"]%></title>
<link rel="stylesheet" type="text/css" href="css/yuki.css" />
<link rel="stylesheet" type="text/css" href="css/combo.css" />
<link rel="stylesheet" type="text/css" href="css/calendar.css" />
</head>

<body>

<yuki:header Id="MCHeader" runat="server" />

<div align="center" style="padding:0px 16px 16px 16px"> 
<table border="0" cellpadding="0" cellspacing="0" align="center" style="width:1%">
<tr>
 <td align="left">
 
<div style="width:600px">

<img src="images/mod.protocol.add.png" alt="" style="vertical-align:text-bottom" />
<span class="mod"><%=Language["HEADER_PROTOCOL_EDIT"]%> - <%=MCProtocol.StepTitle%></span><br /><br />

<yuki:protocol ID="MCProtocol" runat="server" />

</div>

 </td>
</tr>
</table>
</div>

</body>

</html>
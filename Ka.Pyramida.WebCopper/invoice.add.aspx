﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="_Invoice_Add" Codebehind="invoice.add.aspx.cs" %>
<%@ Register TagPrefix="yuki" TagName="header" Src="Controls/Header.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Language.Code%>" dir="ltr" lang="<%=Language.Code%>">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><%=Language["APP_TITLE"]%></title>
<link rel="stylesheet" type="text/css" href="<%= ResolveClientUrl("~/css/yuki.css") %>" />
</head>

<body>

<yuki:header Id="MCHeader" runat="server" />

<div align="center" style="padding:0px 16px 16px 16px"> 
<table border="0" cellpadding="0" cellspacing="0" align="center" style="width:1%">
<tr>
 <td align="left">
 
<div style="width:600px">

<img src="images/mod.invoice.add.png" alt="" style="vertical-align:text-bottom" />
<span class="mod"><%=Language["HEADER_INVOICE_ADD"]%></span><br /><br />

<script language="javascript" type="text/javascript" src="<%= ResolveClientUrl("~/js/invoice.js") %>"></script>

<form action="invoice.add.aspx" method="post">
<%=GenerateSeznamHTML()%>
</form>

</div>

 </td>
</tr>
</table>
</div>

</body>

</html>
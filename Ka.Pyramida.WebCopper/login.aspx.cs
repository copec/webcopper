﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Ka.Garance.Lang;

public partial class _Login : Ka.Garance.Web.GarancePage
{
	private bool m_LoginFailed = false;
	public bool LoginFailed
	{
		get { return m_LoginFailed; }
	}

	public string LogUser
	{
		get { return GetFormVar("username"); }
	}

	public string LogPass
	{
		get { return GetFormVar("password"); }
	}

	public string LogLang
	{
		get
		{
			string v = "";
			// 1. - check if posted!
			if (Request.Form["language"] != null)
				v = Request.Form["language"];
			// 2. - load from cookie
			try { if (v == "") v = Request.Cookies["lang"].Value; }
			catch { v = ""; }
			// 3. - check the client capabilities for a language
			// look for czech/slovak first, if not found use eng
			if (v == "")
			for (int i = 0; i < Request.UserLanguages.Length; i++)
			{
				try
				{
					switch (Request.UserLanguages[i].Substring(0, 2).ToLower())
					{
						case "sk":
							v = "sk-SK";
							break;
						case "en":
							v = "en-GB";
							break;
					}
				}
				catch { }
				if (v != "") break;
			}
			// 4. return default language
			if (v == "")
				v = "cs-CZ";

			LoadLanguages();
			LanguageStrings lsDefault = InstalledLanguages.Languages[0];
			foreach (LanguageStrings ls in InstalledLanguages.Languages)
			{
				if (ls.Code == v) return v;
				if (ls.Default) lsDefault = ls;
			}

			return lsDefault.Code;
		}
	}

	public string GetLangOptions()
	{
		string lc = LogLang;
		StringBuilder sb = new StringBuilder();

		foreach (LanguageStrings ls in InstalledLanguages.Languages)
		{
			if (sb.Length > 0) sb.Append("\n");
			sb.AppendFormat(
				"   <option value=\"{0}\"{1}>{2}</option>",
				ls.Code,
				(ls.Code == lc ? " selected=\"selected\"" : ""),
				ls.Name
			);
		}

		return sb.ToString();
	}

	protected override void OnInit(EventArgs e)
	{
		HttpCookie c = new HttpCookie("lang", LogLang);
		c.Path = Request.ApplicationPath;
		c.Expires = DateTime.Now.AddYears(2);
		Response.Cookies.Add(c);
		try { Request.Cookies["lang"].Value = LogLang; }
		catch { }
		m_IsLoginPage = true;

		base.OnInit(e);
	}

	private string GetFormVar(string name)
	{
		try
		{
			string v = Request.Form[name];
			if (v == null) v = "";
			return v;
		}
		catch
		{
			return "";
		}
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		if (Request.QueryString["logout"] != null)
		{
			User.LogOut();
			Response.Redirect("login.aspx");
		}
		else if (Request.Form["login"] != null)
		{
			User.LogIn(
				Request.Form["username"], Request.Form["password"],
				(Request.Form["persistent"] == "1")
			);
			if (User.LoggedIn)
			{
				User.Cache.Clear();
				Response.Redirect("index.aspx");
			}
			else
				m_LoginFailed = true;
		}
	}
}

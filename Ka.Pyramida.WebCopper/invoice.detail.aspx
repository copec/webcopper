﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="_Invoice_Detail" Codebehind="invoice.detail.aspx.cs" %>
<%@ Register TagPrefix="yuki" TagName="header" Src="Controls/Header.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Language.Code%>" dir="ltr" lang="<%=Language.Code%>">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><%=Language["APP_TITLE"]%></title>
<link rel="stylesheet" type="text/css" href="css/yuki.css" />
</head>

<body>

<yuki:header Id="MCHeader" LocationID="LOCATION_INVOICE_DETAIL" runat="server" />

<div align="center" style="padding:0px 16px 16px 16px"> 
<table border="0" cellpadding="0" cellspacing="0" align="center" style="width:1%">
<tr>
 <td align="left">
 
<div style="width:600px">

<table border="0" cellpadding="0" cellspacing="0" style="width:100%">
<tr>
 <td align="left">
<img src="images/mod.invoice.add.png" alt="" style="vertical-align:text-bottom" />
<span class="mod"><%=Language["HEADER_INVOICE_DETAIL"]%></span>
 </td>
 <td align="right" valign="bottom">
  <button type="button" class="main" style="padding:4px;width:150px" onclick="location='invoice.detail.aspx?fa=<%=Faktura.Cislo%>\x26pdf=letter';">
   <img src="images/print.pdf.png" alt="" />
   <%=Language["INVOICE_PRINT_LETTER"]%>
  </button>   
   <button type="button" class="main" style="padding:4px;width:100px" onclick="location='invoice.detail.aspx?fa=<%=Faktura.Cislo%>\x26pdf=';">
    <img src="images/print.pdf.png" alt="" />
    <%=Language["INVOICE_PRINT_INVOICE"]%>
   </button>
 </td> 
</tr>
</table>&nbsp;
<br />

<table border="0" cellpadding="4" cellspacing="1" class="list">
<tr class="header">
 <th>
<table border="0" cellpadding="0" cellspacing="0" style="width:100%">
<tr>
 <td align="left" style="color:#fff"><%=Language["HEADER_INVOICE_DETAIL"]%></td>
 <td align="right" style="color:#fff"><%=Language["INVOICE_NO"]%>: <%=Faktura.Cislo%></td>
</tr>
</table>
 </th>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
<br />

<table border="0" cellpadding="4" cellspacing="1" class="list">
<tr class="even">
 <td align="left" rowspan="2" valign="top" width="49%"><%=Language["INVOICE_DATE"]%></td>
 <td class="odd"><%=Faktura.DatumVystaveni.ToString("d'.'M'.'yyyy")%></td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
<br />

<table border="0" cellpadding="0" cellspacing="0" style="width:100%">
<tr>
 <td align="left" rowspan="2" valign="top" width="49%">
<table border="0" cellpadding="4" cellspacing="1" class="list">
<tr class="category">
 <td colspan="2"><%=Language["INVOICE_SUPPLIER"]%></td> 
</tr>
<tr class="even">
 <td width="25%"><%=Language["INVOICE_ADDRESS"]%></td>
 <td class="odd"><b><%=Faktura.Dodavatel.Jmeno%><br /><%=Faktura.Dodavatel.Adresa%><br /><%=Faktura.Dodavatel.PSC%> <%=Faktura.Dodavatel.Obec%></b></td>
</tr>
<tr class="even">
 <td><%=Language["PROTO_PHONE"]%></td>
 <td class="odd"><%=Faktura.Dodavatel.Tel%></td>
</tr>
<tr class="even">
 <td><%=Language["PROTO_FAX"]%></td>
 <td class="odd"><%=Faktura.Dodavatel.Fax%></td>
</tr>
<tr class="even">
 <td><%=Language["INVOICE_MOBILE"]%></td>
 <td class="odd"><%=Faktura.Dodavatel.Mobil%></td>
</tr>
<tr class="even">
 <td><%=Language["INVOICE_EMAIL"]%></td>
 <td class="odd"><a href="mailto:<%=Faktura.Dodavatel.Email%>" class="menu"><%=Faktura.Dodavatel.Email%></a></td>
</tr>
<tr class="even">
 <td><%=Language["INVOICE_ICO"]%></td>
 <td class="odd"><%=Faktura.Dodavatel.ICO%></td>
</tr>
<tr class="even">
 <td><%=Language["INVOICE_DIC"]%></td>
 <td class="odd"><%=Faktura.Dodavatel.DIC%></td>
</tr>
<tr class="even">
 <td><%=Language["INVOICE_ACCOUNT"]%></td>
 <td class="odd"><%=Faktura.Dodavatel.Ucet%></td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
 </td>
 <td>&nbsp;</td>
 <td valign="top" align="left" width="49%">
<table border="0" cellpadding="4" cellspacing="1" class="list">
<tr class="category">
 <td colspan="2"><%=Language["INVOICE_ORDER"]%></td> 
</tr>
<tr class="even">
 <td width="40%"><%=Language["INVOICE_CSYM"]%></td>
 <td class="odd"><%=Faktura.KonstantniSymbol%></td>
</tr>
<tr class="even">
 <td><%=Language["INVOICE_VSYM"]%></td>
 <td class="odd"><%=Faktura.VariabilniSymbol%></td>
</tr>
<tr class="even">
 <td><%=Language["INVOICE_SSYM"]%></td>
 <td class="odd"><%=Faktura.SpecifickySymbol%></td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
 </td>
</tr> 
<tr>
 <td></td>
 <td align="left" valign="bottom">
<table border="0" cellpadding="4" cellspacing="1" class="list">
<tr class="category">
 <td colspan="2"><%=Language["INVOICE_CUSTOMER"]%></td> 
</tr>
<tr class="even">
 <td width="25%"><%=Language["INVOICE_ADDRESS"]%></td>
 <td class="odd"><b><%=Faktura.Odberatel.Jmeno%><br /><%=Faktura.Odberatel.Adresa%><br /><%=Faktura.Odberatel.PSC%> <%=Faktura.Odberatel.Obec%></b></td>
</tr>
<tr class="even">
 <td><%=Language["INVOICE_ICO"]%></td>
 <td class="odd"><%=Faktura.Odberatel.ICO%></td>
</tr>
<tr class="even">
 <td><%=Language["INVOICE_DIC"]%></td>
 <td class="odd"><%=Faktura.Odberatel.DIC%></td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
 </td>
</tr>
</table>
<br />

<%=GetSeznamProtokoluHTML()%>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
<br />

<table border="0" cellpadding="2" cellspacing="1" class="list">
<tr class="footer">
 <td align="right">
  <button type="button" class="main" onclick="location='invoice.aspx';">
   <img src="images/cancel.png" alt="" />
   <%=Language["BUTTON_CLOSE"]%>
  </button>
 </td> 
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>

</div>

 </td>
</tr>
</table>
</div>

</body>

</html>

﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.xml;
using iTextSharp.text.html;
using Ka.Garance.Claim.Yuki;
using Ka.Garance.Invoice.Yuki;

public partial class _Claim_Detail : Ka.Garance.Web.GarancePage
{
	private Listy m_Listy;
	public Listy Claim
	{
		get { return m_Listy; }
	}

    protected void Page_Load(object sender, EventArgs e)
    {
		try
		{
			m_Listy = new Listy(User, Convert.ToInt32(Request.QueryString["id"]));
		}
		catch
		{
			Response.Redirect("claim.aspx");
		}

		if (Request.QueryString["pdf"] != null)
		{
			MemoryStream ms = RenderPdf();
			try
			{
				Ka.Garance.Util.GaranceUtil.ForceDownload(Context, ms, string.Format("Yuki-List-{0:D4}.pdf", Claim.Id));
			}
			finally
			{
				ms.Dispose();
			}
			//Response.End();
		}
    }

	protected string GetSeznamPolozek()
	{
		StringBuilder sb = new StringBuilder();

		int idx = 0;
		foreach (ListDil d in m_Listy)
		{
			sb.AppendFormat(@"
<tr class=""{0}"">
 <td align=""left"" width=""1%"" style=""width:1px"">{7}.</td>
 <td align=""left"" width=""1%"" nowrap=""nowrap"">{1}</td>
 <td align=""left"" width=""1%"" nowrap=""nowrap"">{2}</td>
 <td align=""left"" width=""1%"" nowrap=""nowrap"">{3}</td>
 <td align=""left"">{4}</td>
 <td align=""center"" width=""1%"">{5}</td>
 <td align=""left"">{6}</td>
</tr>", (idx++ % 2 == 0 ? "even" : "odd"), d.Protokol, d.Model, d.SklCislo, d.Nazev, d.Pocet, Server.HtmlEncode(d.Poznamka), idx);
		}

		return sb.ToString();
	}

	private MemoryStream RenderPdf()
	{
		PdfMemoryStream ms = new PdfMemoryStream();
		ms.LeaveOpen = true;
		Document document = new Document(new Rectangle(842, 595), 45, 45, 45, 45);
		try
		{
			PdfWriter pf = PdfWriter.GetInstance(document, ms);
			document.Open();

			Claim.Render(document);
		}
		finally
		{
			try { document.Close(); }
			catch { }
		}
		ms.LeaveOpen = false;
		ms.Position = 0;
		return ms;
	}


}

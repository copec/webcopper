﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="_Index" Codebehind="index.aspx.cs" %>
<%@ Register TagPrefix="yuki" Namespace="Ka.Garance.Web.Panel" Assembly="garance" %>
<%@ Register TagPrefix="yuki" TagName="header" Src="Controls/Header.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Language.Code%>" dir="ltr" lang="<%=Language.Code%>">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><%=Language["APP_TITLE"]%></title>
<link rel="stylesheet" type="text/css" href="css/yuki.css" />
<link rel="stylesheet" type="text/css" href="css/calendar.css" />
<link rel="stylesheet" type="text/css" href="css/combo.css" />
</head>

<body>

<yuki:header Id="MCHeader" LocationID="LOCATION_INDEX" runat="server" />

<br />
<table border="0" cellpadding="0" cellspacing="0" style="width:100%">
<tr>
 <td align="center">

<table border="0" cellpadding="0" cellspacing="0">
<tr>
 <td colspan="3">
<table border="0" cellpadding="0" cellspacing="0" class="panel" style="width:100%">
<tr class="panel-header">
 <td align="left" valign="top"><img src="images/panel.tl.png" alt="" /></td>
 <td align="left" style="width:99.9%"><div style="padding:2px 4px 2px 4px"><%=Language["HEADER_VALIDATE"]%></div></td>
 <td align="right" valign="top"><img src="images/panel.tr.png" alt="" /></td>
</tr>
<tr><td colspan="3"><img src="images/panel.s.png" alt="" width="100%" height="4" /></td></tr>
<tr class="panel-row"><td colspan="3">&nbsp;</td></tr>
<tr class="panel-row">
 <td colspan="3" align="center" valign="middle" height="22">
<div style="height:22px">
<div id="pg_validate_div"></div>
<table id="pg_validate_tbl" border="0" cellpadding="0" cellspacing="0"> 
<tr>
 <td onclick="try{document.getElementById('ci101').focus();}catch(e){}">VIN:&nbsp;</td>
 <td><div id="protocol.div.bike" style="width:140px"></div></td>
 <td>&nbsp; &nbsp; <label for="pg_pdate"><%=Language["PROTO_LIST_DATE"]%>:&nbsp;</label></td> 
 <td><input id="pg_pdate" type="text" value="<%=DateTime.Now.ToString("d'. 'M'. 'yyyy")%>" size="10" class="sw-input" onkeyup="if(event.keyCode==13)pg_Submit();" /></td>
 <td style="padding-left:1px"><a href="javascript:void(0);" onclick="displayCalendar();"><img src="images/calendar.png" alt="" /></a></td> 
 <td>&nbsp; &nbsp; <label for="pg_pkm"><%=Language["PROTO_KM"]%>:&nbsp;</label></td>
 <td><input id="pg_pkm" type="text" size="4" class="sw-input" onkeyup="if(event.keyCode==13)pg_Submit();" /></td>
 <td>&nbsp; &nbsp;</td>
 <td><input type="button" value="<%=Language["BUTTON_VALIDATE"]%>" onclick="pg_Submit();" /></td><% if (this.MessageNotify>0) { %>
 <td valign="middle" style="padding-left:32px">
  <div style="cursor:pointer" onclick="FlashImageMsg();"><img id="imgMessage001" src="images/message.png" alt="" title="" /></div>
 </td><% } %> 
</tr>
</table>  
</div>
 </td>
</tr>
<tr class="panel-row">
 <td align="left" valign="bottom" style="width:1px"><img src="images/panel.bl.png" alt="" /></td>
 <td align="left" style="width:99.9%">&nbsp;</td>
 <td align="right" valign="bottom" style="width:1px"><img src="images/panel.br.png" alt="" /></td>
</tr>
</table> 

 </td>
</tr>
<tr><td colspan="3"><div style="height:16px">&nbsp;</div></td></tr>
<tr>
 <td align="left" valign="top">
<yuki:Panel ID="MCPanelProtokol" runat="server"> 
 <yuki:PanelItem ID="PanelItem1" Href="protocols.aspx?add=" Image="images/mod.protocol.add.png" runat="server" />
 <yuki:PanelItem ID="PanelItem2" Href="protocols.aspx" Image="images/mod.protocol.png" runat="server" />
</yuki:Panel>
 </td>
 <td><div style="width:20px">&nbsp;</div></td>
 <td align="left" valign="top">
<yuki:Panel ID="MCPanelKatalog" runat="server"> 
 <yuki:PanelItem ID="PanelItem3" Href="bikes.aspx" Image="images/mod.moto.png" runat="server" />
 <yuki:PanelItem ID="PanelItem4" Href="parts.aspx" Image="images/mod.catalog.png" runat="server" />
</yuki:Panel>  
 </td>
</tr>
<tr><td colspan="3"><div style="height:16px">&nbsp;</div></td></tr>
<tr> 
 <td>
<yuki:Panel ID="MCPanelFaktury" runat="server">
 <yuki:PanelItem ID="PanelItem5" Href="invoice.add.aspx" Image="images/mod.invoice.add.png" runat="server" />
 <yuki:PanelItem ID="PanelItem6" Href="invoice.aspx" Image="images/mod.invoice.png" runat="server" />
</yuki:Panel>  
 </td>
 <td><div style="width:20px">&nbsp;</div></td> 
 <td align="left" valign="bottom">
<yuki:Panel ID="MCPanelClaim" runat="server">
 <yuki:PanelItem ID="PanelItem7" Href="claim.add.aspx" Image="images/mod.claim.add.png" runat="server" />
 <yuki:PanelItem ID="PanelItem8" Href="claim.aspx" Image="images/mod.claim.png" runat="server" />
</yuki:Panel>
 </td>
</tr>
<tr><td colspan="3"><div style="height:16px">&nbsp;</div></td></tr>
</table>
 
 </td>
</tr>
</table>

<script language="javascript" type="text/javascript" src="js/ajax.js"></script>
<script language="javascript" type="text/javascript" src="js/combo.js"></script>
<script language="javascript" type="text/javascript" src="js/calendar.js"></script>
<script language="javascript" type="text/javascript" src="js/message.js"></script>
<script language="javascript" type="text/javascript">
//<![CDATA[

// bike combo
var bikeCombo = new StylusWebCombo(document.getElementById('protocol.div.bike'), 'pg_pvin');
bikeCombo.disableCheck = true;
bikeCombo._input.id = 'ci101';
bikeCombo._input.value = '';
bikeCombo._inputh.value = '';
bikeCombo.div.combo = bikeCombo;
bikeCombo.onajaxdata = function(combo, value) { Ajax.execute("protocol.add.aspx?ajaxbikes="+uEscape(value), updateBikeCombo); }
bikeCombo._input.style.width = '120px';
bikeCombo._input.onkeyup_OLD = bikeCombo._input.onkeyup;
bikeCombo._input.onkeyup = function(e) {
  var keyCode = 0;
  if (window.event) keyCode = window.event.keyCode;
  else if(e&&e.keyCode) keyCode = e.keyCode;  
  if (keyCode == 13 && !this.sw.expanded) {
    pg_Submit();
    return false;
  }
  return this.onkeyup_OLD(e);
}

function updateBikeCombo(msg) {
  if (msg.length == 0) return;
  if (msg.substr(0,3) != 'OK-') {
    return;
  }
  
  msg = msg.substr(3, msg.length);   
   
  bikeCombo.ajaxClear();    
  eval(msg);

  bikeCombo.collapse();
  bikeCombo.expand();
}

// calendar constants
Calendar._DN = '<%=Language["JS_DATE_DN"]%>'.split(',');
Calendar._SDN = '<%=Language["JS_DATE_DNS"]%>'.split(',');
Calendar._MN = '<%=Language["JS_DATE_MN"]%>'.split(',');
Calendar._SMN = '<%=Language["JS_DATE_MNS"]%>'.split(',');
Calendar._TT["PREV_YEAR"] = '<%=Language["JS_DATE_PREV_YEAR"]%>';
Calendar._TT["PREV_MONTH"] = '<%=Language["JS_DATE_PREV_MONTH"]%>';
Calendar._TT["GO_TODAY"] = '<%=Language["JS_DATE_GO_TODAY"]%>';
Calendar._TT["NEXT_MONTH"] = '<%=Language["JS_DATE_NEXT_MONTH"]%>';
Calendar._TT["NEXT_YEAR"] = '<%=Language["JS_DATE_NEXT_YEAR"]%>';
Calendar._TT["SEL_DATE"] = '<%=Language["JS_DATE_SELECT"]%>';
Calendar._TT["PART_TODAY"] = '<%=Language["JS_DATE_PART_TODAY"]%>';
Calendar._TT["DAY_FIRST"] = '<%=Language["JS_DATE_DAY_FIRST"]%>';
Calendar._TT["CLOSE"] = '<%=Language["BUTTON_CLOSE"]%>';
Calendar._TT["TODAY"] = '<%=Language["JS_DATE_GO_TODAY"]%>';

function dateFromString(s) {
  if (s.replace(/[0-9\.\s]+/g,'') != '') return null;
  
  var r = s.split('.');
  if (r.length != 3) return null;
  
  var d = parseInt(r[0].replace(/^[\s]+|[\s]+$/,''));
  var m = parseInt(r[1].replace(/^[\s]+|[\s]+$/,''));
  var y = parseInt(r[2].replace(/^[\s]+|[\s]+$/,''));
    
  if (d >= 1 && d <= 32 && m >= 1 && m <= 12 && y >= 1980 && y <= 3000)
    r = new Date(y, m-1, d);
  else
    r = null; 
    
  return r;  
}

function onSelectCalendarDay(cal, date) {
  if (cal.dateClicked) {
      var y = cal.date.getFullYear();
      var m = cal.date.getMonth() + 1;
      var d = cal.date.getDate();
      
      cal.myTextInput.value = '' + d + '. ' + m + '. ' + y;      

	  cal.hide();	
	  
      validateFields();
  }
}

function onCloseCalendar(cal) {
  cal.hide();
}

function displayCalendar() {
  var txt = document.getElementById('pg_pdate');
  
  if (txt.calendar == null) {
    txt.calendar = new Calendar(1, new Date(), onSelectCalendarDay, onCloseCalendar);
    txt.calendar.weekNumbers = false;    
    txt.calendar.showOthers = true;
    txt.calendar.create();
    txt.calendar.myTextInput = txt;
  }
  
  var d = dateFromString(txt.value);
  if (d != null) txt.calendar.setDate(d);

  txt.style.backgroundColor = '';
  txt.calendar.showAtElement(txt);
}

function pg_Reset() {
  var pdiv = document.getElementById('pg_validate_div');
  pdiv.innerHTML = '';
  pdiv.style.display = 'none';  
  document.getElementById('pg_validate_tbl').style.display = (document.all?'block':'table');
}

function pg_Result(msg) {
  if (msg.substr(0,3)=='OK-') {
    msg = msg.substr(3, msg.length) + '.';
  } else if (msg.substr(0,4)=='ERR-') {
    msg = '<span style="color:red"><b><%=Language["ERROR"]%></b>! ' + msg.substr(4, msg.length) + '!</span>';
  } else
    return;
    
  msg =
    '<table border="0" cellpadding="0" cellspacing="0"><tr><td>'+msg+' &nbsp; &nbsp;</td><td>' +
    '<input type="button" value="<%=Language["BUTTON_CONTINUE"]%>" onclick="pg_Reset();" /></td></tr></table>';
  var pdiv = document.getElementById('pg_validate_div');
  pdiv.innerHTML = msg;
}

function pg_Submit() { 
  var datum = document.getElementById('pg_pdate').value;
  var km = document.getElementById('pg_pkm').value;
  var vin = bikeCombo._input.value;
  
  document.getElementById('pg_validate_tbl').style.display = 'none';
  
  var pdiv = document.getElementById('pg_validate_div');
  pdiv.style.display = 'block';
  pdiv.innerHTML = 'Loading <img src="images/wait.gif" style="vertical-align:bottom;margin-bottom:2px" />';    
  
  Ajax.execute(
    'index.aspx?ajax=bike\x26date='+uEscape(datum)+'\x26vin='+uEscape(vin)+'\x26km='+uEscape(km),
    pg_Result
  );
}
<% if (this.MessageNotify>0) { %>

function StartFlashImage(e)
{
	<% if (this.MessageNotify>=2) { %>FlashImageMsg();<% } else { %>toggleMessage(); <% } %>
}

function FlashImageMsg()
{
	stopMessage();
	var img = document.getElementById('imgMessage001');
	displayMessage(img,'<%=GetMessageNotify()%>',false,null,true);	
}

MSG_BUTTON_CLOSE = '<%=Language["BUTTON_CLOSE"]%>';
MSG_MESSAGE = '<%=Language["PROTO_MESSAGE"]%>';

if (window.addEventListener) window.addEventListener('load', StartFlashImage, false);
else if (window.attachEvent) window.attachEvent('onload', StartFlashImage);

<% } %>

//]]>
</script>

</body>

</html>

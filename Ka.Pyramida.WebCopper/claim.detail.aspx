﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="_Claim_Detail" Codebehind="claim.detail.aspx.cs" %>
<%@ Register TagPrefix="yuki" TagName="header" Src="Controls/Header.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Language.Code%>" dir="ltr" lang="<%=Language.Code%>">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><%=Language["APP_TITLE"]%></title>
<link rel="stylesheet" type="text/css" href="css/yuki.css" />
</head>

<body>

<yuki:header Id="MCHeader" LocationID="LOCATION_CLAIM_DETAIL" runat="server" />

<div align="center" style="padding:0px 16px 16px 16px"> 
<table border="0" cellpadding="0" cellspacing="0" align="center" style="width:1%">
<tr>
 <td align="left">
 
<div style="width:800px"> 

<table border="0" cellpadding="0" cellspacing="0" style="width:100%">
<tr>
 <td align="left">
<img src="images/mod.claim.png" alt="" style="vertical-align:text-bottom" />
<span class="mod"><%=Language["HEADER_CLAIM_DETAL"]%></span>
 </td>
 <td align="right" valign="bottom">
   <button type="button" class="main" onclick="location='claim.detail.aspx?id=<%=Request.QueryString["id"]%>\x26pdf=';" style="padding:4px">
    <img src="images/print.pdf.png" alt="" />
    <%=Language["BUTTON_PRINT"]%>
   </button>
 </td>
</tr>
</table>&nbsp;
<br />

<table border="0" cellpadding="4" cellspacing="1" class="list">
<tr class="category">
 <td colspan="2"><%=Language["CLAIM_DETAILS"]%></td>
</tr>
<tr class="even">
 <td align="left" width="20%"><%=Language["CLAIM_LIST_DATE"]%></td>
 <td align="left" class="odd"><%=Claim.Datum.ToString("d'. 'M'. 'yyyy' 'HH':'mm")%></td>
</tr>
<tr class="even">
 <td align="left" width="20%"><%=Language["CLAIM_TITLE"]%></td>
 <td align="left" class="odd"><%=Server.HtmlEncode(Claim.Nazev)%></td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
<br />

<table border="0" cellpadding="4" cellspacing="1" class="list">
<tr class="category">
 <td colspan="7"><%=Language["CLAIM_ITEMS"]%></td>
</tr>
<tr class="header">
 <th style="width:1px">&nbsp;</th>
 <th align="left" nowrap="nowrap"><%=Language["HEADER_PROTOCOL"]%></th>
 <th align="left" nowrap="nowrap"><%=Language["BIKES_LIST_MODEL"]%></th> 
 <th align="left" nowrap="nowrap"><%=Language["CLAIM_LIST_STOCK"]%></th>
 <th align="left" nowrap="nowrap"><%=Language["CLAIM_LIST_TITLE"]%></th>
 <th align="center" nowrap="nowrap"><%=Language["CLAIM_LIST_COUNT"]%></th>
 <th align="left" nowrap="nowrap"><%=Language["CLAIM_LIST_NOTE"]%></th>
</tr>
<%=GetSeznamPolozek()%>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
<br />

<table border="0" cellpadding="2" cellspacing="1" class="list">
<tr class="footer">
 <td align="right">
  <button type="button" class="main" onclick="location='claim.aspx';">
   <img src="images/cancel.png" alt="" />
   <%=Language["BUTTON_CLOSE"]%>
  </button>  
 </td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>

</div>

 </td>
</tr>
</table>
</div>

</body>

</html>
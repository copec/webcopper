﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Text.RegularExpressions;
using Ka.Garance.Data;
using Ka.Garance.Invoice.Yuki;

public partial class _Invoice : Ka.Garance.Web.GarancePage
{
	protected int noInvoices = 0;

	protected void Page_Load(object sender, EventArgs e)
	{
		switch (Request.QueryString["state"])
		{
			case "1":
				TabItem1.Selected = true;
				break;
			case "2":
				TabItem2.Selected = true;
				break;
			case "3":
				TabItem3.Selected = true;
				break;
		}

		string filterAdd = "";
		if (TabItem2.Selected) filterAdd = "A.stav IN (4,5,7)";
		else if (TabItem3.Selected) filterAdd = "A.stav=6";
		else filterAdd = "A.stav>=4";

		bool isSKDealer = User.IdZeme == 2;

		MCInvoiceList.SQLCount = string.Format("SELECT COUNT(DISTINCT A.cislofa) FROM tprotocol A WHERE A.iddealer={0} AND " + filterAdd + " AND A.cislofa IS NOT NULL", User.Id);
		MCInvoiceList.SQL = @"
SELECT
	A.cislofa AS cislofa,
	A.datumfa AS datum,
	IF(A.datumfa<'" + Ka.Garance.Protocol.Yuki.YukiProtocol.DATE_NEW_RECOUNT.ToString("yyyy'-'MM'-'dd") + @"',
		SUM(ROUND(B.material * (1 + (A.dphfa / 100)) * (1 - (A.rabatfa / 100))," + (isSKDealer ? "IF(A.timestamp>='" + Ka.Garance.Protocol.Yuki.YukiProtocol.DATE_SK_EURO.ToString("yyyy'-'MM'-'dd") + "',2,1)" : "1") + @")),
		IF(A.datumfa<'" + Ka.Garance.Protocol.Yuki.YukiProtocol.DATE_NEW_RECOUNT_2012.ToString("yyyy'-'MM'-'dd") + @"',
            SUM(ROUND(B.material * 0.75 * (1 + (A.dphfa / 100)) * (1 + (A.rabatfa / 100))," + (isSKDealer ? "IF(A.timestamp>='" + Ka.Garance.Protocol.Yuki.YukiProtocol.DATE_SK_EURO.ToString("yyyy'-'MM'-'dd") + "',2,1)" : "1") + @")),
            IF(A.datumfa<'" + Ka.Garance.Protocol.Yuki.YukiProtocol.DATE_NEW_RECOUNT_2012_10.ToString("yyyy'-'MM'-'dd") + @"',
                SUM(ROUND(B.material * 0.35 * (1 + (A.dphfa / 100)) * (1 + (A.rabatfa / 100))," + (isSKDealer ? "IF(A.timestamp>='" + Ka.Garance.Protocol.Yuki.YukiProtocol.DATE_SK_EURO.ToString("yyyy'-'MM'-'dd") + "',2,1)" : "1") + @")),
                IF(A.datumfa<'" + Ka.Garance.Protocol.Yuki.YukiProtocol.DATE_NEW_RECOUNT_2013.ToString("yyyy'-'MM'-'dd") + @"',
                    SUM(ROUND(B.material * 0.605 * (1 + (A.dphfa / 100)) * (1 + (A.rabatfa / 100))," + (isSKDealer ? "IF(A.timestamp>='" + Ka.Garance.Protocol.Yuki.YukiProtocol.DATE_SK_EURO.ToString("yyyy'-'MM'-'dd") + "',2,1)" : "1") + @")),
                    SUM(ROUND(B.material * 0.7 * (1 + (A.dphfa / 100)) * (1 + (A.rabatfa / 100))," + (isSKDealer ? "IF(A.timestamp>='" + Ka.Garance.Protocol.Yuki.YukiProtocol.DATE_SK_EURO.ToString("yyyy'-'MM'-'dd") + "',2,1)" : "1") + @"))
                )
            )
        )
	) + SUM(ROUND(B.prace * (1 + (A.dphfa / 100))," + (isSKDealer ? "IF(A.timestamp>='" + Ka.Garance.Protocol.Yuki.YukiProtocol.DATE_SK_EURO.ToString("yyyy'-'MM'-'dd") + "',2,1)" : "1") + @")) * 1.00 AS cena,
	A.mena AS mena
FROM tprotocol A
LEFT JOIN tzavada B ON B.cisloprotokol=A.cislo AND B.stav=1
";
		MCInvoiceList.Filter = string.Format("A.iddealer={0} AND " + filterAdd + " AND A.cislofa IS NOT NULL GROUP BY A.cislofa", User.Id);

		ListItem2.Validate += new Ka.Garance.Web.List.ValidateListItem(ListItem2_Validate);
		ListItem3.Validate += new Ka.Garance.Web.List.ValidateListItem(ListItem3_Validate);

		this.msg1.Text = string.Format("{0}", Database.ExecuteScalar(MCInvoiceList.SQLCount));
		this.msg2.Text = GetComputedSums();
	}

	private void ListItem2_Validate(object sender, Ka.Garance.Web.List.ValidateEventArgs e)
	{
		try
		{
			DateTime dt = Convert.ToDateTime(e.Value);
			if (dt < DateTime.MinValue.AddYears(5))
			{
				e.Value = "";
				e.Validated = true;
			}
		}
		catch { }
	}

	private void ListItem3_Validate(object sender, Ka.Garance.Web.List.ValidateEventArgs e)
	{
		if (e.Value is DBNull || e.Value == null) e.Value = (decimal)0;
		e.Validated = true;
		e.Value = string.Format("{0:F2} {1}", e.Value, e.Reader.GetValue("mena"));
	}

	private string GetComputedSums()
	{
		StringBuilder sb = new StringBuilder();
		bool isSKDealer = User.IdZeme == 2;

		sb.AppendFormat(@"
SELECT
	IF(A.datumfa<'" + Ka.Garance.Protocol.Yuki.YukiProtocol.DATE_NEW_RECOUNT.ToString("yyyy'-'MM'-'dd") + @"',
		SUM(ROUND(B.material * (1 + (A.dphfa / 100)) * (1 - (A.rabatfa / 100))," + (isSKDealer ? "IF(A.timestamp>='" + Ka.Garance.Protocol.Yuki.YukiProtocol.DATE_SK_EURO.ToString("yyyy'-'MM'-'dd") + "',2,1)" : "1") + @")),
		IF(A.datumfa<'" + Ka.Garance.Protocol.Yuki.YukiProtocol.DATE_NEW_RECOUNT_2012.ToString("yyyy'-'MM'-'dd") + @"',
            SUM(ROUND(B.material * 0.75 * (1 + (A.dphfa / 100)) * (1 + (A.rabatfa / 100))," + (isSKDealer ? "IF(A.timestamp>='" + Ka.Garance.Protocol.Yuki.YukiProtocol.DATE_SK_EURO.ToString("yyyy'-'MM'-'dd") + "',2,1)" : "1") + @")),
            IF(A.datumfa<'" + Ka.Garance.Protocol.Yuki.YukiProtocol.DATE_NEW_RECOUNT_2012_10.ToString("yyyy'-'MM'-'dd") + @"',
                SUM(ROUND(B.material * 0.35 * (1 + (A.dphfa / 100)) * (1 + (A.rabatfa / 100))," + (isSKDealer ? "IF(A.timestamp>='" + Ka.Garance.Protocol.Yuki.YukiProtocol.DATE_SK_EURO.ToString("yyyy'-'MM'-'dd") + "',2,1)" : "1") + @")),
                IF(A.datumfa<'" + Ka.Garance.Protocol.Yuki.YukiProtocol.DATE_NEW_RECOUNT_2013.ToString("yyyy'-'MM'-'dd") + @"',
                    SUM(ROUND(B.material * 0.605 * (1 + (A.dphfa / 100)) * (1 + (A.rabatfa / 100))," + (isSKDealer ? "IF(A.timestamp>='" + Ka.Garance.Protocol.Yuki.YukiProtocol.DATE_SK_EURO.ToString("yyyy'-'MM'-'dd") + "',2,1)" : "1") + @")),
                    SUM(ROUND(B.material * 0.7 * (1 + (A.dphfa / 100)) * (1 + (A.rabatfa / 100))," + (isSKDealer ? "IF(A.timestamp>='" + Ka.Garance.Protocol.Yuki.YukiProtocol.DATE_SK_EURO.ToString("yyyy'-'MM'-'dd") + "',2,1)" : "1") + @"))
                )
            )
        )
	)
	+ SUM(ROUND(B.prace * (1 + (A.dphfa / 100))," + (isSKDealer ? "IF(A.timestamp>='" + Ka.Garance.Protocol.Yuki.YukiProtocol.DATE_SK_EURO.ToString("yyyy'-'MM'-'dd") + "',2,1)" : "1") + @")) * 1.00 AS cena,
	A.mena AS mena
FROM tprotocol A
LEFT JOIN tzavada B ON B.cisloprotokol=A.cislo AND B.stav=1
WHERE A.iddealer={0} AND A.stav>=4 AND A.cislofa IS NOT NULL GROUP BY A.mena;
", User.Id);

		MySQLDataReader dr = Database.ExecuteReader(sb.ToString());

		if (dr.Count == 0)
			return "N/A";

		sb.Length = 0;
		sb.AppendFormat("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n<tbody>\n");
		while (dr.Read())
		{
			object mena = dr.GetValue(1);
			if (mena is DBNull) mena = "";
			object cena = dr.GetValue(0);
			if (cena is DBNull || cena == null) cena = (decimal)0;
			else cena = User.FormatPrice(cena, false);
			sb.AppendFormat("<tr><td><b>{0}</b></td><td>&nbsp;</td><td><b>{1}</b></td></tr>\n", cena, mena);
		}
		sb.AppendFormat("</tbody>\n</table>\n");
		return sb.ToString();
	}
}

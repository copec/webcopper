﻿using System;
using System.Collections.Specialized;
using System.Text;
using Ka.Garance.Data;
using Ka.Garance.Protocol.Yuki;

public partial class _Invoice_Add : Ka.Garance.Web.GarancePage
{
	private const string PCHECK_STEP1 = "ghf4678vk_fh.35x";
	private const string PCHECK_STEP2 = "bHJ68.djj__jgdg";

	private int m_ActualStep = 1;
	public int ActualStep
	{
		get { return m_ActualStep; }
	}

	private string m_CisloFaktury;
	public string CisloFaktury
	{
		get { return m_CisloFaktury; }
	}

    protected void Page_Load(object sender, EventArgs e)
    {
		MCHeader.Location = Language["LOCATION_INVOICE_ADD"];

		if (Request.HttpMethod == "POST")
		{
			switch (Request.Form["pcheck"])
			{
				case PCHECK_STEP1:
					ValidateStep1();
					break;
				case PCHECK_STEP2:
					ValidateStep2();
					break;
			}
		}
		else if (Request.QueryString["fa"] != null)
		{
			m_ActualStep = 3;
		}

    }

	private NameValueCollection m_Errors = new NameValueCollection(); 
	private void ValidateStep1()
	{
		m_Errors.Clear();
		string s = Request.Form["iproto"];
		if (s == null || s.Trim() == "")
		{
			m_ActualStep = 1;
			m_Errors.Add("proto", Language["INVOICE_ERR_NOPROTO"]);
			return;
		}
		else
		{
			string[] iproto = Request.Form["iproto"].Split(",".ToCharArray());
			foreach(string protocolcislo in iproto)
				Ka.Garance.Protocol.Yuki.YukiProtocol.UpdateZavadaPrice(Database, protocolcislo);
		}


		m_ActualStep = 2;
	}

	private void ValidateStep2()
	{
		ValidateStep1();
		m_ActualStep = (m_Errors.Count == 0 ? 3 : 2);
		if (m_ActualStep == 3)
			VystavitFakturu();
	}

	private void VystavitFakturu()
	{
		// nastav priznaky v DB
		string[] iproto = Request.Form["iproto"].Split(",".ToCharArray());
		for (int i = 0; i < iproto.Length; i++)
		{
			iproto[i] = MySQLDatabase.Escape(iproto[i]);
		}

		// skus zistit, ci uz boli polozky vyfakturovane ...
		MySQLDataReader dr = Database.ExecuteReader(string.Format("SELECT cislofa FROM tprotocol WHERE cislo={0} AND iddealer={1} AND stav=4 LIMIT 1", iproto[0], User.Id));
		if (dr.Read())
		{
			m_CisloFaktury = Convert.ToString(dr.GetValue("cislofa"));
			return;
		}

		// polozky nie su vyfakturovane, vytvor novu FA
		m_CisloFaktury = User.GenerateCisloFa();
		Database.Execute(string.Format(
			"UPDATE tprotocol SET cislofa={{0}},datumfa=NOW(),splatnostfa={{2}},stav=4 WHERE iddealer={{1}} AND stav=3 AND cislo IN ('{0}')",
			string.Join("','", iproto)
		), m_CisloFaktury, User.Id, DateTime.Now.AddDays(30));
        
        // nezabudnut zvysit cislo fa
        User.IncrementCisloFa();

		Database.Execute(
			"UPDATE tprotocol SET dphfa={0},rabatfa={1} WHERE stav=4 AND iddealer={2} AND cislofa={3}", // AND dphfa IS NULL (ale pozor! musia byt faktury s rovnakym cislom s rovnakou DPH !!!)
			(int)User.DPH, (int)User.Rabat, User.Id, m_CisloFaktury
		);

		

		Response.Redirect(string.Format("invoice.add.aspx?fa={0}", m_CisloFaktury), true);
	}

	private string GenerateErrorHTML()
	{
		if (m_Errors.Count == 0) return "";

		StringBuilder sb = new StringBuilder();
		sb.Append("<ul>\n<li>");
		for (int i = 0; i < m_Errors.Count; i++)
		{
			if (i > 0) sb.Append("</li>\n<li>");
			sb.Append(m_Errors[i]);
		}
		sb.Append("</li>\n</ul>");

		return string.Format(@"
<table border=""0"" cellpadding=""4"" cellspacing=""1"" class=""list"">
<tr class=""category""><td><b style=""color:red"">{0}</b></td></tr>
<tr>
 <td style=""background-color:#FFFFE2"">
{1}
 </td>
</tr>
</table>
<br />
", Language["ERROR_PROCESSING_DATA"], sb.ToString());
	}

	/*
	private string GetNoveCisloFa()
	{
		// skus zistit, ci je zadane cislovane v DB
		MySQLDataReader dr = Database.ExecuteReader("SELECT cislofa FROM tdealer WHERE id={0} AND cislofa IS NOT NULL", User.Id);
		if (dr.Read())
		{
			string fa = string.Format("{0}", dr.GetIntValue(0) + 1);
			// ak uz je pouzite v tprotocol tak najdi dalsie !!!
			if (Convert.ToInt32(Database.ExecuteScalar("SELECT COUNT(*) FROM tprotocol WHERE cislofa={0}", fa)) == 0)
				return fa;
		}
		string y = DateTime.Now.Year.ToString();
		return string.Format("{1}{0:D4}", Database.ExecuteScalar("SELECT IFNULL(MAX(CAST(RIGHT(cislofa,4) AS SIGNED)),0)+1 FROM tprotocol WHERE LEFT(cislofa,4)={0} AND iddealer={1}", y, User.Id), y);
	}
	*/

	private string GenerateSeznam1()
	{
		StringBuilder sb = new StringBuilder();

		sb.AppendFormat(@"
<table border=""0"" cellpadding=""4"" cellspacing=""1"" class=""list"">
<tr class=""header""><th align=""left"">1 / 2 - {0}</th></tr>
</table>
<div style=""padding:0px 3px 0px 3px""><img src=""images/bk.shadow.png"" width=""100%"" height=""1"" /></div>
<br />", Language["INVOICE_NEW_SELECT"]);

		sb.Append(GenerateErrorHTML());

		MySQLDataReader dr = Database.ExecuteReader(@"
			SELECT
				cislo,
				karoserie,
				CAST(IF(IFNULL(datumoprava,'')='',NULL,CONCAT('20',MID(datumoprava,5,2),'-',MID(datumoprava,3,2),'-',LEFT(datumoprava,2))) AS DATE) AS datum,
				timestamp AS vytvoreno
			FROM tprotocol
			WHERE iddealer={0} AND stav=3
			ORDER BY datum,vytvoreno", User.Id
		);

		sb.Append("\n\n<table border=\"0\" cellpadding=\"4\" cellspacing=\"1\" class=\"list\">\n");
		sb.AppendFormat("<tr class=\"category\"><td colspan=\"5\">{0}</td></tr>\n", Language["INVOICE_PROTO_LIST"]);
		sb.Append("<tr class=\"header\">\n");
		sb.AppendFormat("<th align=\"left\">{0}</th>\n", Language["PROTO_LIST_NUMBER"]);
		sb.AppendFormat("<th align=\"left\">{0}</th>\n", Language["PROTO_LIST_CHASSIS"]);
		sb.AppendFormat("<th align=\"left\">{0}</th>\n", Language["PROTO_LIST_DATE"]);
		sb.AppendFormat("<th align=\"left\">{0}</th>\n", Language["PROTO_LIST_DATE_CREATED"]);
		if (dr.Count > 0)
			sb.Append("<th style=\"width:1px\"><input type=\"checkbox\" style=\"margin:0px 2px 0px 2px;padding:0px;height:14px\" onclick=\"invoice_SelectAll(this);\" /></th>\n");
		else
			sb.Append("<th style=\"width:1px\">&nbsp;</th>\n");
		sb.Append("</tr>\n");


		while (dr.Read())
		{
			sb.AppendFormat("<tr class=\"{0}\" style=\"cursor:pointer\" onmouseover=\"if(!this.h)this.h=this.className;this.className=this.h+'-active';\" onmouseout=\"this.className=this.h;\" onclick=\"invoice_SelectProtocol(this,null);\">\n", (dr.Index % 2 == 0 ? "even" : "odd"));
			sb.AppendFormat("<td align=\"left\">{0}</td>\n", dr.GetValue("cislo"));
			sb.AppendFormat("<td align=\"left\">{0}</td>\n", dr.GetValue("karoserie"));
			sb.AppendFormat("<td align=\"left\">{0:d'. 'M'. 'yyyy}</td>\n", dr.GetValue("datum"));
			sb.AppendFormat("<td align=\"left\">{0:d'. 'M'. 'yyyy' 'HH':'mm}</td>\n", dr.GetValue("vytvoreno"));
			sb.AppendFormat("<td style=\"width:1px\"><input type=\"checkbox\" name=\"iproto\" value=\"{1}\" style=\"cursor:pointer;margin:0px 2px 0px 2px;padding:0px;height:14px\" onmouseover=\"invoice_MouseIn(this);\" onclick=\"invoice_SelectProtocol(null,this,{0});\" /></td>\n", dr.Index, dr.GetValue("cislo"));
			sb.Append("</tr>\n");
		}

		if (dr.Count == 0)
		{
			sb.AppendFormat("<tr class=\"info\"><td colspan=\"5\">{0}</td></tr>", Language["INVOICE_NOPROTO"]);
		}

		sb.Append("</table>\n<div style=\"padding:0px 3px 0px 3px\"><img src=\"images/bk.shadow.png\" width=\"100%\" height=\"1\" /></div>\n<br />\n\n");

		sb.Append("<table border=\"0\" cellpadding=\"2\" cellspacing=\"1\" class=\"list\">\n<tr class=\"footer\">\n <td align=\"right\">\n");
		sb.AppendFormat("<input type=\"hidden\" name=\"pcheck\" value=\"{0}\" />\n", PCHECK_STEP1);
		sb.AppendFormat("<input id=\"btn_invoice_1\" type=\"submit\" name=\"next\" disabled=\"disabled\" value=\"{0} &raquo;\" />\n", Language["BUTTON_NEXT"]);
		sb.Append(" </td>\n</tr>\n");
		sb.Append("</table>\n<div style=\"padding:0px 3px 0px 3px\"><img src=\"images/bk.shadow.png\" width=\"100%\" height=\"1\" /></div>");

		return sb.ToString();
	}

	private string GenerateSeznam2()
	{
		StringBuilder sb = new StringBuilder();

		sb.AppendFormat(@"
<table border=""0"" cellpadding=""4"" cellspacing=""1"" class=""list"">
<tr class=""header""><th align=""left"">2 / 2 - {0}</th></tr>
</table>
<div style=""padding:0px 3px 0px 3px""><img src=""images/bk.shadow.png"" width=""100%"" height=""1"" /></div>
<br />", Language["INVOICE_NEW_CONFIRM"]);

		m_CisloFaktury = User.GenerateCisloFa();
		string[] iproto = Request.Form["iproto"].Split(",".ToCharArray());
		for (int i = 0; i < iproto.Length; i++)
		{
			iproto[i] = MySQLDatabase.Escape(iproto[i]);
		}

		sb.Append(GenerateErrorHTML());

		sb.Append("\n\n<table border=\"0\" cellpadding=\"4\" cellspacing=\"1\" class=\"list\">\n");
		sb.AppendFormat("<tr class=\"category\"><td colspan=\"4\">{0}</td></tr>\n", Language["INVOICE_PROTO_LIST2"]);
		sb.Append("<tr class=\"header\">\n");
		sb.AppendFormat("<th align=\"left\">{0}</th>\n", Language["PROTO_LIST_NUMBER"]);
		sb.AppendFormat("<th align=\"left\">{0}</th>\n", Language["PROTO_LIST_CHASSIS"]);
		sb.AppendFormat("<th align=\"left\">{0}</th>\n", Language["PROTO_LIST_DATE"]);
		sb.AppendFormat("<th align=\"left\">{0}</th>\n", Language["PROTO_LIST_DATE_CREATED"]);
		sb.Append("</tr>\n");

		DateTime dtCreated = DateTime.MinValue;
		MySQLDataReader dr = Database.ExecuteReader(string.Format(@"
			SELECT
				cislo,
				karoserie,
				CAST(IF(IFNULL(datumoprava,'')='',NULL,CONCAT('20',MID(datumoprava,5,2),'-',MID(datumoprava,3,2),'-',LEFT(datumoprava,2))) AS DATE) AS datum,
				timestamp AS vytvoreno
			FROM tprotocol
			WHERE iddealer={{0}} AND stav=3 AND cislo IN ('{0}')
			ORDER BY datum,vytvoreno", string.Join("','", iproto)), User.Id
		);
		while (dr.Read())
		{
			sb.AppendFormat("<tr class=\"{0}\">\n", (dr.Index % 2 == 0 ? "even" : "odd"));
			sb.AppendFormat("<td align=\"left\">{0}</td>\n", dr.GetValue("cislo"));
			sb.AppendFormat("<td align=\"left\">{0}</td>\n", dr.GetValue("karoserie"));
			sb.AppendFormat("<td align=\"left\">{0:d'. 'M'. 'yyyy}</td>\n", dr.GetValue("datum"));
			sb.AppendFormat("<td align=\"left\">{0:d'. 'M'. 'yyyy' 'HH':'mm}</td>\n", dr.GetValue("vytvoreno"));
			sb.Append("</tr>\n");
			dtCreated = Convert.ToDateTime(dr.GetValue("vytvoreno"));
		}

		sb.Append("</table>\n<div style=\"padding:0px 3px 0px 3px\"><img src=\"images/bk.shadow.png\" width=\"100%\" height=\"1\" /></div>\n<br />\n\n");

		// ceny
		bool isSKDealer = User.IdZeme == 2 && dtCreated >= YukiProtocol.DATE_SK_EURO;
		decimal cenaM = 0.0m, cenaP = 0.0m, cenaM_DPH = 0.0m, cenaP_DPH = 0.0m;

		// poriadne spocitat ceny
		dr = Database.ExecuteReader(string.Format("SELECT A.material AS m,A.prace AS p,B.datumfa AS datumfa FROM tzavada A INNER JOIN tprotocol B ON B.cislo=A.cisloprotokol WHERE B.iddealer={{0}} AND B.stav=3 AND B.cislo IN ('{0}')", string.Join("','", iproto)), User.Id);
		
        decimal dealerPriceReduction = 0.75m;
        decimal dealerPriceReduction2012 = 0.35m;
        decimal dealerPriceReduction2012_10 = 0.605m;
        decimal dealerPriceReduction2013 = 0.7m;


        while (dr.Read())
		{
			bool useOldCalc = false;
            bool useNew2013Calc = false;
            bool useNew2012_10Calc = false;
            bool useNew2012Calc = false;

            if (dtCreated < Ka.Garance.Protocol.Yuki.YukiProtocol.DATE_NEW_RECOUNT)
                useOldCalc = true;
            else if (dtCreated >= Ka.Garance.Protocol.Yuki.YukiProtocol.DATE_NEW_RECOUNT_2013)
                useNew2013Calc = true;
            else if (dtCreated >= Ka.Garance.Protocol.Yuki.YukiProtocol.DATE_NEW_RECOUNT_2012_10)
                useNew2013Calc = true;
            else if (dtCreated >= Ka.Garance.Protocol.Yuki.YukiProtocol.DATE_NEW_RECOUNT_2012)
                useNew2012Calc = true;            

			decimal c = Convert.ToDecimal(dr.GetValue("m"));
			cenaM += c;
			if (useOldCalc)
				cenaM_DPH += (decimal)Math.Round(c * ((100 - User.Rabat) / 100) * ((100 + User.DPH) / 100), isSKDealer ? 2 : 1);
			else if (useNew2012Calc)
                cenaM_DPH += (decimal)Math.Round(c * dealerPriceReduction2012 * ((100 + User.Rabat) / 100) * ((100 + User.DPH) / 100), isSKDealer ? 2 : 1);
            else if (useNew2012_10Calc)
                cenaM_DPH += (decimal)Math.Round(c * dealerPriceReduction2012_10 * ((100 + User.Rabat) / 100) * ((100 + User.DPH) / 100), isSKDealer ? 2 : 1);
            else if (useNew2013Calc)
                cenaM_DPH += (decimal)Math.Round(c * dealerPriceReduction2013 * ((100 + User.Rabat) / 100) * ((100 + User.DPH) / 100), isSKDealer ? 2 : 1);
            else
                cenaM_DPH += (decimal)Math.Round(c * dealerPriceReduction * ((100 + User.Rabat) / 100) * ((100 + User.DPH) / 100), isSKDealer ? 2 : 1);
			c = (decimal)Convert.ToDouble(dr.GetValue("p"));
			cenaP += c;
			cenaP_DPH += /*(decimal)*/Math.Round(c * (100 + User.DPH) / 100, isSKDealer ? 2 : 1);
		}

		// platebni info
		sb.Append("\n\n<table border=\"0\" cellpadding=\"4\" cellspacing=\"1\" class=\"list\">\n");
		sb.AppendFormat("<tr class=\"category\"><td colspan=\"{1}\" align=\"left\">{0}</td></tr>\n", Language["INVOICE_PAYMENT_INFO"], (User.PlatceDPH?4:3));
		sb.AppendFormat("<tr class=\"even\">\n <td width=\"25%\">{0}:</td>\n <td class=\"odd\">{1}</td>\n", Language["INVOICE_NO"], m_CisloFaktury);
		if (User.PlatceDPH)
			sb.AppendFormat(" <td align=\"right\" width=\"10%\">{0}</td>", Language["INVOICE_NO_TAX"]);
		sb.AppendFormat(" <td align=\"right\" width=\"10%\">{0}</td>", Language["INVOICE_TAX"]);
		sb.Append("</tr>\n");

		sb.AppendFormat("<tr class=\"even\">\n <td colspan=\"2\">{0}:</td>\n", Language["INVOICE_TOTAL_MATERIAL"]);
        decimal cenaMBezDPH = 0;
        if (User.PlatceDPH)
        {
            cenaMBezDPH = cenaM_DPH / ((100 + User.DPH) / 100);
            sb.AppendFormat(" <td class=\"odd\" align=\"right\">{0:F2}</td>\n", Math.Round(cenaMBezDPH, isSKDealer ? 2 : 1));
        }
		sb.AppendFormat(" <td class=\"odd\" align=\"right\">{0:F2}</td>\n", Math.Round(cenaM_DPH, isSKDealer ? 2 : 1));
		sb.Append("</tr>\n");

		sb.AppendFormat("<tr class=\"even\">\n <td colspan=\"2\">{0}:</td>\n", Language["INVOICE_TOTAL_WORK"]);
		if (User.PlatceDPH)
			sb.AppendFormat(" <td class=\"odd\" align=\"right\">{0:F2}</td>\n", Math.Round(cenaP, isSKDealer ? 2 : 1));
		sb.AppendFormat(" <td class=\"odd\" align=\"right\">{0:F2}</td>\n", Math.Round(cenaP_DPH, isSKDealer ? 2 : 1));
		sb.Append("</tr>\n");

		sb.AppendFormat("<tr class=\"footer\"><td colspan=\"{0}\" style=\"padding:0px;margin:0px\"><div style=\"height:1px\">&nbsp;</div></td></tr>\n", (User.PlatceDPH?4:3));

		sb.AppendFormat("<tr class=\"even\">\n <td colspan=\"2\">{0}:</td>\n", Language["INVOICE_TOTAL"]);
		if (User.PlatceDPH)
            sb.AppendFormat(" <td class=\"odd\" align=\"right\">{0:F2}</td>\n", Math.Round(cenaMBezDPH, isSKDealer ? 2 : 1) + cenaP);
		sb.AppendFormat(" <td class=\"odd\" align=\"right\">{0:F2}</td>\n", cenaM_DPH + cenaP_DPH);
		sb.Append("</tr>\n");

		sb.Append("</table>\n<div style=\"padding:0px 3px 0px 3px\"><img src=\"images/bk.shadow.png\" width=\"100%\" height=\"1\" /></div>\n<br />\n\n");

		sb.Append("<table border=\"0\" cellpadding=\"4\" cellspacing=\"1\" style=\"width:100%\">\n");
		sb.AppendFormat("<tr class=\"invoice-total\"><td align=\"right\">{0}</td><td width=\"25%\" align=\"right\">{1:F2}</td></tr>\n", Language["INVOICE_TOTAL_CURRENCY"].Replace("%s", User.Currency), cenaM_DPH + cenaP_DPH);
		sb.Append("</table>\n<br />\n\n");

		sb.Append("<table border=\"0\" cellpadding=\"2\" cellspacing=\"1\" class=\"list\">\n<tr class=\"footer\">\n <td align=\"right\">\n");
		sb.Append("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:100%\">\n<tr>\n <td align=\"left\">\n");
		sb.AppendFormat("<input type=\"hidden\" name=\"iproto\" value=\"{0}\" />\n", Server.HtmlEncode(Request.Form["iproto"]));
		sb.AppendFormat("<input type=\"hidden\" name=\"pcheck\" value=\"{0}\" />", PCHECK_STEP2);
		sb.AppendFormat(@"
  <button type=""submit"" name=""fileinvoice"" class=""main"" style=""font-weight:bold"">
   <img src=""images/ok.png"" alt="""" />
   {0}
  </button>", Language["INVOICE_BUTTON_OK"]);
		sb.Append("\n </td>\n <td align=\"right\">\n");
		sb.AppendFormat("<input type=\"button\" value=\"&laquo; {0}\" onclick=\"location='invoice.add.aspx';\" />\n", Language["BUTTON_BACK"]);
		sb.Append(" </td>\n</tr>\n");
		sb.Append("</table>");
		sb.Append(" </td>\n</tr>\n");
		sb.Append("</table>\n<div style=\"padding:0px 3px 0px 3px\"><img src=\"images/bk.shadow.png\" width=\"100%\" height=\"1\" /></div>");

		return sb.ToString();
	}

	private string GenerateSeznam3()
	{
		StringBuilder sb = new StringBuilder();

		sb.AppendFormat(@"
<table border=""0"" cellpadding=""4"" cellspacing=""1"" class=""list"">
<tr class=""header""><th align=""left"">2 / 2 - {0}</th></tr>
</table>
<div style=""padding:0px 3px 0px 3px""><img src=""images/bk.shadow.png"" width=""100%"" height=""1"" /></div>
<br />", Language["INVOICE_NEW_CONFIRM"]);

		sb.AppendFormat(@"
<table border=""0"" cellpadding=""4"" cellspacing=""1"" class=""list"">
<tr class=""category"">
 <td>{0}</td>
</tr>
<tr class=""odd"">
 <td align=""center"">
  <div style=""padding:32px 8px 32px 8px"">
   {1}<br /><br />
   <button type=""button"" class=""main"" onclick=""location='invoice.detail.aspx?fa={2}\x26pdf=';"" style=""padding:4px;width:100px"">
    <img src=""images/print.pdf.png"" alt="""" />
    {3}
   </button>
   <button type=""button"" class=""main"" onclick=""location='invoice.detail.aspx?fa={2}\x26pdf=letter';"" style=""padding:4px;width:150px"">
    <img src=""images/print.pdf.png"" alt="""" />
    {4}
   </button>
  </div>
 </td>
</tr>
</table>
<div style=""padding:0px 3px 0px 3px""><img src=""images/bk.shadow.png"" width=""100%"" height=""1"" /></div>
<br />", Language["INVOICE_NEW_CONFIRM"], Language["INVOICE_NEW_INFO"], Request.QueryString["fa"], Language["INVOICE_PRINT_INVOICE"], Language["INVOICE_PRINT_LETTER"]);

		sb.AppendFormat(@"
<table border=""0"" cellpadding=""2"" cellspacing=""1"" class=""list"">
<tr class=""footer"">
 <td align=""left"">
<table border=""0"" cellpadding=""0"" cellspacing=""0"" style=""width:100%"">
<tr>
 <td>
  <button type=""button"" class=""main"" onclick=""location='invoice.add.aspx';"">
   <img src=""images/newproto.png"" alt="""" />
   {0}
  </button>
 </td>
 <td align=""right"">
  <button type=""button"" class=""main"" onclick=""location='index.aspx';"">
   <img src=""images/cancel.png"" />
   {1}
  </button> 
 </td>
</tr> 
</table>
 </td>
</tr>
</table>
<div style=""padding:0px 3px 0px 3px""><img src=""images/bk.shadow.png"" width=""100%"" height=""1"" /></div>
", Language["HEADER_INVOICE_ADD"], Language["BUTTON_CLOSE"]);

		return sb.ToString();
	}

	public string GenerateSeznamHTML()
	{
		switch (ActualStep)
		{
			case 1: return GenerateSeznam1();
			case 2: return GenerateSeznam2();
			case 3: return GenerateSeznam3();
		}

		return "";
	}

}

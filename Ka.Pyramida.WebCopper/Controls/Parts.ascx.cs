﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Ka.Garance.Data;
using Ka.Garance.Util;

public partial class Controls_Parts : Ka.Garance.Web.GaranceControl
{
	private string m_Model = "";
	private string m_Vzor = "";
	private string m_Group = "";
	private string m_ModelOptions = "";
	private string m_GroupOptions = "";

	public string Title
	{
		get
		{
			if (IsGroupSpecified)
				return string.Format("{0} - {1}", m_Model, m_Group);
			else
				return m_Model;
		}
	}

	public string ImagePath
	{
		get { return string.Format("nd/{0}/{1}.jpg", m_Vzor, m_Group); }
	}

	public int ImageWidth
	{
		get
		{
			if (IsImageAvailable)
			{
				try
				{
					System.Drawing.Image img = System.Drawing.Image.FromFile(Request.MapPath(ImagePath));
					int w = Math.Min(794, img.Width);
					img.Dispose();
					return w;
				}
				catch { }				
			}
			return 794;
		}
	}

	public bool IsGroupSpecified
	{
		get { return (m_Group != null && m_Group != ""); }
	}

	public bool IsImageAvailable
	{
		get { return System.IO.File.Exists(Request.MapPath(ImagePath)); }
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		if (Request.QueryString.Count == 0)
		{
			(Page as Ka.Garance.Web.GarancePage).User.Cache["parts.model"] = null;
			(Page as Ka.Garance.Web.GarancePage).User.Cache["parts.group"] = null;
		}
		if (Request.QueryString["model"] != null)
			(Page as Ka.Garance.Web.GarancePage).User.Cache["parts.model"] = Request.QueryString["model"];
		if (Request.QueryString["group"] != null)
			(Page as Ka.Garance.Web.GarancePage).User.Cache["parts.group"] = Request.QueryString["group"];
		m_Model = Convert.ToString((Page as Ka.Garance.Web.GarancePage).User.Cache["parts.model"]);
		m_Group = Convert.ToString((Page as Ka.Garance.Web.GarancePage).User.Cache["parts.group"]);
		m_Vzor = GaranceUtil.GetPartsVzor(m_Model, Database);
		
        m_ModelOptions = GaranceUtil.GetPartsModelOptions(ref m_Model, Database, Language);
		
        if (m_Model == null) m_Model = "";
		if (IsGroupSpecified)
		{
			m_GroupOptions = GaranceUtil.GetPartsGroupOptions(ref m_Group, m_Vzor, Database, Language);
		}
		if (m_Group == null) m_Group = "";
	}

	private void RenderPartsFilter(HtmlTextWriter writer)
	{
		TabItem1.LangID = "PARTS_FILTER";
		//MCSpecsTabs.RenderControl(writer);

		writer.Write("<table border=\"0\" cellpadding=\"4\" cellspacing=\"1\" class=\"list\">\n");
		writer.Write("<tr class=\"category\"><td colspan=\"2\">{0}</td></tr>\n", Language["PARTS_FILTER"]);

		// model
		writer.Write(
			"<tr class=\"even\">\n <td width=\"10%\">{0}</td>\n <td class=\"odd\">\n<select style=\"width:300px\" onchange=\"location='parts.aspx?model='+this.value+'\\x26group=';\">\n{1}</select>\n </td>\n</tr>\n",
			Language["PARTS_MODEL"],
			m_ModelOptions
		);

		// group
		if (IsGroupSpecified)
		{
			writer.Write(
				"<tr class=\"even\">\n <td width=\"10%\">{0}</td>\n <td class=\"odd\">\n<select style=\"width:300px\" onchange=\"location='parts.aspx?group='+this.value;\">\n{1}</select>\n </td>\n</tr>\n",
				Language["PARTS_GROUP"],
				m_GroupOptions
			);
		}

		writer.Write("</table>\n<div style=\"padding:0px 3px 0px 3px\"><img src=\"images/bk.shadow.png\" width=\"100%\" height=\"1\" /></div>\n<br />\n\n");
	}

	private void RenderPartsListGroup(HtmlTextWriter writer)
	{		
		TabItem1.LangID = "PARTS_LIST";
		//MCSpecsTabs.RenderControl(writer);
		//writer.Write("\n");

		MCGroupList.CategoryTitle = Language["PARTS_LIST"];
		MCGroupList.Filter = string.Format("state<>9 AND skupina IS NOT NULL AND typmoto='{0}' GROUP BY skupina", MySQLDatabase.Escape(m_Vzor));
		MCGroupList.SQL = string.Format(
			"SELECT skupina AS id,skupinatext{0} AS grp_name, pozice FROM tcskodnicisla",
			(Language.UseEnglishNames ? "_en" : "")
		);

		MCGroupList.SQLCount = string.Format("SELECT COUNT(DISTINCT skupina) FROM tcskodnicisla WHERE state<>9 AND skupina IS NOT NULL AND typmoto='{0}'", MySQLDatabase.Escape(m_Vzor));
		MCGroupList.Link = string.Format("parts.aspx?model={0}\\x26group={{0}}", m_Model);
		MCGroupList.Identifier = "id";

		MCGroupList.RenderControl(writer);
		writer.Write("\n<div style=\"padding:0px 3px 0px 3px\"><img src=\"images/bk.shadow.png\" width=\"100%\" height=\"1\" /></div>\n\n");
	}

	private void RenderPartsListPosition(HtmlTextWriter writer)
	{
		TabItem1.LangID = "PARTS_LIST";

		MCPosList.CategoryTitle = Language["PARTS_LIST"];
		MCPosList.Filter = string.Format("A.skupina={0} AND A.typmoto='{1}' AND A.state<>9", m_Group, MySQLDatabase.Escape(m_Vzor));
		MCPosList.SQL = string.Format(
			@"
SELECT DISTINCT
  A.sklcislo AS sklcislo,
  A.pozice AS id,
  IF(C.sklcislo IS NULL,A.nazevdilu{0},C.nazevdilu{0}) AS partname,
  B.Barva{1} AS barva
FROM tcskodnicisla A
LEFT JOIN tcskodnicisla C ON A.objcislo=C.sklcislo AND C.state<>9
LEFT JOIN ndbarvy B ON B.Identifikator=SUBSTRING(A.sklcislo,8)
",
			(Language.UseEnglishNames ? "_en" : ""),
			(Language.UseEnglishNames ? "En" : "Cz")
		);
		MCPosList.SQLCount = "SELECT COUNT(DISTINCT A.sklcislo) FROM tcskodnicisla A WHERE " + MCPosList.Filter;

		MCPosList.RenderControl(writer);
		writer.Write("\n<div style=\"padding:0px 3px 0px 3px\"><img src=\"images/bk.shadow.png\" width=\"100%\" height=\"1\" /></div>\n\n");
	}

	private void RenderPartsImage(HtmlTextWriter writer)
	{
		//TabItem1.LangID = "PARTS_IMAGE";
		//MCSpecsTabs.RenderControl(writer);
		writer.Write("<table border=\"0\" cellpadding=\"4\" cellspacing=\"1\" class=\"list\">\n");
		writer.Write("<tr class=\"category\"><td>{0}</td></tr>\n", Language["PARTS_IMAGE"]);
		writer.Write("<tr>\n");
		if (IsImageAvailable)
			writer.Write(" <td align=\"center\" valign=\"middle\"><img src=\"{0}\" alt=\"{1}-{2}\" width=\"{3}\" />", ImagePath, m_Model, m_Group, ImageWidth);
		else
			writer.Write(" <td align=\"center\" valign=\"middle\" style=\"background:#FFFFF0\"><div style=\"padding:32px 0px 32px 0px\">{0}</div>", Language["PARTS_IMAGE_NOT_AVAILABLE"]);
		writer.Write("</td>\n</tr>\n</table>\n<div style=\"padding:0px 3px 0px 3px\"><img src=\"images/bk.shadow.png\" width=\"100%\" height=\"1\" /></div>\n<br />\n\n");
	}

	protected override void Render(HtmlTextWriter writer)
	{
		// filter
		RenderPartsFilter(writer);		 

		// list
		if (IsGroupSpecified)
		{
			// render the image first
			RenderPartsImage(writer);
			// pos
			RenderPartsListPosition(writer);
		}
		else
		{
			// groups
			RenderPartsListGroup(writer);
		}
	}

}

﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Controls_ProtocolAdd" Codebehind="ProtocolAdd.ascx.cs" %>

<table border="0" cellpadding="4" cellspacing="1" class="list">
<tr class="header">
 <th>3 / 3 - <%=Language["PROTO_ADD_CONFIRM"]%></th>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
<br />

<table border="0" cellpadding="4" cellspacing="1" class="list">
<tr class="category">
 <td><%=Language["PROTO_ADD_REVIEW"]%></td>
</tr>
<tr class="odd">
 <td align="center">
  <div style="padding:32px 8px 32px 8px"><%=ProtoInfoText%></div>
 </td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
<br />

<table border="0" cellpadding="2" cellspacing="1" class="list">
<tr class="footer">
 <td align="left">
<table border="0" cellpadding="0" cellspacing="0" style="width:100%"> 
<tr>
 <td>
  <button type="button" class="main" onclick="location='protocols.aspx?add=';">
   <img src="images/newproto.png" alt="" />
   <%=Language["HEADER_PROTOCOL_ADD"]%>
  </button>
 </td>
 <td align="right">
  <button type="button" class="main" onclick="location='protocol.detail.aspx?id=<%=Protocol.Cislo%>';">
   <img src="images/icon.larr.png" alt="" />
   <%=Language["HEADER_PROTOCOL_DETAIL"]%>
  </button> 
  <button type="button" class="main" onclick="location='index.aspx';">
   <img src="images/cancel.png" alt="" />
   <%=Language["BUTTON_CLOSE"]%>
  </button>
 </td>
</tr> 
</table>
 </td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
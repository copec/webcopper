﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Controls_Parts" Codebehind="Parts.ascx.cs" %>
<%@ Register TagPrefix="yuki" Namespace="Ka.Garance.Web.List" Assembly="garance" %>
<%@ Register TagPrefix="yuki" Namespace="Ka.Garance.Web.TabList" Assembly="garance" %>

<yuki:TabList ID="MCSpecsTabs" TabWidth="75px" runat="server">
 <yuki:TabItem ID="TabItem1" LangID="PARTS_FILTER" Selected="true" runat="server" />
</yuki:TabList>

<yuki:List ID="MCGroupList" Name="parts.group" RowNumbers="false" runat="server">
 <yuki:ListItem ID="ListItem1" ColumnName="id" Title="ID" width="2%" align="left" Default="true" runat="server" />
 <yuki:ListItem ID="ListItem2" ColumnName="grp_name" LangID="PARTS_GROUP" align="left" runat="server" />
</yuki:List>

<yuki:List ID="MCPosList" Name="parts.pos" RowNumbers="false" runat="server">
 <yuki:ListItem ID="ListItem3" ColumnName="id" LangID="PARTS_POS" align="left" width="2%" Default="true" runat="server" />
 <yuki:ListItem ID="ListItem4" ColumnName="partname" LangID="PARTS_NAME" align="left" runat="server" />
 <yuki:ListItem ID="ListItem5" ColumnName="sklcislo" LangID="PARTS_STOCKNO" align="left" width="1%" td_width="1%" nowrap="nowrap" td_nowrap="nowrap" runat="server" />
 <yuki:ListItem ID="ListItem6" ColumnName="barva" LangID="PARTS_COLOUR" align="left" width="1%" td_width="1%" td_nowrap="nowrap" runat="server" />
</yuki:List>
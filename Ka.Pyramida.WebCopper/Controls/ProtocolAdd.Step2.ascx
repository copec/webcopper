﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Controls_ProtocolAdd_Step2" Codebehind="ProtocolAdd.Step2.ascx.cs" %>

<table border="0" cellpadding="0" cellspacing="0" style="width:100%"> 
<tr>
 <td align="left" width="99%">
<table border="0" cellpadding="4" cellspacing="1" class="list">
<tr class="header">
 <th>
<table border="0" cellpadding="0" cellspacing="0" style="width:100%"> 
<tr>
 <td align="left" style="color:#ffffff">2 / 3 &raquo; <%=Language["PROTO_ADD_ISSUE"]%></td>
 <td align="right" style="color:#ffffff"><%=Protocol.NazevProtokolu%></td>
</tr> 
</table> 
 </th>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
 </td>
 <td align="right">
  <div style="padding-left:4px;cursor:pointer" onclick="displayMessage(this,'<%=JsEncodedMessage%>',true,'ch101_zprava');"><img id="imgMessage001" src="images/message.png" width="24" height="24" alt="" /></div>
  <script language="javascript" type="text/javascript" src="js/message.js"></script>  
  <script language="javascript" type="text/javascript">
  //<![CDATA[
  MSG_BUTTON_CLOSE = '<%=Language["BUTTON_SAVE"]%>';
  MSG_MESSAGE = '<%=Language["PROTO_MESSAGE"]%>';
  MSG_dontToggleMessage = true;
  //]]>
  </script>  
 </td>
</tr>
</table>
<br />

<%=GenerateErrorHTML()%>

<script language="javascript" type="text/javascript" src="js/protocol.js"></script>

<form action="<%=Request.FilePath%>" method="post">

<table border="0" cellpadding="4" cellspacing="1" class="list">
<tr class="category">
 <td colspan="4"><%=Language["PROTO_ADD_BASICS"]%></td>
</tr>
<tr class="even">
 <td width="20%"><%=Language["PROTO_GROUP_OWNER"]%></td>
 <td colspan="3" class="odd">
<table border="0" cellpadding="0" cellspacing="0"> 
<tr>
 <td valign="top" align="left"><%=Protocol.Jmeno%><br /><%=(Protocol.Adresa != null && Protocol.Adresa.Trim() != "" ? string.Format("{0}<br />", Protocol.Adresa) : "")%><%=Protocol.Obec%> <%=Protocol.PSC%><%=GetICOString()%></td>
 <td width="50">&nbsp;</td>
 <td valign="top" align="left">Tel: <%=Protocol.Telefon%></td> 
</tr> 
</table>
 </td>
</tr>
<tr class="even">
 <td width="20%"><%=Language["BIKES_LIST_MODEL"]%></td>
 <td class="odd"><%=Protocol.Model%></td>
 <td width="20%"><%=Language["BIKES_LIST_CHASSIS"]%></td>
 <td width="25%" class="odd"><%=Protocol.VIN%></td> 
</tr>
<tr class="even">
 <td width="20%"><%=Language["PROTO_DATE_REPAIR"]%></td>
 <td class="odd"><%=GenerateDateRepairHTML()%></td>  
 <td width="20%"><%=Language["PROTO_KM"]%></td>
 <td width="25%" class="odd"><%=Protocol.Km.ToString()%></td> 
</tr>
<tr class="even">
 <td width="20%"><%=Language["BIKES_LIST_DATE"]%></td>
 <td colspan="3" class="odd"><%=Protocol.DateSold.ToString("d'. 'M'. 'yyyy")%></td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
<br />

<table border="0" cellpadding="4" cellspacing="1" class="list">
<tr class="category">
 <td colspan="4"><%=Language["PROTO_ADD_EXTEND"]%></td>
</tr>
<tr class="even">
 <td width="20%"><label for="c101_nz_ko"><%=Language["PROTO_KO"]%></label></td>
 <td width="30%" class="odd">
<select id="c101_nz_ko" name="nz_ko"> 
 <%=GenerateKOOptions()%>
</select>
 </td>
 <td width="20%"><label for="c102_nz_dz"><%=Language["PROTO_GT"]%></label></td>
 <td width="30%" class="odd">
<select id="c102_nz_dz" name="nz_dz"> 
 <%=GenerateDZOptions()%>
</select>
 </td>
</tr>
<tr class="even">
 <td width="20%"><label for="c103"><%=Language["PROTO_WORKER"]%></label></td>
 <td colspan="3" width="80%" class="odd"><input id="c103" type="text" name="nz_resitel" value="<%=Server.HtmlEncode(Protocol.Resitel)%>" size="48" style="width:99%" class="sw-input" /></td>
</tr>
<tr class="even">
 <td width="20%"><label for="c104"><%=Language["PROTO_PHONE"]%></label></td>
 <td width="30%" class="odd"><input id="c104" type="text" name="nz_resitel_tel" value="<%=Server.HtmlEncode(Protocol.ResitelTel)%>" size="28" style="width:97%" class="sw-input" /></td>
 <td width="20%"><label for="c105"><%=Language["PROTO_FAX"]%></td>
 <td width="30%" class="odd"><input id="c105" type="text" name="nz_resitel_fax" value="<%=Server.HtmlEncode(Protocol.ResitelFax)%>" size="28" style="width:97%" class="sw-input" /></td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
<input id="ch101_zprava" type="hidden" name="nz_zprava" value="<%=Server.HtmlEncode(Protocol.Zprava)%>" />
<br />

<%=GetSeznamZavad()%>

<table border="0" cellpadding="2" cellspacing="1" class="list">
<tr class="footer">
 <td align="right">
  <input type="hidden" name="_step" value="a634b76dgfhreJKA_121.1" />
  <input type="button" value="&laquo; <%=Language["BUTTON_BACK"]%>" class="main" onclick="this.form.action='<%=Request.FilePath%>?step=1';this.form.submit();" />
  <input type="submit" name="next_121" value="<%=Language["PROTO_BUTTON_SAVE_PROTO"]%>" class="main" style="font-weight:bold" />
 </td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>

</form>

<script language="javascript" type="text/javascript">
//<![CDATA[
var tbl = document.getElementById('tbl_s_zavady_p');
if (tbl != null && tbl.rows.length > 3)
  protocol_SetZavadaForDeletion(tbl.rows[2]);
//]]>
</script>
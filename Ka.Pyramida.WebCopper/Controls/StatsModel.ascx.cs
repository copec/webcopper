using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Controls_StatsModel : Ka.Garance.Web.GaranceControl
{
	protected class Filter
	{
		public string model = null;
		public int countryID = 1;
	}

	protected Filter filter = null;

	protected override void OnPreRender(EventArgs e)
	{
		base.OnPreRender(e);

		if (this.Visible)
			DisplayStats();
	}

	private void DisplayStats()
	{
		filter = (Session["Controls_StatsModel.filter"] as Filter);
		if (filter == null)
		{
			filter = new Filter();
			Session["Controls_StatsModel.filter"] = filter;
		}

		if (Request.RequestType == "POST")
			filter.model = Request.Form[tbName.UniqueID];
		else if (Request.QueryString["filter"] != null)
			filter.model = null;

		tbName.Value = filter.model;

		MCStats0.Visible = true;

		if (Request.QueryString["lang"] == "cz")
			filter.countryID = 1;
		else if (Request.QueryString["lang"] == "sk")
			filter.countryID = 2;

		if (filter.countryID == 2)
			tabSK.Select();
		else
			tabCZ.Select();

		string SQLfilter = string.Format("\tAND tdealer.idzeme={0} ", filter.countryID);
		SQLfilter += (filter.model != null && filter.model.Trim() != "" ? string.Format("AND (model LIKE '%{0}%' OR jmeno LIKE '%{0}%')", Ka.Garance.Data.MySQLDatabase.Escape(filter.model.TrimEnd(null), true)) : "");

		MCStats0.SQL = string.Format(@"
SELECT
  iddealer AS id,
  IFNULL(CONCAT(jmeno,' (',model,')'),model) AS name,
  COUNT(IF(IFNULL(stav,0)=2,2,NULL)) AS stav2,
  COUNT(IF(IFNULL(stav,0)=0,0,NULL)) AS stav0
FROM tmotocykl
	INNER JOIN tcmodelklic ON tcmodelklic.id=tmotocykl.model
	INNER JOIN tdealer ON tdealer.id=tmotocykl.iddealer
WHERE TRIM(IFNULL(model,''))<>''
{0}
GROUP BY model
		", SQLfilter);
		MCStats0.SQLCount = string.Format(@"
			SELECT COUNT(DISTINCT model) FROM tmotocykl
				INNER JOIN tcmodelklic ON tcmodelklic.id=tmotocykl.model
				INNER JOIN tdealer ON tdealer.id=tmotocykl.iddealer
			WHERE TRIM(IFNULL(model,''))<>'' {0}", SQLfilter);
	}
}

﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Controls_ProtocolAdd_Step3" Codebehind="ProtocolAdd.Step3.ascx.cs" %>

<table border="0" cellpadding="4" cellspacing="1" class="list">
<tr class="header">
 <th>
<table border="0" cellpadding="0" cellspacing="0" style="width:100%">
<tr>
 <td align="left" style="color:#ffffff">3 / 3 - <%=Language["PROTO_ADD_CONFIRM"]%></td>
 <td align="right" style="color:#ffffff"><%=Protocol.NazevProtokolu%></td>
</tr>
</table> 
 </th>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
<br />

<form action="<%=Request.FilePath%>" method="post">

<table border="0" cellpadding="4" cellspacing="1" class="list">
<tr class="category">
 <td colspan="4"><%=Language["PROTO_ADD_BASICS"]%></td>
</tr>
<tr class="even">
 <td width="20%"><%=Language["PROTO_GROUP_OWNER"]%></td>
 <td colspan="3" class="odd"><%=Protocol.Jmeno%>, <%=Protocol.Adresa%>, <%=Protocol.Obec%> <%=Protocol.PSC%></td>
</tr>
<tr class="even">
 <td width="20%"><%=Language["BIKES_LIST_MODEL"]%></td>
 <td class="odd"><%=Protocol.Model%></td>
 <td width="20%"><%=Language["BIKES_LIST_CHASSIS"]%></td>
 <td width="25%" class="odd"><%=Protocol.VIN%></td> 
</tr>
<tr class="even">
 <td width="20%"><%=Language["PROTO_DATE_REPAIR"]%></td>
 <td class="odd"><%=Protocol.DateRepair.ToString("d'. 'M'. 'yyyy")%></td>  
 <td width="20%"><%=Language["PROTO_KM"]%></td>
 <td width="25%" class="odd"><%=Protocol.Km.ToString()%></td> 
</tr>
<tr class="even">
 <td width="20%"><%=Language["BIKES_LIST_DATE"]%></td>
 <td colspan="3" class="odd"><%=Protocol.DateSold.ToString("d'. 'M'. 'yyyy")%></td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
<br />

<table border="0" cellpadding="4" cellspacing="1" class="list">
<tr class="category">
 <td colspan="4"><%=Language["PROTO_ADD_EXTEND"]%></td>
</tr>
<tr class="even">
 <td width="20%"><%=Language["PROTO_KO"]%></td>
 <td width="30%" class="odd"><%=Protocol.KOTitle%></td>
 <td width="20%"><%=Language["PROTO_GT"]%></td>
 <td width="30%" class="odd"><%=Protocol.DZTitle%></td>
</tr>
<tr class="even">
 <td width="20%"><%=Language["PROTO_WORKER"]%></td>
 <td colspan="3" width="80%" class="odd"><%=Server.HtmlEncode(Protocol.Resitel)%></td>
</tr>
<tr class="even">
 <td width="20%"><%=Language["PROTO_PHONE"]%></td>
 <td width="30%" class="odd"><%=Server.HtmlEncode(Protocol.ResitelTel)%></td>
 <td width="20%"><%=Language["PROTO_FAX"]%></td>
 <td width="30%" class="odd"><%=Server.HtmlEncode(Protocol.ResitelFax)%></td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
<br />

<%=GenerateSeznamZavad()%>

<table border="0" cellpadding="2" cellspacing="1" class="list">
<tr class="footer">
 <td align="left">
<table border="0" cellpadding="0" cellspacing="0" style="width:100%"> 
<tr>
 <td align="left">
  <input type="hidden" name="_step" value="0x5566fea590xfg_674.45" />
  <button type="submit" name="p_btn_save" class="main" style="font-weight:bold;width:190px">
   <img src="images/save.png" alt="" />
   <%=Language["PROTO_BUTTON_SAVE"]%>
  </button>
  <button type="button"<%=(Protocol.HasValidPrices?"":" disabled=\"disabled\"")%> class="main" style="width:160px" onclick="this.form.p_btn_save.name='p_btn_send';this.form.submit();">
   <img src="images/process.png" alt="" />
   <%=Language["PROTO_BUTTON_SEND"]%>
  </button>
 </td>
 <td align="right">
  <input type="button" value="&laquo; <%=Language["BUTTON_BACK"]%>" class="main" onclick="location='<%=Request.FilePath%>?step=2';" />
 </td>  
</tr> 
</table>
 </td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>

</form>
﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Controls_Code_Ajax" Codebehind="Code.Ajax.ascx.cs" %>
<%@ Register TagPrefix="yuki" Namespace="Ka.Garance.Web.List" Assembly="garance" %>
<%@ Register TagPrefix="yuki" Namespace="Ka.Garance.Web.TabList" Assembly="garance" %>

<yuki:List ID="MCPosList" Name="parts.pos[ajax]" RowNumbers="false" RowSelect="true" runat="server">
 <yuki:ListItem ID="ListItem1" ColumnName="pozice" LangID="PARTS_POS" width="1%" Default="true" align="center" td_align="center" runat="server" />
 <yuki:ListItem ID="ListItem2" ColumnName="partname" LangID="PARTS_NAME" width="99%" runat="server" />
</yuki:List>

<yuki:List ID="MCNdList" Name="parts.nd[ajax]" RowNumbers="false" RowSelect="true" runat="server">
 <yuki:ListItem ID="ListItem3" ColumnName="pozice" LangID="PARTS_POS" width="1%" Default="true" align="center" td_align="center" runat="server" />
 <yuki:ListItem ID="ListItem4" ColumnName="partname" LangID="PARTS_NAME" width="99%" runat="server" />
 <yuki:ListItem ID="ListItem8" ColumnName="barva" LangID="PROTO_COLOUR" width="1%" align="left" td_align="left" td_nowrap="nowrap" runat="server" />
 <yuki:ListItem ID="ListItem5" ColumnName="cena" LangID="PARTS_PRICE" Format="{0:F1}0" width="1%" align="right" td_align="right" runat="server" />
 <yuki:ListItem ID="ListItem6" ColumnName="ks" Orderable="false" LangID="PROTO_QTY" width="1%" align="center" td_align="center" runat="server" /> 
 <yuki:ListItem ID="ListItem7" ColumnName="ks" Orderable="false" LangID="PROTO_ORDER" width="1%" align="center" td_align="center" td_nowrap="nowrap" runat="server" />
 <yuki:ListItem ID="ListItem9" ColumnName="pozn" LangID="PARTS_NOTE" width="1%" align="left" td_align="left" td_nowrap="nowrap" runat="server" />
</yuki:List>
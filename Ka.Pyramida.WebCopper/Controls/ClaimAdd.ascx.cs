﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Ka.Garance.Data;
using Ka.Garance.Web;

public partial class Controls_ClaimAdd : Ka.Garance.Web.GaranceControl
{
	private int m_ActualStep = 1;
	protected void Page_Load(object sender, EventArgs e)
	{
		if (Request.HttpMethod == "POST")
		{
			try { m_ActualStep = Convert.ToInt32(Request.Form["pstep"]); }
			catch {  }

			switch (m_ActualStep)
			{
				case 2:
					// zisti, ci bol krok spat
					if (Request.Form["pback_step"] != null)
						m_ActualStep = 1;
					else if (ValidateStep())
						m_ActualStep = 3;
					break;
				default:
					if (ValidateStep()) m_ActualStep = 2;
					else m_ActualStep = 1;
					break;
			}

			if (m_ActualStep == 3)
			{
				SavePredavaciListToDB();
			}
		}
		else if (Request.QueryString["id"] != null)
		{
			m_ActualStep = 3;
		}
	}

	private void SavePredavaciListToDB()
	{
		Database.Execute("INSERT INTO tlisty(nazev,datum,iddealer) VALUES({0},NOW(),{1})", Request.Form["ptitle"], (Page as GarancePage).User.Id);
		int idlist = Convert.ToInt32(Database.ExecuteScalar("SELECT MAX(id) FROM tlisty"));

		MySQLDataReader dr = Database.ExecuteReader(string.Format(@"
			SELECT B.cislo AS protokol,C.typmoto AS model,A.iddil AS sklcislo,C.nazevdilu{0} AS nazev,IFNULL(A.mnozstvi,0) AS pocet,A.id AS id
			FROM tzdil A
				INNER JOIN tprotocol B on B.cislo=A.cisloprotokol
				LEFT JOIN tcskodnicisla C on C.sklcislo=A.iddil
			WHERE B.iddealer={1} AND B.stav=4 AND IFNULL(A.mnozstvi,0) > 0
			ORDER BY B.cislo DESC, A.id DESC", (Language.UseEnglishNames ? "_en" : ""), (Page as GarancePage).User.Id));
		while (dr.Read())
		{
			bool isSelected = (Request.Form[string.Format("pclaim[{0}]", dr.Index)] == Convert.ToString(dr.GetValue("id")));
			if (!isSelected) continue;

			Database.Execute("INSERT INTO tlistydil (idlist,cisloprotokol,iddil,poznamka) VALUES({0},{1},{2},{3})", idlist, dr.GetValue("protokol"), dr.GetValue("id"), Request.Form[string.Format("ptxt[{0}]", dr.Index)]);
		}

		Response.Redirect(string.Format("claim.add.aspx?id={0}", idlist));
	}

	private bool ValidateStep()
	{
		if (Request.Form["next"] == null) return false;
		for(int i=0;i<Request.Form.Count;i++)
		{
			try
			{
				if (Request.Form.GetKey(i).Substring(0, 7) == "pclaim[")
					return true;
			}
			catch { }
		}
		return false;
	}

	protected override void Render(HtmlTextWriter writer)
	{
		switch (m_ActualStep)
		{
			case 3:
				base.Render(writer);
				break;
			case 2:
				LoadControl("ClaimAdd.Step2.ascx").RenderControl(writer);
				break;
			default:
				LoadControl("ClaimAdd.Step1.ascx").RenderControl(writer);
				break;
		}		
	}
}

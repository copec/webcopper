<%@ Control Language="C#" AutoEventWireup="true" Inherits="Controls_StatsBikes" Codebehind="StatsBikes.ascx.cs" %>
<%@ Register TagPrefix="yuki" Namespace="Ka.Garance.Web.List" Assembly="garance" %>
<%@ Register TagPrefix="yuki" Namespace="Ka.Garance.Web.TabList" Assembly="garance" %>

<form action="stats.aspx" method="post">
<table border="0" cellpadding="4" cellspacing="1" class="list">
<tr class="category">
 <td colspan="2"><%=Language["PARTS_FILTER"]%></td>
</tr>
<tr class="even">
 <td width="15%"><%=Language["STATS_DEALER"]%></td>
 <td class="odd"><input id="tbName" type="text" runat="server" size="120" /></td>
</tr>
<tr class="footer">
 <td colspan="2">
  <input name="filter" type="submit" value="<%=Language["BUTTON_FILTER"]%>" style="font-weight:bold" />
  <input type="button" value="<%=Language["BUTTON_FILTER_RESET"]%>" onclick="location='stats.aspx?filter=';" />
 </td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
</form>

<br />

<yuki:TabList ID="MCCountryTabs" runat="server">
 <yuki:TabItem ID="tabCZ" LangID="LANG_CZ" Link="stats.aspx?lang=cz" runat="server" />
 <yuki:TabItem ID="tabSK" LangID="LANG_SK" Link="stats.aspx?lang=sk" runat="server" />
</yuki:TabList>
<yuki:List ID="MCStats0" Name="Stats0List" runat="server">
 <yuki:ListItem ID="ListItem1" LangID="STATS_DEALER" ColumnName="name" align="left" width="80%" td_nowrap="nowrap" Default="true" DefaultState="Desc" runat="server" />
 <yuki:ListItem ID="ListItem2" LangID="BIKES_STATE_SOLD" ColumnName="stav2" align="right" td_align="right" width="10%" runat="server" />
 <yuki:ListItem ID="ListItem3" LangID="BIKES_STATE_STOCK" ColumnName="stav0" align="right" td_align="right" width="10%" runat="server" />
</yuki:List>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
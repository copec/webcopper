﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Ka.Garance.Protocol.Yuki;

public partial class Controls_ProtocolAdd_Step3 : Ka.Garance.Web.GaranceControl
{
	public YukiProtocol Protocol
	{
		get
		{
			return (Page as Ka.Garance.Web.GarancePage).User.CachedProtocol;
		}
	}

	public string GenerateSeznamZavad()
	{
		bool isSKDealer = Protocol.User.IdZeme == 2 && Protocol.DateCreated >= YukiProtocol.DATE_SK_EURO;

		StringBuilder sb = new StringBuilder();
		sb.Append("<table border=\"0\" cellpadding=\"4\" cellspacing=\"1\" class=\"list\">\n");
		sb.AppendFormat("<tr class=\"category\"><td colspan=\"8\">{0}</td></tr>\n", Language["PROTO_GROUP_ISSUES"]);
		sb.AppendFormat("<tr class=\"header\">\n");
		sb.Append(" <th style=\"width:1%\">&nbsp;</th>\n");
		sb.AppendFormat(" <th align=\"left\" style=\"width:1%\">{0}</th>\n", Language["PROTO_ISSUE_CODE"]);
		sb.AppendFormat(" <th align=\"left\" style=\"width:1%\">{0}</th>\n", Language["PROTO_ISSUE_DZ"]);
		sb.AppendFormat(" <th align=\"left\" style=\"width:1%\">{0}</th>\n", Language["PROTO_ISSUE_MANUFACTURER"]);
		sb.AppendFormat(" <th>{0}</th>\n", Language["PROTO_ISSUE_TITLE"]);
		sb.AppendFormat(" <th>{0}</th>\n", Language["PROTO_ISSUE_TEXT"]);
		sb.AppendFormat(" <th align=\"center\" style=\"width:1%\">{0}</th>\n", Language["PROTO_ISSUE_WORK"]);
		sb.AppendFormat(" <th align=\"center\" style=\"width:1%\">{0}</th>\n", Language["PROTO_ISSUE_MAT"]);
		sb.Append("</tr>\n");

		for (int i = 0; i < Protocol.Zavady.Count; i++)
		{
			sb.AppendFormat("<tr class=\"{0}\">\n", (i % 2 == 0 ? "even" : "odd"));
			sb.AppendFormat(" <td align=\"right\" style=\"width:1%\">{0}.</td>\n", (i + 1));
			sb.AppendFormat(" <td align=\"left\" style=\"width:1%\">{0}</td>\n", Protocol.Zavady[i].Kod);
			sb.AppendFormat(" <td align=\"left\" style=\"width:1%\">{0}</td>\n", Protocol.Zavady[i].DZ);
			sb.AppendFormat(" <td align=\"left\" style=\"width:1%\">{0}</td>\n", Protocol.Zavady[i].Vyrobce);
			sb.AppendFormat(" <td>{0}</td>\n", Protocol.Zavady[i].Nazev);
			sb.AppendFormat(" <td>{0}</td>\n", Protocol.Zavady[i].Popis);
			sb.AppendFormat(" <td align=\"right\" style=\"width:1%\">{0:F2}</td>\n", Math.Round(Protocol.Zavady[i].CelkemCjCenaDPH, isSKDealer ? 2 : 1));
			sb.AppendFormat(" <td align=\"right\" style=\"width:1%\">{0:F2}</td>\n", Math.Round(Protocol.Zavady[i].CelkemCenaDPH, isSKDealer ? 2 : 1));
			sb.Append("</tr>");
		}

		if (!Protocol.HasValidPrices)
		{
			sb.AppendFormat("\n<tr class=\"infotext\"><td align=\"left\" colspan=\"8\">{0}</td></tr>", Language["PROTO_ERR_NOPRICES"]);
		}

		sb.Append("\n</table>\n<div style=\"padding:0px 3px 0px 3px\"><img src=\"images/bk.shadow.png\" width=\"100%\" height=\"1\" /></div>\n<br />\n");

		return sb.ToString();
	}

	private void Initialize()
	{
		if (Request.HttpMethod == "POST" && Request.Form["_step"] == "0x5566fea590xfg_674.45")
		{
			// ulozit podla typu buttonu
			Protocol.Rozpracovany = (Request.Form["p_btn_save"] != null);
			Protocol.Save();
			Protocol.ActualStep = 4;

			// if any
			(Page as Ka.Garance.Web.GarancePage).User.ResetActiveProtocol();

			Response.Redirect(Request.FilePath);			
		}

		if (Request.QueryString["step"] == "2")
		{
			Protocol.ActualStep = 2;
			Response.Redirect(Request.FilePath);
		}

	}

	protected override void Render(HtmlTextWriter writer)
	{
		Initialize();
		base.Render(writer);
	}

}

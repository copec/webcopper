﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Controls_ClaimAdd" Codebehind="ClaimAdd.ascx.cs" %>

<table border="0" cellpadding="4" cellspacing="1" class="list">
<tr>
 <th class="header" align="left">2 / 2 - <%=Language["PROTO_ADD_REVIEW"]%></th>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
<br />

<table border="0" cellpadding="4" cellspacing="1" class="list">
<tr class="category">
 <td><%=Language["PROTO_ADD_REVIEW"]%></td>
</tr>
<tr class="odd">
 <td align="center">
  <div style="padding:32px 8px 32px 8px">
   <%=Language["CLAIM_MSG_LETTER"]%>.<br /><br />
   <button type="button" class="main" onclick="location='claim.detail.aspx?id=<%=Request.QueryString["id"]%>\x26pdf=';" style="padding:4px">
    <img src="images/print.pdf.png" alt="" />
    <%=Language["BUTTON_PRINT"]%>
   </button>  
  </div>
 </td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
<br />

<table border="0" cellpadding="2" cellspacing="1" class="list">
<tr class="footer">
 <td align="left">
<table border="0" cellpadding="0" cellspacing="0" style="width:100%"> 
<tr>
 <td>
  <button type="button" class="main" onclick="location='claim.add.aspx';">
   <img src="images/newproto.png" />
   <%=Language["HEADER_CLAIM_ADD"]%>
  </button>
 </td>
 <td align="right">
  <button type="button" class="main" onclick="location='index.aspx';">
   <img src="images/cancel.png" />
   <%=Language["BUTTON_CLOSE"]%>
  </button>
 </td>
</tr>
</table>
 </td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Ka.Garance.Data;
using Ka.Garance.Web;

public partial class Controls_ClaimAdd_Step1 : Ka.Garance.Web.GaranceControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

	protected bool PartsAvailable
	{
		get
		{
			return (Convert.ToInt32(Database.ExecuteScalar("SELECT COUNT(A.id) FROM tzdil A INNER JOIN tprotocol B ON B.cislo=A.cisloprotokol WHERE B.iddealer={0} AND B.stav=4 AND IFNULL(A.mnozstvi,0) > 0", (Page as GarancePage).User.Id)) > 0);
		}
	}

	private bool m_OneSelected = false;
	public bool OneSelected
	{
		get { return m_OneSelected; }
	}

	protected string GetSeznamPolozek()
	{
		StringBuilder sb = new StringBuilder();

		MySQLDataReader dr = Database.ExecuteReader(string.Format(@"
			SELECT A.id AS id, B.cislo AS protokol,C.typmoto AS model,A.iddil AS sklcislo,C.nazevdilu{0} AS nazev,IFNULL(A.mnozstvi,0) AS pocet
			FROM tzdil A
				INNER JOIN tprotocol B ON B.cislo=A.cisloprotokol
				LEFT JOIN tcskodnicisla C ON C.sklcislo=A.iddil
			WHERE B.iddealer={1} AND B.stav=4 AND IFNULL(A.mnozstvi,0) > 0
			ORDER BY B.cislo DESC, A.id DESC", (Language.UseEnglishNames ? "_en" : ""), (Page as GarancePage).User.Id));
		while (dr.Read())
		{
			bool isSelected = (Request.Form[string.Format("pclaim[{0}]", dr.Index)] == Convert.ToString(dr.GetValue("id")));
			sb.AppendFormat(@"
<tr class=""{0}"" style=""cursor:pointer"" onmouseover=""if(!this.h)this.h=this.className;this.className=this.h+'-active';"" onmouseout=""this.className=this.h;"" onclick=""claim_SelectItem(this,-1);"">
 <td align=""left"" nowrap=""nowrap"">{1}</td>
 <td align=""left"" nowrap=""nowrap"">{2}</td>
 <td align=""left"" nowrap=""nowrap"">{3}</td>
 <td align=""left"" nowrap=""nowrap"">{4}</td>
 <td align=""center"">{5}</td>
 <td align=""left""><input type=""text"" name=""ptxt[{7}]"" value=""{8}"" class=""sw-input"" style=""width:98.5%;visibility:{9}"" onfocus=""this.active=true;"" onblur=""this.active=false;"" /></td>
 <td style=""width:1px""><input type=""checkbox"" name=""pclaim[{7}]"" value=""{6}""{10} style=""cursor:pointer;margin:0px 2px 0px 2px;padding:0px;height:14px"" /></td>
</tr>", (isSelected ? "rs" : (dr.Index % 2 == 0 ? "even" : "odd")), dr.GetValue("protokol"), dr.GetValue("model"), dr.GetValue("sklcislo"), dr.GetValue("nazev"), dr.GetValue("pocet"), dr.GetValue("id"), dr.Index, Server.HtmlEncode(Request.Form[string.Format("ptxt[{0}]", dr.Index)])
	  , (isSelected ? "visible" : "hidden")
	  , (isSelected ? " checked=\"checked\"" : ""));
			if (isSelected) m_OneSelected = true;
		}

		return sb.ToString();
	}

}

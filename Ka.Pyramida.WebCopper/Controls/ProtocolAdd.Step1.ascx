﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Controls_ProtocolAdd_Step1" Codebehind="ProtocolAdd.Step1.ascx.cs" %>

<table border="0" cellpadding="4" cellspacing="1" class="list">
<tr class="header">
 <th>
<table border="0" cellpadding="0" cellspacing="0" style="width:100%"> 
<tr>
 <td align="left" style="color:#ffffff">1 / 3 - <%=Language["PROTO_ADD_BASICS"]%></td>
 <td align="right" style="color:#ffffff"><%=Protocol.NazevProtokolu%></td>
</tr> 
</table> 
 </th>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
<br />

<%=GenerateErrorHTML()%>

<form action="<%=Request.FilePath%>" method="post">

<table border="0" cellpadding="4" cellspacing="1" class="list">
<tr class="category">
 <td colspan="2" align="left"><%=Language["PROTO_GROUP_VEHICLE"]%></td>
</tr>
<tr class="even">
 <td align="left" width="20%"><label for="pdate"><%=Language["PROTO_DATE_REPAIR"]%></label><input type="hidden" id="proto_chk" value="" runat="server" enableviewstate="false" /></td>
 <td class="odd">
<table border="0" cellpadding="0" cellspacing="0"> 
<tr>
 <td><input id="pdate" type="text" name="pdate" value="<%=(Request.Form["pdate"]!=null?Server.HtmlEncode(Request.Form["pdate"]):Protocol.DateRepair.ToString("d'. 'M'. 'yyyy"))%>" size="10" class="sw-input" /></td>
 <td style="padding-left:3px"><a href="javascript:void(0);" onclick="displayCalendar();"><img src="images/calendar.png" alt="" /></a></td>
</tr>
</table>
 </td>
</tr>
<tr class="even">
 <td align="left" width="20%"><%=Language["PROTO_BIKE"]%></td>
 <td class="odd">
<table border="0" cellpadding="0" cellspacing="0"> 
<tr> 
 <td><div id="protocol.div.bike" style="width:200px"></div></td>
 <td><input id="cb110A" type="checkbox" name="ppservis" value="1"<%=(Protocol.PredprodejniServis?" checked=\"checked\"":"")%> /></td>
 <td><label for="cb110A"><%=Language["PROTO_PREGSERVICE"]%></label></td>
</tr>  
</table>
  <div style="font-size:7pt;font-style:italic"><%=Language["PROTO_MSG_BIKE"].Replace("%d", TotalBikesAvailable.ToString())%></div>
 </td>
</tr>
<tr class="even">
 <td align="left" width="20%"><label for="c110"><%=Language["PROTO_KM"]%></label></td>
 <td class="odd">
<table border="0" cellpadding="0" cellspacing="0"> 
<tr>
 <td><input id="c110" type="text" name="pkm" value="<%=(Request.Form["pkm"]!=null?Request.Form["pkm"]:(Protocol.Km<0?"":Convert.ToString(Protocol.Km)))%>" size="10" maxlength="32" class="sw-input" /></td>
 <td>&nbsp;</td>
 <td><input id="c111" type="checkbox" name="ptacho" value="1"<%=(Protocol.VymenaTachometra?" checked=\"checked\"":"")%> /></td>
 <td><label for="c111"><%=Language["PROTO_TACHO"]%></label></td>
</tr>
</table>
 </td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
<br /><% if (CanDisplayOwner) { %>

<table border="0" cellpadding="4" cellspacing="1" class="list">
<tr class="category">
 <td colspan="2" align="left"><%=Language["PROTO_GROUP_OWNER"]%></td>
</tr>
<tr class="even">
 <td align="left" width="20%"><label for="c102"><%=Language["PROTO_NAME"]%></label></td>
 <td class="odd">
	<input id="c102" type="text" name="pname" value="<%=Server.HtmlEncode(Protocol.Jmeno)%>" size="40" maxlength="255" class="sw-input" />
	<select id="owner_type" runat="server" enableviewstate="false" class="sw-input">
		<option value="0">Fyzicka_osoba</option>
		<option value="1">Fyzicka_osoba_ICO</option>
		<option value="2">Pravnicka_osoba</option>
	</select>
 </td>
</tr>
<tr class="even">
 <td align="left" width="20%"><label for="cICO"><%=Language["INVOICE_ICO"]%></label></td>
 <td class="odd"><input id="cICO" type="text" name="pico" value="<%=Server.HtmlEncode(Protocol.ICO)%>" size="32" maxlength="40" class="sw-input" /></td>
</tr>
<tr class="even">
 <td align="left" width="20%"><label for="c102a"><%=Language["PROTO_TELEPHONE"]%></label></td>
 <td class="odd"><input id="c102a" type="text" name="pphone" value="<%=Server.HtmlEncode(Protocol.Telefon)%>" size="32" maxlength="255" class="sw-input" /></td>
</tr>
<tr class="even">
 <td align="left" width="20%"><label for="c103"><%=Language["PROTO_ADDRESS"]%></label></td>
 <td class="odd"><input id="c103" type="text" name="paddress" value="<%=Server.HtmlEncode(Protocol.Adresa)%>" size="80" maxlength="255" class="sw-input" /></td>
</tr>
<tr class="even">
 <td align="left" width="20%"><label for="c104"><%=Language["PROTO_ZIPCODE"]%></label></td>
 <td class="odd"><input id="c104" type="text" name="pzipcode" value="<%=Server.HtmlEncode(Protocol.PSC)%>" size="8" maxlength="5" class="sw-input" /></td>
</tr>
<tr class="even">
 <td align="left" width="20%"><label for="c105"><%=Language["PROTO_CITY"]%></label></td>
 <td class="odd"><input id="c105" type="text" name="pcity" value="<%=Server.HtmlEncode(Protocol.Obec)%>" size="32" maxlength="64" class="sw-input" /></td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
<br /><% } %>

<table border="0" cellpadding="2" cellspacing="1" class="list">
<tr class="footer">
 <td align="right">
   <input type="hidden" name="_step" value="ax564jdhrk134nfg5" />
   <input type="submit" name="next" value="<%=Language["BUTTON_NEXT"]%> &raquo;" class="main" style="font-weight:bold" />
 </td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>

<script language="javascript" type="text/javascript" src="js/ajax.js"></script>
<script language="javascript" type="text/javascript" src="js/combo.js"></script>
<script language="javascript" type="text/javascript" src="js/calendar.js"></script>
<script language="javascript" type="text/javascript">
//<![CDATA[

// bike combo
var bikeCombo = new StylusWebCombo(document.getElementById('protocol.div.bike'), 'pvin');
bikeCombo.disableCheck = true;
bikeCombo._input.value = '<%=JsProtocolVIN%>';
bikeCombo._inputh.value = '<%=JsProtocolVIN%>';
bikeCombo.div.combo = bikeCombo;
bikeCombo.onajaxdata = function(combo, value) {
  Ajax.execute("protocol.add.aspx?ajaxbikes="+uEscape(value), updateBikeCombo);
}
bikeCombo._input.style.width = '180px';

function updateBikeCombo(msg) {
  if (msg.length == 0) return;
  if (msg.substr(0,3) != 'OK-') {
    return;
  }
  
  msg = msg.substr(3, msg.length);   
   
  bikeCombo.ajaxClear();    
  eval(msg);

  bikeCombo.collapse();
  bikeCombo.expand();
}


// calendar constants
Calendar._DN = '<%=Language["JS_DATE_DN"]%>'.split(',');
Calendar._SDN = '<%=Language["JS_DATE_DNS"]%>'.split(',');
Calendar._MN = '<%=Language["JS_DATE_MN"]%>'.split(',');
Calendar._SMN = '<%=Language["JS_DATE_MNS"]%>'.split(',');
Calendar._TT["PREV_YEAR"] = '<%=Language["JS_DATE_PREV_YEAR"]%>';
Calendar._TT["PREV_MONTH"] = '<%=Language["JS_DATE_PREV_MONTH"]%>';
Calendar._TT["GO_TODAY"] = '<%=Language["JS_DATE_GO_TODAY"]%>';
Calendar._TT["NEXT_MONTH"] = '<%=Language["JS_DATE_NEXT_MONTH"]%>';
Calendar._TT["NEXT_YEAR"] = '<%=Language["JS_DATE_NEXT_YEAR"]%>';
Calendar._TT["SEL_DATE"] = '<%=Language["JS_DATE_SELECT"]%>';
Calendar._TT["PART_TODAY"] = '<%=Language["JS_DATE_PART_TODAY"]%>';
Calendar._TT["DAY_FIRST"] = '<%=Language["JS_DATE_DAY_FIRST"]%>';
Calendar._TT["CLOSE"] = '<%=Language["BUTTON_CLOSE"]%>';
Calendar._TT["TODAY"] = '<%=Language["JS_DATE_GO_TODAY"]%>';

function dateFromString(s) {
  if (s.replace(/[0-9\.\s]+/g,'') != '') return null;
  
  var r = s.split('.');
  if (r.length != 3) return null;
  
  var d = parseInt(r[0].replace(/^[\s]+|[\s]+$/,''));
  var m = parseInt(r[1].replace(/^[\s]+|[\s]+$/,''));
  var y = parseInt(r[2].replace(/^[\s]+|[\s]+$/,''));
    
  if (d >= 1 && d <= 32 && m >= 1 && m <= 12 && y >= 1980 && y <= 3000)
    r = new Date(y, m-1, d);
  else
    r = null; 
    
  return r;  
}

function onSelectCalendarDay(cal, date) {
  if (cal.dateClicked) {
      var y = cal.date.getFullYear();
      var m = cal.date.getMonth() + 1;
      var d = cal.date.getDate();
      
      cal.myTextInput.value = '' + d + '. ' + m + '. ' + y;      
     
	  cal.hide();	
  }
}

function onCloseCalendar(cal) {
  cal.hide();
}

function displayCalendar() {
  var txt = document.getElementById('pdate');
  
  if (txt.calendar == null) {
    txt.calendar = new Calendar(1, new Date(), onSelectCalendarDay, onCloseCalendar);
    txt.calendar.weekNumbers = false;    
    txt.calendar.showOthers = true;
    txt.calendar.create();    
    txt.calendar.myTextInput = txt;
  }
  
  var d = dateFromString(txt.value);
  if (d != null) txt.calendar.setDate(d);

  txt.style.backgroundColor = '';
  txt.calendar.showAtElement(txt);
}

//]]>
</script>

</form>
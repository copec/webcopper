﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Controls_Issue_Ajax : Ka.Garance.Web.GaranceControl
{

	public Ka.Garance.Protocol.Yuki.NovaZavada NovaZavada
	{
		get
		{
			return Ka.Garance.Protocol.Yuki.NovaZavada.Create((Page as Ka.Garance.Web.GarancePage).User.CachedProtocol);
		}
	}

    private void Initialize()
    {
		if (Request.QueryString["setcode"] != null && Request.QueryString["setcode"] != "")
		{
			NovaZavada.Kod = Request.QueryString["setcode"];
		}
		else if (Request.QueryString["addpos"] == "1")
		{
			NovaZavada.Pozice.Add(Convert.ToInt32(Request.QueryString["id"]));
		}
		else if (Request.QueryString["addpos"] == "2")
		{
			NovaZavada.Pozice.Add(
				Request.QueryString["name"],
				Request.QueryString["op"],
				Convert.ToInt32(Request.QueryString["cj"])
			);
		}
		else if (Request.QueryString["addnd"] != null)
		{
			string[] nd = Request.QueryString["addnd"].Split(",".ToCharArray());
			NovaZavada.Nd.Add(
				Convert.ToInt32(nd[0]),
				Convert.ToInt32(nd[1]),
				Convert.ToInt32(nd[2])
			);
		}
		else if (Request.QueryString["delpos"] != null)
		{
			NovaZavada.Pozice.Delete(Convert.ToInt32(Request.QueryString["delpos"]));
		}
		else if (Request.QueryString["delnd"] != null)
		{
			NovaZavada.Nd.Delete(Convert.ToInt32(Request.QueryString["delnd"]));
		}
		else
			return;

		NovaZavada.SaveToCache();
    }

	private void RenderPozice(HtmlTextWriter writer)
	{
		// headers
		writer.Write("<table id=\"tbl_21_pozice_t\" border=\"0\" cellpadding=\"4\" cellspacing=\"1\" class=\"list\">\n");
		writer.Write("<tr class=\"category\">\n <td colspan=\"8\">{0}</td>\n</tr>\n", Language["PROTO_WORK_POS"]);
		writer.Write("<tr class=\"header\">\n");
		writer.Write(" <th style=\"width:1%\">&nbsp;</th>\n");
		writer.Write(" <th align=\"center\" width=\"10%\" nowrap=\"nowrap\">{0}</th>\n", Language["PROTO_POS_BPART"]);
		writer.Write(" <th align=\"center\" width=\"10%\" nowrap=\"nowrap\">{0}</th>\n", Language["PROTO_POS_POS"]);
		writer.Write(" <th align=\"left\">{0}</th>\n", Language["PROTO_POS_NAME"]);
		writer.Write(" <th align=\"left\">{0}</th>\n", Language["PROTO_POS_OP"]);
		writer.Write(" <th align=\"center\" width=\"5%\">{0}</th>\n", Language["PROTO_POS_TIME"]);
		writer.Write(" <th align=\"right\" width=\"10%\">{0}</th>\n", Language["PARTS_PRICE"]);
		writer.Write("</tr>\n");

		int idx = 0;
		foreach (Ka.Garance.Protocol.Yuki.NovaZavadaPozice p in NovaZavada.Pozice)
		{
			writer.Write("<tr class=\"{0}\" style=\"cursor:pointer\" onmouseover=\"if(!this.h)this.h=this.className;this.className=this.h+'-active';\" onmouseout=\"this.className=this.h;\" onclick=\"protocol_SetPosForDeletion(this);\">\n", (idx++ % 2 == 0 ? "even" : "odd"));
			writer.Write(" <td class=\"left\" style=\"width:1%\">&nbsp;</td>\n");
			writer.Write(" <td align=\"center\" width=\"10%\">{0}</td>\n", p.Kod);
			writer.Write(" <td align=\"center\" width=\"10%\">{0}</td>\n", p.Pozice);
			writer.Write(" <td align=\"left\">{0}</td>\n", p.Nazev);
			writer.Write(" <td align=\"left\">{0}</td>\n", p.Operace);
			writer.Write(" <td align=\"center\" width=\"5%\">{0}</td>\n", p.Cj);
			writer.Write(" <td align=\"right\" width=\"10%\">{0:F2}</td>\n", p.CenaDPH);
			writer.Write("</tr>\n");
		}

		// footer
		writer.Write(@"
<tr class=""footer"">
 <td colspan=""8"" align=""left"" style=""padding-top:2px;padding-bottom:2px"">
  <button type=""button"" name=""issue""{0} class=""main"" onclick=""protocol_SetPos(this);"">
   <img src=""images/add.png"" alt="""" />
   {1}
  </button>
  <button id=""btn_posf_delete_p"" type=""button"" class=""main"" style=""visibility:hidden"" onclick=""protocol_DeletePos();"">
   <img src=""images/delete.png"" alt="""" />
   {2}
  </button>
 </td>
</tr>", (NovaZavada.Pozice.Count>0||Regex.IsMatch(NovaZavada.Kod, "^([0-9]{2}\\.|[0-9]{2})[0-9\\-\\.]+$") ? "" : " disabled=\"disabled\""), Language["BUTTON_ADD"], Language["BUTTON_DELETE"]);
		writer.Write("\n</table>\n<div style=\"padding:0px 3px 0px 3px\"><img src=\"images/bk.shadow.png\" width=\"100%\" height=\"1\" /></div>\n<br />\n");
	}

	private void RenderNd(HtmlTextWriter writer)
	{
		writer.Write("<table id=\"tbl_21_nd_t\" border=\"0\" cellpadding=\"4\" cellspacing=\"1\" class=\"list\">\n");
		writer.Write("<tr class=\"category\"><td colspan=\"8\">{0}</td></tr>\n", Language["PROTO_WORK_ND"]);
		writer.Write("<tr class=\"header\">\n");
		writer.Write(" <th width=\"1%\">&nbsp;</th>\n");
		writer.Write(" <th align=\"center\" width=\"10%\">{0}</th>\n", Language["PARTS_GROUP"]);
		writer.Write(" <th align=\"center\" width=\"10%\">{0}</th>\n", Language["PARTS_POS"]);
		writer.Write(" <th align=\"left\">{0}</th>\n", Language["PARTS_NAME"]);
		writer.Write(" <th align=\"center\" width=\"5%\">{0}</th>\n", Language["PROTO_COLOUR"]);
		writer.Write(" <th align=\"center\" width=\"5%\">{0}</th>\n", Language["PROTO_QTY"]);
		writer.Write(" <th align=\"center\" width=\"5%\">{0}</th>\n", Language["PROTO_ORDER"]);
		writer.Write(" <th align=\"right\" width=\"10%\">{0}</th>\n", Language["PARTS_PRICE"]);
		writer.Write("</tr>\n");

		int idx = 0;
		foreach (Ka.Garance.Protocol.Yuki.NovaZavadaNd nd in NovaZavada.Nd)
		{
			writer.Write("<tr class=\"{0}\" style=\"cursor:pointer\" onmouseover=\"if(!this.h)this.h=this.className;this.className=this.h+'-active';\" onmouseout=\"this.className=this.h;\" onclick=\"protocol_SetNdForDeletion(this);\">\n", (idx++ % 2 == 0 ? "even" : "odd"));
			writer.Write(" <td width=\"1%\" class=\"left\">&nbsp;</td>\n");
			writer.Write(" <td align=\"center\" width=\"10%\">{0}</td>", nd.Skupina);
			writer.Write(" <td align=\"center\" width=\"10%\">{0}</td>", nd.Pozice);
			writer.Write(" <td align=\"left\">{0}</td>", (Language.UseEnglishNames ? nd.NazevEn : nd.Nazev));
			writer.Write(" <td align=\"center\">{0}</td>", (Language.UseEnglishNames ? nd.BarvaEn : nd.Barva));
			writer.Write(" <td align=\"center\" width=\"5%\">{0}</td>", nd.Mnozstvi);
			writer.Write(" <td align=\"center\" width=\"5%\">{0}</td>", nd.Objednat);
			writer.Write(" <td align=\"right\" width=\"10%\">{0:F2}</td>", nd.CenaDPH);
			writer.Write("</tr>\n");
		}

		writer.Write(@"
<tr class=""footer"">
 <td colspan=""8"" align=""left"" style=""padding-top:2px;padding-bottom:2px"">
  <button type=""button"" name=""issue""{0} class=""main"" onclick=""protocol_SetNd(this);"">
   <img src=""images/add.png"" alt="""" />
   {1}
  </button>
  <button id=""btn_ndf_delete_p"" type=""button"" class=""main"" style=""visibility:hidden"" onclick=""protocol_DeleteNd();"">
   <img src=""images/delete.png"" alt="""" />
   {2}
  </button>
 </td>
</tr>", (NovaZavada.Nd.Count > 0 || Regex.IsMatch(NovaZavada.Kod, "^([0-9]{2}\\.|[0-9]{2})[0-9\\-\\.]+$") ? "" : " disabled=\"disabled\""), Language["BUTTON_ADD"], Language["BUTTON_DELETE"]);
		writer.Write("\n</table>\n<div style=\"padding:0px 3px 0px 3px\"><img src=\"images/bk.shadow.png\" width=\"100%\" height=\"1\" /></div>\n<br />\n");
	}

	protected override void Render(HtmlTextWriter writer)
	{
		Initialize();

		RenderPozice(writer);
		RenderNd(writer);

		base.Render(writer);
	}

}

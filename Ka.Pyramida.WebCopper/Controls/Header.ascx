﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Controls_Header" Codebehind="Header.ascx.cs" %>

<table border="0" cellpadding="0" cellspacing="0" style="width:100%">
<tr>
 <td style="background:url(images/bk.top.png) repeat-x 0% 0% #8BA8C7"><img src="images/<%=Language["APP_ICON"]%>" alt="<%=Language["APP_TITLE_ICON"]%>" style="margin:2px 7px 2px 7px" /></td>
 <td style="background:url(images/bk.top.png) repeat-x 0% 0% #8BA8C7; text-align: right"><div style="margin:2px 7px 2px 7px"><asp:Label ID="lblVersion" runat="server" Text="" /></div></td>
 
</tr>
<tr>
 <td align="left" valign="bottom" width="99%" height="50" nowrap="nowrap" style="border-bottom:1px solid #C0C0C0;background:url(images/logo.top.png) no-repeat 70% 100% #F3F3F3">
  <div style="padding:0px 0px 2px 28px;color:#C0C0C0">
   <a href="index.aspx" class="menu" onmouseover="if(document._active_ymenu)document._active_ymenu.collapse();"><%=Language["HEADER_INDEX"]%> <img src="images/icon.index.png" alt="<%=Language["HEADER_INDEX"]%>" /></a> |
   <a id="ymd_protocol" href="protocols.aspx" class="menu"><%=Language["HEADER_PROTOCOLS"]%> <img src="images/icon.protocol.png" alt="<%=Language["HEADER_PROTOCOLS"]%>" /></a> |
   <a id="ymd_list" href="index.aspx" class="menu" style="cursor:default" onclick="return false;" onfocus="this.blur();"><%=Language["HEADER_CATALOGS"]%> <img src="images/icon.list.png" alt="<%=Language["HEADER_CATALOGS"]%>" /></a> |
   <a id="ymd_claim" href="claim.aspx" class="menu" onfocus="this.blur();"><%=Language["HEADER_CLAIMS"]%> <img src="images/icon.claim.png" alt="<%=Language["HEADER_CLAIMS"]%>" /></a> |
   <a id="ymd_invoice" href="invoice.aspx" class="menu"><%=Language["HEADER_INVOICES"]%><img src="images/icon.invoice.png" alt="<%=Language["HEADER_INVOICES"]%>" /></a><% if (UserName=="01-000") { %> |
   <a id="ymd_stats" href="stats.aspx" class="menu" onmouseover="if(document._active_ymenu)document._active_ymenu.collapse();"><%=Language["HEADER_STATISTICS"]%><img src="images/icon.stats.png" alt="<%=Language["HEADER_STATISTICS"]%>" /></a><% } %>
  </div>
 </td>
 <td align="right" valign="bottom" nowrap="nowrap" style="border-bottom:1px solid #C0C0C0;background:#F3F3F3">
  <div style="padding:0px 28px 2px 0px;color:#C0C0C0"> 
   <a href="settings.aspx?state=0" class="menu"><%=Language["HEADER_SETTINGS"]%> <img src="images/icon.settings.png" alt="<%=Language["HEADER_SETTINGS"]%>" /></a> |
   <a href="login.aspx?logout=" class="menu"><%=Language["HEADER_LOGOUT"]%> <img src="images/icon.login.png" alt="<%=Language["HEADER_LOGOUT"]%>" /></a>
  </div>
 </td>
</tr>
<tr>
 <td colspan="2" style="padding:0px 2px 0px 2px"><img src="images/bk.shadow.png" width="100%" height="1" alt="" /></td>
</tr>
<tr>
 <td align="left" valign="top" height="28" style="background:url(images/logo.bottom.png) no-repeat 70% 0%"><div style="padding:2px 0px 0px 28px;color:#808080"><b><%=Language["LOCATION"]%></b> <%=Location%></div></td>
 <td align="right" valign="top" nowrap="nowrap">
  <div style="padding:2px 28px 0px 0px">
   <%=Language["HEADER_USER"]%>: <b><%=UserTitle%></b> |
   <%=DateTime.Now.ToString("D", System.Globalization.CultureInfo.GetCultureInfo(Language.Code))%>
  </div>
 </td>
</tr>
</table>

<script language="javascript" type="text/javascript" src="js/menu.js"></script>
<script language="javascript" type="text/javascript">
//<![CDATA[
var m1 = new yuki_Menu('ymd_protocol');
m1.add('<%=Language["HEADER_PROTOCOL_ADD"]%>', 'images/icon.protocol.add.png', 'protocols.aspx?add=', '<%=Language["INDEX_PROTOADD_DESC"]%>');
var m2 = new yuki_Menu('ymd_invoice');
m2.add('<%=Language["HEADER_INVOICE_ADD"]%>', 'images/icon.invoice.add.png', 'invoice.add.aspx', '<%=Language["INDEX_INVOICEADD_DESC"]%>');
var m3 = new yuki_Menu('ymd_claim');
m3.add('<%=Language["HEADER_CLAIM_ADD"]%>', 'images/icon.claim.add.png', 'claim.add.aspx', '<%=Language["INDEX_CLAIMADD_DESC"]%>');
var m4 = new yuki_Menu('ymd_list');
m4.add('<%=Language["HEADER_BIKES"]%>', 'images/icon.moto.png', 'bikes.aspx', '<%=Language["INDEX_BIKES_DESC"]%>');
m4.add('<%=Language["HEADER_PARTS"]%>', 'images/icon.catalog.png', 'parts.aspx', '<%=Language["INDEX_PARTS_DESC"]%>');
//]]>
</script>
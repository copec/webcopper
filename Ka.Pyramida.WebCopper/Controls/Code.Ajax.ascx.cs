﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Ka.Garance.Data;
using System.Collections.Generic;

public partial class Controls_Code_Ajax : Ka.Garance.Web.GaranceControl
{
	public Ka.Garance.Protocol.Yuki.NovaZavada NovaZavada
	{
		get
		{
			Ka.Garance.User.GaranceUser user = (Page as Ka.Garance.Web.GarancePage).User;
			string id = (Request.QueryString["ajax"] == "nd" ? "nd" : "code");
			return Ka.Garance.Protocol.Yuki.NovaZavada.Create(user.CachedProtocol.VIN, user, id);
		}
	}

	public Ka.Garance.Protocol.Yuki.YukiProtocol Protocol
	{
		get
		{
			return (Page as Ka.Garance.Web.GarancePage).User.CachedProtocol;
		}
	}

	public string ImagePath
	{
		get { return string.Format("nd/{0}/{1}.jpg", NovaZavada.ModelVzor, NovaZavada.TmpSkupina); }
	}

	public bool IsImageAvailable
	{
		get { return System.IO.File.Exists(Request.MapPath(ImagePath)); }
	}

	public System.Drawing.Size ImageSize
	{
		get
		{
			System.Drawing.Size s = new System.Drawing.Size(0, 0);
			if (IsImageAvailable)
			{
				try
				{
					System.Drawing.Image img = System.Drawing.Image.FromFile(Request.MapPath(ImagePath));
					try
					{
						s.Width = img.Width;
						s.Height = img.Height;
					}
					finally
					{
						img.Dispose();
					}
				}
				catch { }
			}
			return s;
		}
	}

	private string GetGroupOptions()
	{
		StringBuilder sb = new StringBuilder();
		sb.Append("<select id=\"pcd_101_codea\" style=\"width:100%\" onchange=\"this.disabled=true;this.blur();__gwl_ajax('code.p','groupId',this.value);\">\n");

	    var groupItems = new List<Ka.Garance.Util.GaranceUtil.GroupItem>();

        Ka.Garance.Util.GaranceUtil.GetPartsGroupOptionsItems(ref groupItems, NovaZavada.ModelVzor, Database, Language);
        
        //MySQLDataReader dr = Database.ExecuteReader(string.Format(
        //    "SELECT IFNULL(skupina,'') AS id,skupinatext{0} AS grp_name FROM tcskodnicisla WHERE skupina IS NOT NULL AND typmoto='{1}' AND state<>9 GROUP BY skupina",
        //    (Language.UseEnglishNames ? "_en" : ""),
        //    MySQLDatabase.Escape(NovaZavada.ModelVzor)
        //));
        //while (dr.Read())
        //{
        //    if (NovaZavada.TmpSkupina == "" || NovaZavada.TmpSkupina == null)
        //        NovaZavada.TmpSkupina = dr.GetStringValue(0);
        //    sb.AppendFormat("<option value=\"{0}\"", dr.GetValue("id"));
        //    if (dr.GetStringValue(0).ToLower() == NovaZavada.TmpSkupina.ToLower())
        //        sb.AppendFormat(" selected=\"selected\"");
        //    sb.AppendFormat(">{0}</option>\n", dr.GetValue("grp_name"));
        //}
        //sb.Append("</select>");

       foreach(var gi in groupItems)
        {
            if (NovaZavada.TmpSkupina == "" || NovaZavada.TmpSkupina == null)
                NovaZavada.TmpSkupina = gi.Id;
            sb.AppendFormat("<option value=\"{0}\"", gi.Id);
            if (gi.Id.ToLower() == NovaZavada.TmpSkupina.ToLower())
                sb.AppendFormat(" selected=\"selected\"");
            sb.AppendFormat(">{0}</option>\n", gi.Name);
        }
        sb.Append("</select>");

		return sb.ToString();
	}

	private void RenderHeader(HtmlTextWriter writer)
	{
		// text if specified
		writer.Write("<table border=\"0\" cellpadding=\"4\" cellspacing=\"1\" class=\"list\">\n");
		writer.Write("<tr class=\"header\"><th align=\"left\">{0}</th></tr>\n", (Request.QueryString["ajax"] == "nd" ? Language["PROTO_PART"] : Language["PROTO_SET_CODE"]));
		writer.Write("</table>\n<br />\n");
		writer.Write("<table border=\"0\" cellpadding=\"4\" cellspacing=\"1\" class=\"list\">\n");
		writer.Write("<tr class=\"category\"><td colspan=\"2\" align=\"left\">{0}</td></tr>", Language["PROTO_BIKE"]);
		writer.Write("<tr class=\"even\">\n");
		writer.Write(" <td width=\"90\">{0}</td>\n", Language["BIKES_LIST_CHASSIS"]);
		writer.Write(" <td class=\"odd\"><input type=\"text\" readonly=\"readonly\" value=\"{0}\" class=\"sw-input\" style=\"width:98.5%;color:#808080;background:#F8F8F8\" /></td>\n", Server.HtmlEncode(NovaZavada.VIN));
		writer.Write("</tr>\n<tr class=\"even\">\n");
		writer.Write(" <td width=\"90\">{0}</td>\n", Language["BIKES_LIST_MODEL"]);
		writer.Write(" <td class=\"odd\"><input type=\"text\" readonly=\"readonly\" value=\"{0}\" class=\"sw-input\" style=\"width:98.5%;color:#808080;background:#F8F8F8\" /></td>\n", Server.HtmlEncode(NovaZavada.ModelNazev));
		writer.Write("</tr>\n</table>\n<br />\n");
	}

	private void RenderImage(HtmlTextWriter writer)
	{
		writer.Write("<table border=\"0\" cellpadding=\"4\" cellspacing=\"1\" class=\"list\">\n");
		writer.Write("<tr class=\"category\"><td colspan=\"2\" align=\"left\">{0}</td></tr>", (Request.QueryString["ajax"] == "nd" ? Language["PROTO_PART"] : Language["PROTO_SET_CODE"]));
		writer.Write("<tr class=\"infotext\"><td align=\"left\" colspan=\"2\">{0}</td></tr>\n", Language["PROTO_NOTE_IMAGE"]);
		writer.Write("<tr class=\"even\">\n");
		writer.Write(" <td width=\"90\"><label for=\"pcd_101_codea\">{0}</label></td>\n", Language["PARTS_GROUP"]);
		writer.Write(" <td width=\"399\" class=\"odd\">{0}</td>\n", GetGroupOptions());
		writer.Write("</tr>\n");
		if (IsImageAvailable)
		{
			System.Drawing.Size s = ImageSize;
			writer.Write("<tr><td colspan=\"2\" style=\"padding:0px;border-top:1px solid #CDD6E3\"><div style=\"height:240px;background:url({0}) no-repeat 8px 8px\" onmousemove=\"this.cx={1};this.cy={2};positionNdImage(this,event);\">&nbsp;</div></td></tr>\n", ImagePath, s.Width, s.Height);
		}
		else
			writer.Write("<tr class=\"even\"><td colspan=\"2\" align=\"center\" style=\"background-color:#F3F3F3\">{0}</td></tr>\n", Language["PARTS_IMAGE_NOT_AVAILABLE"]);
		writer.Write("</table>\n<div style=\"padding:0px 3px 0px 3px\"><img src=\"images/bk.shadow.png\" width=\"100%\" height=\"1\" /></div>\n<br />\n");
	}

	private void RenderPozice(HtmlTextWriter writer)
	{
        MCPosList.Filter = " (";

        if (!string.IsNullOrEmpty(NovaZavada.ModelMotor))
            MCPosList.Filter += string.Format(" (A.skupina='{0}' AND A.typmoto='{1}' AND A.state<>9 AND A.partgroup = 1) OR ", MySQLDatabase.Escape(NovaZavada.TmpSkupina), MySQLDatabase.Escape(NovaZavada.ModelMotor));
        else if (!string.IsNullOrEmpty(NovaZavada.ModelRam))
            MCPosList.Filter += string.Format(" (A.skupina='{0}' AND A.typmoto='{1}' AND A.state<>9 AND A.partgroup = 0) OR ", MySQLDatabase.Escape(NovaZavada.TmpSkupina), MySQLDatabase.Escape(NovaZavada.ModelRam));
        
        MCPosList.Filter += string.Format(" (A.skupina='{0}' AND A.typmoto='{1}' AND A.state<>9) ", MySQLDatabase.Escape(NovaZavada.TmpSkupina), MySQLDatabase.Escape(NovaZavada.ModelVzor));        
        MCPosList.Filter += ") ";
	    
        MCPosList.GroupBy = "GROUP BY id,partname,pozice";

        MCPosList.SQL = string.Format(
			@"SELECT DISTINCT
				A.pozice AS id,
				IF(TRIM(IFNULL(D.sklcislo,''))='',A.nazevdilu{0},D.nazevdilu{0}) AS partname,
				CONCAT(A.skupina,'.',A.pozice) AS pozice,
				MAX(A.id) AS kodid
			FROM tcskodnicisla A
			LEFT JOIN tcskodnicisla D ON A.objcislo=D.sklcislo AND D.state<>9",
			(Language.UseEnglishNames ? "_en" : "")
		);
		MCPosList.AjaxId = "code.p";
		MCPosList.Identifier = "kodid";
		MCPosList.JsClickRow = "protocol_SetCodeValue2(this,{0});";

		MCPosList.CategoryTitle = Language["PROTO_POS_POS"];
		MCPosList.RenderControl(writer);
		writer.Write("\n<div style=\"padding:0px 3px 0px 3px\"><img src=\"images/bk.shadow.png\" width=\"100%\" height=\"1\" /></div>\n");
	}

	/// <summary>
	/// Vykreslenie nahradnych dielov cez ajax na stranke http://localhost/ka/webcopper/protocol.edit.aspx
	/// pridavanie dielov do protokolu !!!
	/// </summary>
	/// <param name="writer"></param>
	private void RenderNd(HtmlTextWriter writer)
	{
		ListItem5.FormatProvider = System.Globalization.CultureInfo.InvariantCulture;
		ListItem6.Format = "<input type=\"text\" size=\"2\" value=\"{0}\" disabled=\"disabled\" class=\"sw-input\" style=\"visibility:hidden\" onfocus=\"this.maxValue={0};\" onmouseover=\"protocol_DisplayQtyTip(this,1);\" />";
		ListItem7.Format = "<input type=\"text\" size=\"2\" value=\"{0}\" disabled=\"disabled\" class=\"sw-input\" style=\"visibility:hidden\" onfocus=\"this.maxValue={0};\" onmouseover=\"protocol_DisplayQtyTip(this,2);\" />";
		
		// konstanta pre nasobenie ceny
		// pre stary protokol ? 
		decimal cenaK = 0.7m * ((100 + (Page as Ka.Garance.Web.GarancePage).User.Rabat) / 100) * ((100 + (Page as Ka.Garance.Web.GarancePage).User.DPH) / 100);
        if (this.Protocol.DateCreated < Ka.Garance.Protocol.Yuki.YukiProtocol.DATE_NEW_RECOUNT_2013)
            cenaK = 0.605m * ((100 + (Page as Ka.Garance.Web.GarancePage).User.Rabat) / 100) * ((100 + (Page as Ka.Garance.Web.GarancePage).User.DPH) / 100);
        if (this.Protocol.DateCreated < Ka.Garance.Protocol.Yuki.YukiProtocol.DATE_NEW_RECOUNT_2012_10)
            cenaK = 0.35m * ((100 + (Page as Ka.Garance.Web.GarancePage).User.Rabat) / 100) * ((100 + (Page as Ka.Garance.Web.GarancePage).User.DPH) / 100);
        if (this.Protocol.DateCreated < Ka.Garance.Protocol.Yuki.YukiProtocol.DATE_NEW_RECOUNT_2012)
            cenaK = 0.75m * ((100 + (Page as Ka.Garance.Web.GarancePage).User.Rabat) / 100) * ((100 + (Page as Ka.Garance.Web.GarancePage).User.DPH) / 100);
        if (this.Protocol.DateCreated < Ka.Garance.Protocol.Yuki.YukiProtocol.DATE_NEW_RECOUNT)
			cenaK = ((100 - (Page as Ka.Garance.Web.GarancePage).User.Rabat) / 100) * ((100 + (Page as Ka.Garance.Web.GarancePage).User.DPH) / 100);

		//string filter = string.Format("A.skupina={0} AND A.typmoto='{1}' AND A.state<>9 AND B.idzeme={2}", NovaZavada.TmpSkupina, MySQLDatabase.Escape(NovaZavada.ModelVzor), (Page as Ka.Garance.Web.GarancePage).User.IdZeme);
        string filter = " (";

        if (!string.IsNullOrEmpty(NovaZavada.ModelMotor))
            filter += string.Format(" (A.skupina='{0}' AND A.typmoto='{1}' AND A.state<>9 AND A.partgroup = 1) OR ", MySQLDatabase.Escape(NovaZavada.TmpSkupina), MySQLDatabase.Escape(NovaZavada.ModelMotor));

        else if (!string.IsNullOrEmpty(NovaZavada.ModelRam))
            filter += string.Format(" (A.skupina='{0}' AND A.typmoto='{1}' AND A.state<>9 AND A.partgroup = 0) OR ", MySQLDatabase.Escape(NovaZavada.TmpSkupina), MySQLDatabase.Escape(NovaZavada.ModelRam));

        filter += string.Format(" (A.skupina={0} AND A.typmoto='{1}' AND A.state<>9 ) ", NovaZavada.TmpSkupina, MySQLDatabase.Escape(NovaZavada.ModelVzor));

        filter += ") ";

        string sql = string.Format(
			System.Globalization.CultureInfo.InvariantCulture,
            @"
				SELECT DISTINCT
					A.sklcislo AS sklcislo,
					A.id AS kodid,
					A.pozice AS id,
					IF(TRIM(IFNULL(D.sklcislo,''))='',A.nazevdilu{0},D.nazevdilu{0}) AS partname,
					CONCAT(A.skupina,'.',A.pozice) AS pozice,
					ROUND(B.NM*{1:F6},2) AS cena,
					A.ks,
					C.Barva{2} AS barva,
                    IF(TRIM(IFNULL(A.pozn,''))='',D.pozn,A.pozn) AS pozn
				FROM tcskodnicisla A
					LEFT JOIN tcskodnicisla D ON A.objcislo=D.sklcislo AND D.state<>9
					LEFT JOIN ndcenik B ON B.SklCislo=IF(D.sklcislo IS NULL,A.sklcislo,D.sklcislo) AND B.idzeme={3}
					LEFT JOIN ndbarvy C ON C.Identifikator=SUBSTRING(A.sklcislo, 8)",
			(Language.UseEnglishNames ? "_en" : ""), cenaK,
			(Language.UseEnglishNames ? "EN" : "CZ"),
			(Page as Ka.Garance.Web.GarancePage).User.IdZeme
        );
        MCNdList.Filter = filter;
        MCNdList.SQL = sql;
		//MCNdList.SQLCount = string.Format("SELECT COUNT(DISTINCT A.id) FROM tcskodnicisla A LEFT JOIN ndcenik B ON B.SklCislo=A.sklcislo WHERE A.skupina={0} AND A.typmoto='{1}' AND A.state<>9 AND B.idzeme={2}", NovaZavada.TmpSkupina, MySQLDatabase.Escape(NovaZavada.ModelVzor), (Page as Ka.Garance.Web.GarancePage).User.IdZeme); ;
        MCNdList.SQLCount = string.Format("SELECT COUNT(DISTINCT A.id) FROM tcskodnicisla A LEFT JOIN ndcenik B ON B.SklCislo=A.ObjCislo WHERE {0}", filter) ;
		MCNdList.AjaxId = "code.p";
		MCNdList.Identifier = "kodid";
		MCNdList.JsClickRow = "protocol_SetCodeValue2(this,'{0}');";

		MCNdList.CategoryTitle = Language["PROTO_POS_POS"];

		MCNdList.RenderControl(writer);
		writer.Write("\n<div style=\"padding:0px 3px 0px 3px\"><img src=\"images/bk.shadow.png\" width=\"100%\" height=\"1\" /></div>\n");
        
        //writer.Write("\n<div>" + sql + " " + filter + "</div>");

	}

	private void RenderFooter(HtmlTextWriter writer)
	{
		writer.Write("<br />\n\n<table border=\"0\" celpadding=\"2\" cellspacing=\"1\" class=\"list\">\n<tr class=\"footer\">\n <td align=\"right\">");
		writer.Write(@"
  <button id=""t_proto_codef_btnok"" type=""button"" disabled=""disabled"" class=""main"" style=""font-weight:bold"" onclick=""protocol_SubmitCode2();"">
   <img src=""images/ok.png"" />
   {0}
  </button>
  <button type=""button"" class=""main"" onclick=""__gwl_ajax('code.p', 'close');"">
   <img src=""images/cancel.png"" />
   {1}
  </button>", Language["BUTTON_CONFIRM"], Language["BUTTON_CANCEL"]);
		writer.Write("\n </td>\n</tr>\n</table>\n<div style=\"padding:0px 3px 0px 3px\"><img src=\"images/bk.shadow.png\" width=\"100%\" height=\"1\" /></div>\n");
	}

	protected override void Render(HtmlTextWriter writer)
	{
		if (Request.QueryString["groupId"] != null)
		{
			NovaZavada.TmpSkupina = Request.QueryString["groupId"];
		}

		// RenderHeader(writer);
		writer.Write("<div id=\"Code.Ajax.cs_PreloadingDiv\">\n");
		RenderImage(writer);
		if (Request.QueryString["ajax"] == "nd")
			RenderNd(writer);
		else
			RenderPozice(writer);
		RenderFooter(writer);
		writer.Write("</div>\n");
	}
}

﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Ka.Garance.Data;

public partial class Controls_Pozice_Ajax : Ka.Garance.Web.GaranceControl
{

	public Ka.Garance.Protocol.Yuki.NovaZavada NovaZavada
	{
		get
		{
			return Ka.Garance.Protocol.Yuki.NovaZavada.Create((Page as Ka.Garance.Web.GarancePage).User.CachedProtocol);
		}
	}

    protected void Page_Load(object sender, EventArgs e)
    {

    }

	private void RenderHeader(HtmlTextWriter writer)
	{
		writer.Write("<table border=\"0\" cellpadding=\"4\" cellspacing=\"1\" class=\"list\">\n");
		writer.Write("<tr class=\"header\"><th align=\"left\">{0}</th></tr>\n", Language["PROTO_POS_SELECT"]);
		writer.Write("</table>\n<br />\n");
	}

	private string GetSkodniCisloCond(string skodniCislo)
	{
		string c1 = "", c2 = "";
		string[] sp = skodniCislo.Split(new char[] { '.' }, 2);
		if (sp.Length == 2)
		{
			c1 = sp[0];
			c2 = sp[1];
		}
		else
		{
			c1 = skodniCislo.Substring(0, 2);
			c2 = skodniCislo.Substring(2);
		}

		int i = c2.IndexOf('-');
		if (i > 0)
		{
			c2 = c2.Substring(0, i);
		}

		int n1 = 0, n2 = 0;

		if (int.TryParse(c1, out n1) && int.TryParse(c2, out n2))
		{
			return string.Format(@"
				skodnicislo='{0}.{1}' OR skodnicislo LIKE '{0}.{1}-%' OR skodnicislo='{0}.{1:D2}' OR
				skodnicislo LIKE '{0}.{1:D2}-%' OR skodnicislo='{0}{1:D2}' OR skodnicislo LIKE '{0}{1:D2}%' OR
				skodnicislo='{0:D2}.{1}' OR skodnicislo LIKE '{0:D2}.{1}-%' OR skodnicislo='{0:D2}.{1:D2}' OR
				skodnicislo LIKE '{0:D2}.{1:D2}-%' OR skodnicislo='{0:D2}{1:D2}' OR skodnicislo LIKE '{0:D2}{1:D2}%' OR
				skodnicislo='{0:D2}{1:D2}' OR skodnicislo='{2}'",
				n1, n2, skodniCislo
			);
		}

		return string.Format(
			"skodnicislo='{0}.{1}' OR skodnicislo LIKE '{0}.{1}-%' OR skodnicislo='{0}{1}' OR skodnicislo='{2}'",
			MySQLDatabase.Escape(c1),
			MySQLDatabase.Escape(c2),
			MySQLDatabase.Escape(skodniCislo)
		);
	}

	private void RenderItems(HtmlTextWriter writer)
	{
		// header
		writer.Write("<table id=\"t_proto_posf_tbl\" border=\"0\" cellpadding=\"4\" cellspacing=\"1\" class=\"list\">\n");
		writer.Write("<tr class=\"category\"><td align=\"left\" colspan=\"6\">{0}</td></tr>\n", Language["PROTO_WORK_POS"]);
		writer.Write("<tr>\n");
		writer.Write(" <th width=\"1%\">&nbsp;</th>\n");
		writer.Write(" <th align=\"center\" width=\"10%\">{0}</th>\n", Language["PROTO_POS_BPART"]);
		writer.Write(" <th align=\"center\" width=\"10%\">{0}</th>\n", Language["PROTO_POS_POS"]);
		writer.Write(" <th>{0}</th>\n", Language["PROTO_POS_NAME"]);
		writer.Write(" <th>{0}</th>\n", Language["PROTO_POS_OP"]);
		writer.Write(" <th>{0}</th>\n", Language["PROTO_POS_TIME"]);
		writer.Write("</tr>\n");

		// items
		int idx = 0;
		// get the items
		MySQLDataReader dr = Database.ExecuteReader(@"
			SELECT id,skodnicislo,pozicecislo,nazev,operace,cj
			FROM tcpozice
			WHERE model={0} AND (
				" + GetSkodniCisloCond(NovaZavada.SkodniCislo) + @"
			) AND state<>9 ORDER BY nazev", NovaZavada.ModelVzor
		);
		while (dr.Read())
		{
			writer.Write("<tr class=\"{0}\" onmouseover=\"this.cid='{1}';this.style.cursor='pointer';if(!this.h)this.h=this.className;this.className=this.h+'-active';\" onmouseout=\"this.className=this.h;\" onclick=\"protocol_SelectPos(this);\">\n", (idx % 2 == 0 ? "even" : "odd"), Ka.Garance.Web.GarancePage.JsEncode(dr.GetStringValue(0)));
			writer.Write(" <td class=\"left\">&nbsp;</td>\n");
			writer.Write(" <td align=\"center\">{0}</td>\n", dr.GetValue("skodnicislo"));
			writer.Write(" <td align=\"center\">{0}</td>\n", dr.GetValue("pozicecislo"));
			writer.Write(" <td>{0}</td>\n", dr.GetValue("nazev"));
			writer.Write(" <td>{0}</td>\n", dr.GetValue("operace"));
			writer.Write(" <td>{0}</td>\n", dr.GetValue("cj"));
			writer.Write("</tr>\n");
			idx++;
		}

		// volna pozice
		writer.Write("<tr class=\"{0}\" onmouseover=\"this.style.cursor='pointer';if(!this.h)this.h=this.className;this.className=this.h+'-active';\" onmouseout=\"this.className=this.h;\" onclick=\"protocol_SelectPos(this,true);\">\n", (idx % 2 == 0 ? "even" : "odd"));
		writer.Write(" <td class=\"left\">&nbsp;</td>\n");
		writer.Write(" <td align=\"center\">99</td>\n");
		writer.Write(" <td align=\"center\">99</td>\n");
		writer.Write(" <td>{0}</td>\n", Language["PROTO_POS_UNAME"]);
		writer.Write(" <td>{0}</td>\n", Language["PROTO_POS_UOP"]);
		writer.Write(" <td>0</td>\n");
		writer.Write("</tr>\n");

		// footer
		writer.Write("</table>\n<div style=\"padding:0px 3px 0px 3px\"><img src=\"images/bk.shadow.png\" width=\"100%\" height=\"1\" /></div>\n<br />\n");

	}

	private void RenderVolnaPolozka(HtmlTextWriter writer)
	{
		writer.Write("<table border=\"0\" cellpadding=\"4\" cellspacing=\"1\" class=\"list\">\n");
		writer.Write("<tr class=\"category\"><td colspan=\"4\" align=\"left\">{0}</td></tr>\n", Language["PROTO_POS_FTITLE"]);
		writer.Write("<tr class=\"even\">\n <td width=\"15%\"><label for=\"t_proto_posf_name\">{0}</label></td>\n", Language["PROTO_POS_NAME"]);
		writer.Write(" <td colspan=\"3\" class=\"odd\"><input id=\"t_proto_posf_name\" type=\"text\" value=\"{0}\" disabled=\"disabled\" class=\"sw-input\" style=\"width:99%\" onkeyup=\"protocol_ValidatePosCustom();\" /></td>\n", Server.HtmlEncode(Language["PROTO_POS_UNAME"]));
		writer.Write("</tr>\n");
		writer.Write("<tr class=\"even\">\n <td><label for=\"t_proto_posf_op\">{0}</label></td>\n", Language["PROTO_POS_OP"]);
		writer.Write(" <td class=\"odd\"><input id=\"t_proto_posf_op\" type=\"text\" value=\"{0}\" disabled=\"disabled\" class=\"sw-input\" style=\"width:98.5%\" onkeyup=\"protocol_ValidatePosCustom();\" /></td>\n", Server.HtmlEncode(Language["PROTO_POS_UOP"]));
		writer.Write(" <td width=\"1%\" nowrap=\"nowrap\"><label for=\"t_proto_posf_cj\">{0}</label>&nbsp;</td>\n", Language["PROTO_POS_TIME"]);
		writer.Write(" <td width=\"1%\" class=\"odd\"><input id=\"t_proto_posf_cj\" type=\"text\" size=\"2\" value=\"0\" disabled=\"disabled\" class=\"sw-input\" onkeyup=\"protocol_ValidatePosCustom();\" /></td>\n");
		writer.Write("</tr>\n");
		writer.Write("</table>\n<div style=\"padding:0px 3px 0px 3px\"><img src=\"images/bk.shadow.png\" width=\"100%\" height=\"1\" /></div>\n<br />\n");
	}

	private void RenderFooter(HtmlTextWriter writer)
	{
		base.Render(writer);
	}

	protected override void Render(HtmlTextWriter writer)
	{
		// RenderHeader(writer);
		RenderItems(writer);
		RenderVolnaPolozka(writer);
		RenderFooter(writer);
	}
}

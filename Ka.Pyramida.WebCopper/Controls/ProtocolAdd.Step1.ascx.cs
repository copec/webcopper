﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Ka.Garance.Data;
using Ka.Garance.Protocol.Yuki;

public partial class Controls_ProtocolAdd_Step1 : Ka.Garance.Web.GaranceControl
{
	public YukiProtocol Protocol
	{
		get
		{
			return (Page as Ka.Garance.Web.GarancePage).User.CachedProtocol;
		} 
	}

	public string JsProtocolVIN
	{
		get
		{
			if (Request.Form["pvin"] != null)
				return Ka.Garance.Web.GarancePage.JsEncode(Request.Form["pvin"]);
			return Ka.Garance.Web.GarancePage.JsEncode(Protocol.VIN);
		}
	}

	private DateTime DateFromString(string date)
	{
		string[] ds = date.Split(".".ToCharArray());
		return new DateTime(
			Convert.ToInt32(ds[2].Trim()),
			Convert.ToInt32(ds[1].Trim()),
			Convert.ToInt32(ds[0].Trim())
		);
	}

	public int TotalBikesAvailable
	{
		get
		{
			return Convert.ToInt32(Database.ExecuteScalar("SELECT COUNT(*) FROM tmotocykl WHERE iddealer={0} AND IFNULL(prodej_datum,'')<>'' AND stav=2", (Page as Ka.Garance.Web.GarancePage).User.Id));
		}
	}

	protected bool CanDisplayOwner
	{
		get { return (!Protocol.JeNovyProtokol || proto_chk.Value == "1"); }
	}

	private NameValueCollection m_Errors = new NameValueCollection();
	private void CollectPostedData()
	{
		bool isBadKm = false, isBadDate = false;

		if (CanDisplayOwner)
		{
			Protocol.JeFO = owner_type.SelectedIndex;
			Protocol.ICO = Request.Form["pico"];
			Protocol.Jmeno = Request.Form["pname"];
			Protocol.Telefon = Request.Form["pphone"];
			Protocol.Adresa = Request.Form["paddress"];
			Protocol.PSC = Request.Form["pzipcode"];
			Protocol.Obec = Request.Form["pcity"];
		}

		try
		{
			Protocol.Km = Convert.ToInt32(Request.Form["pkm"]);
		}
		catch
		{
			isBadKm = true;			
			Protocol.Km = -1;
		}
		try
		{
            int ProtocolMaxBackwardCreateDays = -720;
            int.TryParse(ConfigurationManager.AppSettings["Protocol.MaxBackwardCreateDays"], out ProtocolMaxBackwardCreateDays);

            Protocol.DateRepair = DateFromString(Request.Form["pdate"]);
            if (Protocol.DateRepair < DateTime.Today.AddDays(ProtocolMaxBackwardCreateDays))
            {
                Protocol.DateRepair = DateTime.Today.AddDays(ProtocolMaxBackwardCreateDays);
                Protocol.DateRepairOverflow = true;
            }

		}
		catch { isBadDate = true;  }
		Protocol.VIN = Request.Form["pvin"];
		Protocol.VymenaTachometra = (Request.Form["ptacho"] == "1");
		Protocol.SaveToCache();
		Protocol.PredprodejniServis = (Request.Form["ppservis"] == "1");

		m_Errors.Clear();

		if (CanDisplayOwner)
		{
			if (Protocol.Jmeno == null || Protocol.Jmeno.Trim() == "")
				m_Errors.Add("jmeno", Language["PROTO_ERR_NAME"]);
			if (Protocol.JeFO > 0 && (Protocol.ICO == null || Protocol.ICO.Trim() == ""))
				m_Errors.Add("ico", Language["PROTO_ERR_ICO"]);
			if (Protocol.Telefon == null || Protocol.Telefon.Trim() == "")
				m_Errors.Add("tel", Language["PROTO_ERR_TELEPHONE"]);
			if (Protocol.Adresa == null || Protocol.Adresa.Trim() == "")
				m_Errors.Add("adresa", Language["PROTO_ERR_ADDRESS"]);
			if (Protocol.PSC == null || Protocol.PSC.Trim() == "")
				m_Errors.Add("psc", Language["PROTO_ERR_ZIP"]);
			if (Protocol.Obec == null || Protocol.Obec.Trim() == "")
				m_Errors.Add("mesto", Language["PROTO_ERR_CITY"]);
		}

		if (isBadDate)
			m_Errors.Add("date", Language["PROTO_ERR_DATE"]);
		if (!Protocol.IsValidVIN)
			m_Errors.Add("vin", Language["PROTO_ERR_VIN"]);
		else if (!Protocol.PredprodejniServis && !Protocol.IsProdanyVIN)
			m_Errors.Add("vin", Language["PROTO_ERR_VIN2"]);
		else if (isBadKm)
			m_Errors.Add("km", Language["PROTO_ERR_INVALID_KM"].Replace("%s", Request.Form["pkm"]));
		else if (!Protocol.IsValidKm)
			m_Errors.Add("km", Language["PROTO_ERR_KM"]);
		else if (Protocol.KmMimoZaruky || Protocol.ZarukaVyprsela)
			m_Errors.Add("km", Language["PROTO_ERR_KM2"]);

		if (!CanDisplayOwner && m_Errors.Count == 0)
		{
			// load protocol user details
			Protocol.LoadMajitel();
		}
	}

    private void Initialize()
    {
		try { owner_type.SelectedIndex = Convert.ToInt32(Request.Form[owner_type.UniqueID]); }
		catch { }

		owner_type.Items[0].Text = Language["PROTO_OWNER_FO"];
		owner_type.Items[1].Text = Language["PROTO_OWNER_FO_ICO"];
		owner_type.Items[2].Text = Language["PROTO_OWNER_PO"];

		if (Request.HttpMethod == "POST" && Request.Form["_step"] == "ax564jdhrk134nfg5")
		{
			if (!Protocol.JeNovyProtokol)
				proto_chk.Value = "1";
			else
				proto_chk.Value = Request.Form[proto_chk.UniqueID];

			CollectPostedData();

			if (!CanDisplayOwner)
			{
				proto_chk.Value = "1";
			}
			else if (m_Errors.Count == 0)
			{
				Protocol.ActualStep = 2;
				Response.Redirect(Request.FilePath);
			}
		}
    }

	private void RenderBikesAjax(HtmlTextWriter writer, string pat)
	{
		// Povodne: ORDER BY datum DESC,ciskar DESC,model DESC
		// 24. 10. 2006 - Honza - Podle tcmodelklic.jmeno
		writer.Write("OK-");
		MySQLDataReader dr = Database.ExecuteReader(string.Format(@"
SELECT
	tmotocykl.ciskar AS ciskar,
	CONCAT(tmotocykl.model, ' - ', tcmodelklic.jmeno) AS model,
	CAST(IF(IFNULL(tmotocykl.prodej_datum,'')='',NULL,CONCAT('20',MID(tmotocykl.prodej_datum,5,2),'-',MID(tmotocykl.prodej_datum,3,2),'-',LEFT(tmotocykl.prodej_datum,2))) AS DATE) AS datum
FROM tmotocykl
LEFT JOIN tcmodelklic ON tcmodelklic.id=tmotocykl.model
WHERE tmotocykl.iddealer={{0}} AND IFNULL(tmotocykl.prodej_datum,'')<>'' AND tmotocykl.stav=2 AND (tmotocykl.ciskar LIKE '{1}%' OR tmotocykl.model LIKE '{1}%' OR tcmodelklic.jmeno LIKE '{1}%')
GROUP BY ciskar
ORDER BY tcmodelklic.jmeno,datum DESC,ciskar DESC
LIMIT 20",
		(Language.UseEnglishNames ? "_en" : ""), MySQLDatabase.Escape(pat, true)), (Page as Ka.Garance.Web.GarancePage).User.Id);

		while (dr.Read())
		{
			// zisti, ci je v zaruke
			/*if (YukiProtocol.JePoZaruke(Database, dr.GetStringValue(0), DateTime.Now.AddDays(-31)))
				continue;*/
			writer.Write("bikeCombo.ajaxAdd('{0}','{0}','{1}','{3} {2}');\n",
				Ka.Garance.Web.GarancePage.JsEncode(dr.GetStringValue(0)),
				Ka.Garance.Web.GarancePage.JsEncode(dr.GetStringValue(1)),
				Ka.Garance.Web.GarancePage.JsEncode(dr.GetDateValue(2).ToString("d'. 'M'. 'yyyy")),
				Ka.Garance.Web.GarancePage.JsEncode(Language["PROTO_SOLD"])
			);
		}
	}

	protected override void Render(HtmlTextWriter writer)
	{
		Initialize();
		if (Request.QueryString["ajaxbikes"] != null)
		{
			writer.Flush();
			Response.Clear();
			RenderBikesAjax(writer, Request.QueryString["ajaxbikes"]);
			Response.End();
		}
		base.Render(writer);
	}

	public string GenerateErrorHTML()
	{
		if (m_Errors.Count == 0) return "";

		StringBuilder sb = new StringBuilder();
		sb.Append("<ul>\n<li>");
		for (int i = 0; i < m_Errors.Count; i++)
		{
			if (i > 0) sb.Append("</li>\n<li>");
			sb.Append(m_Errors[i]);			
		}
		sb.Append("</li>\n</ul>");

		return string.Format(@"<table border=""0"" cellpadding=""4"" cellspacing=""1"" class=""list"">
<tr class=""category""><td><b style=""color:red"">{0}</b></td></tr>
<tr>
 <td style=""background-color:#FFFFE2"">
{1}
 </td>
</tr>
</table>
<div style=""padding:0px 3px 0px 3px""><img src=""images/bk.shadow.png"" width=""100%"" height=""1"" /></div>
<br />", Language["ERROR_PROCESSING_DATA"], sb.ToString());
	}
}

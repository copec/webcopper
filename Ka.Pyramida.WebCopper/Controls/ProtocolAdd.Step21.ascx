﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Controls_ProtocolAdd_Step21" Codebehind="ProtocolAdd.Step21.ascx.cs" %>
<%@ Register TagPrefix="yuki" Namespace="Ka.Garance.Web.List" Assembly="garance" %>
<%@ Register TagPrefix="yuki" Namespace="Ka.Garance.Web.TabList" Assembly="garance" %>
<%@ Register TagPrefix="yuki" TagName="issue" Src="Issue.Ajax.ascx" %>

<table border="0" cellpadding="4" cellspacing="1" class="list">
<tr class="header">
 <th>
<table border="0" cellpadding="0" cellspacing="0" style="width:100%"> 
<tr>
 <td align="left" style="color:#ffffff">2 / 3 &raquo; <%=Language["PROTO_ADD_ISSUE"]%> &raquo; <%=(Protocol.Zavady.SelectedIndex==-1?Language["PROTO_ADD_ISSNEW"]:Language["PROTO_ADD_ISSEDIT"])%></td>
 <td align="right" style="color:#ffffff"><%=Protocol.NazevProtokolu%></td>
</tr> 
</table> 
 </th>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
<br />

<script language="javascript" type="text/javascript" src="js/ajax.js"></script>
<script language="javascript" type="text/javascript" src="js/protocol.js"></script>
<script language="javascript" type="text/javascript">
//<![CDATA[
  IMG_QTY_TIP = '<%=Language["PROTO_MSG_QTYTIP"]%>.';
  IMG_OBJ_TIP = '<%=Language["PROTO_MSG_ORDTIP"]%>.';
//]]>
</script>

<form action="<%=Request.FilePath%>" method="post" onsubmit="return protocol_ValidateStep21();"><%=GetSubmitErrors()%>
<table border="0" cellpadding="4" cellspacing="1" class="list">
<tr class="category">
 <td colspan="6"><%=Language["PROTO_ISSUE_DETAILS"]%></td>
</tr>
<tr class="even">
 <td width="10%"><label for="MP_ProtoBtn_Code"><%=Language["PROTO_ISSUE_CODE"]%></label></td>
 <td class="odd">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
 <td><input id="MP_ProtoBtn_Text" type="text" readonly="readonly" size="4" value="<%=NovaZavada.Kod%>" class="sw-input" style="color:#808080;background:#F8F8F8" /></td>
 <td>&nbsp;</td>
 <td>
   <button id="MP_ProtoBtn_Code" type="button" class="main" onclick="protocol_SetCode();">
    <img src="images/icon.larr.png" alt="" />
    <%=Language["BUTTON_SET"]%>
   </button>
 </td>
</tr>
</table>
 </td>
 <td width="10%"><label for="c101"><%=Language["PROTO_ISSUE_DZ"]%></label></td>
 <td class="odd">
<select id="c101" name="nz_dz"> 
<%=GenerateDzOptions()%>
</select>
 </td>
 <td width="10%"><label for="c102"><%=Language["PROTO_ISSUE_MANUFACTURER"]%></label></td>
 <td class="odd"><input id="c102" type="text" readonly="readonly" size="4" value="Y00" class="sw-input" style="color:#808080;background:#F8F8F8" /></td> 
</tr>
<tr class="even">
 <td width="10%"><label for="txt_21_nazev_t"><%=Language["PROTO_ISSUE_TITLE"]%></label></td>
 <td colspan="5" class="odd"><input id="txt_21_nazev_t" type="text" name="nz_nazev" value="<%=Server.HtmlEncode(NovaZavada.Nazev)%>" class="sw-input" style="width:98.5%" onkeyup="protocol_ValidateStep21();" /></td>
</tr>
<tr class="even">
 <td width="10%"><label for="txt_21_popis_t"><%=Language["PROTO_ISSUE_TEXT"]%></label></td>
 <td colspan="5" class="odd"><input id="txt_21_popis_t" type="text" name="nz_popis" value="<%=Server.HtmlEncode(NovaZavada.Popis)%>" class="sw-input" style="width:98.5%" onkeyup="protocol_ValidateStep21();" /></td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
<br />

<div id="MP_ProtoDiv_Issue">

<yuki:issue ID="Issue1" runat="server" />

</div>

</form>
﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Ka.Garance.Data;
using Ka.Garance.Web;
using Ka.Garance.Protocol.Yuki;

public partial class Controls_ClaimAdd_Step2 : Ka.Garance.Web.GaranceControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }


	private bool m_OneSelected = false;
	public bool OneSelected
	{
		get { return m_OneSelected; }
	}

	protected string GetSeznamPolozek()
	{
		StringBuilder sb = new StringBuilder();

		MySQLDataReader dr = Database.ExecuteReader(string.Format(@"
			SELECT B.cislo AS protokol,C.typmoto AS model,A.iddil AS sklcislo,C.nazevdilu{0} AS nazev,IFNULL(A.mnozstvi,0) AS pocet,A.id AS id
			FROM tzdil A
				INNER JOIN tprotocol B on B.cislo=A.cisloprotokol
				LEFT JOIN tcskodnicisla C on C.sklcislo=A.iddil
			WHERE B.iddealer={1} AND B.stav=4 AND IFNULL(A.mnozstvi,0) > 0
			ORDER BY B.cislo DESC, A.id DESC", (Language.UseEnglishNames ? "_en" : ""), (Page as GarancePage).User.Id));
		while (dr.Read())
		{
			bool isSelected = (Request.Form[string.Format("pclaim[{0}]", dr.Index)] == Convert.ToString(dr.GetValue("id")));
			if (!isSelected) continue;
			sb.AppendFormat(@"
<tr class=""{0}"">
 <td align=""left"" nowrap=""nowrap"">{1}</td>
 <td align=""left"" nowrap=""nowrap"">{2}</td>
 <td align=""left"" nowrap=""nowrap"">{3}</td>
 <td align=""left"" nowrap=""nowrap"">{4}</td>
 <td align=""center"">{5}</td>
 <td align=""left""><input type=""hidden"" name=""pclaim[{7}]"" value=""{6}"" /><input type=""hidden"" name=""ptxt[{7}]"" value=""{8}"" />{8}</td>
</tr>", (dr.Index % 2 == 0 ? "even" : "odd")
	  , dr.GetValue("protokol")
	  , dr.GetValue("model")
	  , dr.GetValue("sklcislo")
	  , dr.GetValue("nazev")
	  , dr.GetValue("pocet")
	  , dr.GetValue("id")
	  , dr.Index
	  , Server.HtmlEncode(Request.Form[string.Format("ptxt[{0}]", dr.Index)]));
			if (isSelected) m_OneSelected = true;
		}

		return sb.ToString();
	}

}

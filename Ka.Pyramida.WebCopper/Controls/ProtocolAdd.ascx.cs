﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Ka.Garance.Protocol.Yuki;

public partial class Controls_ProtocolAdd : Ka.Garance.Web.GaranceControl
{
	public YukiProtocol Protocol
	{
		get
		{
			return (Page as Ka.Garance.Web.GarancePage).User.CachedProtocol;
		}
	}

	public string ProtoInfoText
	{
		get
		{
			if (Protocol.Stav == 0)
				return Language["PROTO_ADD_INFOB"];
			return Language["PROTO_ADD_INFOA"];
		}
	}

	public string StepTitle
	{
		get
		{
			switch (Protocol.ActualStep)
			{
				case 1: return Language["PROTO_ADD_BASICS"];
				case 2: return Language["PROTO_ADD_ISSUE"];
				case 3: return Language["PROTO_ADD_REVIEW"];
				case 4: return Language["PROTO_ADD_REVIEW"];
				case 21: return Language["PROTO_ADD_ISSNEW"];
				case 22: return Language["PROTO_ADD_ISSEDIT"];
			}
			return "";
		}
	}

	protected override void Render(HtmlTextWriter writer)
	{
		switch (Protocol.ActualStep)
		{
			case 1:
				LoadControl("ProtocolAdd.Step1.ascx").RenderControl(writer);
				break;
			case 2:
				LoadControl("ProtocolAdd.Step2.ascx").RenderControl(writer);
				break;
			case 21:
			case 22:
				LoadControl("ProtocolAdd.Step21.ascx").RenderControl(writer);
				break;
			case 3:
				LoadControl("ProtocolAdd.Step3.ascx").RenderControl(writer);
				break;
			case 4:

				Ka.Garance.User.GaranceUser user = new Ka.Garance.User.GaranceUser(this.Database, this.Context, this.Language);
				user.ResetActiveProtocol();

				base.Render(writer);
				break;
		}
	}

}

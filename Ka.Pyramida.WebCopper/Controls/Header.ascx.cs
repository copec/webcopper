﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Controls_Header : Ka.Garance.Web.GaranceControl
{
	private string m_LocationID = null;
	private string m_Location = "";

	protected string UserName
	{
		get
		{
			return (Page as Ka.Garance.Web.GarancePage).User.UserName;
		}
	}

	public string Location
	{
		get
		{
			if (m_LocationID != null && m_LocationID != "")
				return Language[m_LocationID];
			return m_Location;
		}
		set { m_Location = value; }
	}

	public string LocationID
	{
		get { return m_LocationID; }
		set { m_LocationID = value; }
	}

	public string UserTitle
	{
		get	{ return (Page as Ka.Garance.Web.GarancePage).User.Title; }
	}

	protected void Page_Load(object sender, EventArgs e)
	{
        lblVersion.Text = System.Reflection.Assembly.GetExecutingAssembly().ImageRuntimeVersion;
	}
}

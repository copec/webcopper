using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Controls_StatsVIN : Ka.Garance.Web.GaranceControl
{
	protected class Filter
	{
		public string vin = null;
	}
	protected Filter filter = null;

	protected override void OnPreRender(EventArgs e)
	{
		base.OnPreRender(e);

		if (this.Visible)
			DisplayStats();
	}

	private void DisplayStats()
	{
		filter = (Session["stats.filter1"] as Filter);
		if (filter == null)
		{
			filter = new Filter();
			Session["stats.filter1"] = filter;
		}

		if (Request.RequestType == "POST")
		{
			filter.vin = Request.Form[tbVin.UniqueID];
		}
		else if (Request.QueryString["filter"] != null)
			filter.vin = null;

		tbVin.Value = filter.vin;

		MCStats1.Visible = true;

		string SQLfilter = (filter.vin != null && filter.vin.Trim() != "" ? string.Format("a.ciskar LIKE '%{0}%' AND", Ka.Garance.Data.MySQLDatabase.Escape(filter.vin.TrimEnd(null), true)) : "");
		SQLfilter = string.Format("WHERE {0} a.idzeme=1 ", SQLfilter);

		MCStats1.SQL = string.Format(@"
SELECT
	a.id_auta,
	z.id_zakaznik,
	a.ciskar,
	a.m_modrada,
	t.operace,
	t.doklad,
	t.datum_vy,
	z.firma_1,
	z.ico
FROM `tx_autaar_d` a
	INNER JOIN tx_trans__d t on t.id_auta = a.id_auta
	INNER JOIN tx_tblzakaz z on z.id_zakaznik = t.id_zakaznik
{0}
		", SQLfilter);
		MCStats1.SQLCount = string.Format(@"
SELECT COUNT(*) FROM `tx_autaar_d` a
	INNER JOIN tx_trans__d t on t.id_auta = a.id_auta
	INNER JOIN tx_tblzakaz z on z.id_zakaznik = t.id_zakaznik
{0}
		", SQLfilter);

		ListItem7.Format = "{0:d'.'M'.'yyyy}";
	}
}

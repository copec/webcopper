﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Ka.Garance.Data;
using Ka.Garance.Protocol.Yuki;

public partial class Controls_ProtocolAdd_Step2 : Ka.Garance.Web.GaranceControl
{
	public YukiProtocol Protocol
	{
		get
		{
			return (Page as Ka.Garance.Web.GarancePage).User.CachedProtocol;
		}
	}

	protected string JsEncodedMessage
	{
		get { return Ka.Garance.Web.GarancePage.JsEncode(Protocol.Zprava); }
	}

    private void Initialize()
    {
		if (Request.HttpMethod == "POST" && Request.Form["_step"] == "a634b76dgfhreJKA_121.1")
			CollectPostedData();

		switch (Request.QueryString["step"])
		{
			case "1":
				Protocol.ActualStep = 1;
				Response.Redirect(Request.FilePath);
				break;
			case "2.1":
				Protocol.ActualStep = 21;
				Protocol.Zavady.SelectedIndex = -1;
				Protocol.SaveToCache();
				NovaZavada.Create(Protocol).Clear();
				Response.Redirect(Request.FilePath);
				break;
		}

		if (Request.HttpMethod == "POST" && Request.Form["next_121"] != null)
		{
			ProcessCurrentStep();
		}
		else
			m_Errors.Clear();

		if (Request.HttpMethod == "POST" && Request.QueryString["deli"] != null)
		{
			int deli = -1;
			try
			{
				deli = Convert.ToInt32(Request.QueryString["deli"]);
			}
			catch { }
			if (deli > -1)
			{
				Protocol.Zavady.Delete(deli);
				Protocol.SaveToCache();
				Response.Redirect(Request.FilePath);
			}
		}

		if (Request.HttpMethod == "POST" && Request.QueryString["editi"] != null)
		{
			int editi = -1;
			try
			{
				editi = Convert.ToInt32(Request.QueryString["editi"]);
			}
			catch { }
			if (editi > -1)
			{
				Protocol.Zavady.SelectedIndex = editi;
				NovaZavada.Create(Protocol, Protocol.Zavady.SelectedIndex);
				Protocol.ActualStep = 22;
				Protocol.SaveToCache();
				Response.Redirect(Request.FilePath);
			}
		}
    }

	protected string GetICOString()
	{
		if (Protocol.ICO == null || Protocol.ICO.Trim() == "")
			return "";
		return string.Format("<br /><br />{0}: {1}", Language["INVOICE_ICO"], Protocol.ICO);
	}

	protected override void Render(HtmlTextWriter writer)
	{
		Initialize();
		base.Render(writer);
	}

	private NameValueCollection m_Errors = new NameValueCollection();
	private void CollectPostedData()
	{
		Protocol.KO = Request.Form["nz_ko"];
		Protocol.DZ = Request.Form["nz_dz"];
		Protocol.Resitel = Request.Form["nz_resitel"];
		Protocol.ResitelFax = Request.Form["nz_resitel_fax"];
		Protocol.ResitelTel = Request.Form["nz_resitel_tel"];
		Protocol.Zprava = Request.Form["nz_zprava"];
		Protocol.Zavady.SelectedIndex = -1;
		Protocol.SaveToCache();

		if (Protocol.KO == null || Protocol.KO.Trim() == "")
			m_Errors.Add("ko", Language["PROTO_ERR_KO"]);
		if (Protocol.DZ == null || Protocol.DZ.Trim() == "")
			m_Errors.Add("dz", Language["PROTO_ERR_DZ"]);
		if (Protocol.Zavady.Count == 0)
			m_Errors.Add("zavady", Language["PROTO_ERR_NOISSUE"]);
	}

	private void ProcessCurrentStep()
	{
		if (m_Errors.Count == 0)
		{
			Protocol.ActualStep = 3;
			Response.Redirect(Request.FilePath);
		}
	}

	public string GenerateErrorHTML()
	{
		if (m_Errors.Count == 0) return "";

		StringBuilder sb = new StringBuilder();
		sb.Append("<ul>\n<li>");
		for (int i = 0; i < m_Errors.Count; i++)
		{
			if (i > 0) sb.Append("</li>\n<li>");
			sb.Append(m_Errors[i]);
		}
		sb.Append("</li>\n</ul>");

		return string.Format(@"<table border=""0"" cellpadding=""4"" cellspacing=""1"" class=""list"">
<tr class=""category""><td><b style=""color:red"">{0}</b></td></tr>
<tr>
 <td style=""background-color:#FFFFE2"">
{1}
 </td>
</tr>
</table>
<br />", Language["ERROR_PROCESSING_DATA"], sb.ToString());
	}

	public string GenerateDZOptions()
	{
		StringBuilder sb = new StringBuilder();
		MySQLDataReader dr = Database.ExecuteReader(string.Format("SELECT id,nazev{0} FROM tcdz ORDER BY nazev{0}", (Language.UseEnglishNames ? "_en" : "")));
		while (dr.Read())
		{
			string id = Convert.ToString(dr.GetValue("id"));
			if (sb.Length > 0) sb.Append("\n");
			sb.AppendFormat("<option value=\"{0}\"{1}>{2}</option>", id, (id==Protocol.DZ?" selected=\"selected\"":""), dr.GetValue(1));
		}
		return sb.ToString();
	}

	public string GenerateKOOptions()
	{
		StringBuilder sb = new StringBuilder();
		MySQLDataReader dr = Database.ExecuteReader(string.Format("SELECT id,nazev{0} FROM tcko ORDER BY nazev{0}", (Language.UseEnglishNames ? "_en" : "")));
		while (dr.Read())
		{
			string id = Convert.ToString(dr.GetValue("id"));
			if (sb.Length > 0) sb.Append("\n");
			sb.AppendFormat("<option value=\"{0}\"{1}>{2}</option>", id, (id == Protocol.KO ? " selected=\"selected\"" : ""), dr.GetValue(1));
		}

		return sb.ToString();
	}

    public string GenerateDateRepairHTML()
    {
        if (Protocol.DateRepairOverflow)
        {
            return string.Format(@"<span style=""color:red"">{0} ({1})</span>", Protocol.DateRepair.ToString("d'. 'M'. 'yyyy"), Language["REPAIR_DATE_LIMIT"]);
        }
        else
        {
            return string.Format(@"{0}", Protocol.DateRepair.ToString("d'. 'M'. 'yyyy"));
        }
    }

	public string GetSeznamZavad()
	{
		bool isSKDealer = Protocol.User.IdZeme == 2 && Protocol.DateCreated >= YukiProtocol.DATE_SK_EURO;

		if (Protocol.Zavady.Count == 0)
		{
			return string.Format(@"<table border=""0"" cellpadding=""4"" cellspacing=""1"" class=""list"">
<tr class=""category"">
 <td>{0}</td>
</tr>
<tr>
 <td align=""center"" style=""background-color:#F3F3F3"">{1}</td>
</tr>
<tr class=""footer"">
 <td align=""left"">
  <button type=""button"" class=""main"" onclick=""this.form.action='{3}?step=2.1';this.form.submit();"">
   <img src=""images/add.png"" alt="""" />
   {2}
  </button>
 </td>
</tr>
</table>
<div style=""padding:0px 3px 0px 3px""><img src=""images/bk.shadow.png"" width=""100%"" height=""1"" /></div>
<br />", Language["PROTO_GROUP_ISSUES"], Language["PROTO_ISSUES_NODATA"], Language["BUTTON_ADD"], Request.FilePath);
		}

		StringBuilder sb = new StringBuilder();
		sb.Append("<table id=\"tbl_s_zavady_p\" border=\"0\" cellpadding=\"4\" cellspacing=\"1\" class=\"list\">\n");
		sb.AppendFormat("<tr class=\"category\"><td colspan=\"9\">{0}</td></tr>\n", Language["PROTO_GROUP_ISSUES"]);
		sb.AppendFormat("<tr class=\"header\">\n");
		sb.Append(" <th style=\"width:1px\">&nbsp;</th>\n");
		sb.Append(" <th style=\"width:1%\">&nbsp;</th>\n");
		sb.AppendFormat(" <th align=\"left\" style=\"width:1%\">{0}</th>\n", Language["PROTO_ISSUE_CODE"]);
		sb.AppendFormat(" <th align=\"left\" style=\"width:1%\">{0}</th>\n", Language["PROTO_ISSUE_DZ"]);
		sb.AppendFormat(" <th align=\"left\" style=\"width:1%\">{0}</th>\n", Language["PROTO_ISSUE_MANUFACTURER"]);
		sb.AppendFormat(" <th align=\"left\">{0}</th>\n", Language["PROTO_ISSUE_TITLE"]);
		sb.AppendFormat(" <th align=\"left\">{0}</th>\n", Language["PROTO_ISSUE_TEXT"]);
		sb.AppendFormat(" <th align=\"right\" style=\"width:1%\">{0}</th>\n", Language["PROTO_ISSUE_WORK"]);
		sb.AppendFormat(" <th align=\"right\" style=\"width:1%\">{0}</th>\n", Language["PROTO_ISSUE_MAT"]);
		sb.Append("</tr>\n");

		for (int i = 0; i < Protocol.Zavady.Count; i++)
		{
			sb.AppendFormat("<tr class=\"{0}\" style=\"cursor:pointer\" onmouseover=\"if(!this.h)this.h=this.className;this.className=this.h+'-active';\" onmouseout=\"this.className=this.h;\" onclick=\"protocol_SetZavadaForDeletion(this,{1});\">\n", (i % 2 == 0 ? "even" : "odd"), i);
			sb.Append(" <td class=\"left\" style=\"width:1px\">&nbsp;</td>\n");
			sb.AppendFormat(" <td align=\"right\" style=\"width:1%\">{0}.</td>\n", (i+1));
			sb.AppendFormat(" <td align=\"left\" style=\"width:1%\">{0}</td>\n", Protocol.Zavady[i].Kod);
			sb.AppendFormat(" <td align=\"left\" style=\"width:1%\">{0}</td>\n", Protocol.Zavady[i].DZ);
			sb.AppendFormat(" <td align=\"left\" style=\"width:1%\">{0}</td>\n", Protocol.Zavady[i].Vyrobce);
			sb.AppendFormat(" <td>{0}</td>\n", Protocol.Zavady[i].Nazev);
			sb.AppendFormat(" <td>{0}</td>\n", Protocol.Zavady[i].Popis);
			sb.AppendFormat(" <td align=\"right\" style=\"width:1%\">{0:F2}</td>\n", Math.Round(Protocol.Zavady[i].CelkemCjCenaDPH, isSKDealer ? 2 : 1));
			sb.AppendFormat(" <td align=\"right\" style=\"width:1%\">{0:F2}</td>\n", Math.Round(Protocol.Zavady[i].CelkemCenaDPH, isSKDealer ? 2 : 1));
			sb.Append("</tr>");
		}

		// buttons
		sb.AppendFormat(@"
<tr class=""footer"">
 <td colspan=""9"" align=""left"" style=""padding-top:2px;padding-bottom:2px"">
  <button type=""button"" class=""main"" onclick=""this.form.action='{0}?step=2.1';this.form.submit();"">
   <img src=""images/add.png"" alt="""" />
   {1}
  </button>
  <button id=""tdb_2_button_e"" type=""button"" class=""main"" style=""visibility:hidden"" onclick=""protocol_EditZavada(this);"">
   <img src=""images/op.edit.png"" alt="""" />
   {2}
  </button>
  <button id=""tdb_2_button_d"" type=""button"" class=""main"" style=""visibility:hidden"" onclick=""protocol_DeleteZavada(this);"">
   <img src=""images/delete.png"" alt="""" />
   {3}
  </button>
 </td>
</tr>", Request.FilePath, Language["BUTTON_ADD"], Language["BUTTON_EDIT"], Language["BUTTON_DELETE"]);

		sb.Append("\n</table>\n<div style=\"padding:0px 3px 0px 3px\"><img src=\"images/bk.shadow.png\" width=\"100%\" height=\"1\" /></div>\n<br />\n");

		return sb.ToString();
	}
}

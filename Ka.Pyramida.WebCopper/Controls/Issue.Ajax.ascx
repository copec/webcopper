﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Controls_Issue_Ajax" Codebehind="Issue.Ajax.ascx.cs" %>

<table border="0" cellpadding="2" cellspacing="1" class="list">
<tr class="footer">
 <td align="right">
  <button id="btn_submit_zavada_p" disabled="disabled" name="save" type="submit" class="main" style="font-weight:bold">
   <img src="images/ok.png" />
   <%=Language["PROTO_BUTTON_SAVE_ISSUE"]%>
  </button>
  <button type="button" class="main" onclick="location='<%=Request.FilePath%>?step=2';">
   <img src="images/cancel.png" />
   <%=Language["BUTTON_CANCEL"]%>
  </button>  
 </td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>

<script language="javascript" type="text/javascript">
//<![CDATA[
protocol_ValidateStep21(true);
//]]>
</script>
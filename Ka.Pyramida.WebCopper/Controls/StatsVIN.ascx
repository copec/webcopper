<%@ Control Language="C#" AutoEventWireup="true" Inherits="Controls_StatsVIN" Codebehind="StatsVIN.ascx.cs" %>
<%@ Register TagPrefix="yuki" Namespace="Ka.Garance.Web.List" Assembly="garance" %>
<%@ Register TagPrefix="yuki" Namespace="Ka.Garance.Web.TabList" Assembly="garance" %>

<form action="stats.aspx" method="post">
<table border="0" cellpadding="4" cellspacing="1" class="list">
<tr class="category">
 <td colspan="2"><%=Language["PARTS_FILTER"]%></td>
</tr>
<tr class="even">
 <td width="15%"><%=Language["STATS_VIN"]%></td>
 <td class="odd"><input id="tbVin" type="text" runat="server" size="120" /></td>
</tr>
<tr class="footer">
 <td colspan="2">
  <input name="filter" type="submit" value="<%=Language["BUTTON_FILTER"]%>" style="font-weight:bold" />
  <input type="button" value="<%=Language["BUTTON_FILTER_RESET"]%>" onclick="location='stats.aspx?filter=';" />
 </td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
</form>

<br />

<yuki:List ID="MCStats1" Name="Stats1List" runat="server">
 <yuki:ListItem ID="ListItem1" LangID="STATS_IDCAR" ColumnName="id_auta" width="1%" td_nowrap="nowrap" Default="true" DefaultState="Asc" runat="server" />
 <yuki:ListItem ID="ListItem2" LangID="STATS_IDCUSTOMER" ColumnName="id_zakaznik" width="1%" runat="server" />
 <yuki:ListItem ID="ListItem3" LangID="STATS_VIN" ColumnName="ciskar" runat="server" />
 <yuki:ListItem ID="ListItem4" LangID="STATS_MODRADA" ColumnName="m_modrada" runat="server" />
 <yuki:ListItem ID="ListItem5" LangID="STATS_OP" ColumnName="operace" runat="server" />
 <yuki:ListItem ID="ListItem6" LangID="STATS_ID" ColumnName="doklad" runat="server" />
 <yuki:ListItem ID="ListItem7" LangID="STATS_CREATED" ColumnName="datum_vy" runat="server" />
 <yuki:ListItem ID="ListItem8" LangID="STATS_COMPANY" ColumnName="firma_1" runat="server" />
 <yuki:ListItem ID="ListItem9" LangID="INVOICE_ICO" ColumnName="ico" runat="server" />
</yuki:List>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Controls_Pozice_Ajax" Codebehind="Pozice.Ajax.ascx.cs" %>

<table border="0" cellpadding="2" cellspacing="1" class="list">
<tr class="footer">
 <td align="right">
  <button id="t_proto_posf_btnok" type="button" disabled="disabled" class="main" style="font-weight:bold" onclick="protocol_SubmitPos();">
   <img src="images/ok.png" />
   <%=Language["BUTTON_CONFIRM"]%>
  </button>
  <button type="button" class="main" onclick="protocol_CancelPos();">
   <img src="images/cancel.png" />
   <%=Language["BUTTON_CANCEL"]%>
  </button>
 </td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>

﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Ka.Garance.Data;
using Ka.Garance.Protocol.Yuki;

public partial class Controls_ProtocolAdd_Step21 : Ka.Garance.Web.GaranceControl
{

	public YukiProtocol Protocol
	{
		get
		{
			return (Page as Ka.Garance.Web.GarancePage).User.CachedProtocol;
		}
	}

	public Ka.Garance.Protocol.Yuki.NovaZavada NovaZavada
	{
		get
		{
			return Ka.Garance.Protocol.Yuki.NovaZavada.Create((Page as Ka.Garance.Web.GarancePage).User.CachedProtocol);
		}
	}

	private string[] m_Errors = null;
	public string GetSubmitErrors()
	{
		if (m_Errors == null || m_Errors.Length == 0) return "";

		return string.Format(@"<table border=""0"" cellpadding=""4"" cellspacing=""1"" class=""list"">
<tr class=""category""><td><b style=""color:red"">{0}</b></td></tr>
<tr>
 <td style=""background-color:#FFFFE2"">
  <ul>
   <li>{1}</li>
  </ul>
 </td>
</tr>
</table>
<br />
", Language["ERROR_PROCESSING_DATA"], string.Join("</li>\n   <li>", m_Errors));
	}

	public string GenerateDzOptions()
	{
		StringBuilder sb = new StringBuilder();
		MySQLDataReader dr = Database.ExecuteReader("SELECT id,jmeno FROM tcdruhzavady ORDER BY id,jmeno");
		while (dr.Read())
		{
			if (sb.Length > 0) sb.Append("\n");
			string id = Convert.ToString(dr.GetValue("id"));
			sb.AppendFormat(" <option value=\"{0}\"{1}>{0} - {2}</option>", id, (NovaZavada.DZ == id?" selected=\"selected\"":""), dr.GetValue("jmeno"));
		}
		return sb.ToString();
	}

	protected override void Render(HtmlTextWriter writer)
	{
		Initialize();
		base.Render(writer);
	}

	private void Initialize()
    {
		if (Request.QueryString["step"] == "2")		
		{
			(Page as Ka.Garance.Web.GarancePage).User.CachedProtocol.ActualStep = 2;
			Response.Redirect(Request.FilePath);
		}

		// postback
		if (Request.HttpMethod == "POST")
		{
			// zisti, ci je dobre zadana nova zavada a pridaj do zoznamu
			NovaZavada.DZ = Request.Form["nz_dz"];
			NovaZavada.Nazev = Request.Form["nz_nazev"];
			NovaZavada.Popis = Request.Form["nz_popis"];
			m_Errors = NovaZavada.GetSubmitErrors(Language);
			if (m_Errors.Length == 0)
			{
				NovaZavada.AddToProtocol((Page as Ka.Garance.Web.GarancePage).User.CachedProtocol);
				(Page as Ka.Garance.Web.GarancePage).User.CachedProtocol.ActualStep = 2;
				Response.Redirect(Request.FilePath);				
			}
		}
    }
}

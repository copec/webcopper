﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Controls_ClaimAdd_Step2" Codebehind="ClaimAdd.Step2.ascx.cs" %>

<table border="0" cellpadding="4" cellspacing="1" class="list">
<tr>
 <th class="header" align="left">2 / 2 - <%=Language["PROTO_ADD_REVIEW"]%></th>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
<br />

<form action="claim.add.aspx" method="post">
<table border="0" cellpadding="4" cellspacing="1" class="list">
<tr class="category">
 <td><%=Language["CLAIM_TITLE"]%></td>
</tr>
<tr class="odd">
 <td align="left">
  <input type="hidden" name="ptitle" value="<%=Server.HtmlEncode(Request.Form["ptitle"])%>" />
  <%=Server.HtmlEncode(Request.Form["ptitle"])%>
 </td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
<br />

<table border="0" cellpadding="4" cellspacing="1" class="list">
<tr class="category">
 <td colspan="6"><%=Language["CLAIM_MSG_SELECTB"]%></td>
</tr>
<tr class="header">
 <th align="left" width="1%" nowrap="nowrap"><%=Language["HEADER_PROTOCOL"]%></th>
 <th align="left" width="1%" nowrap="nowrap"><%=Language["BIKES_LIST_MODEL"]%></th>
 <th align="left" width="1%" nowrap="nowrap"><%=Language["CLAIM_LIST_STOCK"]%></th>
 <th align="left" width="1%" nowrap="nowrap"><%=Language["PARTS_NAME"]%></th>
 <th align="left" width="1%" nowrap="nowrap"><%=Language["CLAIM_LIST_COUNT"]%></th>
 <th align="left" width="94%" nowrap="nowrap"><%=Language["CLAIM_LIST_NOTE"]%></th>
</tr>
<%=GetSeznamPolozek()%>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
<br />

<table border="0" cellpadding="2" cellspacing="1" class="list">
<tr class="footer">
 <td align="right"> 
<input type="hidden" name="pstep" value="2" />
<input type="submit" name="pback_step" value="&laquo; <%=Language["BUTTON_BACK"]%>" class="main" />
<input type="submit" name="next"<%=(OneSelected?"":" disabled=\"disabled\"")%> value="<%=Language["CLAIM_BUTTON_SAVE"]%> &raquo;" style="font-weight:bold" class="main" />
 </td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
</form>
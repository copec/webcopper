﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Controls_ClaimAdd_Step1" Codebehind="ClaimAdd.Step1.ascx.cs" %>

<table border="0" cellpadding="4" cellspacing="1" class="list">
<tr>
 <th class="header" align="left">1 / 2 - <%=Language["PROTO_ADD_BASICS"]%></th>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
<br />

<% if (!PartsAvailable) { %>
<table border="0" cellpadding="4" cellspacing="1" class="list">
<tr class="odd">
 <td align="center"><div style="padding:24px"><%=Language["CLAIM_NO_ITEMS"]%>.</div></td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
<br />

<table border="0" cellpadding="2" cellspacing="1" class="list">
<tr class="footer">
 <td align="right">
<button type="button" class="main" onclick="location='index.aspx';">
 <img src="images/cancel.png" alt="" />
 <%=Language["BUTTON_CLOSE"]%>
</button>
 </td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
<% } else { %>

<form action="claim.add.aspx" method="post">
<table border="0" cellpadding="4" cellspacing="1" class="list">
<tr class="category">
 <td><label for="c100s"><%=Language["CLAIM_TITLE"]%></label></td>
</tr>
<tr class="odd">
 <td align="left">
  <div><input id="c100s" type="text" name="ptitle" value="<%=Server.HtmlEncode(Request.Form["ptitle"])%>" maxlength="128" class="sw-input" style="width:99.5%" /></div>
  <div style="font-size:7pt;font-style:italic"><%=Language["CLAIM_TITLE_INFO"]%></div>
 </td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
<br />

<table border="0" cellpadding="4" cellspacing="1" class="list">
<tr class="category">
 <td colspan="7"><%=Language["CLAIM_MSG_SELECTB"]%></td>
</tr>
<tr class="header">
 <th align="left" width="1%" nowrap="nowrap"><%=Language["HEADER_PROTOCOL"]%></th>
 <th align="left" width="1%" nowrap="nowrap"><%=Language["BIKES_LIST_MODEL"]%></th>
 <th align="left" width="1%" nowrap="nowrap"><%=Language["CLAIM_LIST_STOCK"]%></th>
 <th align="left" width="1%" nowrap="nowrap"><%=Language["PARTS_NAME"]%></th>
 <th align="left" width="1%" nowrap="nowrap"><%=Language["CLAIM_LIST_COUNT"]%></th>
 <th align="left" width="94%" nowrap="nowrap"><%=Language["CLAIM_LIST_NOTE"]%></th>
 <th width="1%"><input type="checkbox" style="cursor:pointer;margin:0px 2px 0px 2px;padding:0px;height:14px" onclick="claim_SelectAll(this);" /></th>
</tr>
<%=GetSeznamPolozek()%>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
<br />

<table border="0" cellpadding="2" cellspacing="1" class="list">
<tr class="footer">
 <td align="right">
<input type="hidden" name="pstep" value="1" />
<input id="btn_claim_1" type="submit" name="next"<%=(OneSelected?"":" disabled=\"disabled\"")%> value="<%=Language["BUTTON_NEXT"]%> &raquo;" class="main" style="font-weight:bold" />
 </td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
</form>

<% } %>
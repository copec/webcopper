using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Controls_StatsBikes : Ka.Garance.Web.GaranceControl
{
	protected class Filter
	{
		public string name = null;
		public int countryID = 1;
	}

	protected Filter filter = null;

	protected override void OnPreRender(EventArgs e)
	{
		base.OnPreRender(e);

		if (this.Visible)
			DisplayStats();
	}

	private void DisplayStats()
	{
		filter = (Session["stats.filter"] as Filter);
		if (filter == null)
		{
			filter = new Filter();
			Session["stats.filter"] = filter;
		}

		if (Request.RequestType == "POST")
		{
			filter.name = Request.Form[tbName.UniqueID];
		}
		else if (Request.QueryString["filter"] != null)
			filter.name = null;


		if (Request.QueryString["lang"] == "cz")
			filter.countryID = 1;
		else if (Request.QueryString["lang"] == "sk")
			filter.countryID = 2;

		if (filter.countryID == 2)
			tabSK.Select();
		else
			tabCZ.Select();

		tbName.Value = filter.name;

		MCStats0.Visible = true;

		string SQLfilter = string.Format("WHERE B.idzeme={0}", filter.countryID);

		SQLfilter += (filter.name != null && filter.name.Trim() != "" ? string.Format(" AND B.nazev LIKE '%{0}%'", Ka.Garance.Data.MySQLDatabase.Escape(filter.name.TrimEnd(null), true)) : "");

		MCStats0.SQL = string.Format(@"
SELECT
  A.iddealer AS id,
  B.nazev AS name,
  COUNT(IF(IFNULL(A.stav,0)=2,2,NULL)) AS stav2,
  COUNT(IF(IFNULL(A.stav,0)=0,0,NULL)) AS stav0
FROM tmotocykl A
INNER JOIN tdealer B ON B.id=A.iddealer
{0}
GROUP BY A.iddealer
		", SQLfilter);
		MCStats0.SQLCount = string.Format("SELECT COUNT(DISTINCT A.iddealer) FROM tmotocykl A INNER JOIN tdealer B ON B.id=A.iddealer {0}", SQLfilter);
	}
}

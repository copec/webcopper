﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.3603
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------



public partial class _Settings {
    
    /// <summary>
    /// MCHeader control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Controls_Header MCHeader;
    
    /// <summary>
    /// MCSettingsTabs control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Ka.Garance.Web.TabList.TabList MCSettingsTabs;
    
    /// <summary>
    /// TabItem1 control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Ka.Garance.Web.TabList.TabItem TabItem1;
    
    /// <summary>
    /// TabItem2 control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Ka.Garance.Web.TabList.TabItem TabItem2;
    
    /// <summary>
    /// TabItem3 control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Ka.Garance.Web.TabList.TabItem TabItem3;
    
    /// <summary>
    /// TabItem4 control.
    /// </summary>
    /// <remarks>
    /// Auto-generated field.
    /// To modify move field declaration from designer file to code-behind file.
    /// </remarks>
    protected global::Ka.Garance.Web.TabList.TabItem TabItem4;
}

﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class _Protocol_Edit : Ka.Garance.Web.GarancePage
{
	public Ka.Garance.Protocol.Yuki.YukiProtocol Protocol
	{
		get
		{
			return (Page as Ka.Garance.Web.GarancePage).User.CachedProtocol;
		}
	}

    protected void Page_Load(object sender, EventArgs e)
    {
		if (Protocol.Cislo == null || Protocol.Cislo.Trim() == "")
			Response.Redirect("index.aspx");
		string s = "";
		switch (Protocol.ActualStep)
		{
			case 1:
				s = Language["PROTO_ADD_BASICS"];
				break;
			case 2:
				s = Language["PROTO_ADD_ISSUE"];
				break;
			case 3:
				s = Language["PROTO_ADD_REVIEW"];
				break;
			case 4:
				s = Language["PROTO_ADD_REVIEW"];
				break;
			case 21:
				s = Language["PROTO_ADD_ISSNEW"];
				break;
			case 22:
				s = Language["PROTO_ADD_ISSEDIT"];
				break;
		}
		MCHeader.Location = string.Format("{0} - {1} &gt; {2}", Language["LOCATION_PROTOCOL_EDIT"], Protocol.Cislo, s);
    }

	protected override void Render(HtmlTextWriter writer)
	{
		if (Request.QueryString["ajax"] != null)
			ExecuteAjaxRequest(writer);
		else
			base.Render(writer);
	}

	private void ExecuteAjaxRequest(HtmlTextWriter writer)
	{
		switch (Request.QueryString["ajax"])
		{
			case "code":
			case "nd":
				LoadControl("Controls/Code.Ajax.ascx").RenderControl(writer);
				break;
			case "issue":
				LoadControl("Controls/Issue.Ajax.ascx").RenderControl(writer);
				break;
			case "pos":
				LoadControl("Controls/Pozice.Ajax.ascx").RenderControl(writer);
				break;
			case "parts":
				LoadControl("Controls/Parts.Ajax.ascx").RenderControl(writer);
				break;
		}
	}

}

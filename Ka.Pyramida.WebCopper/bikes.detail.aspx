﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="_BikesDetail" Codebehind="bikes.detail.aspx.cs" %>
<%@ Register TagPrefix="yuki" Namespace="Ka.Garance.Web.TabList" Assembly="garance" %>
<%@ Register TagPrefix="yuki" TagName="header" Src="Controls/Header.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Language.Code%>" dir="ltr" lang="<%=Language.Code%>">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><%=Language["APP_TITLE"]%></title>
<link rel="stylesheet" type="text/css" href="css/yuki.css" />
<link rel="stylesheet" type="text/css" href="css/combo.css" />
<link rel="stylesheet" type="text/css" href="css/calendar.css" />
</head>

<body>

<yuki:header Id="MCHeader" LocationID="LOCATION_BIKE_DETAIL" runat="server" />

<div align="center" style="padding:0px 16px 16px 16px"> 
<table border="0" cellpadding="0" cellspacing="0" align="center" style="width:1%">
<tr>
 <td align="left">
 
<div style="width:600px">

<img src="images/mod.moto.png" alt="" style="vertical-align:text-bottom" />
<span class="mod"><%=Language["HEADER_BIKES_DETAIL"]%></span><br /><br />

<yuki:TabList ID="MCFilterTabs" runat="server">
 <yuki:TabItem ID="TabItem1" LangID="HEADER_BIKES_DETAIL" runat="server" />
 <yuki:TabItem ID="TabItem2" LangID="BIKES_ASSIGN" runat="server" />
</yuki:TabList>

<% if (TabItem1.Selected) { %>
<table border="0" cellpadding="4" cellspacing="1" class="list">
<tr>
 <th colspan="2"><%=Language["HEADER_BIKES_DETAIL"]%></td>
</tr>
<tr class="even">
 <td><b><%=Language["BIKES_LIST_MODEL"]%></b></td>
 <td bgcolor="#F3F3F3"><%=Model%></td> 
</tr>
<tr class="even">
 <td width="25%"><b><%=Language["BIKES_LIST_CHASSIS"]%></b></td>
 <td width="75%" bgcolor="#F3F3F3"><%=ChassisNum%></td>
</tr><% if (!BikeAssigned) { %>
<tr class="even">
 <td><b><%=Language["BIKES_LIST_STATE"]%></b></td>
 <td bgcolor="#F3F3F3"><%=Status%></td> 
</tr><% } %>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>

<% } else if (TabItem2.Selected) { %>

<table border="0" cellpadding="4" cellspacing="1" class="list">
<tr>
 <th colspan="2"><%=Language["BIKES_ASSIGN"]%></td>
</tr>
<tr class="even">
 <td><b><%=Language["BIKES_LIST_MODEL"]%></b></td>
 <td bgcolor="#F3F3F3"><%=Model%></td> 
</tr>
<tr class="even">
 <td width="25%"><b><%=Language["BIKES_LIST_CHASSIS"]%></b></td>
 <td width="75%" bgcolor="#F3F3F3"><%=ChassisNum%></td>
</tr>
<tr class="even">
 <td width="25%"><b><%=Language["STATS_DEALER"]%></b></td>
 <td width="75%" bgcolor="#F3F3F3"><% if (!BikeAssigned) { %>
<form action="" method="post">
<div>
<asp:Literal ID="cbDealers" runat="server" />
<input type="submit" name="ctl1" value="<%=Language["BUTTON_CONFIRM"]%>" onclick="if(!confirm('<%=Language["BIKES_MSG_ASSIGN_CONFIRM"]%>'))return false;this.name='assignDealer';" />
</div>
</form><% } else { %><%=Language["BIKES_MSG_ASSIGNED"].Replace("%s", PrevedenDealer())%><% } %>
 </td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" alt="" /></div>

<% } %>

<% if (TabItem1.Selected) { %>
<div id="divSellBike"<%=(BikeSold||IsProdejSubmitted?"":" style=\"display:none\"")%>>
<br /><asp:literal ID="ASPL_ErrorSell" EnableViewState="false" runat="server" />
<form id="frmSellBike" action="" method="post">
<table border="0" cellpadding="4" cellspacing="1" class="list">
<tr>
 <th colspan="2"><%=Language["BIKE_SELL"]%></td>
</tr>
<tr class="even">
 <td width="25%"><b><%=Language["BIKES_LIST_DATE"]%></b></td>
 <td width="75%" bgcolor="#F3F3F3"><%=GetSoldDate()%></td>
</tr>
<tr class="even">
 <td width="25%"><b><%=Language["PROTO_LIST_NAME"]%></b></td>
 <td width="75%" bgcolor="#F3F3F3">
	<input type="text" id="owner_name" runat="server" class="sw-input" size="32" maxlength="255" />
	<select id="owner_type" class="sw-input" runat="server">
		<option value="0">Fyzicka_osoba</option>
		<option value="1">Fyzicka_osoba_ICO</option>
		<option value="2">Pravnicka_osoba</option>
	</select>
 </td>
</tr>
<tr class="even">
 <td width="25%"><b><%=Language["INVOICE_ICO"]%></b></td>
 <td width="75%" bgcolor="#F3F3F3"><input type="text" id="owner_ico" runat="server" class="sw-input" size="32" maxlength="32" /></td>
</tr>
<tr class="even">
 <td width="25%"><b><%=Language["PROTO_ADDRESS"]%></b></td>
 <td width="75%" bgcolor="#F3F3F3"><input type="text" id="owner_address" runat="server" class="sw-input" size="80" maxlength="120" /></td>
</tr>
<tr class="even">
 <td width="25%"><b><%=Language["PROTO_ZIPCODE"]%></b></td>
 <td width="75%" bgcolor="#F3F3F3"><input type="text" id="owner_zip" runat="server" class="sw-input" size="8" maxlength="5" /></td>
</tr>
<tr class="even">
 <td width="25%"><b><%=Language["PROTO_CITY"]%></b></td>
 <td width="75%" bgcolor="#F3F3F3"><input type="text" id="owner_city" runat="server" class="sw-input" size="32" maxlength="64" /></td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" alt="" /></div>
</form>
</div>
<%}%>

<br />

<table border="0" cellpadding="2" cellspacing="1" class="list">
<tr class="footer">
 <td align="left">
	<table border="0" cellpadding="0" cellspacing="0" style="width:100%">
	<tr><% if (TabItem1.Selected && !BikeSold) { %>
  <td align="left" nowrap="nowrap">
	<button id="btnInitSellBike" type="button" class="main"<%=(!IsProdejSubmitted?"":" style=\"display:none\"")%> onclick="InitSellBike();">
		<img src="images/process.png" alt="" />
		<%=Language["BUTTON_SELL"]%>
	</button>
	<button id="btnConfirmSellBike" type="button" class="main"<%=(IsProdejSubmitted?"":" style=\"display:none\"")%> onclick="ConfirmSellBike();">
		<img src="images/ok.png" alt="" />
		<%=Language["BUTTON_SELL_CONFIRM"]%>
	</button>
	<button id="btnCancelSellBike" type="button" class="main"<%=(IsProdejSubmitted?"":" style=\"display:none\"")%> onclick="CancelSellBike();">
		<img src="images/delete.png" alt="" />
		<%=Language["BUTTON_CANCEL"]%>
	</button>
 </td><%}%>
 <td align="right">
<button type="button" class="main" onclick="location='bikes.aspx';">
 <img src="images/cancel.png" alt="" />
 <%=Language["BUTTON_CLOSE"]%>
</button>
 </td>
	</tr>
	</table>
 </td>
</tr>
</table>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>
</div>

<script language="javascript" type="text/javascript" src="js/calendar.js"></script>
<script language="javascript" type="text/javascript">
//<![CDATA[

// calendar constants
Calendar._DN = '<%=Language["JS_DATE_DN"]%>'.split(',');
Calendar._SDN = '<%=Language["JS_DATE_DNS"]%>'.split(',');
Calendar._MN = '<%=Language["JS_DATE_MN"]%>'.split(',');
Calendar._SMN = '<%=Language["JS_DATE_MNS"]%>'.split(',');
Calendar._TT["PREV_YEAR"] = '<%=Language["JS_DATE_PREV_YEAR"]%>';
Calendar._TT["PREV_MONTH"] = '<%=Language["JS_DATE_PREV_MONTH"]%>';
Calendar._TT["GO_TODAY"] = '<%=Language["JS_DATE_GO_TODAY"]%>';
Calendar._TT["NEXT_MONTH"] = '<%=Language["JS_DATE_NEXT_MONTH"]%>';
Calendar._TT["NEXT_YEAR"] = '<%=Language["JS_DATE_NEXT_YEAR"]%>';
Calendar._TT["SEL_DATE"] = '<%=Language["JS_DATE_SELECT"]%>';
Calendar._TT["PART_TODAY"] = '<%=Language["JS_DATE_PART_TODAY"]%>';
Calendar._TT["DAY_FIRST"] = '<%=Language["JS_DATE_DAY_FIRST"]%>';
Calendar._TT["CLOSE"] = '<%=Language["BUTTON_CLOSE"]%>';
Calendar._TT["TODAY"] = '<%=Language["JS_DATE_GO_TODAY"]%>';

var calTxt = null;
function InitSellBike()
{
	document.getElementById('btnInitSellBike').style.display = 'none';
	document.getElementById('btnConfirmSellBike').style.display = '';
	document.getElementById('btnCancelSellBike').style.display = '';
	document.getElementById('divSellBike').style.display = 'block';
}
function ConfirmSellBike()
{
	document.getElementById('frmSellBike').submit();
}
function CancelSellBike()
{
	document.getElementById('btnInitSellBike').style.display = '';
	document.getElementById('btnConfirmSellBike').style.display = 'none';
	document.getElementById('btnCancelSellBike').style.display = 'none';
	document.getElementById('divSellBike').style.display = 'none';
}

function IsValidDate() {
  var d = dateFromString(calTxt.value);
  if (d == null) return false;
  return true;
}

function dateFromString(s) {
  if (s.replace(/[0-9\.\s]+/g,'') != '') return null;
  
  var r = s.split('.');
  if (r.length != 3) return null;
  
  var d = parseInt(r[0].replace(/^[\s]+|[\s]+$/,''));
  var m = parseInt(r[1].replace(/^[\s]+|[\s]+$/,''));
  var y = parseInt(r[2].replace(/^[\s]+|[\s]+$/,''));
    
  if (d >= 1 && d <= 32 && m >= 1 && m <= 12 && y >= 1980 && y <= 3000)
    r = new Date(y, m-1, d);
  else
    r = null; 
    
  return r;  
}

function onSelectCalendarDay(cal, date) {
  if (cal.dateClicked) {
      var y = cal.date.getFullYear();
      var m = cal.date.getMonth() + 1;
      var d = cal.date.getDate();
      
      cal.myTextInput.value = '' + d + '. ' + m + '. ' + y;
     
	  cal.hide();
  }
}

function onCloseCalendar(cal) {
  cal.hide();
}

function displayCalendar(txt) {
	if (txt==null)
	{
		calTxt = document.getElementById('sell_calTxt');
		txt = calTxt;	
	}
	
  if (txt.calendar == null) {
    txt.calendar = new Calendar(1, new Date(), onSelectCalendarDay, onCloseCalendar);
    txt.calendar.weekNumbers = false;    
    txt.calendar.showOthers = true;
    txt.calendar.create();    
    txt.calendar.myTextInput = txt;
  }
  
  var d = dateFromString(txt.value);
  if (d != null) txt.calendar.setDate(d);

  txt.calendar.showAtElement(txt);
}

//]]>
</script>

 </td>
</tr>
</table>
</div>

</body>

</html>

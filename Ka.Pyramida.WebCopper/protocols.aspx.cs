﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Ka.Garance.Data;

public partial class _Protocol : Ka.Garance.Web.GarancePage
{

	public string FilterNum
	{
		get { return User.Cache["proto.filter.num"]; }
	}

	public string FilterVin
	{
		get { return User.Cache["proto.filter.vin"]; } 
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		if (Request.QueryString["add"] != null)
		{
			User.CachedProtocol.Clear();
			User.ResetActiveProtocol();
			Response.Redirect("protocol.add.aspx");
		}

		if (Request.QueryString["state"] != null)
		{
			User.Cache["proto.state"] = Request.QueryString["state"];
		}

		if (Request.HttpMethod == "POST")
		{
			User.Cache["proto.filter.num"] = Request.Form["pnum"];
			User.Cache["proto.filter.vin"] = Request.Form["pvin"];
		}
		else if (Request.QueryString["filter"] != null)
		{
			User.Cache["proto.filter.num"] = null;
			User.Cache["proto.filter.vin"] = null;
		}

		int state = 0;
		try
		{
			state = Convert.ToInt32(User.Cache["proto.state"]);
		}
		catch
		{
		}

		state = Math.Max(-1, Math.Min(7, state));
		if (state < 0)
			MCFilterTabs.SelectedIndex = 0;
		else
			MCFilterTabs.SelectedIndex = state + 1;


		MCProtoList.SQL = string.Format(@"
SELECT
	id,
	cislo,
	jmeno,
	obec,
	karoserie,
	CAST(IF(IFNULL(datumoprava,'')='',NULL,CONCAT('20',MID(datumoprava,5,2),'-',MID(datumoprava,3,2),'-',LEFT(datumoprava,2))) AS DATE) AS datum_opravy,
	CASE stav
		WHEN 2 THEN '{0}'
		WHEN 3 THEN '{1}'
		WHEN 4 THEN '{2}'
		WHEN 5 THEN '{3}'
		WHEN 6 THEN '{4}'
		WHEN 7 THEN '{5}'
		WHEN 1 THEN '{6}'
		WHEN 0 THEN '{7}'
		ELSE ''
	END AS tcstav
FROM tprotocol",
			   Language["PROTO_STATE_ERROR"],
			   Language["PROTO_STATE_NOINVOICE"],
			   Language["PROTO_STATE_INVOICE"],
			   Language["PROTO_STATE_AUTHORIZED"],
			   Language["PROTO_STATE_CANCELED"],
			   Language["PROTO_STATE_REMOVED"],
			   Language["PROTO_STATE_SENT"],
			   Language["PROTO_STATE_NEW"]);

		MCProtoList.Filter = string.Format("iddealer={0}{1}", User.Id, (state < 0 ? "" : string.Format(" AND tprotocol.stav={0}", state)));

		string filterVIN = FilterVin;
		if (filterVIN != null && filterVIN.Trim() != "")
			MCProtoList.Filter += string.Format(" AND karoserie LIKE '%{0}%'", MySQLDatabase.Escape(filterVIN.Trim(), true));

		string filterNum = FilterNum;
		if (filterNum != null && filterNum.Trim() != "")
			MCProtoList.Filter += string.Format(" AND cislo LIKE '%{0}%'", MySQLDatabase.Escape(filterNum.Trim(), true));

	}
}

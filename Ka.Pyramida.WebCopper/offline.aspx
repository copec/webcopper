﻿<%@ Page Language="C#" %>

<% if (Request.QueryString["mode"] == "logout") { %>

<div align="center" style="border:1px dotted #E2E2E2;background-color:#FFFFE2;padding:16px;color:red">
 <b>Error!</b><br /><br />
 Session timeout!
</div>

<% } else if (Request.QueryString["ajax"] == null) { %>

<DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-GB" dir="ltr" lang="en-GB">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>YUKI Guarantees</title>
<link rel="stylesheet" type="text/css" href="css/login.css" />
</head>

<body>

<table border="0" cellpadding="0" cellspacing="0" style="width:100%">
<tr>
 <td colspan="2" style="background:url(images/bk.top.png) repeat-x 0% 0% #8BA8C7"><img src="images/yuki.en.png" alt="YUKI Guarantees" style="margin:2px 0px 2px 6px" /></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" style="width:100%">
<tr>
 <td align="center" style="padding-top:64px">
 
<% } %><div style="<%=(Request.QueryString["ajax"] == null?"width:600px;":"")%>text-align:left;border:1px dotted #E2E2E2;background-color:#FFFFE2;padding:16px;color:red">
 <b>DB error!</b><br /><br />
 <%=Request.QueryString["error"]%>
</div><% if (Request.QueryString["ajax"] == null) { %>
 
 </td>
</tr>
</table>
	
</body>

</html><% } %>
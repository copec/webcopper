﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.xml;
using iTextSharp.text.html;
using Ka.Garance.Data;
using Ka.Garance.Invoice.Yuki;
using Ka.Garance.Protocol.Yuki;

public partial class _Protocol_Detail : Ka.Garance.Web.GarancePage
{
	protected const string PCHECK_DELETE = "yut657.q23_vFExR3";
	protected const string PCHECK_SEND = "67ghGEK_576.GFa56";

	private bool m_SentForReview = false;
	private bool m_Deleting = false;

	public YukiProtocol Protocol
	{
		get { return User.ActiveProtocol; }
	}
	public bool SentForReview
	{
		get { return m_SentForReview; }
	}
	public bool Deleting
	{
		get { return m_Deleting; }
	}
	protected bool ProtocolHasZprava
	{
		get { return (Protocol.Zprava != null && Protocol.Zprava.Trim() != "" && Protocol.Zprava.Trim() != "-"); }
	}
	protected string JsProtocolZprava
	{
		get { return JsEncode(Protocol.Zprava); }
	}
	private bool m_Deleted = false;
	public bool Deleted
	{
		get { return m_Deleted; }
	}

	protected string GetICOString()
	{
		if (Protocol.ICO == null || Protocol.ICO.Trim() == "")
			return "";
		return string.Format("<br /><br />{0}: {1}", Language["INVOICE_ICO"], Protocol.ICO);
	}

    protected void Page_Load(object sender, EventArgs e)
    {
		if (Request.QueryString["delid"] != null)
		{
			m_Deleting = true;
			User.GetActiveProtocol(Request.QueryString["delid"]);
			return;
		}

		if (Request.QueryString["deleid"] != null)
		{
			m_Deleted = true;
			return;
		}

		if (Request.QueryString["editid"] != null && Protocol.Editovatelny)
		{
			try
			{
				User.CachedProtocol.Load(Request.QueryString["editid"]);
			}
			catch
			{
				Response.Redirect("protocols.aspx");
			}

			User.CachedProtocol.ActualStep = 1;
			User.CachedProtocol.SaveToCache();

			User.ResetActiveProtocol();

			Response.Redirect("protocol.edit.aspx");
		}

		if (Request.QueryString["ajax"] == null)
		{
			User.ResetActiveProtocol();
			User.GetActiveProtocol(Request.QueryString["id"]);
		}
		else
			User.GetActiveProtocol(Request.QueryString["cislo"]);

		if (Request.QueryString["pdf"] != null)
		{
			System.IO.MemoryStream ms = RenderPdf();
			try
			{
				Ka.Garance.Util.GaranceUtil.ForceDownload(Context, ms, string.Format("Yuki-Protocol-{0}.pdf", Protocol.Cislo));
			}
			finally
			{
				ms.Dispose();
			}
			return;
		}

		if (Request.HttpMethod == "POST")
		{
			// check if can delete
			if (Request.Form["p_btn_delete"] != null && Request.Form["pcheck"] == PCHECK_DELETE && Protocol.Editovatelny)
			{
				Protocol.Delete();
				Response.Redirect(string.Format("protocol.detail.aspx?deleid={0}", Protocol.Cislo));
			}
			else if (Request.Form["p_btn_send"] != null && Request.Form["pcheck"] == PCHECK_SEND && Protocol.Editovatelny)
			{
				if (Protocol.HasValidPrices)
					m_SentForReview = Protocol.SendForReview();
				else
					m_SentForReview = false;
			}
		}

		// prepocitat !!!
		Ka.Garance.Protocol.Yuki.YukiProtocol.UpdateZavadaPrice(this.Database, User.ActiveProtocol.Cislo);

		ListItem6.FormatProvider = System.Globalization.CultureInfo.InvariantCulture;
		ListItem7.FormatProvider = System.Globalization.CultureInfo.InvariantCulture;


		MCProtocolDetailList.SQL = string.Format("SELECT poradi,kod,druh,vyrobce,zavada,popis,prace,material,IF(stav=1,'ok','delete') AS stav,idzavadadealer FROM tzavada");
		MCProtocolDetailList.CategoryTitle = Language["PROTO_GROUP_ISSUES"];
		MCProtocolDetailList.JsClickRow = string.Format("pdetail_DisplayIssue('{{0}}', '{0}');", Protocol.Cislo);
		MCProtocolDetailList.Filter = string.Format("cisloprotokol={0}", MySQLDatabase.Escape(Protocol.Cislo));
		MCProtocolDetailList.InfoText = ((Protocol.Stav < 1 && this.Protocol.HasValidPrices) || Protocol.Stav >= 1 ? "" : Language["PROTO_ERR_NOPRICES"]);

		if (Protocol.Stav != 0)
			ListItem8.Format = "<img src=\"images/{0}.png\" alt=\"\" />";
		else
			ListItem8.Format = "&nbsp;";

		ListItem6.Validate += new Ka.Garance.Web.List.ValidateListItem(ListItem6_Validate);
		ListItem7.Validate += new Ka.Garance.Web.List.ValidateListItem(ListItem7_Validate);


		if (Protocol.Stav == 2)
			MessageNotify = 1;
    }

	protected int MessageNotify
	{
		get
		{
			try { return (int)Session["index.MessageNotify"]; }
			catch { return 0; }
		}
		set
		{
			Session["index.MessageNotify"] = value;
		}
	}

	private void ListItem6_Validate(object sender, Ka.Garance.Web.List.ValidateEventArgs e)
	{
		e.Validated = true;

		/*
		if (Convert.ToString(e.Reader.GetValue("stav")) != "ok")
		{
			e.Value = string.Format("{0:F2}", 0.0D);
			return;
		}
		*/

		decimal total = 0;
		bool isSKDealer = (User.IdZeme == 2 && Protocol.DateCreated >= new DateTime(2009, 1, 1));

		MySQLDataReader dr = Database.ExecuteReader("SELECT A.cena FROM tzpozice A WHERE A.cisloprotokol={0} AND A.idzavadadealer={1} AND A.stav=1", Protocol.Cislo, e.Reader.GetValue("idzavadadealer"));
		while (dr.Read())
		{
			decimal cena = Convert.ToDecimal(dr.GetValue("cena"));
			cena = Math.Round(cena * Protocol.DPHConst, isSKDealer ? 2 : 1);
			total += cena;
		}

		dr = Database.ExecuteReader("SELECT A.cena FROM tzpozicevolna A WHERE A.cisloprotokol={0} AND A.idzavadadealer={1} AND A.stav=1", Protocol.Cislo, e.Reader.GetValue("idzavadadealer"));
		while (dr.Read())
		{
			decimal cena = Convert.ToDecimal(dr.GetValue("cena"));
			cena = Math.Round(cena * Protocol.DPHConst, isSKDealer ? 2 : 1);
			total += cena;
		}

		e.Value = string.Format("{0:F2}", total);
	}

	private void ListItem7_Validate(object sender, Ka.Garance.Web.List.ValidateEventArgs e)
	{
		e.Validated = true;

		/*
		if (Convert.ToString(e.Reader.GetValue("stav")) != "ok")
		{
			e.Value = string.Format("{0:F2}", 0.0D);
			return;
		}
		*/
		bool isSKDealer = (User.IdZeme == 2 && Protocol.DateCreated >= new DateTime(2009, 1, 1));
		decimal total = 0;
		string query = string.Format("SELECT A.cena FROM tzdil A WHERE A.cisloprotokol='{0}' AND A.idzavadadealer={1} AND A.stav=1", Protocol.Cislo, e.Reader.GetValue("idzavadadealer"));

		MySQLDataReader dr = Database.ExecuteReader(query);
		while (dr.Read())
		{
			decimal cena = Convert.ToDecimal(dr.GetValue("cena"));
			cena = Math.Round(cena * Protocol.DPHRabatConst, isSKDealer ? 2 : 1);
			total += cena;
		}

		e.Value = string.Format("{0:F2}", total);
	}

	public string ProtocolStavText
	{
		get
		{
			if (Deleted) return Language["PROTO_STATE_DELETED"];
			switch (Protocol.Stav)
			{
				case 0: return Language["PROTO_STATE_NEW"];
				case 1: return Language["PROTO_STATE_SENT"];
				case 2: return Language["PROTO_STATE_ERROR"];
				case 3: return Language["PROTO_STATE_NOINVOICE"];
				case 4: return Language["PROTO_STATE_INVOICE"];
				case 5: return Language["PROTO_STATE_AUTHORIZED"];
				case 6: return Language["PROTO_STATE_CANCELED"];
				case 7: return Language["PROTO_STATE_REMOVED"];
			}
			return "";
		}
	}

	public string GetEditButton()
	{
		if (Protocol.Stav == 5)
			return string.Format(@"
   <td align=""left"">
    <form action=""protocol.detail.aspx"" method=""post"">
    <input type=""hidden"" name=""discardid"" value=""{0}"" />
    <button type=""submit"" class=""main"" style=""font-weight:bold"">
     <img src=""images/discard.png"" alt="""" />
     {1}
    </button>
    </form>
   </td>
", Protocol.Cislo, Language["PROTO_BUTTON_DISCARD"]);

		// send for review
		// only if all prices available
		if (Protocol.Stav < 1 || Protocol.Stav == 2) return string.Format(@"
   <td align=""left"">
    <form action=""protocol.detail.aspx?id={0}"" method=""post""><input type=""hidden"" name=""pcheck"" value="""" />
    <button type=""submit"" class=""main"" style=""font-weight:bold"" onclick=""location='protocol.detail.aspx?editid={0}';return false;"">
     <img src=""images/edit.png"" alt="""" />
     {1}
    </button>
    <button type=""submit""{5} class=""main"" style=""width:160px"" name=""p_btn_send"" onclick=""this.form.pcheck.value='{4}';return true;"">
     <img src=""images/process.png"" alt="""" />
     {2}
    </button>    
    <button type=""button"" name=""p_btn_delete"" class=""main"" onclick=""location='protocol.detail.aspx?delid={0}';"">
     <img src=""images/delete.png"" alt="""" />
     {3}
    </button>
    </form>
   </td>",
			Protocol.Cislo, Language["BUTTON_EDIT"], Language["PROTO_BUTTON_SEND"], Language["BUTTON_DELETE"], PCHECK_SEND,
			(Protocol.HasValidPrices ? "" : " disabled=\"disabled\"")
		);
		return "";
	}

	protected override void Render(HtmlTextWriter writer)
	{
		if (Request.QueryString["ajax"] != null)
		{
			ExecuteAjaxHandler(Request.QueryString["ajax"], writer);
		}
		else
			base.Render(writer);
	}

	private void ExecuteAjaxHandler(string handler, HtmlTextWriter writer)
	{
		switch (handler)
		{
			case "issue":
				RenderIssue(Convert.ToInt32(Request.QueryString["id"]), writer);
				break;
		}
	}

	private void RenderIssue(int idzavadadealer, HtmlTextWriter writer)
	{
		bool isSKDealer = User.IdZeme == 2 && Protocol.DateCreated >= new DateTime(2009, 1, 1);
		int nZavada = Convert.ToInt32(Database.ExecuteScalar("SELECT COUNT(*) FROM tzavada WHERE cisloprotokol={0}", Protocol.Cislo));

		MySQLDataReader dr = Database.ExecuteReader("SELECT kod,druh,vyrobce,zavada,popis,poradi FROM tzavada WHERE cisloprotokol={0} AND idzavadadealer={1}", Protocol.Cislo, idzavadadealer);
		dr.Read();

		// hlavicka
		writer.Write("<table border=\"0\" cellpadding=\"4\" cellspacing=\"1\" class=\"list\">\n");
		writer.Write("<tr class=\"header\">\n");
		writer.Write(" <th align=\"left\">{0} - {1}/{2}</th>\n", Language["PROTO_ISSUE_DETAILCAPTION"], dr.GetValue("poradi"), nZavada);
		writer.Write("</tr>\n");
		writer.Write("</table>\n<div style=\"padding:0px 3px 0px 3px\"><img src=\"images/bk.shadow.png\" width=\"100%\" height=\"1\" /></div>\n<br />\n\n");

		// pricina
		writer.Write("<table border=\"0\" cellpadding=\"4\" cellspacing=\"1\" class=\"list\">\n");
		writer.Write("<tr class=\"category\">\n");
		writer.Write(" <td colspan=\"6\">{0}</td>\n", Language["PROTO_ISSUE_DETAILS"]);
		writer.Write("</tr>\n");
		writer.Write("<tr class=\"even\">\n");
		writer.Write(" <td width=\"10%\">{0}</td>\n", Language["PROTO_ISSUE_CODE"]);
		writer.Write(" <td class=\"odd\">{0}</td>\n", dr.GetValue("kod"));
		writer.Write(" <td width=\"10%\">{0}</td>\n", Language["PROTO_ISSUE_DZ"]);
		writer.Write(" <td class=\"odd\">{0}</td>\n", Database.ExecuteScalar("SELECT jmeno FROM tcdruhzavady WHERE id={0}", dr.GetValue("druh")));
		writer.Write(" <td width=\"10%\">{0}</td>\n", Language["PROTO_ISSUE_MANUFACTURER"]);
		writer.Write(" <td class=\"odd\">{0}</td>\n", dr.GetValue("vyrobce"));
		writer.Write("</tr>\n");
		writer.Write("<tr class=\"even\">\n");
		writer.Write(" <td width=\"10%\">{0}</td>", Language["PROTO_ISSUE_TITLE"]);
		writer.Write(" <td colspan=\"5\" width=\"10%\" class=\"odd\">{0}</td>", dr.GetValue("zavada"));
		writer.Write("</tr>\n");
		writer.Write("<tr class=\"even\">\n");
		writer.Write(" <td width=\"10%\">{0}</td>", Language["PROTO_ISSUE_TEXT"]);
		writer.Write(" <td colspan=\"5\" width=\"10%\" class=\"odd\">{0}</td>", dr.GetValue("popis"));
		writer.Write("</tr>\n");
		writer.Write("</table>\n<div style=\"padding:0px 3px 0px 3px\"><img src=\"images/bk.shadow.png\" width=\"100%\" height=\"1\" /></div>\n<br />\n\n");

		// pracovni pozice
		writer.Write("<table border=\"0\" cellpadding=\"4\" cellspacing=\"1\" class=\"list\">\n");
		writer.Write("<tr class=\"category\">\n");
		writer.Write(" <td colspan=\"7\">{0}</td>\n", Language["PROTO_WORK_POS"]);
		writer.Write("</tr>\n");
		writer.Write("<tr class=\"header\">\n");
		writer.Write(" <th align=\"center\" nowrap=\"nowrap\" width=\"10%\">{0}</th>\n", Language["PROTO_POS_BPART"]);
		writer.Write(" <th align=\"center\" nowrap=\"nowrap\" width=\"10%\">{0}</th>\n", Language["PROTO_POS_POS"]);
		writer.Write(" <th align=\"left\">{0}</th>\n", Language["PROTO_POS_NAME"]);
		writer.Write(" <th align=\"left\">{0}</th>\n", Language["PROTO_POS_OP"]);
		writer.Write(" <th align=\"center\" width=\"5%\">{0}</th>\n", Language["PROTO_POS_TIME"]);
		writer.Write(" <th align=\"right\" width=\"10%\">{0}</th>\n", Language["PARTS_PRICE"]);
		writer.Write(" <th align=\"center\" width=\"1%\">{0}</th>\n", Language["PROTO_ISSUE_STATE"]);
		writer.Write("</tr>\n");
	
		dr = Database.ExecuteReader(@"
		SELECT CONCAT('0-',A.id) AS id,B.skodnicislo,B.pozicecislo,B.nazev,B.operace,A.cj,A.cena,A.stav
		FROM tzpozice A
		LEFT JOIN tcpozice B ON B.id=A.idpozice
		WHERE A.cisloprotokol={0} AND A.idzavadadealer={1}
		UNION
		SELECT CONCAT('1-',id) AS id,skodnicislo,pozicecislo,nazev,operace,cj,cena,stav FROM tzpozicevolna
		WHERE cisloprotokol={0} AND idzavadadealer={1}
		ORDER BY id
		", Protocol.Cislo, idzavadadealer);
		bool even = false;
		while (dr.Read())
		{
			even = !even;
			bool schvaleno = (Protocol.Stav > 2 && Convert.ToInt32(dr.GetValue("stav")) == 1);
			writer.Write("<tr class=\"{0}\">\n", (even ? "even" : "odd"));
			writer.Write(" <td align=\"center\" width=\"10%\">{0}</td>\n", dr.GetValue("skodnicislo"));
			writer.Write(" <td align=\"center\" width=\"10%\">{0}</td>\n", dr.GetValue("pozicecislo"));
			writer.Write(" <td align=\"left\">{0}</td>\n", dr.GetValue("nazev"));
			writer.Write(" <td align=\"left\">{0}</td>\n", dr.GetValue("operace"));
			writer.Write(" <td align=\"center\" width=\"5%\">{0}</td>\n", dr.GetValue("cj"));
			writer.Write(" <td align=\"right\" width=\"10%\">{0:F2}</td>\n", Math.Round(Convert.ToDecimal(dr.GetValue("cena")) * Protocol.DPHConst, isSKDealer ? 2 : 1));
			if  (Protocol.Stav > 2)
				writer.Write(" <td align=\"center\" width=\"1%\"><img src=\"images/{0}.png\" alt=\"\" /></td>\n", (schvaleno?"ok":"delete"));
			else
				writer.Write(" <td align=\"center\" width=\"1%\">&nbsp;</td>\n");
			writer.Write("</tr>\n");
		}	

		writer.Write("</table>\n<div style=\"padding:0px 3px 0px 3px\"><img src=\"images/bk.shadow.png\" width=\"100%\" height=\"1\" /></div>\n<br />\n\n");

		// nahradni dily
		writer.Write("<table border=\"0\" cellpadding=\"4\" cellspacing=\"1\" class=\"list\">\n");
		writer.Write("<tr class=\"category\">\n");
		writer.Write(" <td colspan=\"8\">{0}</td>\n", Language["PROTO_WORK_ND"]);
		writer.Write("</tr>\n");
		writer.Write("<tr class=\"header\">\n");
		writer.Write(" <th align=\"center\" nowrap=\"nowrap\" width=\"10%\">{0}</th>\n", Language["PARTS_GROUP"]);
		writer.Write(" <th align=\"center\" nowrap=\"nowrap\" width=\"10%\">{0}</th>\n", Language["PARTS_POS"]);
		writer.Write(" <th align=\"left\">{0}</th>\n", Language["PARTS_NAME"]);
		writer.Write(" <th align=\"center\" width=\"5%\">{0}</th>\n", Language["PROTO_COLOUR"]);
		writer.Write(" <th align=\"center\" width=\"5%\">{0}</th>\n", Language["PROTO_QTY"]);
		writer.Write(" <th align=\"center\" width=\"5%\">{0}</th>\n", Language["PROTO_ORDER"]);
		writer.Write(" <th align=\"right\" width=\"10%\">{0}</th>\n", Language["PARTS_PRICE"]);
		writer.Write(" <th align=\"center\" width=\"1%\">{0}</th>\n", Language["PROTO_ISSUE_STATE"]);
		writer.Write("</tr>\n");

		Hashtable hProcessed = new Hashtable();

		dr = Database.ExecuteReader(string.Format(@"
SELECT DISTINCT
	B.id AS B_id,
	B.skupina AS skupina,
	B.pozice AS pozice,
	B.nazevdilu{0} AS nazev,
	A.cena AS cena,
	A.mnozstvi AS qty,
	IFNULL(A.mnozstviobj,0) AS obj,
	IFNULL(A.stav,0) AS stav,
	C.Barva{1} AS barva,
	A.iddil AS iddil,
	B.sklcislo AS sklcislo
FROM tzdil A
	LEFT JOIN tcskodnicisla B ON B.sklcislo=A.iddil
	LEFT JOIN ndbarvy C ON C.Identifikator=SUBSTRING(B.sklcislo, 8)
WHERE A.cisloprotokol={{0}} AND A.idzavadadealer={{1}}
ORDER BY A.id, B.state ASC
		", (Language.UseEnglishNames ? "_en" : ""), (Language.UseEnglishNames ? "EN" : "CZ")), Protocol.Cislo, idzavadadealer, Protocol.ModelIdVzor);
		even = false;
		while (dr.Read())
		{

			if (hProcessed[dr.GetValue("sklcislo")] != null)
				continue;

			hProcessed[dr.GetValue("sklcislo")] = true;

			even = !even;
			bool schvaleno = (Protocol.Stav > 2 && Convert.ToInt32(dr.GetValue("stav")) == 1);

			writer.Write("<tr class=\"{0}\">\n", (even ? "even" : "odd"));
			writer.Write(" <td align=\"center\" width=\"10%\">{0}</td>\n", dr.GetValue("skupina"));
			writer.Write(" <td align=\"center\" width=\"10%\">{0}</td>\n", dr.GetValue("pozice"));
			writer.Write(" <td align=\"left\">{0}</td>\n", dr.GetValue("nazev"));
			writer.Write(" <td align=\"center\">{0}</td>\n", dr.GetValue("barva"));
			writer.Write(" <td align=\"center\" width=\"5%\">{0}</td>\n", dr.GetValue("qty"));
			writer.Write(" <td align=\"center\" width=\"5%\">{0}</td>\n", dr.GetValue("obj"));
			writer.Write(" <td align=\"right\" width=\"10%\">{0:F2}</td>\n", Math.Round(Convert.ToDecimal(dr.GetValue("cena")) * Protocol.DPHRabatConst, isSKDealer ? 2 : 1));
			if (Protocol.Stav > 2)
				writer.Write(" <td align=\"center\" width=\"1%\"><img src=\"images/{0}.png\" alt=\"\" /></td>\n", (schvaleno ? "ok" : "delete"));
			else
				writer.Write(" <td align=\"center\" width=\"1%\">&nbsp;</td>\n");
			writer.Write("</tr>\n");
		}
		writer.Write("</table>\n<div style=\"padding:0px 3px 0px 3px\"><img src=\"images/bk.shadow.png\" width=\"100%\" height=\"1\" /></div>\n<br />\n");

		// tlacitka
		writer.Write("<table border=\"0\" cellpadding=\"2\" cellspacing=\"1\" class=\"list\">\n");
		writer.Write("<tr class=\"footer\">\n <td align=\"right\">\n");
		writer.Write("<button type=\"button\" class=\"main\" onclick=\"pdetail_DisplayRoot();\">\n<img src=\"images/icon.larr.png\" alt=\"\" />\n{0}\n</button>\n", Language["BUTTON_BACK"]);
		writer.Write(" </td>\n</tr>\n");
		writer.Write("</table>\n<div style=\"padding:0px 3px 0px 3px\"><img src=\"images/bk.shadow.png\" width=\"100%\" height=\"1\" /></div>\n");
	}

	private System.IO.MemoryStream RenderPdf()
	{
		PdfMemoryStream ms = new PdfMemoryStream();
		ms.LeaveOpen = true;
		Document document = new Document(PageSize.A4, 45, 45, 45, 45);
		try
		{
			PdfWriter pf = PdfWriter.GetInstance(document, ms);
			document.Open();
			new YukiProtocolPdfRenderer(this.Protocol).Render(document);
		}
		finally
		{
			try { document.Close(); }
			catch { }
		}
		ms.LeaveOpen = false;
		ms.Position = 0;
		return ms;
	}

}

using System;
using System.Data;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Reflection;
using Ka.Garance.Data;
using Ka.Garance.Web;
using Ka.Garance.Protocol.Yuki;

public partial class _Index : Ka.Garance.Web.GarancePage
{
	protected int MessageNotify
	{
		get
		{
			try { return (int)Session["index.MessageNotify"]; }
			catch { return 0; }
		}
		set
		{
			Session["index.MessageNotify"] = value;
		}
	}


    protected void Page_Load(object sender, EventArgs e)
    {
		if (Request.QueryString["ajax"] != null)
		{
			ValidateBikeAjax();
		}

		ProcessMessageNotify();

		MCPanelProtokol.Title = Language["HEADER_PROTOCOL"];
		PanelItem1.Title = Language["HEADER_PROTOCOL_ADD"];
		PanelItem1.Description = Language["INDEX_PROTOADD_DESC"];
		PanelItem2.Title = Language["HEADER_PROTOCOLS"];
		PanelItem2.Description = Language["INDEX_PROTO_DESC"];

		MCPanelFaktury.Title = Language["HEADER_INVOICE"];
		PanelItem5.Title = Language["HEADER_INVOICE_ADD"];
		PanelItem5.Description = Language["INDEX_INVOICEADD_DESC"];
		PanelItem6.Title = Language["HEADER_INVOICES"];
		PanelItem6.Description = Language["INDEX_INVOICES_DESC"];

		MCPanelClaim.Title = Language["HEADER_CLAIM"];
		PanelItem7.Title = Language["HEADER_CLAIM_ADD"];
		PanelItem7.Description = Language["INDEX_CLAIMADD_DESC"];
		PanelItem8.Title = Language["HEADER_CLAIMS"];
		PanelItem8.Description = Language["INDEX_CLAIMS_DESC"];

		MCPanelKatalog.Title = Language["HEADER_CATALOGS"];
		PanelItem3.Title = Language["HEADER_BIKES"];
		PanelItem3.Description = Language["INDEX_BIKES_DESC"];
		PanelItem4.Title = Language["HEADER_PARTS"];
		PanelItem4.Description = Language["INDEX_PARTS_DESC"];


    }

	private DateTime DateFromString(string date)
	{
		string[] ds = date.Split(".".ToCharArray());
		if (ds.Length != 3) throw new Exception("Invalid date format");
		return new DateTime(
			Convert.ToInt32(ds[2].Trim()),
			Convert.ToInt32(ds[1].Trim()),
			Convert.ToInt32(ds[0].Trim())
		);
	}

	private void ValidateBikeAjax()
	{
		YukiProtocol p = new YukiProtocol(Database, User);

		// vin
		p.VIN = Request.QueryString["vin"];

		// datum
		try
		{
			p.DateRepair = DateFromString(Request.QueryString["date"]);
		}
		catch
		{
			Response.Write(string.Format("ERR-{0}", Language["PROTO_ERR_DATE"]));
			Response.End();
		}

		// km
		try
		{
			p.Km = Convert.ToInt32(Request.QueryString["km"]);
		}
		catch
		{
			Response.Write(string.Format("ERR-{0}", Language["PROTO_ERR_INVALID_KM"].Replace("%s", Request.QueryString["km"])));
			Response.End();
		}

		string err = "";

		if (!p.IsValidVIN)
			err = Language["PROTO_ERR_VIN"];
		else if (!p.IsProdanyVIN)
			err = Language["PROTO_ERR_VIN2"];
		else if (!p.IsValidKm)
			err = Language["PROTO_ERR_KM"];
		else if (p.KmMimoZaruky || p.ZarukaVyprsela)
			err = Language["PROTO_ERR_KM2"];
		else
		{
			Response.Write(string.Format("OK-{0}", Language["PROTO_MSG_VERIFIED"].Replace("%s", p.VIN)));
			Response.End();
			return;
		}

		Response.Write(string.Format("ERR-{0}", err));
		Response.End();
	}

	protected StringBuilder sbMessageNotifyData = new StringBuilder();

	private void ProcessMessageNotify()
	{
		MySQLDataReader dr = Database.ExecuteReader("SELECT cislo,karoserie,jmeno FROM tprotocol WHERE stav=2 AND iddealer={0} ORDER BY id LIMIT 5", User.Id);

		if (dr.Count > 0)
		{
			if (MessageNotify <= 0) MessageNotify = 2;
			else MessageNotify = 1;
		}
		else
		{
			MessageNotify = 0;
			return;
		}

		sbMessageNotifyData.Length = 0;
		sbMessageNotifyData.Append("<div align=\"left\" style=\"padding:12px;line-height:150%\">");
		sbMessageNotifyData.Append(Language["PROTO_ERR_NOTVALID"]);
		sbMessageNotifyData.Append("<br />");

		while (dr.Read())
		{

			sbMessageNotifyData.AppendFormat(
				"<br />\n&nbsp; &bull; <a href=\"protocol.detail.aspx?id={0}\" class=\"protomsg\"><b>{0} / {1}</b> - {2}</a>",
				dr.GetStringValue(0),
				dr.GetStringValue(1),
				HttpUtility.HtmlEncode(dr.GetStringValue(2))
			);
		}

		sbMessageNotifyData.Append("</div>");
	}

	protected string GetMessageNotify()
	{
		return GarancePage.JsEncode(sbMessageNotifyData.ToString());
	}
}

﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class _ProtocolAdd : Ka.Garance.Web.GarancePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
		string s = "";
		switch (Protocol.ActualStep)
		{
			case 1:
				s = Language["PROTO_ADD_BASICS"];
				break;
			case 2:
				s = Language["PROTO_ADD_ISSUE"];
				break;
			case 3:
				s = Language["PROTO_ADD_REVIEW"];
				break;
			case 4:
				s = Language["PROTO_ADD_REVIEW"];
				break;
			case 21:
				s = Language["PROTO_ADD_ISSNEW"];
				break;
			case 22:
				s = Language["PROTO_ADD_ISSEDIT"];
				break;
		}
		MCHeader.Location = string.Format("{0} &gt; {1}", Language["LOCATION_PROTOCOL_ADD"], s);
    }

	public Ka.Garance.Protocol.Yuki.YukiProtocol Protocol
	{
		get
		{
			return (Page as Ka.Garance.Web.GarancePage).User.CachedProtocol;
		}
	}

	protected override void Render(HtmlTextWriter writer)
	{
		if (Request.QueryString["ajax"] != null)
			ExecuteAjaxRequest(writer);
		else
			base.Render(writer);
	}

	private void ExecuteAjaxRequest(HtmlTextWriter writer)
	{
		switch (Request.QueryString["ajax"])
		{
			case "code":
			case "nd":
				Control ppp = LoadControl("Controls/Code.Ajax.ascx");
				ppp.RenderControl(writer);
				break;
			case "issue":
				LoadControl("Controls/Issue.Ajax.ascx").RenderControl(writer);
				break;
			case "pos":
				LoadControl("Controls/Pozice.Ajax.ascx").RenderControl(writer);
				break;
			case "parts":
				LoadControl("Controls/Parts.Ajax.ascx").RenderControl(writer);
				break;
		}
	}



}

<%@ Application Language="C#" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.IO.Compression" %>

<script runat="server">

    void Application_BeginRequest(object sender, EventArgs e) 
    {
        string encoding = this.Request.Headers["Accept-Encoding"];
        if (encoding == null) return;
        else encoding = encoding.ToLower();

        // current response stream
        Stream baseStream = this.Response.Filter;
        if (baseStream is GZipStream || baseStream is DeflateStream)
            return;

        // enable the compression
        if (encoding.Contains("gzip"))
        {
            this.Response.Filter = new GZipStream(baseStream, CompressionMode.Compress);
            this.Response.AppendHeader("Content-Encoding", "gzip");
        }
        else if (encoding.Contains("deflate"))
        {
            this.Response.Filter = new DeflateStream(baseStream, CompressionMode.Compress);
            this.Response.AppendHeader("Content-Encoding", "deflate");
        }
    }   
       
</script>

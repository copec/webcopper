﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="_Claim" Codebehind="claim.aspx.cs" %>
<%@ Register TagPrefix="yuki" Namespace="Ka.Garance.Web.List" Assembly="garance" %>
<%@ Register TagPrefix="yuki" TagName="header" Src="Controls/Header.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Language.Code%>" dir="ltr" lang="<%=Language.Code%>">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><%=Language["APP_TITLE"]%></title>
<link rel="stylesheet" type="text/css" href="css/yuki.css" />
</head>

<body>

<yuki:header Id="MCHeader" LocationID="LOCATION_CLAIM" runat="server" />

<div align="center" style="padding:0px 16px 16px 16px"> 
<table border="0" cellpadding="0" cellspacing="0" align="center" style="width:1%">
<tr>
 <td align="left">
 
<div style="width:800px"> 

<img src="images/mod.claim.png" alt="" style="vertical-align:text-bottom" />
<span class="mod"><%=Language["HEADER_CLAIMS"]%></span><br /><br />

<yuki:List ID="MCClaimList" Name="ClaimList" Identifier="id" Link="claim.detail.aspx?id={0}" runat="server">
 <yuki:ListItem ID="ListItem1" LangID="CLAIM_LIST_DATE" ColumnName="datum" align="left" width="10%" td_nowrap="nowrap" Default="true" DefaultState="Desc" Format="{0:d'. 'M'. 'yyyy'  'HH':'mm}" runat="server" />
 <yuki:ListItem ID="ListItem2" LangID="CLAIM_LIST_TITLE" ColumnName="nazev" align="left" width="84%" runat="server" />
 <yuki:ListItem ID="ListItem3" LangID="CLAIM_LIST_PROTO" ColumnName="pocet" align="left" width="6%" runat="server" />
</yuki:List>
<div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>

</div>
 </td>
</tr>
</table>
</div>

</body>

</html>
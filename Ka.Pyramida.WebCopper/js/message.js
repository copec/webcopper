﻿
var MSG_BUTTON_CLOSE = '';
var MSG_MESSAGE = '';
var MSG_dontToggleMessage = false;
var MSG_DisplayStartup = '';

function stopMessage() {
  var img = document.getElementById('imgMessage001');
  img.stopped = true;
}
function toggleMessage() {
  var img = document.getElementById('imgMessage001');
  
  if (img == null || MSG_dontToggleMessage) return;
  
  if (img.style.visibility != 'hidden')
    img.style.visibility = 'hidden';
  else
    img.style.visibility = 'visible';
    
  if (img.stopped && img.style.visibility != 'hidden') return;
    
  window.setTimeout('toggleMessage();', (img.style.visibility == 'hidden' ? 750 : 4500));
}
function hideMessage(img) {  
  var div = img.offsetParent.offsetParent.offsetParent;
  if (div.hiddenInput != null) {
    div.hiddenInput.value = div.childNodes[0].rows[1].cells[0].childNodes[0].value;
  }  
  div.style.display = 'none';
}
function displayMessage(e,msg,bEditable,idHidden,infoOnly) {

  if (e.msgDiv == null) {
    e.msgDiv = document.createElement('DIV');
    e.msgDiv.style.position = 'absolute';
    e.msgDiv.style.border = '1px solid #fff';
    e.msgDiv.style.display = 'none';
    e.msgDiv.style.zIndex = 4678;
        
    if (infoOnly)
		e.msgDiv.innerHTML =
		  '<table border="0" cellpadding="0" cellspacing="0" class="list" style="width:350px">' +
		  '<tr><th align="left" style="padding:2px">'+MSG_MESSAGE+'</th><th align="right" style="font-family:Tahoma,Verdana;padding:2px 4px 2px 2px">&nbsp;</th></tr>' +
		  '<tr><td colspan="2" align="center" style="padding:4px;background:#FFF0A0;border-top:1px solid #fff">'+msg+'</td></tr>' +
		  '<tr><td colspan="2" align="center" style="padding:0px 4px 4px 4px;background:#FFF0A0"><input type="button" value="'+MSG_BUTTON_CLOSE+'" onclick="hideMessage(this);" /></td></tr>' +
		  '</table>';
    else    
		e.msgDiv.innerHTML =
		  '<table border="0" cellpadding="0" cellspacing="0" class="list" style="width:350px">' +
		  '<tr><th align="left" style="padding:2px">'+MSG_MESSAGE+'</th><th align="right" style="font-family:Tahoma,Verdana;padding:2px 4px 2px 2px">&nbsp;</th></tr>' +
		  '<tr><td colspan="2" align="center" style="padding:4px;background:#FFF0A0;border-top:1px solid #fff"><textarea readonly="readonly" style="width:342px;height:125px;background-color:white;border:1px solid #A5ACB2;padding:2px;overflow:auto"></textarea></td></tr>' +
		  '<tr><td colspan="2" align="center" style="padding:0px 4px 4px 4px;background:#FFF0A0"><input type="button" value="'+MSG_BUTTON_CLOSE+'" onclick="hideMessage(this);" /></td></tr>' +
		  '</table>';
    
    document.body.insertBefore(e.msgDiv, document.body.childNodes[0]);    
  }
  
  stopMessage();
  
  // get x and y
  var el = e;
  var x = -350, y = el.offsetHeight - 3;
  while (el != null) {
    x += el.offsetLeft;
    y += el.offsetTop;
    el = el.offsetParent;
  }  
  
  e.msgDiv.style.left = x + 'px';
  e.msgDiv.style.top = y + 'px';  
  e.msgDiv.style.display = 'block';
  
  if (bEditable)
    e.msgDiv.hiddenInput = document.getElementById(idHidden);
  else  
    e.msgDiv.hiddenInput = null;
   
  if (!infoOnly)
  { 
	  e = e.msgDiv.childNodes[0].rows[1].cells[0].childNodes[0];

	  if (!e.isModified)  {
		e.value = msg; 
		e.isModified = true; 
	  }
	  e.readOnly = (!bEditable);
  }
 
}

function displayStartup(e) {  
  if (MSG_DisplayStartup != '') {
    var img = document.getElementById(MSG_DisplayStartup);
    if (img != null) {
      img = img.parentNode;
      if (img.click) img.click();
      else img.onclick();
      return;
    }
  }
  toggleMessage();
}

if (window.addEventListener) window.addEventListener('load', displayStartup, false);
else if (window.attachEvent) window.attachEvent('onload', displayStartup);


function uEscape(s) {
  if (typeof(encodeURIComponent) == 'function')
    return encodeURIComponent(s);
   else
    return escape(s);
}

function uUnescape(s) {
  if (typeof(decodeURIComponent) == 'function')
    return decodeURIComponent(s);
  else
    return unescape(s);
}

function XMLHttpAjaxCallback(x,callback) {

  this.x = x;
  this.callback = callback;

}

function XMLHttpAjax() {

  this._items = new Array();

  this._add = function(x,callback) {
    if (callback)
      this._items[this._items.length] = new XMLHttpAjaxCallback(x,callback);
  }
  
  this._callback = function(x,msg) {
    for(var i=0;i<this._items.length;i++)
      if (x == this._items[i].x)
        return this._items[i].callback(msg);
    return null;
  }


  this._init = function() {

    var A = null;
    var M = new Array(
      'Msxml2.XMLHTTP.5.0',
      'Msxml2.XMLHTTP.4.0',
      'Msxml2.XMLHTTP.3.0',
      'Msxml2.XMLHTTP',
      'Microsoft.XMLHTTP'
    );
    for (var i = 0; i < M.length; i++) {
      try { A = new ActiveXObject(M[i]); }
      catch (e) { A = null; }
      if (A != null) break;
    }

    if(!A && typeof XMLHttpRequest != 'undefined') A = new XMLHttpRequest();
    if (!A) return null;
    return A;
  }
  
  this._exec = function(uri,oncallback) {
  
    var x = this._init();
    if (x == null) return false;
    
    this._add(x, oncallback);
    
    x.onreadystatechange = function() {
      if (x.readyState != 4)
        return;
      var txt = x.responseText.replace(/^\s+|\s+$/g, '');
      if (txt == '') return;
      if (Ajax._callback(x, txt))
        x.onreadystatechange = null;
    }
    
    x.open('GET', uri, true);
    x.send('');
    
    return true;
  }
  
  this.execute = function(url,oncallback) { return this._exec(url.replace(/&amp;/g, '&'),oncallback); }
  
}

var Ajax = new XMLHttpAjax();

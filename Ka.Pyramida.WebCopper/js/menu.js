﻿
function yuki_Menu(parentId) {

  this._step = 0;

  this._parentId = parentId;
  this._parent = document.getElementById(this._parentId);
  this._parent._menu = this;
  this._parent.onmouseover = function(e) {
    this._menu.expand();
  }  
 
  this._tbl = document.createElement('TABLE');
  this._tbl.border = 0;
  this._tbl.cellPadding = 0;
  this._tbl.cellSpacing = 0;
  this._tbl.insertRow(0).insertCell(0);
  this._tbl.insertRow(0).insertCell(0);
  this._tbl.insertRow(0).insertCell(0);
  this._tbl.rows[0].cells[0].className = 'ymenu';
  this._tbl.rows[0].cells[0].innerHTML = '<div style="height:2px;overflow:hidden">&nbsp;</div>';
  this._tbl.rows[1].cells[0].className = 'ymenu';
  this._tbl.rows[1].cells[0].style.padding = '3px';
  this._tbl.rows[2].cells[0].innerHTML =
    '<table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%"><tr>'+
    '<td align="left" valign="bottom" style="width:4px;height:4px"><img src="images/menu.bl.png" /></td>' +
    '<td valign="bottom" style="background:#F3F3F3;width:99.9%"><div style="border-bottom:1px solid #C0C0C0;font-size:1px;line-height:1px">&nbsp;</div></td>' +
    '<td align="right" valign="bottom" style="width:4px;height:4px"><img src="images/menu.br.png" /></td>' +
    '</tr></table><div style="padding:0px 3px 0px 3px"><img src="images/bk.shadow.png" width="100%" height="1" /></div>';
  this._tblItems = document.createElement('TABLE');
  this._tblItems.border = 0;
  this._tblItems.cellPadding = 0;
  this._tblItems.cellSpacing = 0;
  this._tblItems.insertRow(0);
  this._tbl.rows[1].cells[0].appendChild(this._tblItems);

  this.add = function(title,img,link,tip) {
    var tr = this._tblItems.rows[0];
    if (tr.cells.length > 0) {
      var th = tr.insertCell(tr.cells.length);
      th.style.color = '#C0C0C0';
      th.innerHTML = '|';
      th.vAlign = 'bottom';
    }
    var td = tr.insertCell(tr.cells.length);
    td.style.padding = '0px 4px 0px 4px';
    td.valign = 'bottom';
    td.innerHTML = '<a href="'+link+'" title="'+tip+'" class="menu">'+title+' <img src="'+img+'" alt="'+tip+'" style="vertical-align:text-bottom" /></a>';
  }
  
  this.expand = function() {   
  
    if (this._tm != null) {
      window.clearTimeout(this._tm);
      this._tm = null;
    }
  
    if (document._active_ymenu != null && document._active_ymenu != this) {
      document._active_ymenu.collapse();
      document._active_ymenu = null;
    }    
    document._active_ymenu = this;
  
    if (this._div == null) {
      this._div = document.createElement('DIV');
      this._div.style.position = 'absolute';
      this._div.style.display = 'none';
      this._div.style.zIndex = 1027;
      this._div._menu = this;
      
      document.body.insertBefore(this._div, document.body.firstChild);
      this._div.appendChild(this._tbl);       
      
      this._div.onmouseover = function(e) {
        if (this._menu._tm != null) {
          window.clearTimeout(this._menu._tm);
          this._menu._tm = null;
        }      
      }
      
      this._div.onmouseout = function(e) {
        this._menu._tm = window.setTimeout('document.getElementById(\''+this._menu._parentId+'\')._menu.collapse();', 1500);
      }
    }
       
    // get x and y
    var e = this._parent;
    var x = -5, y = e.offsetHeight + 2;
    while (e != null) {
      x += e.offsetLeft;
      y += e.offsetTop;
      e = e.offsetParent;
    }    
    
    this._div.style.left = x + 'px';
    this._div.style.top = y + 'px';
    this._div.style.display = 'block';    
  }
  
  this.collapse = function() {
    if (this._div == null) return;
       
    this._div.style.display = 'none';
  }
  
}
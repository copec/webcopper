
String.prototype.match = function(r) {
  return r.test(this);
}
String.prototype.trim = function(r) {
  return this.replace(/^[\s]+|[\s+]$/g, '');
}
String.prototype.normalize = function() {
  var CC = 'ÄÁČĎÉĚÍĽŇÓŘŠŤÚŮÝáäčďéěíľňóřšťúůý';
  var NC = 'aacdeeilnorstuuyaacdeeilnorstuuy';
  var s = '';
  
  for(var i=0;i<this.length;i++) {
    c = this.charAt(i);
    for(var j=0;j<CC.length;j++)
      if (c == CC.charAt(j)) {
        c = NC.charAt(j);
        break;
      }
    s += c;
  }
  
  s = s.toLowerCase();
  s = s.replace(/^[\s]+|[\s]+$/g, '');
  
  return s;
}

var _is_opera = (navigator.userAgent.toLowerCase().indexOf('opera') != -1);

function StylusWebComboItem(id,code,title,desc) {
  desc = new String((desc==null?'':desc));
  
  this.id = id;
  this.code = code;
  this.title = title;
  this.desc = desc.substr(0,1).toUpperCase() + desc.substr(1);
  
  this._n_title = title.normalize();
  this._n_code = code.normalize();
}


function StylusWebCombo(div,myName) {

  this.items = new Array();
  this.selectWidth = 80;  
  this.itemSelected = null;
  this.itemSelectedIndex = -1;
  this.itemHilited = -1;
  this.isDisabled = false;
  
  // functions, that can be binded
  // onselect(StylusWebComboItem : item)
  this.onselect = null;
  this.onchange = null;
  
  // constructor
  this.StylusWebCombo = function(div,myName) {
    if (myName == null) myName = '';
    
    this.div = div;
    div.sw = this;
    
    var tb = document.createElement('TABLE');
    tb.cellPadding = 0;
    tb.cellSpacing = 0;
    tb.border = 0;
    tb.style.width = '40px';
    
    var tr = tb.insertRow(-1);
    var td = tr.insertCell(0);
    td.style.width = '99%';
    td.align = 'right';
    
    this._input = document.createElement('INPUT');
    this._input.type = 'text';
    this._input.className = 'sw-combo';
    this._input.sw = this;
    this._input.size = 1;
    td.appendChild(this._input);
   
    td = tr.insertCell(1);
    td.style.width = '1%';
    
    this._img = document.createElement('IMG');
    this._img.src = 'images/combo.normal.png';
    this._img.sw = this;
    td.appendChild(this._img);
    
    this._inputh = document.createElement('INPUT');
    this._inputh.type = 'hidden';
    this._inputh.name = myName;
    td.appendChild(this._inputh);
    
    div.appendChild(tb);
    div.onmouseover = function(e) {
      if (this.sw.isDisabled) return;
      this.sw.isMouseInTop = true;
      this.sw._input.className = 'sw-combo-active';
      this.sw._img.src = 'images/combo.active.png';
    }
    div.onmouseout = function(e) {
      if (this.sw.isDisabled) return;    
      this.sw.isMouseInTop = false;
      if (this.sw.isFocused || this.sw.expanded) return;
      this.sw._input.className = 'sw-combo';
      this.sw._img.src = 'images/combo.normal.png';
    }
    this._img.onclick = function(e) {
      if (this.sw.isDisabled) return;    
      if (!this.sw.expanded) {
        if (this.sw.onajaxdata && this.sw.items.length == 0) {
            this.sw.onajaxdata(this.sw, '');
        }
        this.sw.expand();
      } else {
        this.sw.collapse();
      }
    }
    this._input.onfocus = function(e) {
      if (this.sw.isDisabled) return;    
      this.sw.isFocused = true;
      this.sw._input.className = 'sw-combo-active';
      this.sw._img.src = 'images/combo.active.png';
    }
    this._input.onblur = function(e) {
      if (this.sw.isDisabled) return;    
      if (this.sw.onajaxdata) {
        this.sw._inputh.value = this.sw._input.value;
      }      
      this.sw.isFocused = false;
      if (this.sw.expanded) {
        if (!this.sw.isMouseInDiv)
          this.sw.collapse();
      }      
      if (this.sw.isMouseInTop) return;
      this.sw._input.className = 'sw-combo';
      this.sw._img.src = 'images/combo.normal.png';
    }
    this._input.onkeypress = function(e) {
      if (this.sw.isDisabled) return;    
      if (window.event && window.event.keyCode == 13) return false;
      else if (e && e.keyCode == 13) return false;
    }
    this._input.onkeyup = function(e) {
      if (this.sw.isDisabled) return;
      var keyCode = 0;
      if (window.event) keyCode = window.event.keyCode;
      else if(e&&e.keyCode) keyCode = e.keyCode;
      if (keyCode == 13) {
        this.sw.confirm();        
        return false;
      } else if (keyCode == 27) {
        this.sw.collapse();
        return;
      } else if (keyCode == 40) {
        if (!this.sw.expanded) this.sw.expand();
        else this.sw.hilite(this.sw.itemHilited + 1, true);
        return;
      } else if (keyCode == 38) {
        if (this.sw.expanded) this.sw.hilite(this.sw.itemHilited - 1, true);
        return;      
      } else if (keyCode == 33) {
        if (this.sw.expanded) this.sw.hilite(Math.max(0, this.sw.itemHilited - 15), true);        
        return;
      } else if (keyCode == 34) {
        if (!this.sw.expanded) this.sw.expand();
        else this.sw.hilite(Math.min(this.sw.idiv.tbl.rows.length - 1, this.sw.itemHilited + 15), true);
        return;
      }
      
      if (this.sw.onajaxdata) {        
        if (this.savedValue != this.value) {
          this.savedValue = this.value;
          this.sw.onajaxdata(this.sw, this.value);
          return;          
        }        
      }
      
      var v = this.value.normalize();
      for(var i=0;i<this.sw.items.length;i++)
        if (this.sw.items[i]._n_title.indexOf(v) == 0 || this.sw.items[i]._n_code.indexOf(v) == 0) {
          this.sw.hilite((v == '' ? this.itemHilited : i), true);
          return;
        }
    }
  }
  
  this.add = function(id,code,title,desc) {
    this.items[this.items.length] = new StylusWebComboItem(id,code,title,desc);
    if (this.items.length == 1) {
      this.itemSelected = this.items[0]; 
      this.itemSelectedIndex = 0;
      this._input.value = code;
      this._inputh.value = id;
      this.itemHilited = 0;
    }
    this._input.size = Math.max(this._input.size, new String(code).length);
    this._input.style.width = Math.round(this._input.size * 6) + 'px';
  }
  
  this.ajaxClear = function() {
    this.collapse();
    this.items = new Array();
    this.itemSelected = null;
    this.itemSelectedIndex = -1;
    this.itemHilited = -1;
    this.idivModified = true;
  }
  
  this.ajaxAdd = function(id,code,title,desc) {
    this.items[this.items.length] = new StylusWebComboItem(id,code,title,desc);
    if (this.items.length == 1) {
      this.itemSelected = this.items[0]; 
      this.itemSelectedIndex = 0;
      this.itemHilited = 0;
    }    
    this.idivModified = true;
  }
  
  this.setName = function(name) {
    this._inputh.name = name;
  }
  
  this.setDisabled = function(disabled) {
    if (this.isDisabled == disabled) return;
    this.isDisabled = disabled;  
    if (disabled) {
      this._img.savedSrc = this._img.src;
      this._img.src = 'images/combo.disabled.png';
      this._input.disabled = true;
      this._input.className = 'sw-combo-disabled';
    } else {
      this._img.src = this._img.savedSrc;
      this._input.disabled = false;
      this._input.className = 'sw-combo';
    }
  }
  
  this.expand = function(rebuildOnly) {
    if (document.__activeCombo == this) return;    
    else if (document.__activeCombo != null) {
      document.__activeCombo.collapse();
    }
      
    // get x and y
    var e = this.div;
    var x = 0, y = e.offsetHeight;
    while (e != null) {
      x += e.offsetLeft;
      y += e.offsetTop;
      e = e.offsetParent;
    }
    
    if (this.idiv == null) {
      this.idiv = document.createElement('DIV');
      this.idiv.style.position = 'absolute';
      this.idiv.style.border = '1px solid #C0C0C0';
      this.idiv.style.width = this.selectWidth + 'px';
      this.idiv.style.background = '#FFFFFF';      
      this.idiv.style.overflow = 'auto';
      this.idiv.style.overflowX = 'hidden';
      this.idiv.style.display = 'none';
      this.idiv.style.zIndex = 1021;
      this.idiv.sw = this;
          
      document.body.insertBefore(this.idiv, document.body.firstChild);     
    }
    
    if (this.idiv.tbl == null || this.idivModified) {
      this.idivModified = false;
      
      if (this.idiv.tbl == null) {
        this.idiv.tbl = document.createElement('TABLE');
        this.idiv.tbl.cellPadding = '0';
        this.idiv.tbl.cellSpacing = '0';
        this.idiv.tbl.border = '0';
        this.idiv.tbl.style.width = '100%';
        this.idiv.appendChild(this.idiv.tbl);
      }
      
      while (this.idiv.tbl.rows.length > 0) {
        this.idiv.tbl.deleteRow(0);
      }
      
      for (var i=0;i<this.items.length;i++) {
        var tr = this.idiv.tbl.insertRow(-1);
        tr.insertCell(0).innerHTML = this.items[i].code;
        tr.insertCell(1).innerHTML = this.items[i].title;
        tr.option = this.items[i];   
        tr.optionIndex = i;     
        tr.className = 'sw-combo-tr';
        tr.sw = this;
        tr.swIndex = i;
        tr.onmouseover = function(e) { this.sw.hilite(this.swIndex); }
        tr.onclick = function(e) { this.sw.select(this.option,this.optionIndex); this.sw.div.onmouseout(); }
      }   
    }   
    
    this.idiv.onmouseover = function(e) { this.sw.isMouseInDiv = true; }
    this.idiv.onmouseout = function(e) { this.sw.isMouseInDiv = false; }    
    
    if (this.idiv.tbl.rows.length < 1) return;
    if (rebuildOnly || this.isDisabled) return;
    
    // adjust size
    this.idiv.style.left = '0px';
    this.idiv.style.top = '0px';
    this.idiv.style.width = '4px';
    this.idiv.style.height = '4px';
    this.idiv.style.display = 'block';
    
    var cy = Math.min(this.idiv.tbl.offsetHeight, 240);
    this.idiv.style.overflow = (cy < 240 ? 'hidden' : 'auto');
    
    // adjust to screen height
    var scroll_cy = 0;
    if (self.innerHeight) scroll_cy = self.innerHeight;
    else if (document.documentElement&&document.documentElement.clientHeight) scroll_cy = document.documentElement.clientHeight;
    else if (document.body) scroll_cy = document.body.scrollHeight;
  
    var scroll_y = 0;
    if (self.pageYOffset) scroll_y = self.pageYOffset;
    else if (document.documentElement&&document.documentElement.scrollTop) scroll_y = document.documentElement.scrollTop;
    else if (document.body) scroll_y = document.body.scrollTop;
    
    if (y + cy > scroll_y + scroll_cy) y -= (cy + this.div.offsetHeight + 2);
    
    
    // set the sizes
    this.idiv.style.width = Math.max(this.div.offsetWidth, Math.round(this.idiv.tbl.offsetWidth + 32)) + 'px';
    this.idiv.style.height = cy + 'px';
    this.idiv.style.left = x + 'px';  
    this.idiv.style.top = y + 'px'; 
    
    document.__activeCombo = this;    
    this.expanded = true;
    
    this.hilite(this.itemHilited);
  }
  
  this.collapse = function() {
    this.expanded = false;  
    if (document.__activeCombo == this) document.__activeCombo = null;
    if (!this.idiv) return;
    this.idiv.style.display = 'none';
    if (this.tipDiv) this.tipDiv.style.display = 'none';
    if (this.itemSelected == null) return;
    if (!this.disableCheck) {
      this._input.value = this.itemSelected.code;
      this._inputh.value = this.itemSelected.id;
    }
  }
  
  this.hilite = function(index,fromKeyStroke) {
    if (!this.expanded) this.expand();
       
    var trToDiscard = null;
    var trToHilite = null;
    
    try { trToDiscard = this.idiv.tbl.rows[this.itemHilited]; } catch(e) {}
    try { trToHilite = this.idiv.tbl.rows[index]; } catch(e) {}
    
    if (trToHilite != null) {
      this.itemHilited = index;
      if (trToDiscard != null) trToDiscard.className = 'sw-combo-tr';
      trToHilite.className = 'sw-combo-tr-active';     
                
      // scroll into the view
      if (fromKeyStroke) {
                  
        var y = Math.max(0, trToHilite.offsetHeight * index);
               
        if (y < this.idiv.scrollTop) this.idiv.scrollTop = y;
        else if (y - this.idiv.scrollTop > this.idiv.offsetHeight - trToHilite.offsetHeight) {
          y = y - this.idiv.offsetHeight + trToHilite.offsetHeight + 2;
          this.idiv.scrollTop = y;
        }
      }
      
      this.displayTip(trToHilite, index);
    }    
  }
  
  this.displayTip = function(tr, index) {
    if (this.tipDiv == null) {
      this.tipDiv = document.createElement('DIV');
      this.tipDiv.style.position = 'absolute';
      this.tipDiv.style.border = '1px dashed #C0C0C0';
      this.tipDiv.style.background = '#FFFFE0';
      this.tipDiv.style.padding = '4px';
      this.tipDiv.style.zIndex = 1029;
      this.tipDiv.style.display = 'none';
      this.tipDiv.style.overflow = 'hidden';
      document.body.insertBefore(this.tipDiv, document.body.firstChild);
    }
       
    try {
      if (!this.items[index].desc) {
        this.tipDiv.style.display = 'none';
        return;
      }
    } catch(e) {
      this.tipDiv.style.display = 'none';
      return;
    }
        
    var x = tr.offsetWidth, y = 4 - this.idiv.scrollTop;
    while (tr != null) {
      x += tr.offsetLeft;
      y += tr.offsetTop;
      tr = tr.offsetParent;
    }
    
    if (y > this.idiv.offsetHeight + this.idiv.offsetTop) {
      this.tipDiv.style.display = 'none';
      return;    
    }
    
    this.tipDiv.style.left = x + 'px';
    this.tipDiv.style.top = y + 'px';
    this.tipDiv.innerHTML = '<table border="0" cellpadding="0" cellspacing="0"><tr><td align="left" valign="top" style="width:180px">' + this.items[index].desc + '</td></tr></table>';    
    this.tipDiv.style.display = 'block';
    
    this.tipDiv.style.height = Math.round(this.tipDiv.childNodes[0].offsetHeight) + 'px';
    this.tipDiv.style.width = Math.round(this.tipDiv.childNodes[0].offsetWidth + 4) + 'px';
  }
  
  this.confirm = function() {
    try {
      var item = this.items[this.itemHilited];
      if (item != null) this.select(item, this.itemHilited);
    } catch(e) {}    
  }
  
  this.select = function(item, itemIndex) {
    this.collapse();
    var changedItem = null;
    if (this.itemSelected != item) changedItem = item;    
    this.itemSelected = item;
    this.itemSelectedIndex = itemIndex;
    this._input.value = this.itemSelected.code;
    this._inputh.value = this.itemSelected.id;
    if (this.onselect) this.onselect(this, item);
    if (changedItem != null && this.onchange) this.onchange(this, item);
  }
  
  this.selectId = function(id) {
    this.expand(true);
    for(var i=0;i<this.items.length;i++)
      if (this.items[i].id == id) {
        this.select(this.items[i], i);
        return;
      }
  }
  
   
  this.StylusWebCombo(div,myName);
 
}

function __combo_click(e) {
  if (document.__activeCombo == null) return;  
  if (document.__activeCombo.isMouseInTop || document.__activeCombo.isMouseInDiv) return;
  var dc = document.__activeCombo;
  document.__activeCombo.collapse();
  dc.div.onmouseout();
}

// register common events
if (window.addEventListener) window.addEventListener('click', __combo_click, false);
else if (document.attachEvent) document.attachEvent('onclick', __combo_click);

﻿
function claim_SelectItem(tr,index,Aupd) {
  if (index > -1) {
    tr = tr.offsetParent.offsetParent.rows[index+2];
    return;
  }
  
  var cb = tr.cells[6].childNodes[0];
  var tbl = tr.offsetParent;
  var txt = tr.cells[5].childNodes[0];
  
  if (tr.className.substr(0, 2) == 'rs' && tr.checked == null)
    tr.checked = true;
  
  if (!tr.checked) {
    tr.className = 'rs';
    tr.h = 'rs';
    tr.checked = true;    
    cb.checked = true;
    txt.style.visibility = 'visible';
    if (Aupd) {
      document.getElementById('btn_claim_1').disabled = false;
      return;
    }
    txt.focus();
  } else if (!txt.active) {
    tr.h = (tr.rowIndex%2==0?'even':'odd');
    tr.className = tr.h;
    tr.checked = false;
    cb.checked = false;
    txt.style.visibility = 'hidden';
    tbl.rows[1].cells[6].childNodes[0].checked = false;
    if (Aupd) return;
  }
  
  for(var i=2;i<tbl.rows.length;i++) {
    if (tbl.rows[i].checked) {
      document.getElementById('btn_claim_1').disabled = false;
      return;
    }
  }
  
  document.getElementById('btn_claim_1').disabled = true;
  
}

function claim_SelectAll(cb) {
  var tbl = cb.offsetParent.offsetParent;
  for(var i=2;i<tbl.rows.length;i++) {
    if (cb.checked != tbl.rows[i].checked) claim_SelectItem(tbl.rows[i], -1, true);
  }
  document.getElementById('btn_claim_1').disabled = !cb.checked;
}
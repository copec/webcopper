var imgLoading = new Image();
imgLoading.src = 'images/loading.gif';
var ajaxTimeout = 0;
var protocolGroup = '';
var protocolNd = false;

var SERVER_TIMEOUT = 15000;
var SERVER_PATH = '' + document.location;
if (SERVER_PATH.toLowerCase().indexOf('protocol.edit.aspx') != -1)
  SERVER_PATH = 'protocol.edit.aspx';
else
  SERVER_PATH = 'protocol.add.aspx';
var IMG_QTY_TIP = 'IMG_QTY_TIP';  
var IMG_OBJ_TIP = 'IMG_OBJ_TIP';  
  

function __gwl_ajax(code, method, value) {
  switch (code) {
    case 'code.p':
    case 'code.g':
      switch (method) {
        case 'sort':
          protocol_SetCodeGroupSort(value);
          break;
        case 'page':  
          protocol_SetCodeGroupPage(value);
          break;        
        case 'size':
          protocol_SetCodeGroupSize(value);
          break;
        case 'close':
          protocol_CloseCode();
          break;  
        case 'groupId':
          protocol_SetCodeGroup(value);
          break;
      }
      break;
  }
}

function protocol_SetCode() {
  var b = document.getElementById('MP_ProtoBtn_Code');
  b.blur();  
  b.disabled = true;  
  
  var d = document.getElementById('MP_ProtoDiv_Issue');
  d.cancelled = false;  
  d.innerHTML = '<div align="center" style="padding:64px"><img src="images/loading.gif" /></div>';  
  ajaxTimeout = window.setTimeout('protocol_CancelCode();', SERVER_TIMEOUT);  
  
  var txt = document.getElementById('MP_ProtoBtn_Text').value.substr(0,2);
  
  protocolNd = false;
  Ajax.execute(SERVER_PATH+'?ajax=code&groupId='+txt, protocol_UpdateCode);
}

function protocol_CancelCode() {
  var d = document.getElementById('MP_ProtoDiv_Issue');
  d.cancelled = true;
  d.innerHTML = '<div align="center" style="padding:64px">CONNECTION_LOST</div>';
}

function protocol_UpdateCode(msg) {
  window.clearTimeout(ajaxTimeout);
  ajaxTimeout = 0;
    
  var d = document.getElementById('MP_ProtoDiv_Issue');
  if (!d.cancelled) {
    d.innerHTML = msg;
    protocol_ValidateStep21();
  }
}

function protocol_SetCodeGroup(id) {
  var d = document.getElementById('Code.Ajax.cs_PreloadingDiv');  
  d.innerHTML = '<div style="height:'+d.offsetHeight+'px"><div align="center" style="padding:64px"><img src="images/loading.gif" /></div></div>';
  ajaxTimeout = window.setTimeout('protocol_CancelCode();', SERVER_TIMEOUT);
  Ajax.execute(SERVER_PATH+'?ajax='+(protocolNd?'nd':'code')+'&groupId='+id, protocol_UpdateCode);
}

function protocol_SetCodeGroupSort(sort) {
  ajaxTimeout = window.setTimeout('protocol_CancelCode();', SERVER_TIMEOUT);
  Ajax.execute(SERVER_PATH+'?ajax='+(protocolNd?'nd':'code')+'&sort='+sort, protocol_UpdateCode);
}

function protocol_SetCodeGroupPage(page) {
  ajaxTimeout = window.setTimeout('protocol_CancelCode();', SERVER_TIMEOUT);
  Ajax.execute(SERVER_PATH+'?ajax='+(protocolNd?'nd':'code')+'&page='+page, protocol_UpdateCode);
}

function protocol_SetCodeGroupSize(size) {
  ajaxTimeout = window.setTimeout('protocol_CancelCode();', SERVER_TIMEOUT);
  Ajax.execute(SERVER_PATH+'?ajax='+(protocolNd?'nd':'code')+'&size='+size, protocol_UpdateCode);
}

function protocol_CloseCode(update) {
  document.getElementById('MP_ProtoBtn_Code').disabled = false;
  
  var d = document.getElementById('MP_ProtoDiv_Issue');
  d.innerHTML = '<div align="center" style="padding:64px"><img src="images/loading.gif" /></div>';  
  ajaxTimeout = window.setTimeout('protocol_CancelCode();', SERVER_TIMEOUT);  
  
  Ajax.execute(SERVER_PATH+'?ajax=issue&setcode='+(update?document.getElementById('MP_ProtoBtn_Text').value:''), protocol_UpdateCode);
}

function positionNdImage(div,e) {

  if (e == null) e = window.event;
  if (e == null) return;

  var mx = (e.pageX ? e.pageX : e.clientX + document.body.scrollLeft);
  var my = (e.pageY ? e.pageY : e.clientY + document.body.scrollTop);
  
  if (document.all && e.offsetY) {
    mx = e.offsetX;
    my = e.offsetY;
  } else {
    var x = 0, y = 0;
    e = div;
    while (e != null) {
      x += e.offsetLeft;
      y += e.offsetTop;
      e = e.offsetParent;
    }
    mx -= x;
    my -= y;
  }
     
  var rozdielX = (div.cx+16) - div.offsetWidth;
  var rozdielY = (div.cy+16) - div.offsetHeight;
  var posX = Math.round(0 - Math.max(0, rozdielX * mx / div.offsetWidth)) + 8;
  var posY = Math.round(0 - Math.max(0, rozdielY * my / div.offsetHeight)) + 8;
  
 
  div.style.backgroundPositionX = posX + 'px';
  div.style.backgroundPositionY = posY + 'px';
  div.style.backgroundPosition = posX + 'px ' + posY + 'px';
}

function protocol_SetPos(b) {
  b.blur();  
  b.disabled = true;
  
  var d = document.getElementById('MP_ProtoDiv_Issue');
  d.innerHTML = '<div align="center" style="padding:64px"><img src="images/loading.gif" /></div>';
    
  ajaxTimeout = window.setTimeout('protocol_CancelCode();', SERVER_TIMEOUT);    
  Ajax.execute(SERVER_PATH+'?ajax=pos', protocol_UpdateCode);  
}

function protocol_SelectPos(tr,volna) {
  var tbl = tr.offsetParent;
  if (tbl.selectedItem != null && tbl.selectedItem != tr) {
    tbl.selectedItem.h = tbl.selectedItem.sh;
    tbl.selectedItem.className = tbl.selectedItem.sh;
  }
  
  if (tbl.selectedItem == tr) {
    if (tr.cid != null && !volna)
      protocol_SubmitPos();
    return;
  }
  
  tr.sh = tr.h;
  tr.className = 'rs';
  tr.h = 'rs';
  tbl.selectedItem = tr;  

  document.getElementById('t_proto_posf_name').disabled = (!volna);  
  document.getElementById('t_proto_posf_op').disabled = (!volna);
  document.getElementById('t_proto_posf_cj').disabled = (!volna);
  
  if (volna) {
    document.getElementById('t_proto_posf_name').focus();
    protocol_ValidatePosCustom();
  } else
    document.getElementById('t_proto_posf_btnok').disabled = false;
}

function protocol_ValidatePosCustom() {
  var b = document.getElementById('t_proto_posf_btnok');
  var name = document.getElementById('t_proto_posf_name');
  var op = document.getElementById('t_proto_posf_op');
  var cj = document.getElementById('t_proto_posf_cj');
  
  var isValid = true;
  if (name.value.replace(/^\s+|\s+$/,'')=='') isValid = false;
  else if (op.value.replace(/^\s+|\s+$/,'')=='') isValid = false;
  else if (!cj.value.match(/^[0-9]+$/)) isValid = false;
  
  b.disabled = (!isValid);  
}

function protocol_SubmitPos() { 

  var url = '&addpos=';
  var name = document.getElementById('t_proto_posf_name');
  if (name.disabled) {
    var tbl = document.getElementById('t_proto_posf_tbl');
    url += '1&id=' + tbl.selectedItem.cid;
  } else {
    var op = uEscape(document.getElementById('t_proto_posf_op').value);
    var cj = uEscape(document.getElementById('t_proto_posf_cj').value);
    url += '2&name=' + uEscape(name.value) + '&op=' + op + '&cj=' + cj;
  }
  
  var d = document.getElementById('MP_ProtoDiv_Issue');
  d.innerHTML = '<div align="center" style="padding:64px"><img src="images/loading.gif" /></div>';  
  ajaxTimeout = window.setTimeout('protocol_CancelCode();', SERVER_TIMEOUT);    
  
  Ajax.execute(SERVER_PATH+'?ajax=issue'+url, protocol_UpdateCode);
}

function protocol_CancelPos() {
  var d = document.getElementById('MP_ProtoDiv_Issue');
  d.innerHTML = '<div align="center" style="padding:64px"><img src="images/loading.gif" /></div>';  
  ajaxTimeout = window.setTimeout('protocol_CancelCode();', SERVER_TIMEOUT);  
  
  Ajax.execute(SERVER_PATH+'?ajax=issue', protocol_UpdateCode);
}

// nahradni dil
function protocol_SetNd() {
  var d = document.getElementById('MP_ProtoDiv_Issue');
  d.innerHTML = '<div align="center" style="padding:64px"><img src="images/loading.gif" /></div>';  
  ajaxTimeout = window.setTimeout('protocol_CancelCode();', SERVER_TIMEOUT);  
  
  var txt = document.getElementById('MP_ProtoBtn_Text').value.substr(0,2); 
  protocolNd = true; 
  Ajax.execute(SERVER_PATH+'?ajax=nd&groupId='+txt, protocol_UpdateCode);
}

function protocol_CloseNd(nd,qty,obj) {
  var d = document.getElementById('MP_ProtoDiv_Issue');
  d.innerHTML = '<div align="center" style="padding:64px"><img src="images/loading.gif" /></div>';  
  ajaxTimeout = window.setTimeout('protocol_CancelCode();', SERVER_TIMEOUT);
  
  if (qty == null || qty < 1) qty = 1;
  
  Ajax.execute(SERVER_PATH+'?ajax=issue&addnd='+nd+','+qty+','+obj, protocol_UpdateCode);
}

function protocol_SetCodeValue2(tr,idPolozka) {

  var tbl = tr.offsetParent;
  if (tbl.selectedItem != null && tbl.selectedItem != tr) {
    if (protocolNd) {
      tbl.selectedItem.txt.style.visibility = 'hidden';
      tbl.selectedItem.qty.style.visibility = 'hidden';
    }
    tbl.selectedItem.h = tbl.selectedItem.sh;
    tbl.selectedItem.className = tbl.selectedItem.sh;
  }
  
  if (tbl.selectedItem == tr) {
    if (!protocolNd) protocol_SubmitCode2();
    return;
  }
  
  tr.sh = tr.h;
  tr.className = 'rs';
  tr.h = 'rs';
  tbl.selectedItem = tr;   
  
  if (protocolNd) {
    tr.qty =  tr.cells[5].childNodes[0];
    tr.qty.style.visibility = 'visible';
    tr.qty.disabled = false;
    tr.qty.validate = function() {
      if (!this.value.match(/^[0-9]+$/)) return false;
      if (parseInt(this.value, 10) > this.maxValue) return false;
      return true;    
    }
    tr.qty.onkeyup = function(e) {    
      var keyCode = (window.event?window.event.keyCode:e.keyCode);
      this.bOk.disabled = (!this.txt.validate());
      if (!this.bOk.disabled && keyCode == 13)
        protocol_SubmitCode2();
    }
    tr.qty.onchange = function(e) {
      this.bOk.disabled = (!this.txt.validate());
    }    
    tr.txt = tr.cells[6].childNodes[0];          
    tr.txt.style.visibility = 'visible';
    tr.txt.disabled = false;
    tr.txt.qty = tr.qty;
    tr.qty.txt = tr.txt;
    tr.txt.validate = function() {
      if (!this.qty.validate()) return false;
      if (!this.value.match(/^[0-9]+$/)) return false;
      var val = parseInt(this.value, 10);
      if (val > this.maxValue || val > parseInt(this.qty.value, 10)) return false;
      return true;
    }
    tr.txt.onkeyup = function(e) {    
      var keyCode = (window.event?window.event.keyCode:e.keyCode);
      this.bOk.disabled = (!this.validate());
      if (!this.bOk.disabled && keyCode == 13)
        protocol_SubmitCode2();
    }
    tr.txt.onchange = function(e) {
      this.bOk.disabled = (!this.validate());
    }
    tr.txt.focus();
  }
   
  var b = document.getElementById('t_proto_codef_btnok');
  b.code = idPolozka;
  b.code2 = tr.cells[1].innerHTML;
  if (protocolNd) {
    b.obj = tr.txt;
    b.qty = tr.qty;
    b.disabled = (!tr.txt.validate());
    tr.txt.bOk = b;
    tr.qty.bOk = b;
  } else
    b.disabled = false;
}

function protocol_SubmitCode2() {
  var b = document.getElementById('t_proto_codef_btnok');
  if (protocolNd) {
    protocol_CloseNd(b.code, b.qty.value, b.obj.value);
  } else {
    document.getElementById('MP_ProtoBtn_Text').value = b.code2;
    protocol_CloseCode(true);
  }  
}

function protocol_SetPosForDeletion(tr) {
  var tbl = tr.offsetParent;
  if (tbl.selectedItem != null && tbl.selectedItem != tr) {
    tbl.selectedItem.h = tbl.selectedItem.sh;
    tbl.selectedItem.className = tbl.selectedItem.sh;
  }
  
  if (tbl.selectedItem != null && tbl.selectedItem == tr) {
    tr.btnDel.style.visibility = 'hidden';
    tbl.selectedItem.h = tbl.selectedItem.sh;
    tbl.selectedItem.className = tbl.selectedItem.sh;    
    tbl.selectedItem = null;    
    return;
  }
  
  if (tr.h == null) tr.h = (tr.rowIndex%2==0?'even':'odd'); 
  tr.sh = tr.h;
  tr.className = 'rs';
  tr.h = 'rs';
  tbl.selectedItem = tr;  
  
  var b = document.getElementById('btn_posf_delete_p');
  b.style.visibility = 'visible';
  b.delIndex = (tr.rowIndex - 2);
  tr.btnDel = b;
}

function protocol_DeletePos(b) {
  var b = document.getElementById('btn_posf_delete_p');
  var d = document.getElementById('MP_ProtoDiv_Issue');
  d.innerHTML = '<div align="center" style="padding:64px"><img src="images/loading.gif" /></div>';  
  ajaxTimeout = window.setTimeout('protocol_CancelCode();', SERVER_TIMEOUT);
  
  Ajax.execute(SERVER_PATH+'?ajax=issue&delpos='+b.delIndex, protocol_UpdateCode);
}

function protocol_SetNdForDeletion(tr) {
  var tbl = tr.offsetParent;
  if (tbl.selectedItem != null && tbl.selectedItem != tr) {
    tbl.selectedItem.h = tbl.selectedItem.sh;
    tbl.selectedItem.className = tbl.selectedItem.sh;
  }
  
  if (tbl.selectedItem != null && tbl.selectedItem == tr) {
    tr.btnDel.style.visibility = 'hidden';
    tbl.selectedItem.h = tbl.selectedItem.sh;
    tbl.selectedItem.className = tbl.selectedItem.sh;    
    tbl.selectedItem = null;    
    return;
  }
  
  if (tr.h == null) tr.h = (tr.rowIndex%2==0?'even':'odd');
  tr.sh = tr.h;
  tr.className = 'rs';
  tr.h = 'rs';
  tbl.selectedItem = tr;  
  
  var b = document.getElementById('btn_ndf_delete_p');
  b.style.visibility = 'visible';
  b.delIndex = (tr.rowIndex - 2);
  tr.btnDel = b;
}

function protocol_DeleteNd(b) {
  var b = document.getElementById('btn_ndf_delete_p');
  var d = document.getElementById('MP_ProtoDiv_Issue');
  d.innerHTML = '<div align="center" style="padding:64px"><img src="images/loading.gif" /></div>';  
  ajaxTimeout = window.setTimeout('protocol_CancelCode();', SERVER_TIMEOUT);
  
  Ajax.execute(SERVER_PATH+'?ajax=issue&delnd='+b.delIndex, protocol_UpdateCode);
}

function protocol_ValidateStep21(bPrepare) {
  var b = document.getElementById('btn_submit_zavada_p');
  var pozice = document.getElementById('tbl_21_pozice_t');
  var nd = document.getElementById('tbl_21_nd_t');
  var nazev = document.getElementById('txt_21_nazev_t');
  var popis = document.getElementById('txt_21_popis_t');
   
  if (b == null || pozice == null || nd == null || nazev == null || popis == null) return false;
  
  if (bPrepare) {
    if (nd.rows.length > 3)      
      protocol_SetNdForDeletion(nd.rows[2]);
    if (pozice.rows.length > 3)
      protocol_SetPosForDeletion(pozice.rows[2]);
  }
   
  if (pozice.rows.length > 3)
    b.disabled = false;
  else
    b.disabled = true;
    
  return (!b.disabled);
}

function protocol_SetZavadaForDeletion(tr) {    
  var tbl = tr.offsetParent;
  if (tbl.selectedItem != null && tbl.selectedItem != tr) {
    tbl.selectedItem.h = tbl.selectedItem.sh;
    tbl.selectedItem.className = tbl.selectedItem.sh;
  }
  
  if (tbl.selectedItem != null && tbl.selectedItem == tr) {
    var b = document.getElementById('tdb_2_button_e');
    protocol_EditZavada(b);
    return;

    tr.btnDel.style.visibility = 'hidden';
    tbl.selectedItem.h = tbl.selectedItem.sh;
    tbl.selectedItem.className = tbl.selectedItem.sh;
    tbl.selectedItem = null;
    return;
  }
  
  tr.sh = tr.h;
  tr.className = 'rs';
  tr.h = 'rs';
  tbl.selectedItem = tr;
  
  var b = document.getElementById('tdb_2_button_d');
  b.style.visibility = 'visible';
  b.delIndex = (tr.rowIndex - 2);
  tr.btnDel = b;
  
  b = document.getElementById('tdb_2_button_e');
  b.style.visibility = 'visible';
  b.editIndex = (tr.rowIndex - 2);
  tr.btnEdit = b;    
}

function protocol_DeleteZavada(b) {
  b.form.action = SERVER_PATH+'?deli=' + b.delIndex;
  b.form.submit();
}

function protocol_EditZavada(b) {
  b.form.action = SERVER_PATH+'?editi=' + b.editIndex;
  b.form.submit();
}

var __qty_tip_div = null;
function protocol_DisplayQtyTip(e,idx) {
  // create element and display at specific pos
  if (__qty_tip_div == null) {      
    __qty_tip_div = document.createElement('DIV');
    __qty_tip_div.style.position = 'absolute';
    __qty_tip_div.style.zIndex = 4678;
    __qty_tip_div.style.display = 'none';
    __qty_tip_div.style.backgroundColor = '#FFFFE2';
    __qty_tip_div.style.border = '1px solid #404040';
    __qty_tip_div.style.padding = '4px';
    document.body.appendChild(__qty_tip_div);    
  }
  
  if (!e.onmouseout) {
   e.onmouseout = function(e) {
     if (__qty_tip_div != null) __qty_tip_div.style.display = 'none';
   }
  }
  
  switch (idx) {
    case 1:
      __qty_tip_div.innerHTML = '<img src="images/darr.png" width="8" height="9" alt="" style="vertical-align:middle" /> ' + IMG_QTY_TIP;
      break;
    case 2:
      __qty_tip_div.innerHTML = '<img src="images/darr.png" width="8" height="9" alt="" style="vertical-align:middle" /> ' + IMG_OBJ_TIP;
      break;      
  }
  __qty_tip_div.style.display = 'block';
  
  var x = 2, y = 0;
  while (e != null) {
    x += e.offsetLeft;
    y += e.offsetTop;
    e = e.offsetParent;
  }
  
  y -= __qty_tip_div.offsetHeight;

  __qty_tip_div.style.left = x+'px';
  __qty_tip_div.style.top = y+'px';
}
﻿
var ajaxTimeout = 0;
var SERVER_TIMEOUT = 15000;

var imgLoading = new Image();
imgLoading.src = 'images/loading.gif';

function pdetail_DisplayIssue(id,cislo) {
  var d = document.getElementById('MP_ProtoDiv_Issue');
  d.cancelled = false;
  d.innerHTML = '<div align="center" style="padding:64px"><img src="images/loading.gif" /></div>';  
  ajaxTimeout = window.setTimeout('pdetail_CancelCode();', SERVER_TIMEOUT);  
  Ajax.execute('protocol.detail.aspx?ajax=issue\x26id='+id+'\x26cislo='+cislo, pdetail_Update);
}

function pdetail_Update(msg) {
  window.clearTimeout(ajaxTimeout);
  var d = document.getElementById('MP_ProtoDiv_Issue');  
  if (d.cancelled) return;
  d.innerHTML = msg;
}

function pdetail_CancelCode() {
  var d = document.getElementById('MP_ProtoDiv_Issue');
  d.cancelled = true;
  d.innerHTML = '<div align="center" style="padding:64px">CONNECTION_LOST</div>';
}

function pdetail_DisplayRoot() {
  document.location = '' + document.location;
}
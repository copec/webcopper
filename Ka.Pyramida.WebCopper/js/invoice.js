﻿
function invoice_MouseIn(cb) {
  cb.mouseIn = true;
  if (cb.onmouseout == null) 
    cb.onmouseout = function(e) {
      this.mouseIn = false;
    }
}

function invoice_SelectProtocol(tr,cb,cbIdx) {
  var tbl = (tr != null ? tr.offsetParent : cb.offsetParent.offsetParent);
   
  if (tr == null) {    
    tr = tbl.rows[cbIdx+2];
    cb.checked = !cb.checked;
  } else {
    cbIdx = tr.rowIndex - 2;
    cb = tr.cells[4].childNodes[0];
    //if (!document.all || !cb.mouseIn)
      cb.checked = !cb.checked;
  }
  
  if (cb.checked) {
    tr.className = 'rs';
    tr.h = 'rs';
  } else {
    tr.h = (tr.rowIndex%2==0?'even':'odd');
    tr.className = tr.h;
    tr.offsetParent.rows[1].cells[4].childNodes[0].checked = false;
  }
  
  var disabled = true;
  for(var i=2;i<tbl.rows.length;i++) {
    if (tbl.rows[i].cells[4].childNodes[0].checked) {
      disabled = false;
      break;
    }
  }
  
  document.getElementById('btn_invoice_1').disabled = disabled;  
}

function invoice_SelectAll(cb) {
 
  var tbl = cb.offsetParent.offsetParent;
  for(var i=2;i<tbl.rows.length;i++) {
    try {
      tbl.rows[i].cells[4].childNodes[0].checked = !cb.checked;
      invoice_SelectProtocol(tbl.rows[i]);
    } catch (e) { }
  }
}
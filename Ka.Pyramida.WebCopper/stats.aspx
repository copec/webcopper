﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="_stats" Codebehind="stats.aspx.cs" %>
<%@ Register TagPrefix="yuki" TagName="header" Src="Controls/Header.ascx" %>
<%@ Register TagPrefix="yuki" TagName="StatsBikes" Src="Controls/StatsBikes.ascx" %>
<%@ Register TagPrefix="yuki" TagName="StatsVIN" Src="Controls/StatsVIN.ascx" %>
<%@ Register TagPrefix="yuki" TagName="StatsModel" Src="Controls/StatsModel.ascx" %>
<%@ Register TagPrefix="yuki" Namespace="Ka.Garance.Web.List" Assembly="garance" %>
<%@ Register TagPrefix="yuki" Namespace="Ka.Garance.Web.TabList" Assembly="garance" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<%=Language.Code%>" dir="ltr" lang="<%=Language.Code%>">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><%=Language["APP_TITLE"]%></title>
<link rel="stylesheet" type="text/css" href="css/yuki.css" />
</head>

<body>

<yuki:header Id="MCHeader" LocationID="LOCATION_INVOICES" runat="server" />

<div align="center" style="padding:0px 16px 16px 16px"> 
<table border="0" cellpadding="0" cellspacing="0" align="center" style="width:1%">
<tr>
 <td align="left">
<div style="width:800px">

<img src="images/mod.stats.png" alt="" style="vertical-align:text-bottom" />
<span class="mod"><%=Language["HEADER_STATISTICS"]%></span><br /><br />

<yuki:TabList ID="MCFilterTabs" runat="server">
 <yuki:TabItem ID="TabItem0" LangID="STATS_OVERAL_DEALER" Link="stats.aspx?state=0" runat="server" />
 <yuki:TabItem ID="TabItem1" LangID="STATS_OVERAL_MODEL" Link="stats.aspx?state=1" runat="server" />
 <yuki:TabItem ID="TabItem2" LangID="STATS_VIN" Link="stats.aspx?state=2" runat="server" />
</yuki:TabList>
<yuki:StatsBikes ID="WSBikes" runat="server" />
<yuki:StatsModel ID="WSModels" runat="server" Visible="false" />
<yuki:StatsVIN ID="WSVin" Visible="false" runat="server" />
</div>
 </td>
</tr>
</table>
</div>

</body>

</html>
ALTER TABLE tdealer ADD rokfa int not null default 0;

UPDATE tdealer SET rokfa=2008;
UPDATE tdealer SET cislofa=1;


ALTER TABLE tmotocykl ADD majitel_jmeno varchar(50);
ALTER TABLE tmotocykl ADD majitel_ico varchar(40);
ALTER TABLE tmotocykl ADD majitel_ulice varchar(50);
ALTER TABLE tmotocykl ADD majitel_psc varchar(6);
ALTER TABLE tmotocykl ADD majitel_obec varchar(50);
ALTER TABLE tmotocykl ADD majitel_fo tinyint(1) NOT NULL default '0';

ALTER TABLE tprotocol ADD ico varchar(40) AFTER `jmeno`;
ALTER TABLE tprotocol ADD jeFO tinyint(1) NOT NULL default '0' AFTER `jmeno`;

-- 14.4.2009
-- partgroup = 1 = motor, partgroup = 2 = ram. 
ALTER TABLE tcskodnicisla ADD partgroup smallint;

ALTER TABLE tcmodelklic ADD vzormotor varchar(32);
ALTER TABLE tcmodelklic ADD vzorram varchar(32);

ALTER TABLE `tzavada`
	MODIFY COLUMN `prace` DOUBLE(13,2) NOT NULL DEFAULT 0,
	MODIFY COLUMN `material` DOUBLE(13,2) NOT NULL DEFAULT 0;
	
ALTER TABLE `tzdil` MODIFY COLUMN `cena` DOUBLE(13,2) NOT NULL DEFAULT 0;
ALTER TABLE `tzpozice` MODIFY COLUMN `cena` DOUBLE(13,2) NOT NULL DEFAULT 0;
ALTER TABLE `tzpozicevolna` MODIFY COLUMN `cena` DOUBLE(13,2) NOT NULL DEFAULT 0;
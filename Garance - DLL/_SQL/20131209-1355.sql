﻿--
-- WCP-35
--
-- Logovanie problemu s generovanim cisiel faktur

CREATE TABLE `error_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `iddealer` int(11) DEFAULT NULL,
  `message` text,
  PRIMARY KEY (`id`)
);



using System;
using System.Collections;
using System.Text;
using Ka.Garance.Data;

namespace Ka.Garance.Protocol.Yuki
{
	public class YukiProtocolZavadaList
	{
		private int m_SelectedIndex = -1;
		private ArrayList m_Items = new ArrayList();
		private YukiProtocol m_Protocol;

		public YukiProtocolZavadaList(YukiProtocol protocol)
		{
			m_Protocol = protocol;
		}

		public YukiProtocolZavada this[int index]
		{
			get { return m_Items[index] as YukiProtocolZavada; }
		}

		public int Count
		{
			get { return m_Items.Count; }
		}

		public int SelectedIndex
		{
			get { return m_SelectedIndex; }
			set { m_SelectedIndex = Math.Max(-1, Math.Min(Count - 1, value)); }
		}

		public YukiProtocolZavada Add(string kod, bool schvaleno)
		{
			YukiProtocolZavada ppz = new YukiProtocolZavada(m_Protocol, kod, schvaleno);
			m_Items.Add(ppz);
			return ppz;
		}

		public YukiProtocolZavada Add(int id, string kod, string dz, string vyrobce, string nazev, string popis, bool schvaleno)
		{
			YukiProtocolZavada z = Add(kod, schvaleno);
			z.m_Id = id;
			z.DZ = dz;
			z.Vyrobce = vyrobce;
			z.Nazev = nazev;
			z.Popis = popis;
			return z;
		}

		public YukiProtocolZavada GetById(int id)
		{
			for (int i = 0; i < this.Count; i++)
				if (this[i].Id == id)
					return this[i];
			return null;
		}

		public void Delete(int index)
		{
			m_Items.RemoveAt(index);
		}

		public void Clear()
		{
			m_Items.Clear();
		}
	}
}

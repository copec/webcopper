using System;
using System.Collections;
using System.Text;
using Ka.Garance.Data;

namespace Ka.Garance.Protocol.Yuki
{
	public class YukiProtocolNDList
	{
		private ArrayList m_Items = new ArrayList();
		private YukiProtocol m_Protocol;

		public YukiProtocolNDList(YukiProtocol protocol)
		{
			m_Protocol = protocol;
		}

		public int Count
		{
			get { return m_Items.Count; }
		}

		public YukiProtocolND this[int index]
		{
			get { return m_Items[index] as YukiProtocolND; }
		}

		public YukiProtocolND Add(int id, int mnozstvi, int objednat, float savedCena, bool schvaleno)
		{
			foreach (YukiProtocolND nd in m_Items)
			{
				if (nd.Id == id)
				{
					nd.Mnozstvi = mnozstvi;
					nd.Objednat = objednat;
					return nd;
				}
			}

			YukiProtocolND p = new YukiProtocolND(m_Protocol, id, mnozstvi, objednat, savedCena, schvaleno);
			m_Items.Add(p);
			return p;
		}

		public void Delete(int index)
		{
			m_Items.RemoveAt(index);
		}

		public void Clear()
		{
			m_Items.Clear();
		}

	}
}

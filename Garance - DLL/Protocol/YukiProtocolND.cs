using System;
using System.Collections;
using System.Text;
using Ka.Garance.Data;

namespace Ka.Garance.Protocol.Yuki
{
	// nahradne diely
	public class YukiProtocolND
	{
		private int m_Mnozstvi;
		private int m_Objednat;

		private int m_Id;
		private string m_SklCislo;
		private float m_SavedCena;
		private bool m_Schvaleno;

		private YukiProtocol m_Protocol;

		private float m_CenaJednotka;

		public YukiProtocolND(YukiProtocol protocol, int id, int mnozstvi, int objednat, float savedCena, bool schvaleno)
		{
			m_Protocol = protocol;
			m_Id = id;
			m_Mnozstvi = mnozstvi;
			m_Objednat = objednat;
			m_SavedCena = savedCena;
			m_Schvaleno = schvaleno;

			object cena = m_Protocol.Database.ExecuteScalar("SELECT A.NM AS cena FROM ndcenik A LEFT JOIN tcskodnicisla B ON B.sklcislo=A.SklCislo WHERE A.idzeme={0} AND B.id={1} ORDER BY A.NM DESC", m_Protocol.User.IdZeme, this.Id);
			if (cena == null || cena is DBNull) m_CenaJednotka = 0.0F;
			else m_CenaJednotka = (float)Math.Round(Convert.ToDouble(cena));

			// FIX: price changes
			if (protocol.Stav == 0)
				m_SavedCena = m_CenaJednotka;

			m_SklCislo = Convert.ToString(m_Protocol.Database.ExecuteScalar("SELECT sklcislo FROM tcskodnicisla WHERE id={0}", m_Id));
		}

		public int Id
		{
			get { return m_Id; }
		}

		public string Kod
		{
			get
			{
				return Convert.ToString(m_Protocol.Database.ExecuteScalar("SELECT CONCAT(skupina,'.',pozice) AS kod FROM tcskodnicisla WHERE id={0}", m_Id));
			}
		}

		public string KodSkupina
		{
			get
			{
				return Convert.ToString(m_Protocol.Database.ExecuteScalar("SELECT skupina AS kod FROM tcskodnicisla WHERE id={0}", m_Id));
			}
		}

		public string KodPozice
		{
			get
			{
				return Convert.ToString(m_Protocol.Database.ExecuteScalar("SELECT pozice AS kod FROM tcskodnicisla WHERE id={0}", m_Id));
			}
		}

		public string SklCislo
		{
			get { return m_SklCislo; }
		}

		public string Nazev
		{
			get
			{
				return Convert.ToString(m_Protocol.Database.ExecuteScalar(string.Format("SELECT nazevdilu{0} FROM tcskodnicisla WHERE id={1}", (m_Protocol.User.Language.UseEnglishNames ? "_en" : ""), this.Id)));
			}
		}

		public bool Schvaleno
		{
			get { return m_Schvaleno; }
		}

		public int Mnozstvi
		{
			get { return m_Mnozstvi; }
			set { m_Mnozstvi = value; }
		}

		public int Objednat
		{
			get { return m_Objednat; }
			set { m_Objednat = value; }
		}

		public float CenaJednotka
		{
			get { return m_CenaJednotka; }
		}

		public float CenaJednotkaDPH
		{
			get
			{
				return CenaJednotka * m_Protocol.DPHRabatConst;
			}
		}

		public float Cena
		{
			get { return (float)Math.Round(CenaJednotka * (float)m_Mnozstvi, 1); }
		}

		public float CenaDPH
		{
			get { return (float)Math.Round(CenaJednotka * (float)m_Mnozstvi * m_Protocol.DPHRabatConst, 1); }
		}

		public float SavedCenaDPH
		{
			get { return (float)Math.Round(m_SavedCena * m_Protocol.DPHRabatConst, 1); }
		}

		internal void Save(int idzavadadealer)
		{
			double cenaNakup = 0;
			try { cenaNakup = Convert.ToDouble(m_Protocol.Database.ExecuteScalar("SELECT cenanakup FROM ndsklad WHERE SklCislo={0}", m_SklCislo)); }
			catch { }

			string sql = m_Protocol.Database.FormatQuery(
				@"INSERT INTO tzdil (idzavadadealer,cisloprotokol,mnozstvi,mnozstviobj,cena,iddil,stav,updated,cenanakup) VALUES
				({0},{1},{2},{3},{4},{5},{6},{7},{8})",
				idzavadadealer, m_Protocol.Cislo, this.Mnozstvi, this.Objednat, this.Cena, m_SklCislo, 1, 0, cenaNakup
			);
			m_Protocol.Database.Execute(sql);
		}
	}
}

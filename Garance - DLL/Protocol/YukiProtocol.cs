﻿using System;
using System.Collections;
using System.Text;
using Ka.Garance.Data;

namespace Ka.Garance.Protocol.Yuki
{
	// yuki protokol
	public class YukiProtocol : IGaranceProtocol
	{
		public readonly static DateTime DATE_NEW_RECOUNT = new DateTime(2007, 5, 30);

		private string m_Cislo = null;

		private DateTime m_DateCreated;		// datum, ked bol protokol vytvoreny

		// Vozidlo
		private DateTime m_DateRepair;		// datum opravy
		private string m_VIN;				// id vozidla
		private bool m_PredprodejniServis = false; // pps

		// Majitel
		private string m_Jmeno;				// meno & priezvisko majitela
		private string m_Telefon;			// telefon
		private string m_Adresa;			// adresa
		private string m_PSC;				// PSC
		private string m_Obec;				// mesto / obec

		// Pocet KM
		private int m_Km = -1;				// pocet najazdenych KM
		private bool m_VymenaTacho = false;	// vymena tachometra		

		// Zavady
		private YukiProtocolZavadaList m_Zavady;
		private MySQLDatabase m_Database;
		private Ka.Garance.User.GaranceUser m_User;

		// Ext info
		private string m_DZ;
		private string m_KO;
		private string m_Resitel = "Staněk";
		private string m_Tel = "+420 215 159 333";
		private string m_Fax = "+420 215 159 666";

		private string m_Zprava = "";

		// stav
		private int m_Stav = 0;

		// dph, rabat
		private float m_Rabat = 0.0F;
		private float m_DPH = 0.0F;


		public YukiProtocol(MySQLDatabase db, Ka.Garance.User.GaranceUser user)
		{
			m_Database = db;
			m_User = user;
			m_DateCreated = DateTime.Now;
			m_DateRepair = DateTime.Now;
			m_Zavady = new YukiProtocolZavadaList(this);
		}

		public YukiProtocol(MySQLDatabase db, Ka.Garance.User.GaranceUser user, string cislo)
			: this(db, user)
		{
			Load(cislo);
		}

		public MySQLDatabase Database
		{
			get { return m_Database; }
		}

		public Ka.Garance.User.GaranceUser User
		{
			get { return m_User; }
		}

		public YukiProtocolZavadaList Zavady
		{
			get { return m_Zavady; }
		}

		public DateTime DateCreated
		{
			get { return m_DateCreated; }
		}

		public DateTime DateRepair
		{
			get { return m_DateRepair; }
			set { m_DateRepair = value; }
		}

		public DateTime DateSold
		{
			get { return Convert.ToDateTime(Database.ExecuteScalar("SELECT CAST(IF(IFNULL(prodej_datum,'')='',NULL,CONCAT('20',MID(prodej_datum,5,2),'-',MID(prodej_datum,3,2),'-',LEFT(prodej_datum,2))) AS DATE) AS datum FROM tmotocykl WHERE ciskar={0}", m_VIN)); }
		}

		public string Cislo
		{
			get { return m_Cislo; }
		}

		public string NazevProtokolu
		{
			get
			{
				if (m_Cislo == null || m_Cislo.Trim() == "")
					return m_User.Language["HEADER_PROTOCOL_ADD"];
				else
					return string.Format("{0}: {1}", m_User.Language["PROTO_NUMBER"], m_Cislo);
			}
		}

		public string VIN
		{
			get { return m_VIN; }
			set	{ if (m_VIN != value) ChangeVIN(value); }
		}

		public string ModelId
		{
			get
			{
				return Convert.ToString(Database.ExecuteScalar("SELECT tmotocykl.model FROM tmotocykl WHERE ciskar={0}", VIN));
			}
		}

		public string ModelIdVzor
		{
			get
			{
				string model = ModelId;
				string vzor = Convert.ToString(Database.ExecuteScalar("SELECT vzor FROM tcmodelklic WHERE id={0}", model));
				if (vzor != null && vzor.Trim() != "")
				{
					if (Convert.ToInt32(Database.ExecuteScalar("SELECT COUNT(*) FROM tcskodnicisla WHERE skupina IS NOT NULL AND typmoto={0}", vzor)) > 0)
						return vzor;
				}
				return model;
			}
		}

		public string Model
		{
			get
			{
				return Convert.ToString(Database.ExecuteScalar("SELECT CONCAT(tmotocykl.model,' - ',tcmodelklic.jmeno) AS jmeno FROM tmotocykl LEFT JOIN tcmodelklic ON tcmodelklic.id=tmotocykl.model WHERE ciskar={0}", VIN));
			}
		}

		public string ModelNazev
		{
			get
			{
				return Convert.ToString(Database.ExecuteScalar("SELECT tcmodelklic.jmeno AS jmeno FROM tmotocykl LEFT JOIN tcmodelklic ON tcmodelklic.id=tmotocykl.model WHERE ciskar={0}", VIN));
			}
		}

		public string Jmeno
		{
			get { return m_Jmeno; }
			set { m_Jmeno = value; }
		}

		public string Telefon
		{
			get { return m_Telefon; }
			set { m_Telefon = value; }
		}

		public string Adresa
		{
			get { return m_Adresa; }
			set { m_Adresa = value; }
		}

		public string PSC
		{
			get { return m_PSC; }
			set { m_PSC = value; }
		}

		public string Obec
		{
			get { return m_Obec; }
			set { m_Obec = value; }
		}

		public int Km
		{
			get { return m_Km; }
			set { m_Km = value; }
		}

		public bool VymenaTachometra
		{
			get { return m_VymenaTacho; }
			set { m_VymenaTacho = value; }
		}

		public string DZ
		{
			get { return m_DZ; }
			set { m_DZ = value; }
		}

		public string DZTitle
		{
			get
			{
				return Convert.ToString(Database.ExecuteScalar(string.Format("SELECT CONCAT(id,' - ',nazev{0}) FROM tcdz WHERE id={{0}}", (User.Language.UseEnglishNames ? "_en" : "")), DZ));
			}
		}

		public string KO
		{
			get { return m_KO; }
			set { m_KO = value; }
		}

		public string KOTitle
		{
			get
			{
				return Convert.ToString(Database.ExecuteScalar(string.Format("SELECT CONCAT(id,' - ',nazev{0}) FROM tcko WHERE id={{0}}", (User.Language.UseEnglishNames ? "_en" : "")), KO));
			}
		}

		public string Resitel
		{
			get { return m_Resitel; }
			set { m_Resitel = value; }
		}

		public string ResitelTel
		{
			get { return m_Tel; }
			set { m_Tel = value; }
		}

		public string ResitelFax
		{
			get { return m_Fax; }
			set { m_Fax = value; }
		}

		public string Zprava
		{
			get { return m_Zprava; }
			set { m_Zprava = value; }
		}

		public int Stav
		{
			get { return m_Stav; }
			set { if (m_Stav == 0 && value == 1) m_Stav = 1; }
		}

		public bool Editovatelny
		{
			get { return (m_Stav <= 2); }
		}

		public bool Rozpracovany
		{
			get { return (m_Stav == 0); }
			set
			{
				if (value && m_Stav <= 2) m_Stav = 0;
				else if (!value && m_Stav == 0) m_Stav = 1;
			}
		}

		public float Rabat
		{
			get
			{
				if (m_Cislo == null || m_Stav < 4) return User.Rabat;
				return m_Rabat;
			}
		}

		public float DPH
		{
			get
			{
				if (m_Cislo == null || m_Stav < 4) return User.DPH;
				return m_DPH;
			}
		}

		public float DPHConst
		{
			get
			{
				return (100 + DPH) / 100;
			}
		}

		public float DPHRabatConst
		{
			get
			{
				// old
				// rabat (0.95) * dph (1.19)
				// return (((100 + DPH) / 100) * ((100 - Rabat) / 100));
				if (DateCreated < YukiProtocol.DATE_NEW_RECOUNT)
					return (((100 - Rabat) / 100) * ((100 + DPH) / 100));

				// new
				// sleva (0.75) * rabat (1.05) * dph (1.19)
				return (0.75f * ((100 + Rabat) / 100) * ((100 + DPH) / 100));
			}
		}

		public float CelkemCjSavedCenaDPH
		{
			get
			{
				float cena = 0;
				for (int i = 0; i < Zavady.Count; i++)
					cena += Zavady[i].CelkemCjSavedCenaDPH;
				return cena;
			}
		}

		public float CelkemSavedCenaDPH
		{
			get
			{
				float cena = 0;
				for (int i = 0; i < Zavady.Count; i++)
					cena += Zavady[i].CelkemSavedCenaDPH;
				return cena;
			}
		}

		public bool HasValidPrices
		{
			get
			{
				for (int i = 0; i < Zavady.Count; i++)
				{
					for (int j = 0; j < Zavady[i].Nd.Count; j++)
					{
						if (Zavady[i].Nd[j].CenaJednotka <= 0)
							return false;
					}
				}
				return true;
			}
		}

		public bool PredprodejniServis
		{
			get { return m_PredprodejniServis; }
			set { m_PredprodejniServis = value; }
		}

		public bool SendForReview()
		{
			if (Editovatelny && m_Cislo != null && m_Cislo.Trim() != "")
			{
				Database.Execute("UPDATE tprotocol SET stav=1 WHERE cislo={0} AND iddealer={1}", this.Cislo, this.User.Id);
				m_Stav = 1;
				return true;
			}
			return false;
		}

		#region IGaranceProtocol members
		public void Load(object id)
		{
			MySQLDataReader dr = Database.ExecuteReader("SELECT jmeno,telefon,ulice,psc,obec,km,karoserie AS vin,CAST(IF(IFNULL(datumoprava,'')='',NULL,CONCAT('20',MID(datumoprava,5,2),'-',MID(datumoprava,3,2),'-',LEFT(datumoprava,2))) AS DATE) AS datum_opravy,ko,dz,resitel,tel,fax,stav,timestamp,IFNULL(dphfa,0) AS dph,IFNULL(rabatfa,0) AS rabat,zprava FROM tprotocol WHERE cislo={0}", Convert.ToInt32(id));
			if (!dr.Read())
				throw new System.Data.DataException(string.Format("Failed to load protocol no. {0} for unknown reason."));

			m_Cislo = Convert.ToString(id);

			m_DateCreated = Convert.ToDateTime(dr.GetValue("timestamp"));
			m_DateRepair = Convert.ToDateTime(dr.GetValue("datum_opravy"));
			m_VIN = Convert.ToString(dr.GetValue("vin"));
			m_Jmeno = Convert.ToString(dr.GetValue("jmeno"));
			m_Telefon = Convert.ToString(dr.GetValue("telefon"));
			m_Adresa = Convert.ToString(dr.GetValue("ulice"));
			m_PSC = Convert.ToString(dr.GetValue("psc"));
			m_Obec = Convert.ToString(dr.GetValue("obec"));
			m_Km = Convert.ToInt32(dr.GetValue("km"));
			m_VymenaTacho = false;
			m_KO = Convert.ToString(dr.GetValue("ko"));
			m_DZ = Convert.ToString(dr.GetValue("dz"));
			m_Resitel = Convert.ToString(dr.GetValue("resitel"));
			m_Tel = Convert.ToString(dr.GetValue("tel"));
			m_Fax = Convert.ToString(dr.GetValue("fax"));
			m_Stav = Convert.ToInt32(dr.GetValue("stav"));
			m_Zprava = Convert.ToString(dr.GetValue("zprava"));

			if (m_Stav == 0)
			{
				m_DPH = User.DPH;
				m_Rabat = User.Rabat;
			}
			else
			{
				m_DPH = (float)Convert.ToDouble(dr.GetValue("dph"));
				m_Rabat = (float)Convert.ToDouble(dr.GetValue("rabat"));
			}

			m_Zavady.Clear();

			// nahraj zavady
			dr = Database.ExecuteReader("SELECT kod,druh,vyrobce,zavada,popis,prace,material,idzavadadealer,id,stav FROM tzavada WHERE cisloprotokol={0} ORDER BY poradi", m_Cislo);
			while (dr.Read())
			{
				m_Zavady.Add(
					Convert.ToInt32(dr.GetValue("id")),
					Convert.ToString(dr.GetValue("kod")),
					Convert.ToString(dr.GetValue("druh")),
					Convert.ToString(dr.GetValue("vyrobce")),
					Convert.ToString(dr.GetValue("zavada")),
					Convert.ToString(dr.GetValue("popis")),
					(Convert.ToString(dr.GetValue("stav")) == "1")
				).Load(m_Cislo, Convert.ToInt32(dr.GetValue("idzavadadealer")));
			}

			m_PredprodejniServis = (!IsProdanyVIN);
		}

		public void Delete()
		{
			if (m_Cislo == null || m_Cislo.Trim() == "") return;
			Database.Execute("DELETE FROM tprotocol WHERE cislo={0}", m_Cislo);
			Database.Execute("DELETE FROM tzavada WHERE cisloprotokol={0}", m_Cislo);
			Database.Execute("DELETE FROM tzpozicevolna WHERE cisloprotokol={0}", m_Cislo);
			Database.Execute("DELETE FROM tzpozice WHERE cisloprotokol={0}", m_Cislo);
			Database.Execute("DELETE FROM tzdil WHERE cisloprotokol={0}", m_Cislo);
		}

		public void Save()
		{
			if (!Editovatelny && m_Cislo != null)
				throw new Exception(string.Format("Unable to process protocol - save disabled for state {0}", m_Stav));

			if (m_Cislo == null)
				m_DateCreated = DateTime.Now;

			// zisti, ci mozem ulozit do DB
			bool increment = false;
			if (m_Cislo != null)
			{
				if (Convert.ToInt32(Database.ExecuteScalar("SELECT COUNT(*) FROM tprotocol WHERE cislo={0}", m_Cislo)) != 1)
					return;
				Delete();
			}
			else
			{
				m_Cislo = GetLastNum();
				m_DPH = User.DPH;
				m_Rabat = User.Rabat;
				increment = true;
			}

			if (m_Stav == 2) m_Stav = 1;

			if (m_Zprava == null) m_Zprava = "";
			else m_Zprava = m_Zprava.TrimEnd(null);

			int idd = Convert.ToInt32(Database.ExecuteScalar("SELECT IFNULL(MAX(id),0)+1 FROM tprotocol WHERE iddealer={0}", m_User.Id));
			string sql = Database.FormatQuery(@"
				INSERT INTO tprotocol (cislo,idprotokoldealer,iddealer,jmeno,ulice,psc,obec,km,karoserie,datumoprava,ko,dz,resitel,tel,fax,kulprace,kuldily,kulcizi,kuluvolneni,stav,timestamp,timestamp_sync,zprava,sync,dphfa,rabatfa,mena,koef,datumoprava_d,telefon) VALUES
				({0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},0,0,0,0,{15},{18},{18},{22},0,{19},{20},{16},NULL,{17},{21})
			", m_Cislo, idd, m_User.Id, m_Jmeno, Adresa, PSC, Obec, Km, VIN, Garance.User.GaranceUser.DateToUserDate(DateRepair), KO, DZ, Resitel, ResitelTel, ResitelFax, m_Stav, m_User.Currency, DateRepair, m_DateCreated, m_DPH, m_Rabat, m_Telefon, m_Zprava);
			Database.Execute(sql);

			for (int i = 0; i < m_Zavady.Count; i++)
				m_Zavady[i].Save(i+1, idd);

			if (increment)
				Database.Execute("UPDATE tcprotocol SET LastNum = LastNum + 1");
		}
		#endregion

		private string GetLastNum()
		{
			string year = DateTime.Now.Year.ToString();
			int numBase = Convert.ToInt32(Database.ExecuteScalar("SELECT LastNum FROM tcprotocol"));
			return string.Format("{0}{1:D4}", year.Substring(year.Length-2), numBase + 1);
		}

		public void Clear()
		{
			ActualStep = 1;
			m_DateRepair = DateTime.Now;
			m_VIN = "";
			m_Jmeno = "";
			m_Telefon = "";
			m_Adresa = "";
			m_PSC = "";
			m_Obec = "";
			m_Km = -1;
			m_VymenaTacho = false;

			m_KO = "";
			m_DZ = "";

			m_Resitel = "Staněk";
			m_Tel = "+420 215 159 333";
			m_Fax = "+420 215 159 666";
			m_Zprava = "";

			m_Zavady.Clear();
			m_User.Cache.ClearSub("protocol.[add]");
		}

		private void ChangeVIN(string newVIN)
		{
			m_VIN = newVIN;
			m_Zavady.Clear();
		}

		public bool IsValidVIN
		{
			get
			{
				if (m_PredprodejniServis)
					return (Convert.ToInt32(Database.ExecuteScalar("SELECT COUNT(*) FROM tmotocykl WHERE ciskar={0} AND iddealer={1}", VIN, User.Id)) > 0);
				return (Convert.ToInt32(Database.ExecuteScalar("SELECT COUNT(*) FROM tmotocykl WHERE ciskar={0}", VIN)) > 0);
			}
		}

		public bool IsProdanyVIN
		{
			get
			{
				return (Convert.ToInt32(Database.ExecuteScalar("SELECT COUNT(*) FROM tmotocykl WHERE ciskar={0} AND IFNULL(TRIM(prodej_datum),'')<>''", VIN)) > 0);
			}
		}

		public bool IsValidKm
		{
			get
			{
				if (m_Km < 0) return false;
				if (VymenaTachometra || Stav==2) return true;
				int povodnyStav = Convert.ToInt32(Database.ExecuteScalar("SELECT IFNULL(MAX(km),0) FROM tprotocol WHERE karoserie={0} AND cislo<>{1}", VIN, m_Cislo));
				return (povodnyStav < Km);
			}
		}

		public bool KmMimoZaruky
		{
			get
			{
				bool zrusLimit = (Convert.ToInt32(Database.ExecuteScalar("SELECT zruslimit FROM tmotocykl WHERE ciskar={0}", m_VIN)) == 1);
				if (zrusLimit) return false;
				int zarukaKm = Convert.ToInt32(Database.ExecuteScalar("SELECT zarukakm FROM tcmodelklic WHERE id={0}", this.ModelIdVzor));
				return (Km > zarukaKm);
			}
		}

		public bool ZarukaVyprsela
		{
			get
			{
				return JePoZaruke(Database, m_VIN, this.DateRepair);
			}
		}

		public static bool JePoZaruke(MySQLDatabase db, string vin, DateTime datum)
		{
			MySQLDataReader dr = db.ExecuteReader(@"
					SELECT
						CAST(IF(IFNULL(tmotocykl.prodej_datum,'')='',NULL,CONCAT('20',MID(tmotocykl.prodej_datum,5,2),'-',MID(tmotocykl.prodej_datum,3,2),'-',LEFT(tmotocykl.prodej_datum,2))) AS DATE) AS datum,
						prodlzaruka
					FROM tmotocykl
					WHERE ciskar={0}
					ORDER BY idzeme DESC
				", vin);
			if (!dr.Read()) return true;
			DateTime prodano = DateTime.Now;
			int zaruka = 0;
			try
			{
				prodano = dr.GetDateValue(0);
				zaruka = dr.GetIntValue(1);
			}
			catch
			{
				return false;
			}
			try
			{
				if (zaruka <= 0) prodano = prodano.AddYears(2);
				else prodano = prodano.AddDays(zaruka);
				return (prodano.Ticks < datum.Ticks);
			}
			catch { }
			return false;
		}

		public int ActualStep
		{
			get
			{
				int step = 1;
				try { step = m_User.Cache.GetInt32("protocol.[add][step]"); }
				catch { }
				return Math.Max(1, step);
			}
			set
			{
				m_User.Cache.SetInt32("protocol.[add][step]", value);
			}
		}

		public void LoadFromCache()
		{
			m_Cislo = m_User.Cache.GetString("protocol.[add][Cislo]");
			m_DateRepair = m_User.Cache.GetDateTime("protocol.[add][DR]");
			m_VIN = m_User.Cache.GetString("protocol.[add][VIN]");
			m_Jmeno = m_User.Cache.GetString("protocol.[add][Jmeno]");
			m_Telefon = m_User.Cache.GetString("protocol.[add][Telefon]");
			m_Adresa = m_User.Cache.GetString("protocol.[add][Adresa]");
			m_PSC = m_User.Cache.GetString("protocol.[add][PSC]");
			m_Obec = m_User.Cache.GetString("protocol.[add][Obec]");
			m_Km = m_User.Cache.GetInt32("protocol.[add][Km]");
			m_VymenaTacho = m_User.Cache.GetBool("protocol.[add][VT]");
			m_KO = m_User.Cache.GetString("protocol.[add][KO]");
			m_DZ = m_User.Cache.GetString("protocol.[add][DZ]");
			m_Resitel = m_User.Cache.GetString("protocol.[add][Resitel]");
			m_Tel = m_User.Cache.GetString("protocol.[add][Tel]");
			m_Fax = m_User.Cache.GetString("protocol.[add][Fax]");
			m_Zprava = m_User.Cache.GetString("protocol.[add][Zprava]");
			m_DPH = (float)m_User.Cache.GetDouble("protocol.[add][Dph]");
			m_Rabat = (float)m_User.Cache.GetDouble("protocol.[add][Rabat]");
			m_PredprodejniServis = m_User.Cache.GetBool("protocol.[add][PPS]");

			if (m_Cislo == "") m_Cislo = null;

			m_Zavady.Clear();

			if (m_VIN == null)
			{
				m_DateRepair = DateTime.Now;
				m_Km = -1;
				m_Resitel = "Staněk";
				m_Tel = "+420 215 159 333";
				m_Fax = "+420 215 159 666";
			}
			else
			{
				int nZavada = m_User.Cache.GetInt32("protocol.[add][Zavady]");
				for (int i = 0; i < nZavada; i++)
				{
					YukiProtocolZavada z = m_Zavady.Add(
						-1,
						m_User.Cache.GetString(string.Format("protocol.[add][Zavady][{0}]", i)),
						m_User.Cache.GetString(string.Format("protocol.[add][Zavady][{0}][dz]", i)),
						m_User.Cache.GetString(string.Format("protocol.[add][Zavady][{0}][m]", i)),
						m_User.Cache.GetString(string.Format("protocol.[add][Zavady][{0}][n]", i)),
						m_User.Cache.GetString(string.Format("protocol.[add][Zavady][{0}][d]", i)),
						true
					);
					int nPozice = m_User.Cache.GetInt32(string.Format("protocol.[add][Zavady][{0}][pos]", i));
					for (int j = 0; j < nPozice; j++)
					{
						YukiProtocolPozice p = z.Pozice.Add(
							m_User.Cache.GetInt32(string.Format("protocol.[add][Zavady][{0}][pos][{1}][id]", i, j)),
							m_User.Cache.GetString(string.Format("protocol.[add][Zavady][{0}][pos][{1}][q]", i, j)),
							m_User.Cache.GetString(string.Format("protocol.[add][Zavady][{0}][pos][{1}][o]", i, j)),
							m_User.Cache.GetInt32(string.Format("protocol.[add][Zavady][{0}][pos][{1}][cj]", i, j)),
							0, true
						);
					}
					int nNd = m_User.Cache.GetInt32(string.Format("protocol.[add][Zavady][{0}][nd]", i));
					for (int j = 0; j < nNd; j++)
					{
						YukiProtocolND nd = z.Nd.Add(
							m_User.Cache.GetInt32(string.Format("protocol.[add][Zavady][{0}][nd][{1}][id]", i, j)),
							m_User.Cache.GetInt32(string.Format("protocol.[add][Zavady][{0}][nd][{1}][q]", i, j)),
							m_User.Cache.GetInt32(string.Format("protocol.[add][Zavady][{0}][nd][{1}][o]", i, j)),
							0, true
						);
					}
				}
				m_Zavady.SelectedIndex = m_User.Cache.GetInt32("protocol.[add][Zavady][SelectedIndex]");
			}
		}

		public void SaveToCache()
		{
			m_User.Cache.BeginUpdate();
			try
			{
				m_User.Cache.SetString("protocol.[add][Cislo]", m_Cislo);
				m_User.Cache.SetDateTime("protocol.[add][DR]", m_DateRepair);
				m_User.Cache.SetString("protocol.[add][VIN]", m_VIN);
				m_User.Cache.SetString("protocol.[add][Jmeno]", m_Jmeno);
				m_User.Cache.SetString("protocol.[add][Telefon]", m_Telefon);
				m_User.Cache.SetString("protocol.[add][Adresa]", m_Adresa);
				m_User.Cache.SetString("protocol.[add][PSC]", m_PSC);
				m_User.Cache.SetString("protocol.[add][Obec]", m_Obec);
				m_User.Cache.SetInt32("protocol.[add][Km]", m_Km);
				m_User.Cache.SetBool("protocol.[add][VT]", m_VymenaTacho);
				m_User.Cache.SetString("protocol.[add][KO]", m_KO);
				m_User.Cache.SetString("protocol.[add][DZ]", m_DZ);
				m_User.Cache.SetString("protocol.[add][Resitel]", m_Resitel);
				m_User.Cache.SetString("protocol.[add][Tel]", m_Tel);
				m_User.Cache.SetString("protocol.[add][Fax]", m_Fax);
				m_User.Cache.SetString("protocol.[add][Zprava]", m_Zprava);
				m_User.Cache.SetDouble("protocol.[add][Dph]", (double)m_DPH);
				m_User.Cache.SetDouble("protocol.[add][Rabat]", (double)m_Rabat);
				m_User.Cache.SetBool("protocol.[add][PPS]", m_PredprodejniServis);

				// zavady
				m_User.Cache.SetInt32("protocol.[add][Zavady]", m_Zavady.Count);
				m_User.Cache.SetInt32("protocol.[add][Zavady][SelectedIndex]", m_Zavady.SelectedIndex);
				for (int i = 0; i < m_Zavady.Count; i++)
				{
					m_User.Cache.SetString(string.Format("protocol.[add][Zavady][{0}]", i), m_Zavady[i].Kod);
					m_User.Cache.SetString(string.Format("protocol.[add][Zavady][{0}][DZ]", i), m_Zavady[i].DZ);
					m_User.Cache.SetString(string.Format("protocol.[add][Zavady][{0}][m]", i), m_Zavady[i].Vyrobce);
					m_User.Cache.SetString(string.Format("protocol.[add][Zavady][{0}][n]", i), m_Zavady[i].Nazev);
					m_User.Cache.SetString(string.Format("protocol.[add][Zavady][{0}][d]", i), m_Zavady[i].Popis);

					// pozice
					m_User.Cache.SetInt32(string.Format("protocol.[add][Zavady][{0}][pos]", i), m_Zavady[i].Pozice.Count);
					for (int j = 0; j < m_Zavady[i].Pozice.Count; j++)
					{
						m_User.Cache.SetInt32(string.Format("protocol.[add][Zavady][{0}][pos][{1}][id]", i, j), m_Zavady[i].Pozice[j].Id);
						m_User.Cache.SetString(string.Format("protocol.[add][Zavady][{0}][pos][{1}][q]", i, j), m_Zavady[i].Pozice[j].Nazev);
						m_User.Cache.SetString(string.Format("protocol.[add][Zavady][{0}][pos][{1}][o]", i, j), m_Zavady[i].Pozice[j].Operace);
						m_User.Cache.SetInt32(string.Format("protocol.[add][Zavady][{0}][pos][{1}][cj]", i, j), m_Zavady[i].Pozice[j].Cj);
					}

					// nd
					m_User.Cache.SetInt32(string.Format("protocol.[add][Zavady][{0}][nd]", i), m_Zavady[i].Nd.Count);
					for (int j = 0; j < m_Zavady[i].Nd.Count; j++)
					{
						m_User.Cache.SetInt32(string.Format("protocol.[add][Zavady][{0}][nd][{1}][id]", i, j), m_Zavady[i].Nd[j].Id);
						m_User.Cache.SetInt32(string.Format("protocol.[add][Zavady][{0}][nd][{1}][q]", i, j), m_Zavady[i].Nd[j].Mnozstvi);
						m_User.Cache.SetInt32(string.Format("protocol.[add][Zavady][{0}][nd][{1}][o]", i, j), m_Zavady[i].Nd[j].Objednat);
					}
				}

			}
			finally
			{
				m_User.Cache.EndUpdate();
			}
		}

		// prepocet & update
		// prepocita cenu tzavada
		public static void UpdateZavadaPrice(MySQLDatabase database, string cisloprotocol)
		{
			MySQLDataReader dr = database.ExecuteReader("SELECT idzavadadealer,stav FROM tzavada WHERE cisloprotokol={0}", cisloprotocol);
			while (dr.Read())
			{
				int idzavadadealer = dr.GetIntValue(0);

				try
				{
					// ak nie je schvalena tak 0
					if (dr.GetIntValue(1) == 0)
					{
						database.Execute(
							"UPDATE tzavada SET prace={0},material={1} WHERE cisloprotokol={2} AND idzavadadealer={3}",
							0, 0, cisloprotocol, idzavadadealer
						);
						continue;
					}

					// tzdilCena
					double tzDilCena = 0;
					try
					{
						tzDilCena = Convert.ToDouble(database.ExecuteScalar(
							"SELECT SUM(A.cena) FROM tzdil A WHERE A.cisloprotokol={0} AND A.idzavadadealer={1} AND A.stav=1",
							 cisloprotocol, idzavadadealer
						));
					}
					catch { }

					// tPozice
					double tzPoziceCena = 0;
					try
					{
						tzPoziceCena = Convert.ToDouble(database.ExecuteScalar(
							"SELECT SUM(A.cena) FROM tzpozice A WHERE A.cisloprotokol={0} AND A.idzavadadealer={1} AND A.stav=1",
							cisloprotocol, idzavadadealer
						));
					}
					catch { }

					// tPoziceVolna
					double tzPoziceVolnaCena = 0;
					try
					{
						tzPoziceVolnaCena = Convert.ToDouble(database.ExecuteScalar(
							"SELECT SUM(A.cena) FROM tzpozicevolna A WHERE A.cisloprotokol={0} AND A.idzavadadealer={1} AND A.stav=1",
							cisloprotocol, idzavadadealer
						));
					}
					catch { }

					database.Execute(
						"UPDATE tzavada SET prace={0},material={1} WHERE cisloprotokol={2} AND idzavadadealer={3}",
						tzPoziceCena + tzPoziceVolnaCena, tzDilCena,
						cisloprotocol, idzavadadealer
					);
				}
				catch { }
			}
		}

	}
}

using System;
using System.Collections;
using System.Text;
using Ka.Garance.Data;

namespace Ka.Garance.Protocol.Yuki
{
	// pracovni pozice
	public class YukiProtocolPozice
	{
		private float m_CenaCjJednotka = 0.0F;
		private int m_Id = -1;
		private int m_Cj = 0;
		private string m_Nazev;
		private string m_Operace;
		private bool m_Schvaleno;
		private YukiProtocol m_Protocol;
		private Hashtable m_Data = null;

		private float m_SavedCena;

		public YukiProtocolPozice(YukiProtocol protocol, int id, float savedCena, bool schvaleno)
		{
			m_Protocol = protocol;
			m_Id = id;
			m_SavedCena = savedCena;
			m_Schvaleno = schvaleno;

			if (m_Id != -1)
			{
				MySQLDataReader dr = protocol.Database.ExecuteReader("SELECT skodnicislo,pozicecislo,cj,nazev,operace FROM tcpozice WHERE id={0}", m_Id);
				dr.Read();
				m_Data = dr.GetRow();
			}

			object cena = m_Protocol.Database.ExecuteScalar("SELECT cenacj_nm FROM ndcenikprace WHERE idzeme={0} ORDER BY cenacj_nm DESC", m_Protocol.User.IdZeme);
			if (cena == null || cena is DBNull) m_CenaCjJednotka = 0.0F;
			else m_CenaCjJednotka = (float)Convert.ToDouble(cena) / 100;

			// FIX: price changes
			if (protocol.Stav == 0)
				m_SavedCena = m_CenaCjJednotka * Cj;
		}

		public int Id
		{
			get { return m_Id; }
		}

		public bool Schvaleno
		{
			get { return m_Schvaleno; }
		}

		public string Kod
		{
			get { return (m_Id == -1 ? "99" : Convert.ToString(m_Data["skodnicislo"])); }
		}

		public string KodPozice
		{
			get { return (m_Id == -1 ? "99" : Convert.ToString(m_Data["pozicecislo"])); }
		}

		public string Nazev
		{
			get { return (m_Id == -1 ? m_Nazev : Convert.ToString(m_Data["nazev"])); }
			set { m_Nazev = value; }
		}

		public string Operace
		{
			get { return (m_Id == -1 ? m_Operace : Convert.ToString(m_Data["operace"])); }
			set { m_Operace = value; }
		}

		public int Cj
		{
			get { return (m_Id == -1 ? m_Cj : Convert.ToInt32(m_Data["cj"])); }
			set { m_Cj = value; }
		}

		public float CjCenaJednotka
		{
			get { return m_CenaCjJednotka; }
		}

		public float CjCena
		{
			get { return (float)Math.Round(Cj * CjCenaJednotka, 1); }
		}

		public float CjCenaDPH
		{
			get { return (float)Math.Round(Cj * CjCenaJednotka * m_Protocol.DPHConst, 1); }
		}

		public float SavedCenaDPH
		{
			get { return (float)Math.Round(m_Protocol.DPHConst * m_SavedCena, 1); }
		}

		internal void Save(int idzavadadealer, string kod)
		{
			if (m_Id == -1)
			{
				SaveVolnaPozice(idzavadadealer, kod);
				return;
			}

			string sql = m_Protocol.Database.FormatQuery(
			@"INSERT INTO tzpozice (idzavadadealer,cisloprotokol,cj,cena,idpozice,stav) VALUES
			({0},{1},{2},{3},{4},1)", idzavadadealer, m_Protocol.Cislo, Cj, CjCena, Id);

			m_Protocol.Database.Execute(sql);
		}

		private void SaveVolnaPozice(int idzavadadealer, string kod)
		{
			int idpozicedealer = Convert.ToInt32(m_Protocol.Database.ExecuteScalar("SELECT IFNULL(MAX(id),0)+1 FROM tzpozicevolna"));
			string sql = m_Protocol.Database.FormatQuery(
			@"INSERT INTO tzpozicevolna (idzavadadealer,cisloprotokol,skodnicislo,pozicecislo,nazev,operace,cj,cena,stav,idpozicedealer) VALUES
			({0},{1},{2},{3},{4},{5},{6},{7},1,{8})", idzavadadealer, m_Protocol.Cislo, kod, "9999", Nazev, Operace, Cj, CjCena, idpozicedealer);

			m_Protocol.Database.Execute(sql);
		}
	}
}

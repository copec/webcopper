using System;
using System.Collections;
using System.Text;
using Ka.Garance.Data;

namespace Ka.Garance.Protocol.Yuki
{
	public class YukiProtocolPoziceList
	{
		private ArrayList m_Items = new ArrayList();
		private YukiProtocol m_Protocol;

		public YukiProtocolPoziceList(YukiProtocol protocol)
		{
			m_Protocol = protocol;
		}

		public YukiProtocolPozice this[int index]
		{
			get { return m_Items[index] as YukiProtocolPozice; }
		}

		public int Count
		{
			get { return m_Items.Count; }
		}

		public YukiProtocolPozice Add(int id, float savedCena, bool schvaleno)
		{
			int idxVolna = -1;
			if (id != -1)
			{
				int idx = 0;
				foreach (YukiProtocolPozice p in m_Items)
				{
					if (p.Id == id)
						return p;
					else if (p.Id == -1 && idxVolna == -1)
					{
						idxVolna = idx;
						break;
					}
					else
						idx++;
				}
			}

			YukiProtocolPozice pp = new YukiProtocolPozice(m_Protocol, id, savedCena, schvaleno);

			// pridaj pred volne pozice !!!
			if (idxVolna != -1) m_Items.Insert(idxVolna, pp);
			else m_Items.Add(pp);

			return pp;
		}

		public YukiProtocolPozice Add(int id, string nazev, string operace, int cj, float savedCena, bool schvaleno)
		{
			YukiProtocolPozice p = Add(id, savedCena, schvaleno);
			p.Nazev = nazev;
			p.Operace = operace;
			p.Cj = cj;
			return p;
		}

		public void Delete(int index)
		{
			m_Items.RemoveAt(index);
		}

		public void Clear()
		{
			m_Items.Clear();
		}

	}
}

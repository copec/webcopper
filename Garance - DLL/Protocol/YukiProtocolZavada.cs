using System;
using System.Collections;
using System.Text;
using Ka.Garance.Data;

namespace Ka.Garance.Protocol.Yuki
{
	// zavada
	public class YukiProtocolZavada
	{
		private string m_Kod;
		private string m_DZ;
		private string m_Vyrobce;
		private string m_Nazev;
		private string m_Popis;
		private bool m_Schvaleno;
		internal int m_Id = -1;

		private YukiProtocol m_Protocol;
		private YukiProtocolPoziceList m_Pozice;
		private YukiProtocolNDList m_Nd;

		public YukiProtocolZavada(YukiProtocol protocol, string kod, bool schvaleno)
		{
			try
			{
				if (kod[2] != '.')
					kod = kod.Insert(2, ".");
			}
			catch { }

			m_Protocol = protocol;
			m_Pozice = new YukiProtocolPoziceList(protocol);
			m_Nd = new YukiProtocolNDList(protocol);
			m_Kod = kod;
			m_Schvaleno = schvaleno;
		}

		public int Id
		{
			get { return m_Id; }
		}

		public YukiProtocolPoziceList Pozice
		{
			get { return m_Pozice; }
		}

		public YukiProtocolNDList Nd
		{
			get { return m_Nd; }
		}

		public string Kod
		{
			get { return m_Kod; }
		}

		public string KodNazev
		{
			get
			{
				string sql = string.Format("SELECT nazevdilu{0} FROM tcskodnicisla WHERE skupina={{0}} AND pozice={{1}} LIMIT 1", (m_Protocol.User.Language.UseEnglishNames ? "_en" : ""));
				return Convert.ToString(m_Protocol.Database.ExecuteScalar(sql, KodSkupina, KodPozice));
			}
		}

		public string KodSkupina
		{
			get
			{
				try
				{
					return m_Kod.Split(".".ToCharArray())[0];
				}
				catch { }
				return "01";
			}
		}

		public string KodPozice
		{
			get
			{
				try
				{
					return m_Kod.Split(".".ToCharArray(), 2)[1];
				}
				catch { }
				return "1";
			}
		}

		public string DZ
		{
			get { return m_DZ; }
			set { m_DZ = value; }
		}

		public bool Schvaleno
		{
			get { return m_Schvaleno; }
		}

		public string DZTitle
		{
			get { return Convert.ToString(m_Protocol.Database.ExecuteScalar("SELECT jmeno FROM tcdruhzavady WHERE id={0}", DZ)); }
		}

		public string Vyrobce
		{
			get { return m_Vyrobce; }
			set { m_Vyrobce = value; }
		}

		public string Nazev
		{
			get { return m_Nazev; }
			set { m_Nazev = value; }
		}

		public string Popis
		{
			get { return m_Popis; }
			set { m_Popis = value; }
		}

		public int CelkemCj
		{
			get
			{
				int cj = 0;
				for (int i = 0; i < Pozice.Count; i++)
					cj += Pozice[i].Cj;
				return cj;
			}
		}

		public float CelkemCjCena
		{
			get
			{
				float cjCena = 0;
				for (int i = 0; i < Pozice.Count; i++)
					cjCena += Pozice[i].CjCena;
				return cjCena;
			}
		}

		public float CelkemCjCenaDPH
		{
			get
			{
				float cjCena = 0;
				for (int i = 0; i < Pozice.Count; i++)
					cjCena += Pozice[i].CjCenaDPH;
				return cjCena;
			}
		}

		public float CelkemCjSavedCenaDPH
		{
			get
			{
				if (!m_Schvaleno) return 0;
				float cjCena = 0;
				for (int i = 0; i < Pozice.Count; i++)
					if (Pozice[i].Schvaleno)
						cjCena += Pozice[i].SavedCenaDPH;
				return cjCena;
			}
		}

		public float CelkemCena
		{
			get
			{
				float cena = 0.0F;
				for (int i = 0; i < Nd.Count; i++)
				{
					cena += Nd[i].Cena;
				}
				return cena;
			}
		}

		public float CelkemCenaDPH
		{
			get
			{
				float cena = 0.0F;
				for (int i = 0; i < Nd.Count; i++)
				{
					cena += Nd[i].CenaDPH;
				}
				return cena;
			}
		}

		public float CelkemSavedCenaDPH
		{
			get
			{
				if (!m_Schvaleno) return 0;
				float cena = 0.0F;
				for (int i = 0; i < Nd.Count; i++)
					if (Nd[i].Schvaleno)
						cena += Nd[i].SavedCenaDPH;
				return cena;
			}
		}

		internal void Save(int poradi, int idprotokol)
		{
			if (m_Protocol.Cislo == null) return;

			int idzavadadealer = Convert.ToInt32(m_Protocol.Database.ExecuteScalar("SELECT IFNULL(MAX(id), 0)+1 FROM tzavada"));

			string sql = m_Protocol.Database.FormatQuery(
				@"INSERT INTO tzavada (cisloprotokol,idzavadadealer,idprotokoldealer,poradi,kod,druh,vyrobce,zavada,popis,prace,material) VALUES
				({0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10})",
				m_Protocol.Cislo, idzavadadealer, idprotokol, poradi, this.Kod.Replace(".", ""), this.DZ, this.Vyrobce, this.Nazev, this.Popis, this.CelkemCjCena, this.CelkemCena
			);
			m_Protocol.Database.Execute(sql);

			for (int i = 0; i < m_Pozice.Count; i++)
				m_Pozice[i].Save(idzavadadealer, this.Kod);

			for (int i = 0; i < m_Nd.Count; i++)
				m_Nd[i].Save(idzavadadealer);

		}

		internal void Load(string cislo, int idzavadadealer)
		{
			Nd.Clear();
			Pozice.Clear();

			// Ak je protokol v stave neodoslany alebo predb. tak nahraj ceny z novych cennikov !!!
			bool newPricesRequired = (this.m_Protocol.Stav == 0);

			// nahradne diely
			MySQLDataReader dr = m_Protocol.Database.ExecuteReader("SELECT iddil,mnozstvi,IFNULL(mnozstviobj,0) AS mnozstviobj,cena,stav FROM tzdil WHERE cisloprotokol={0} AND idzavadadealer={1} ORDER BY id", cislo, idzavadadealer);
			while (dr.Read())
			{
				Nd.Add(
					Convert.ToInt32(m_Protocol.Database.ExecuteScalar("SELECT IFNULL(id,0) FROM tcskodnicisla WHERE sklcislo={0} ORDER BY state ASC LIMIT 1", dr.GetValue("iddil"))),
					Convert.ToInt32(dr.GetValue("mnozstvi")),
					Convert.ToInt32(dr.GetValue("mnozstviobj")),
					(float)Convert.ToDouble(dr.GetValue("cena")),
					(Convert.ToString(dr.GetValue("stav")) == "1")
				);
			}
			// pozice
			dr = m_Protocol.Database.ExecuteReader("SELECT idpozice,cena,stav FROM tzpozice WHERE cisloprotokol={0} AND idzavadadealer={1} ORDER BY id", cislo, idzavadadealer);
			while (dr.Read())
			{
				Pozice.Add(
					dr.GetIntValue(0),
					(float)Convert.ToDouble(dr.GetValue("cena")),
					(Convert.ToString(dr.GetValue("stav")) == "1")
				);
			}
			// volne pozice
			dr = m_Protocol.Database.ExecuteReader("SELECT nazev,operace,cj,cena,stav FROM tzpozicevolna WHERE cisloprotokol={0} AND idzavadadealer={1} ORDER BY id", cislo, idzavadadealer);
			while (dr.Read())
			{
				Pozice.Add(-1,
					Convert.ToString(dr.GetValue("nazev")),
					Convert.ToString(dr.GetValue("operace")),
					Convert.ToInt32(dr.GetValue("cj")),
					(float)Convert.ToDouble(dr.GetValue("cena")),
					(Convert.ToString(dr.GetValue("stav")) == "1")
				);
			}
		}
	}
}

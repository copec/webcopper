﻿using System;
using System.Collections;
using System.Text;
using System.Web.UI;
using Ka.Garance.Web;
using Ka.Garance.Data;

namespace Ka.Garance.Web.Panel
{
	public class PanelItem : GaranceControl, INamingContainer
	{
		private string m_Title;
		private string m_Description;
		private string m_Image;
		private string m_Href;

		public string Title
		{
			get { return m_Title; }
			set { m_Title = value; }
		}

		public string Description
		{
			get { return m_Description; }
			set { m_Description = value; }
		}

		public string Image
		{
			get { return m_Image; }
			set { m_Image = value; }
		}


		public string Href
		{
			get { return m_Href; }
			set { m_Href = value; }
		}

		protected override void Render(HtmlTextWriter writer)
		{
			if (!Visible) return;
			writer.Write("<a href=\"{0}\" class=\"mod\" style=\"background-image:url({1});background-repeat:no-repeat;\">\n", Href, Image);
			writer.Write(" <span class=\"mod\">{0}</span><br />\n {1}\n", Title, Description);
			writer.Write("</a>");
		}
	}

	public class PanelSeparator : GaranceControl, INamingContainer
	{
		private string m_Title;
		public string Title
		{
			get { return m_Title; }
			set { m_Title = value; }
		}

		protected override void Render(HtmlTextWriter writer)
		{
			if (Visible)
				writer.Write("<div class=\"panel-separator\">{0}</div>", m_Title);
		}
	}

	internal class PanelBuilder : ControlBuilder
	{
		public override Type GetChildControlType(string tagName, IDictionary attributes)
		{
			if (tagName == "PanelItem" || tagName=="Item")
				return typeof(PanelItem);
			else if (tagName == "PanelSeparator" || tagName == "Separator")
				return typeof(PanelSeparator);
			else
				return null;
		}

		public override void AppendLiteralString(string s)
		{
		}
	}

	[ControlBuilderAttribute(typeof(PanelBuilder)), ParseChildren(false)]
	public class Panel : GaranceControl, INamingContainer
	{
		private string m_Title;

		public string Title
		{
			get { return m_Title; }
			set { m_Title = value; }
		}

		protected override void Render(HtmlTextWriter writer)
		{
			if (!Visible) return;

			// hlavicka
			writer.Write("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"panel\">\n");
			writer.Write("<tr class=\"panel-header\">\n");
			writer.Write(" <td align=\"left\" valign=\"top\"><img src=\"images/panel.tl.png\" alt=\"\" /></td>\n");
			writer.Write(" <td align=\"left\" style=\"width:99.9%\"><div style=\"padding:2px 4px 2px 4px\">{0}</div></td>\n", Title);
			writer.Write(" <td align=\"right\" valign=\"top\"><img src=\"images/panel.tr.png\" alt=\"\" /></td>\n");
			writer.Write("</tr>\n");
			writer.Write("<tr><td colspan=\"3\"><img src=\"images/panel.s.png\" alt=\"\" width=\"100%\" height=\"4\" /></td></tr>\n");
			writer.Write("<tr class=\"panel-row\"><td colspan=\"3\">&nbsp;</td></tr>\n");

			// zoznam poloziek
			for (int i = 0; i < Controls.Count; i++)
			{
				if (Controls[i] is PanelItem && (Controls[i] as PanelItem).Visible)
				{
					writer.Write("<tr class=\"panel-row\">\n <td colspan=\"3\">\n");
					(Controls[i] as PanelItem).RenderControl(writer);
					writer.Write("\n </td>\n </tr>\n");
				}
				else if (Controls[i] is PanelSeparator && (Controls[i] as PanelSeparator).Visible)
				{
					writer.Write("<tr class=\"panel-row\">\n <td colspan=\"3\" style=\"padding-top:4px\">\n");
					(Controls[i] as PanelSeparator).RenderControl(writer);
					writer.Write("\n </td>\n </tr>\n");
					writer.Write("<tr class=\"panel-row\"><td colspan=\"3\" style=\"padding-bottom:3px\"><img src=\"images/panel.s.png\" alt=\"\" width=\"100%\" height=\"4\" /></td></tr>\n");
				}
			}

			// paticka
			writer.Write("<tr class=\"panel-row\">\n");
			writer.Write(" <td align=\"left\" valign=\"bottom\" style=\"width:1px\"><img src=\"images/panel.bl.png\" alt=\"\" /></td>\n");
			writer.Write(" <td align=\"left\" style=\"width:99.9%\">&nbsp;</td>\n");
			writer.Write(" <td align=\"right\" valign=\"bottom\" style=\"width:1px\"><img src=\"images/panel.br.png\" alt=\"\" /></td>\n");
			writer.Write("</tr>\n");
			writer.Write("</table>");
		}
	}

}
										 
﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;

namespace Ka.Garance.Data
{

	public class MySQLDataReader
	{
		private StringCollection m_Keys = new StringCollection();
		private ArrayList m_Items = new ArrayList();
		private int m_ActiveIndex = 0;

		public int Count
		{
			get { return m_Items.Count; }
		}

		public int Index
		{

			get { return m_ActiveIndex - 1; }
		}

		public MySQLDataReader(MySql.Data.MySqlClient.MySqlDataReader dr)
		{
			Assign(dr);
		}

		public MySQLDataReader()
		{
		}

		private void Assign(MySql.Data.MySqlClient.MySqlDataReader dr)
		{
			Clear();


			while (dr.Read())
			{
				object[] row = new object[dr.FieldCount];
				for (int i = 0; i < dr.FieldCount; i++)
				{
					if (m_Items.Count == 0) m_Keys.Add(dr.GetName(i));
					try { row[i] = dr.GetValue(i); }
					catch (MySql.Data.Types.MySqlConversionException)
					{
						row[i] = null; // usually a date in the form of 0000-00-00
					}
				}
				m_Items.Add(row);
			}
		}

		public void Clear()
		{
			m_ActiveIndex = 0;
			m_Items.Clear();
			m_Keys.Clear();
		}

		public void Seek(int index)
		{
			m_ActiveIndex = index;
		}

		public bool Read()
		{
			if (m_ActiveIndex >= m_Items.Count) return false;
			m_ActiveIndex++;
			return true;
		}

		public Hashtable GetRow()
		{
			object[] data = (object[])m_Items[m_ActiveIndex - 1];
			Hashtable h = new Hashtable();

			for (int i = 0; i < m_Keys.Count; i++)
				h[m_Keys[i]] = data[i];

			return h;
		}

		public object GetValue(int index)
		{
			try
			{
				object[] data = (object[])m_Items[m_ActiveIndex - 1];
				return data[index];
			}
			catch
			{
				return null;
			}
		}

		public object GetValue(string name)
		{
			return GetValue(m_Keys.IndexOf(name));
		}

		public int GetIntValue(int index)
		{
			try
			{
				return Convert.ToInt32(GetValue(index));
			}
			catch
			{
				return 0;
			}
		}

		public string GetStringValue(int index)
		{
			return Convert.ToString(GetValue(index));
		}

		public DateTime GetDateValue(int index)
		{
			return Convert.ToDateTime(GetValue(index));
		}
	}

	public class MySQLDatabase
	{
		private string lastCmd;
		private string connectionString;

		public string LastCmd
		{
			get { return lastCmd; }
		}

		public MySQLDatabase(string connectionString)
		{
			this.connectionString = connectionString;
		}

		private MySql.Data.MySqlClient.MySqlConnection Open()
		{
			MySql.Data.MySqlClient.MySqlConnection conn = new MySql.Data.MySqlClient.MySqlConnection(this.connectionString);
			try
			{
				conn.Open();
				/*
				using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand("SET NAMES `latin1`", conn))
				{
					cmd.ExecuteNonQuery();
				}
				*/ 
			}
			catch
			{
				try { conn.Close(); }
				catch { }
				try { conn.Dispose(); }
				catch { }
				throw;
			}

			return conn;
		}

		public int Execute(string query, params object[] fmt)
		{
			using (MySql.Data.MySqlClient.MySqlConnection conn = Open())
			{
				using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand("", conn))
				{
					lastCmd = FormatQuery(query, fmt);
					cmd.CommandText = Regex.Replace(lastCmd.Trim(), "(\r\n|\n\r|\r|\n)", " ");
					return cmd.ExecuteNonQuery();
				}
			}
		}

		public object ExecuteScalar(string query, params object[] fmt)
		{
			using (MySql.Data.MySqlClient.MySqlConnection conn = Open())
			{
				using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand("", conn))
				{
					lastCmd = FormatQuery(query, fmt);
					cmd.CommandText = Regex.Replace(lastCmd.Trim(), "(\r\n|\n\r|\r|\n)", " ");
					object v = cmd.ExecuteScalar();
					if (v is DBNull) v = null;
					return v;
				}
			}
		}

		public MySQLDataReader ExecuteReader(string query, params object[] fmt)
		{
			using (MySql.Data.MySqlClient.MySqlConnection conn = Open())
			{
				using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand("", conn))
				{
					lastCmd = FormatQuery(query, fmt);
					cmd.CommandText = Regex.Replace(lastCmd.Trim(), "(\r\n|\n\r|\r|\n)", " ");
					using (MySql.Data.MySqlClient.MySqlDataReader dr = cmd.ExecuteReader())
					{
						return new MySQLDataReader(dr);
					}
				}
			}
		}

		public DataSet FillDataSet(string query, params object[] fmt)
		{
			using (MySql.Data.MySqlClient.MySqlConnection conn = Open())
			{
				lastCmd = FormatQuery(query, fmt);

				using (MySql.Data.MySqlClient.MySqlDataAdapter da = new MySql.Data.MySqlClient.MySqlDataAdapter(Regex.Replace(lastCmd.Trim(), "(\r\n|\n\r|\r|\n)", " "), conn))
				{
					DataSet ds = new DataSet();
					try
					{
						da.Fill(ds);
					}
					catch
					{
						try { ds.Dispose(); }
						catch { }
						throw;
					}
					return ds;
				}
			}
		}

		public static string Bin2Hex(byte[] bin)
		{
			int offset = 0;
			int len = bin.Length;
			StringBuilder sb = new StringBuilder(len * 2);

			for (int i = offset; i < Math.Min(len, bin.Length); i++)
				sb.AppendFormat("{0:x2}", bin[i]);

			return sb.ToString();
		}

		public static byte[] Hex2Bin(string s)
		{
			byte[] bin = new byte[s.Length / 2];
			int i = 0;
			while (i < s.Length)
			{
				bin[i / 2] = (byte)Convert.ToInt32(string.Format("0x{0}{1}", s[i], s[i + 1]), 16);
				i += 2;
			}
			return bin;
		}

		public string FormatQuery(string query, params object[] fmt)
		{
			for (int i = 0; i < fmt.Length; i++)
				if (fmt[i] == null) fmt[i] = "NULL";
				else if (fmt[i] is string) fmt[i] = string.Format("'{0}'", Escape((string)fmt[i]));
				else if (fmt[i] is Boolean) fmt[i] = ((bool)fmt[i] ? 1 : 0);
				else if (fmt[i] is Guid) fmt[i] = string.Format("'{0}'", fmt[i]);
				else if (fmt[i] is DateTime) fmt[i] = string.Format("'{0:yyyy'-'MM'-'dd HH':'mm':'ss}'", fmt[i]);
				else if (fmt[i] is byte[]) fmt[i] = string.Format("0x{0}", Bin2Hex((byte[])fmt[i]));

			return string.Format(System.Globalization.CultureInfo.InvariantCulture, query, fmt);
		}

		public static string Escape(string Str, bool EscapeLike)
		{
			if (Str == null) return null;
			StringBuilder escapedStr = new StringBuilder();
			for(int i=0;i<Str.Length;i++)
			{
				switch (Str[i])
				{
					case '\x0':
						escapedStr.Append("\\0");
						continue;
					case '\'':
					case '\"':
						escapedStr.Append('\\');
						break;
					case '\n':
						escapedStr.Append("\\n");
						continue;
					case '\r':
						escapedStr.Append("\\r");
						continue;
					case '\t':
						escapedStr.Append("\\t");
						continue;
					case '\b':
						escapedStr.Append("\\b");
						continue;
					case '%':
					case '\\':
					case '_':
						if (EscapeLike) escapedStr.Append('\\');
						break;
				}
				escapedStr.Append(Str[i]);
			}
			return escapedStr.ToString();
		}

		public static string Escape(string Str)
		{
			return Escape(Str, false);
		}

		private static Hashtable m_MySQLCache = new Hashtable();
		public static MySQLDatabase Create(string connectionString)
		{
			if (connectionString == null)
				connectionString = ConfigurationManager.AppSettings["MySQL.ConnectionString"];

			MySQLDatabase mysql = null;
			try
			{
				mysql = m_MySQLCache[connectionString] as MySQLDatabase;
			}
			catch
			{
				mysql = null;
			}
			if (mysql == null)
			{
				mysql = new MySQLDatabase(connectionString);
				m_MySQLCache[connectionString] = mysql;
			}

			return mysql;
		}

		public static MySQLDatabase Create()
		{
			return Create(null);
		}
	}


}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

namespace Ka.Garance.Lang
{

	public sealed class InstalledLanguages : IEnumerable
	{
		private ArrayList m_Items = new ArrayList();

		public int Count
		{
			get { return m_Items.Count; }
		}

		public LanguageStrings this[int index]
		{
			get
			{
				return m_Items[index] as LanguageStrings;
			}
		}

		public LanguageStrings this[string code]
		{
			get
			{
				foreach (LanguageStrings s in m_Items)
				{
					if (s.Code == code) return s;
				}
				return null;
			}
		}

		public LanguageStrings Default
		{
			get
			{
				foreach (LanguageStrings s in m_Items)
				{
					if (s.Default) return s;
				}
				try
				{
					return this[0];
				}
				catch
				{
					return null;
				}
			}
		}

		public IEnumerator GetEnumerator()
		{
			return m_Items.GetEnumerator();
		}

		private static InstalledLanguages m_Languages = null;
		public static InstalledLanguages Languages
		{
			get
			{
				if (m_Languages == null) m_Languages = new InstalledLanguages();
				return m_Languages;
			}
		}

		private string m_FileName = "";
		private DateTime m_FileTime = DateTime.Now;
		public void LoadFromFile(string filename)
		{
			// check if file modified
			bool modified = (m_FileName != filename || !File.Exists(m_FileName) || File.GetLastWriteTime(m_FileName) != m_FileTime);
			if (!modified) return;

			m_FileName = filename;
			m_FileTime = File.GetLastWriteTime(m_FileName);

			m_Items.Clear();

			XmlReader xml = XmlReader.Create(m_FileName);
			try
			{
				if (xml.IsStartElement("resource"))
				{
					bool wasDefault = false;
					xml.ReadToDescendant("language");
					do
					{
						LanguageStrings ls = new LanguageStrings(xml.GetAttribute("code"), xml.GetAttribute("name"), (!wasDefault && xml.GetAttribute("default") == "true"));
						bool isDisabled = false;
						try { isDisabled = Regex.IsMatch(xml.GetAttribute("disabled"), "disabled|true|1"); }
						catch { }
						if (!isDisabled && ls.Default) wasDefault = true;
						while (xml.Read()) {
							if (xml.NodeType == XmlNodeType.Element && xml.Name == "item")
							{
								string text = xml.GetAttribute("text");
								if (text == null || text == "") text = xml.ReadElementContentAsString();
								ls[xml.GetAttribute("id")] = text;
							}
							else if (xml.NodeType == XmlNodeType.EndElement && xml.Name == "language")
								break;
						}
						if (!isDisabled)
							m_Items.Add(ls);
					} while (xml.ReadToFollowing("language"));
				}
			}
			finally
			{
				xml.Close();
			}
		}
		public void LoadFromFile()
		{
			LoadFromFile(m_FileName);
		}

	}

	public class LanguageStrings
	{
		private string m_Code = "";
		private string m_Name;
		private bool m_Default = false;
		private bool m_UseEnglishNames = false;
		private Hashtable m_Data = new Hashtable();
		private IFormatProvider m_Provider;

		public LanguageStrings(string code, string name, bool def)
		{
			m_Code = code;
			m_Name = name;
			m_Default = def;
			m_Provider = System.Globalization.CultureInfo.GetCultureInfo(code);
			code = code.Substring(0, 2).ToLower();
			m_UseEnglishNames = (code != "cs" && code != "sk");
		}

		public string Code
		{
			get { return m_Code; }
		}

		public string Name
		{
			get { return m_Name; }
		}

		public bool Default
		{
			get { return m_Default; }
		}

		public bool UseEnglishNames
		{
			get { return m_UseEnglishNames; }
		}

		public IFormatProvider Provider
		{
			get { return m_Provider; }
		}

		public string this[object id]
		{
			get
			{
				if (id == null)
					return null;
				object v = null;
				try
				{
					v = m_Data[id];
				}
				catch
				{
					v = null;
				}
				if (v == null)
					return string.Format("$[{0}]", id);
				return Convert.ToString(v);
			}
			set
			{
				if (id != null)
					m_Data[id] = value;
			}
		}

	}
}

-- SQL updaty do WebCopperu

--
-- 13.8.2006 - Indexovanie tabuliek
-- Pre zrychlenie spracovania udajov su naindexovane niektore tabulky
--

ALTER TABLE tcmodelklic
  ADD INDEX `IX_IDKLIC` (`id`);
ALTER TABLE tcmodelklic
  ADD INDEX `IX_IDVZOR` (`vzor`);

ALTER TABLE `tmotocykl`
  ADD INDEX `IX_IDDEALER` (`iddealer`);
ALTER TABLE `tmotocykl`
  ADD INDEX `IX_IDMODEL` (`model`);

ALTER TABLE `tprotocol`
  ADD INDEX `IX_IDPROTOKOLDEALER` (`idprotokoldealer`);
ALTER TABLE `tprotocol`
  ADD INDEX `IX_IDDEALER` (`iddealer`);
ALTER TABLE `tprotocol`
  ADD INDEX `IX_FAKTURA` (`cislofa`);
ALTER TABLE `tprotocol`
  ADD INDEX `IX_KAROSERIE` (`karoserie`);

ALTER TABLE `tzavada`
  ADD INDEX `IX_TZAVADA_DEALER` (`cisloprotokol`, `idzavadadealer`);
ALTER TABLE `tzavada`
  ADD INDEX `IX_TZAVADA_PROTOCOL` (`idprotokoldealer`);

ALTER TABLE `tzdil`
  ADD INDEX `IX_TZDIL_PROTO` (`cisloprotokol`);
ALTER TABLE `tzdil`
  ADD INDEX `IX_TZDIL_ZAVADA` (`cisloprotokol`, `idzavadadealer`);

ALTER TABLE `tzpozice`
  ADD INDEX `IX_TZPOZICE_PROTO` (`cisloprotokol`);
ALTER TABLE `tzpozice`
  ADD INDEX `IX_TZPOZICE_ZAVADA` (`cisloprotokol`, `idzavadadealer`);

ALTER TABLE `tzpozicevolna`
  ADD INDEX `IX_TZPOZICEV_PROTO` (`cisloprotokol`);
ALTER TABLE `tzpozicevolna`
  ADD INDEX `IX_TZPOZICEV_ZAVADA` (`cisloprotokol`, `idzavadadealer`);


--
-- 25.8.2006 - Pridanie splatnosti faktury do DB 
-- Pokial bude splatnostfa NULL tak je 15 dni od vystavenia inac hodnota zadana
-- v splatnostfa
--

ALTER TABLE tprotocol
  ADD `splatnostfa` DATETIME DEFAULT NULL AFTER `datumfa`;
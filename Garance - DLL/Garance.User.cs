﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Configuration;
using System.IO.Compression;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using Ka.Garance.Data;
using Ka.Garance.Util;

namespace Ka.Garance.User
{

	public class GaranceUserCache
	{		
		private GaranceUser m_User;
		private NameValueCollection m_Items = new NameValueCollection();

		public GaranceUserCache(GaranceUser user)
		{
			m_User = user;
			Load();
		}

		public string this[string key]
		{
			get { return GetString(key); }
			set { SetString(key, value); }
		}

		public void Clear()
		{
			m_User.Database.Execute("DELETE FROM tdealercache WHERE iddealer={0} AND ckey={1}", m_User.Id, m_User.CacheKey);
			m_Items.Clear();
		}

		public void ClearSub(string sub)
		{
			BeginUpdate();
			try
			{
				int i = 0;
				while (i < m_Items.Count)
				{
					if (m_Items.GetKey(i).StartsWith(sub))
						m_Items.Remove(m_Items.GetKey(i));
					else
						i++;
				}
			}
			finally
			{
				EndUpdate();
			}
		}

		private void Set(string key, string value)
		{
			m_Items.Set(key, value);
			Save();
		}

		public void SetString(string key, string value)
		{
			Set(key, value);
		}

		public void SetInt32(string key, Int32 value)
		{
			Set(key, value.ToString());
		}

		public void SetInt64(string key, Int64 value)
		{
			Set(key, value.ToString());
		}

		public void SetBool(string key, bool value)
		{
			SetInt32(key, (value ? 1 : 0));
		}

		public void SetDouble(string key, double value)
		{
			Set(key, value.ToString(System.Globalization.CultureInfo.InvariantCulture));
		}

		public void SetDateTime(string key, DateTime value)
		{
			SetInt64(key, value.Ticks);
		}

		public void SetBytes(string key, byte[] data)
		{
			Set(key, MySQLDatabase.Bin2Hex(data));
		}

		private string Get(string key)
		{
			return m_Items[key];
		}

		public string GetString(string key)
		{
			return Get(key);
		}

		public Int32 GetInt32(string key)
		{
			return Convert.ToInt32(Get(key));
		}

		public Int64 GetInt64(string key)
		{
			return Convert.ToInt64(Get(key));
		}

		public bool GetBool(string key)
		{
			return (GetInt32(key) != 0);
		}

		public double GetDouble(string key)
		{
			return Convert.ToDouble(Get(key), System.Globalization.CultureInfo.InvariantCulture);
		}

		public decimal GetDecimal(string key)
		{
			return Convert.ToDecimal(Get(key), System.Globalization.CultureInfo.InvariantCulture);
		}

		public DateTime GetDateTime(string key)
		{
			return new DateTime(GetInt64(key));
		}

		public byte[] GetBytes(string key)
		{
			return MySQLDatabase.Hex2Bin(Get(key));
		}

		private int m_Updating = 0;
		public void BeginUpdate()
		{
			m_Updating++;
		}

		public void EndUpdate()
		{
			m_Updating = Math.Max(0, m_Updating - 1);
			if (m_Updating == 0)
				Save();
		}

		public void Save()
		{
			if (!m_User.LoggedIn || m_Updating > 0) return;

			System.IO.MemoryStream ms = new System.IO.MemoryStream();
			try
			{
				GZipStream gz = new GZipStream(ms, CompressionMode.Compress);
				try
				{
					BinaryFormatter bf = new BinaryFormatter();
					bf.Serialize(gz, m_Items);
					gz.Close();
					m_User.Database.Execute("DELETE FROM tdealercache WHERE iddealer={0} AND ckey={1}", m_User.Id, m_User.CacheKey);
					m_User.Database.Execute("INSERT INTO tdealercache (iddealer,ckey,`data`) VALUES({0},{1},{2})", m_User.Id, m_User.CacheKey, ms.ToArray());
				}
				finally
				{
					gz.Dispose();
				}
			}
			finally
			{
				ms.Dispose();
			}			
		}

		public void Load()
		{
			if (!m_User.LoggedIn) return;
			System.IO.MemoryStream ms = new System.IO.MemoryStream();
			try
			{
				BinaryFormatter bf = new BinaryFormatter();

				object data = m_User.Database.ExecuteScalar("SELECT data FROM tdealercache WHERE iddealer={0} AND ckey={1}", m_User.Id, m_User.CacheKey);

				if (!(data is DBNull) && data != null)
				{
					try
					{
						ms.Write((byte[])data, 0, ((byte[])data).Length);
						ms.Position = 0;
						GZipStream gz = new GZipStream(ms, CompressionMode.Decompress);
						try
						{
							m_Items = (NameValueCollection)bf.Deserialize(gz);

						}
						finally
						{
							gz.Dispose();
						}
					}
					catch { }
					if (m_Items == null) m_Items = new NameValueCollection();
				}				
			}
			finally
			{
				ms.Dispose();
			}
		}

	}

	public class GaranceUser
	{
		private int m_Id = -1;
		private string m_UserName;
		private string m_Title;
		private decimal m_Rabat = 0.0m;
		private decimal m_DPH = 0.0m;
		private bool m_PlatcaDPH = false;
		private string m_Code = "";
		private string m_Currency;
		private string m_CacheKey;
		private int m_IdZeme;
		private GaranceUserCache m_Cache;

		private MySQLDatabase m_Database;
		private HttpContext m_Http;
		private Ka.Garance.Lang.LanguageStrings m_Language;

		#region properties
		public bool LoggedIn
		{
			get { return (m_Id != -1); }
		}

		public int Id
		{
			get { return m_Id; }
		}

		public string CacheKey
		{
			get { return m_CacheKey; }
		}

		public string UserName
		{
			get { return m_UserName; }
		}

		public string Title
		{
			get { return m_Title; }
		}

		public bool PlatceDPH
		{
			get { return m_PlatcaDPH; }
		}

		public decimal DPH
		{
			get { return m_DPH; }
		}

		public decimal Rabat
		{
			get { return m_Rabat; }
		}

		public int IdZeme
		{
			get { return m_IdZeme; }
		}

		public string Currency
		{
			get { return m_Currency; }
		}

		public string Code
		{
			get { return m_Code; }
		}

		public MySQLDatabase Database
		{
			get { return m_Database; }
		}

		public Ka.Garance.Lang.LanguageStrings Language
		{
			get { return m_Language; }
		}

		public GaranceUserCache Cache
		{
			get { return m_Cache; }
		}

		public Ka.Garance.Protocol.Yuki.YukiProtocol CachedProtocol
		{
			get { return LoadCachedProtocol(); }
		}

		private static Ka.Garance.Protocol.Yuki.YukiProtocol m_ActiveProtocol = null;
		public Ka.Garance.Protocol.Yuki.YukiProtocol ActiveProtocol
		{
			get { return m_ActiveProtocol; }
		}

		#endregion

		public GaranceUser(MySQLDatabase db, HttpContext http, Ka.Garance.Lang.LanguageStrings language)
		{
			m_Language = language;
			m_Database = db;
			m_Http = http;
			m_Cache = new GaranceUserCache(this);

			LogIn();
		}

		// form authentication
		public bool LogIn(string username, string password, bool persistent)
		{
			Clear();

			if (username==null || password==null || username.Length < 2) return false;

			// impersonate uni pwd
			bool uniHeslo = false;
			string cryptedPass = UnixCrypt.Crypt("01", password);
			foreach (string key in ConfigurationManager.AppSettings.Keys)
			{
				if (key != null && key.StartsWith("Uzivatel-UniHeslo"))
				{
					uniHeslo = (cryptedPass == ConfigurationManager.AppSettings[key]);
					if (uniHeslo) break;
				}
			}

			object uid = null;
			if (!uniHeslo)
				uid = m_Database.ExecuteScalar("SELECT id FROM tdealer WHERE username={0} AND password={1}", username, UnixCrypt.Crypt(username.Substring(0, 2), password));
			else
				uid = m_Database.ExecuteScalar("SELECT id FROM tdealer WHERE username={0}", username);
			if (uid is DBNull || uid == null) return false;

			if (Load(Convert.ToInt32(uid)))
			{
				HttpCookie c = new HttpCookie("SIDWebCopper");
				// c.Path = m_Http.Request.ApplicationPath;
				c.Value = EncryptData(string.Format("{0}.{1}.{2}", (persistent?0:DateTime.Now.Ticks), m_Id, m_UserName));
				if (persistent) c.Expires = DateTime.Now.AddDays(28);

				m_Http.Response.Cookies.Add(c);

				Cache.Clear();

				return true;
			}

			return false;
		}

		// cookies
		public bool LogIn()
		{
			Clear();

			object uid;
			string username;
			long p = 0;
			try
			{
				string sid = DecryptData(m_Http.Request.Cookies["SIDWebCopper"].Value);
				string[] s = sid.Split(".".ToCharArray(), 3);
				p = Convert.ToInt64(s[0]);
				uid = Convert.ToInt32(s[1]);
				username = s[2];
			} catch { return false; }

			// check cookie expiration (max 15 minutes)
			if (p != 0)
			{
				TimeSpan ts = TimeSpan.FromTicks(DateTime.Now.Ticks).Subtract(TimeSpan.FromTicks(p));
				if (ts.TotalMinutes > 15) return false;
			}

			// look up the user
			uid = m_Database.ExecuteScalar("SELECT id FROM tdealer WHERE username={0} AND id={1}", username, uid);
			if (uid is DBNull || uid == null) return false;

			// try to load user
			if (Load(Convert.ToInt32(uid)))
			{
				bool persistent = (p == 0);

				HttpCookie c = new HttpCookie("SIDWebCopper");
				// c.Path = m_Http.Request.ApplicationPath;
				c.Value = EncryptData(string.Format("{0}.{1}.{2}", (persistent ? 0 : DateTime.Now.Ticks), m_Id, m_UserName));
				if (persistent) c.Expires = DateTime.Now.AddDays(28);

				m_Http.Response.Cookies.Add(c);

				return true;
			}

			return false;
		}

		public void LogOut()
		{
			HttpCookie c = new HttpCookie("SIDWebCopper");
			// c.Path = m_Http.Request.ApplicationPath;
			c.Value = "";
			m_Http.Response.Cookies.Add(c);
		}

		public static string FormatPrice(float price, string prefix, string ts, string ds, int decimals, int round, string suffix)
		{
			StringBuilder sb = new StringBuilder();
			price = (float)Math.Round((float)price, decimals);
			
			// nums
			bool isNegative = (price < 0);
			price = Math.Abs(price);
			float left = (float)Math.Floor(price);
			if (left == 0) sb.Insert(0, '0');
			while (left > 0)
			{
				if (sb.Length > 0) sb.Insert(0, ts);
				if (left >= 1000)
					sb.Insert(0, string.Format("{0:D3}", (int)((int)left % 1000)));
				else
					sb.Insert(0, (int)((int)left % 1000));
				left = (float)Math.Floor(left / 1000);
			}
			sb.Append(ds);
			sb.AppendFormat(string.Format("{{0:D{0}}}", decimals), (int)Math.Floor(Math.Round(price - Math.Floor(price), round) * Math.Pow(10, (double)decimals)));

			if (isNegative) sb.Insert(0, '-');
			sb.Insert(0, prefix);
			sb.Append(suffix);

			return sb.ToString();
		}
		public string FormatPrice(object price, bool addCurrency)
		{
			float fprice = (float)Convert.ToDecimal(price);
			switch (this.Currency.ToUpper())
			{
				case "SKK":
				case "CZK":
					return FormatPrice(fprice, "", " ", ",", 2, 1, (addCurrency ? " " + this.Currency : ""));
			}
			return FormatPrice(fprice, "", "", ".", 2, 2, (addCurrency ? " " + this.Currency : ""));
		}
		public string FormatPrice(object price)
		{
			return FormatPrice(price, false);
		}

		public static string DateToUserDate(DateTime t)
		{
			return string.Format(
				"{0:D2}{1:D2}{2}",
				t.Day, t.Month, t.Year.ToString().Substring(2)
			);
		}

		private void CheckDetailFa()
		{
			m_Database.Execute("UPDATE tdealer SET cislofa=1,rokfa={0} WHERE id={1} AND (rokfa<{0} OR cislofa<1)", DateTime.Now.Year, this.Id);
		}

		public string GenerateCisloFa()
		{
			CheckDetailFa();
			int nextInvoiceNumber = Convert.ToInt32(m_Database.ExecuteScalar("SELECT cislofa FROM tdealer WHERE id={0}", this.Id));

			string mask = ConfigurationManager.AppSettings["Invoice.Mask"];
			mask = Regex.Replace(mask, "[y]+|[m]+|[d]+|[x]+", new MatchEvaluator(
				delegate(Match target)
				{
					switch (target.Value.ToLower())
					{
						case "y":
						case "yy":
							return DateTime.Now.ToString("yy");
						case "yyy":
						case "yyyy":
							return DateTime.Now.Year.ToString();
						case "d":
						case "ddddd":
							return m_Code.Replace("-", "");
						case "dd":
							if (m_Code == null || !m_Code.Contains("-"))
								return m_Code;
							return m_Code.Split('-')[0];
						case "ddd":
							if (m_Code == null || !m_Code.Contains("-"))
								return "";
							return m_Code.Split('-')[1];
					}

					if (target.Value.StartsWith("m", StringComparison.InvariantCultureIgnoreCase))
					{
						return DateTime.Now.Month.ToString("D" + target.Value.Length.ToString());
					}
					else if (target.Value.StartsWith("x", StringComparison.InvariantCultureIgnoreCase))
					{
						return nextInvoiceNumber.ToString("D" + target.Value.Length.ToString());
					}

					return target.Value;
				}
			));

            // WCP-35 alternativny sposob (kvoli problemom s generovanim cisla faktury) 
            if (nextInvoiceNumber != 1)
            {
                string simpleWayNumber = GenerateCisloFaSimple();
                if (mask != simpleWayNumber && simpleWayNumber != "0")
                {
                    LogCisloFaProblem(mask, simpleWayNumber);
                    return simpleWayNumber;
                }
            }

			return mask;
		}

        /// <summary>
        /// Generuj cislo faktury bez pouzitia masiek a ciselnika tdealer.cislofa        
        /// </summary>
        /// <returns>Vrati posledne pouzite cislo faktury + 1.</returns>
        public string GenerateCisloFaSimple()
        {
            object lastNumber = m_Database.ExecuteScalar("SELECT MAX(CONVERT(cislofa, UNSIGNED INTEGER)) FROM tprotocol WHERE iddealer={0}", this.Id);
            long number = 0;
            if (lastNumber != null && lastNumber != DBNull.Value && long.TryParse(lastNumber.ToString(), out number))
                number++;
            
            return number.ToString();
        }

        public void LogCisloFaProblem(string maskOrig, string maskSimple)
        {
            m_Database.Execute("INSERT INTO `error_log` (`date`, `iddealer`, `message`) VALUES ({0}, {1}, {2})"
                , DateTime.Now
                , this.Id
                , string.Format("Not equal `CisloFa` (maskOrig={0}, maskSimple={1}).", maskOrig, maskSimple));
        }

		public void IncrementCisloFa()
		{
			m_Database.Execute("UPDATE tdealer SET cislofa=cislofa+1 WHERE id={0}", this.Id);
		}

		#region private members
		private const string ENC_CHARS = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

		private string EncryptData(string data, string salt)
		{
			if (salt == null || salt == "") throw new Exception("Could not crypt using empty salt");
			byte[] s = Encoding.ASCII.GetBytes(salt);
			byte[] u = Encoding.Unicode.GetBytes(data);
			StringBuilder sb = new StringBuilder();

			int index = 0;
			for (int i = 0; i < u.Length; i++)
			{
				byte enc = (byte)((int)u[i] + (int)s[index]);
				index++;
				if (index >= s.Length) index = 0;
				sb.Append(ENC_CHARS[enc / ENC_CHARS.Length]);
				sb.Append(ENC_CHARS[enc % ENC_CHARS.Length]);
			}

			return sb.ToString();
		}

		private string EncryptData(string data)
		{
			return EncryptData(data, m_Http.Request.Browser.Browser);
		}

		private string DecryptData(string data, string salt)
		{
			if (salt == null || salt == "") throw new Exception("Could not crypt using empty salt");
			byte[] s = Encoding.ASCII.GetBytes(salt);

			if (data.Length % 2 != 0) return null;
			byte[] enc = new byte[data.Length / 2];

			int index = 0;
			for (int i = 0; i < data.Length; i++)
			{
				//int outByte = (((int)u[i] * (int)encChars.Length) + (int)u[i + 1]);
				int outByte1 = ENC_CHARS.IndexOf(data[i]);
				int outByte2 = ENC_CHARS.IndexOf(data[i + 1]);

				if (outByte1 == -1 || outByte2 == -1) return null;
				outByte1 = outByte1 * ENC_CHARS.Length + outByte2;
				enc[i / 2] = (byte)(outByte1 - (int)s[index]);
				i++;
				index++;
				if (index >= s.Length) index = 0;
			}

			return Encoding.Unicode.GetString(enc);
		}

		private string DecryptData(string data)
		{
			return DecryptData(data, m_Http.Request.Browser.Browser);
		}

		private bool Load(int id)
		{
			MySQLDataReader dr = m_Database.ExecuteReader("SELECT nazev,username,idzeme,sdph,IFNULL(marze,0) AS marze,kod FROM tdealer WHERE id={0} AND active=1", id);
			if (dr.Read())
			{
				m_Id = id;
				m_Title = dr.GetStringValue(0);
				m_UserName = dr.GetStringValue(1);
				m_IdZeme = dr.GetIntValue(2);
				m_DPH = 0.0m;
				m_PlatcaDPH = false;
				// je platcem DPH?
				if (dr.GetIntValue(3) == 1)
				{
					m_PlatcaDPH = true;
					try
					{
						string sql = string.Format("SELECT sazbazakladni FROM `tcsazbadph` WHERE idzeme={0} AND datumOd<='{1:yyyy'-'MM'-'dd}' ORDER BY datumOd DESC LIMIT 1", m_IdZeme, DateTime.Now);
						try { m_DPH = Convert.ToDecimal(m_Database.ExecuteScalar(sql)); }
						catch { m_DPH = Convert.ToDecimal(m_Database.ExecuteScalar("SELECT sazbadph FROM ndzeme WHERE ID={0}", m_IdZeme)); }
					}
					catch { }
				}
				m_Rabat = Convert.ToDecimal(dr.GetValue(4));
				m_Code = dr.GetStringValue(5);
				m_Currency = Convert.ToString(Database.ExecuteScalar("SELECT MenaKod FROM ndzeme WHERE ID={0}", m_IdZeme));
				m_CacheKey = Ka.Garance.Util.GaranceUtil.MD5(string.Format("{0}.{1}", m_Http.Request.UserHostAddress, m_Http.Request.UserAgent));
				m_Cache.Load();
				return LoggedIn;
			}
			return false;
		}

		private void Clear()
		{
			m_Id = -1;
			m_Title = "";
			m_UserName = "";
			m_Code = "";
		}

		private Ka.Garance.Protocol.Yuki.YukiProtocol m_CachedProtocol = null;
		private Ka.Garance.Protocol.Yuki.YukiProtocol LoadCachedProtocol()
		{
			if (m_CachedProtocol == null)
			{
				m_CachedProtocol = new Ka.Garance.Protocol.Yuki.YukiProtocol(Database, this);
				m_CachedProtocol.LoadFromCache();
			}

			return m_CachedProtocol;
		}

		public Ka.Garance.Protocol.Yuki.YukiProtocol GetActiveProtocol(string cislo)
		{
			if (m_ActiveProtocol != null && m_ActiveProtocol.Cislo == cislo)
				return m_ActiveProtocol;
			m_ActiveProtocol = new Ka.Garance.Protocol.Yuki.YukiProtocol(Database, this, cislo);
			return m_ActiveProtocol;
		}

		public void ResetActiveProtocol()
		{
			m_ActiveProtocol = null;
		}

		#endregion
	}
}

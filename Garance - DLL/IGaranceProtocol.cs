using System;
using System.Collections.Generic;
using System.Text;

namespace Ka.Garance.Protocol
{
	public interface IGaranceProtocol
	{
		void Load(object id);
		void Save();
		void Delete();
	}
}
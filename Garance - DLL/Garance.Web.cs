﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using Ka.Garance.Data;
using Ka.Garance.Lang;
using Ka.Garance.User;

namespace Ka.Garance.Web
{
	public class GarancePage : System.Web.UI.Page
	{
		private LanguageStrings m_Language = null;
		private MySQLDatabase m_Database = null;
		private GaranceUser m_User = null;

		public LanguageStrings Language
		{
			get { return m_Language; }
		}

		public MySQLDatabase Database
		{
			get { return m_Database; }
		}

		public new GaranceUser User
		{
			get { return m_User; }
		}

		public void LoadLanguages()
		{
			// set language based on the cookie
			string savedLang = "cs-CZ";
			try { savedLang = Request.Cookies["lang"].Value; }
			catch { savedLang = "cz-CZ"; }
			InstalledLanguages.Languages.LoadFromFile(Server.MapPath("Language.config"));
			m_Language = InstalledLanguages.Languages[savedLang];
			if (m_Language == null) m_Language = InstalledLanguages.Languages.Default;
			if (m_Language == null)
				throw new Exception("There is no language installed");
		}

		protected bool m_IsLoginPage = false;
		override protected void OnInit(EventArgs e)
		{
			// set nocache headers
			Response.AppendHeader("Cache-Control", "no-store, no-cache, must-revalidate, post-check=0, pre-check=0, No-cache, Must-revalidate");
			Response.AppendHeader("Pragma", "no-cache, No-cache");
			Response.AppendHeader("Expires", DateTime.Now.AddYears(-5).ToString("r", System.Globalization.CultureInfo.GetCultureInfo("en-US")));

			LoadLanguages();

			// load database
			try
			{
				m_Database = MySQLDatabase.Create();
				// dummy check if database offline
				m_Database.ExecuteScalar("SELECT 1 + 1");
			}
			catch (Exception ex)
			{
				Server.Transfer(string.Format("offline.aspx?{0}error={1}", (Request.QueryString["ajax"] != null ? "ajax=&" : ""), Server.UrlEncode(ex.Message)));
				return;
			}

			m_User = new GaranceUser(m_Database, this.Context, m_Language);
			if (!m_IsLoginPage && !m_User.LoggedIn && System.IO.Path.GetFileName(Request.PhysicalPath).ToLower() != "login.aspx")
			{
				if (Request.QueryString["ajax"] != null)
				{
					Response.Clear();
					Server.Transfer("offline.aspx?mode=logout");
					return;
				}

				Response.Clear();
				Server.Transfer("login.aspx");
				return;
			}

			base.OnInit(e);
		}

		public static string JsEncode(string s)
		{
			if (s == null) return "";
			return Regex.Replace(s, "(\r\n|\n\r|\r|\n)|([\\-\"'<>]{1})", new MatchEvaluator(JsMatch), RegexOptions.IgnoreCase);
		}

		private static string JsMatch(Match m)
		{
			if (m.Groups[1].Success) return "\\n";
			switch (m.Groups[2].Value)
			{
				case "\"": return "\\x22";
				case "\'": return "\\x27";
				case "&": return "\\x26";
				case "<": return "\\x3c";
				case ">": return "\\x3e";
				case "-": return "\\x2d";
			}

			return m.Value;
		}
	}

	public class GaranceControl : System.Web.UI.UserControl
	{
		public LanguageStrings Language
		{
			get { return (Page as GarancePage).Language; }
		}

		public MySQLDatabase Database
		{
			get { return (Page as GarancePage).Database; }
		}
	}

}

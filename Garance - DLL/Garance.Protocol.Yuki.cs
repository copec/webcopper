﻿using System;
using System.Collections;
using System.Text;
using Ka.Garance.Data;
using System.Configuration;

namespace Ka.Garance.Protocol.Yuki
{

	// pracovni pozice
	public class YukiProtocolPozice
	{
		private decimal m_CenaCjJednotka = 0.0m;
		private int m_Id = -1;
		private int m_Cj = 0;
		private string m_Nazev;
		private string m_Operace;
		private bool m_Schvaleno;
		private YukiProtocol m_Protocol;
		private Hashtable m_Data = null;

		private decimal m_SavedCena;

		public YukiProtocolPozice(YukiProtocol protocol, int id, decimal savedCena, bool schvaleno)
		{
			m_Protocol = protocol;
			m_Id = id;
			m_SavedCena = savedCena;
			m_Schvaleno = schvaleno;

			if (m_Id != -1)
			{
				MySQLDataReader dr = protocol.Database.ExecuteReader("SELECT skodnicislo,pozicecislo,cj,nazev,operace FROM tcpozice WHERE id={0}", m_Id);
				dr.Read();
				m_Data = dr.GetRow();
			}

			object cena = m_Protocol.Database.ExecuteScalar("SELECT cenacj_nm FROM ndcenikprace WHERE idzeme={0} ORDER BY cenacj_nm DESC", m_Protocol.User.IdZeme);
			if (cena == null || cena is DBNull) m_CenaCjJednotka = 0.0m;
			else m_CenaCjJednotka = (decimal)Convert.ToDecimal(cena) / 100;
		}

		public int Id
		{
			get { return m_Id; }
		}

		public bool Schvaleno
		{
			get { return m_Schvaleno; }
		}

		public string Kod
		{
			get { return (m_Id == -1 ? "99" : Convert.ToString(m_Data["skodnicislo"])); }
		}

		public string KodPozice
		{
			get { return (m_Id == -1 ? "99" : Convert.ToString(m_Data["pozicecislo"])); }
		}

		public string Nazev
		{
			get { return (m_Id==-1?m_Nazev:Convert.ToString(m_Data["nazev"])); }
			set { m_Nazev = value; }
		}

		public string Operace
		{
			get { return (m_Id == -1 ? m_Operace : Convert.ToString(m_Data["operace"])); }
			set { m_Operace = value; }
		}

		public int Cj
		{
			get { return (m_Id == -1 ? m_Cj : Convert.ToInt32(m_Data["cj"])); }
			set { m_Cj = value; }
		}

		public decimal CjCenaJednotka
		{
			get { return m_CenaCjJednotka;  }
		}

		public decimal CjCena
		{
			get { return (decimal)Math.Round(Cj * CjCenaJednotka, m_Protocol.RecalcEUR ? 2 : 1); }
		}

		public decimal CjCenaDPH
		{
			get { return (decimal)Math.Round(Cj * CjCenaJednotka * m_Protocol.DPHConst, m_Protocol.RecalcEUR ? 2 : 1); }
		}

		public decimal SavedCenaDPH
		{
			get { return (decimal)Math.Round(m_Protocol.DPHConst * m_SavedCena, m_Protocol.RecalcEUR ? 2 : 1); }
		}

		internal void Save(int idzavadadealer, string kod)
		{
			if (m_Id == -1)
			{
				SaveVolnaPozice(idzavadadealer, kod);
				return;
			}

			string sql = m_Protocol.Database.FormatQuery(
			@"INSERT INTO tzpozice (idzavadadealer,cisloprotokol,cj,cena,idpozice,stav) VALUES
			({0},{1},{2},{3},{4},1)", idzavadadealer, m_Protocol.Cislo, Cj, CjCena, Id);

			m_Protocol.Database.Execute(sql);
		}

		private void SaveVolnaPozice(int idzavadadealer, string kod)
		{
			int idpozicedealer = Convert.ToInt32(m_Protocol.Database.ExecuteScalar("SELECT IFNULL(MAX(id),0)+1 FROM tzpozicevolna"));
			string sql = m_Protocol.Database.FormatQuery(
			@"INSERT INTO tzpozicevolna (idzavadadealer,cisloprotokol,skodnicislo,pozicecislo,nazev,operace,cj,cena,stav,idpozicedealer) VALUES
			({0},{1},{2},{3},{4},{5},{6},{7},1,{8})", idzavadadealer, m_Protocol.Cislo, kod, "9999", Nazev, Operace, Cj, CjCena, idpozicedealer);

			m_Protocol.Database.Execute(sql);
		}
	}

	public class YukiProtocolPoziceList
	{
		private ArrayList m_Items = new ArrayList();
		private YukiProtocol m_Protocol;

		public YukiProtocolPoziceList(YukiProtocol protocol)
		{
			m_Protocol = protocol;
		}

		public YukiProtocolPozice this[int index]
		{
			get { return m_Items[index] as YukiProtocolPozice; }
		}

		public int Count
		{
			get { return m_Items.Count; }
		}

		public YukiProtocolPozice Add(int id, decimal savedCena, bool schvaleno)
		{
			int idxVolna = -1;
			if (id != -1) {
				int idx = 0;
				foreach (YukiProtocolPozice p in m_Items)
				{
					if (p.Id == id)
						return p;
					else if (p.Id == -1 && idxVolna == -1)
					{
						idxVolna = idx;
						break;
					}
					else
						idx++;
				}
			}

			YukiProtocolPozice pp = new YukiProtocolPozice(m_Protocol, id, savedCena, schvaleno);

			// pridaj pred volne pozice !!!
			if (idxVolna != -1) m_Items.Insert(idxVolna, pp);
			else m_Items.Add(pp);

			return pp;
		}

		public YukiProtocolPozice Add(int id, string nazev, string operace, int cj, decimal savedCena, bool schvaleno)
		{
			YukiProtocolPozice p = Add(id, savedCena, schvaleno);
			p.Nazev = nazev;
			p.Operace = operace;
			p.Cj = cj;
			return p;
		}

		public void Delete(int index)
		{
			m_Items.RemoveAt(index);
		}

		public void Clear()
		{
			m_Items.Clear();
		}

	}

	// nahradne diely
	public class YukiProtocolND
	{
		private int m_Mnozstvi;
		private int m_Objednat;

		private int m_Id;
		private string m_SklCislo = string.Empty;
        private string m_ObjCislo = string.Empty;
		private decimal m_SavedCena;
		private bool m_Schvaleno;

		private YukiProtocol m_Protocol;

		private decimal m_CenaJednotka;

		public YukiProtocolND(YukiProtocol protocol, int id, int mnozstvi, int objednat, decimal savedCena, bool schvaleno)
		{
			m_Protocol = protocol;
			m_Id = id;
			m_Mnozstvi = mnozstvi;
			m_Objednat = objednat;
			m_SavedCena = savedCena;
			m_Schvaleno = schvaleno;

            ReadSklAndObjCislo(m_Id);

            ReadPrice();
		}

		public int Id
		{
			get { return m_Id; }
		}

		public string Kod
		{
			get
			{
				return Convert.ToString(m_Protocol.Database.ExecuteScalar("SELECT CONCAT(skupina,'.',pozice) AS kod FROM tcskodnicisla WHERE id={0}", m_Id));
			}
		}

		public string KodSkupina
		{
			get
			{
				return Convert.ToString(m_Protocol.Database.ExecuteScalar("SELECT skupina AS kod FROM tcskodnicisla WHERE id={0}", m_Id));
			}
		}

		public string KodPozice
		{
			get
			{
				return Convert.ToString(m_Protocol.Database.ExecuteScalar("SELECT pozice AS kod FROM tcskodnicisla WHERE id={0}", m_Id));
			}
		}

		public string SklCislo
		{
			get { return m_SklCislo; }
		}

		public string Nazev
		{
			get
			{
				return Convert.ToString(m_Protocol.Database.ExecuteScalar(string.Format("SELECT nazevdilu{0} FROM tcskodnicisla WHERE id={1}", (m_Protocol.User.Language.UseEnglishNames?"_en":""), this.Id)));
			}
		}

		public bool Schvaleno
		{
			get { return m_Schvaleno; }
		}

		public int Mnozstvi
		{
			get { return m_Mnozstvi; }
			set { m_Mnozstvi = value; }
		}

		public int Objednat
		{
			get { return m_Objednat; }
			set { m_Objednat = value; }
		}

		public decimal CenaJednoka
		{
			get { return m_CenaJednotka; }
		}

		public decimal CenaJednotkaDPH
		{
			get
			{
				return CenaJednoka * m_Protocol.DPHRabatConst;
			}
		}

		public decimal Cena
		{
			get { return Math.Round(CenaJednoka * m_Mnozstvi, m_Protocol.RecalcEUR ? 2 : 1); }
		}

		public decimal CenaDPH
		{
			get { return Math.Round(CenaJednoka * m_Mnozstvi * m_Protocol.DPHRabatConst, m_Protocol.RecalcEUR ? 2 : 1); }
		}

		public decimal SavedCenaDPH
		{
			get { return Math.Round(m_SavedCena * m_Protocol.DPHRabatConst, m_Protocol.RecalcEUR ? 2 : 1); }
		}

		internal void Save(int idzavadadealer)
		{
			decimal cenaNakup = 0;
			try { cenaNakup = Convert.ToDecimal(m_Protocol.Database.ExecuteScalar("SELECT cenanakup FROM ndsklad WHERE SklCislo={0}", m_SklCislo)); }
			catch { }

			string sql = m_Protocol.Database.FormatQuery(
				@"INSERT INTO tzdil (idzavadadealer,cisloprotokol,mnozstvi,mnozstviobj,cena,iddil,stav,updated,cenanakup) VALUES
				({0},{1},{2},{3},{4},{5},{6},{7},{8})",
				idzavadadealer, m_Protocol.Cislo, this.Mnozstvi, this.Objednat, this.Cena, m_SklCislo, 1, 0, cenaNakup
			);
			m_Protocol.Database.Execute(sql);
		}

        private void ReadSklAndObjCislo(int id)
        {
            MySQLDataReader dr = m_Protocol.Database.ExecuteReader("SELECT sklcislo, objcislo FROM tcskodnicisla WHERE id={0} LIMIT 1", id);

            m_SklCislo = string.Empty;
            m_ObjCislo = string.Empty;

            while (dr.Read())
            {
                if (!(dr.GetRow()[0] is DBNull))
                    m_SklCislo = dr.GetStringValue(0);

                if (!(dr.GetRow()[1] is DBNull))
                    m_ObjCislo = dr.GetStringValue(1);
            }
        }

        private void ReadPrice()
        {
            object cena = 0.0m;

            //Zjisti cenu podle objednaciho cisla, pokus je cena 0, zkus nacist cenu podle sklcisla
            if (m_ObjCislo != string.Empty)
                cena = m_Protocol.Database.ExecuteScalar("SELECT nm AS cena FROM ndcenik WHERE idzeme={0} AND sklcislo={1} LIMIT 1", m_Protocol.User.IdZeme, m_ObjCislo);

            if (cena == null || cena is DBNull) 
                    cena = 0.0m;

            if (Convert.ToDecimal(cena) == 0.0m)
            {
                if (m_SklCislo != string.Empty)
                    cena = m_Protocol.Database.ExecuteScalar("SELECT nm AS cena FROM ndcenik WHERE idzeme={0} AND sklcislo={1} LIMIT 1", m_Protocol.User.IdZeme, m_SklCislo);

                if (cena == null || cena is DBNull) 
                    cena = 0.0m;
            }

            m_CenaJednotka = Convert.ToDecimal(cena);
        }
	}

	public class YukiProtocolNDList
	{
		private ArrayList m_Items = new ArrayList();
		private YukiProtocol m_Protocol;

		public YukiProtocolNDList(YukiProtocol protocol)
		{
			m_Protocol = protocol;
		}

		public int Count
		{
			get { return m_Items.Count; }
		}

		public YukiProtocolND this[int index]
		{
			get { return m_Items[index] as YukiProtocolND; }
		}

		public YukiProtocolND Add(int id, int mnozstvi, int objednat, decimal savedCena, bool schvaleno)
		{
			foreach (YukiProtocolND nd in m_Items)
			{
				if (nd.Id == id)
				{
					nd.Mnozstvi = mnozstvi;
					nd.Objednat = objednat;
					return nd;
				}
			}

			YukiProtocolND p = new YukiProtocolND(m_Protocol, id, mnozstvi, objednat, savedCena, schvaleno);
			m_Items.Add(p);
			return p;
		}

		public void Delete(int index)
		{
			m_Items.RemoveAt(index);
		}

		public void Clear()
		{
			m_Items.Clear();
		}

	}

	// zavada
	public class YukiProtocolZavada
	{
		private string m_Kod;
		private string m_DZ;
		private string m_Vyrobce;
		private string m_Nazev;
		private string m_Popis;
		private bool m_Schvaleno;
		internal int m_Id = -1;

		private YukiProtocol m_Protocol;
		private YukiProtocolPoziceList m_Pozice;
		private YukiProtocolNDList m_Nd;

		public YukiProtocolZavada(YukiProtocol protocol, string kod, bool schvaleno)
		{
			try
			{
				if (kod[2] != '.')
					kod = kod.Insert(2, ".");
			}
			catch { }

			m_Protocol = protocol;
			m_Pozice = new YukiProtocolPoziceList(protocol);
			m_Nd = new YukiProtocolNDList(protocol);
			m_Kod = kod;
			m_Schvaleno = schvaleno;
		}

		public int Id
		{
			get { return m_Id; }
		}

		public YukiProtocolPoziceList Pozice
		{
			get { return m_Pozice; }
		}

		public YukiProtocolNDList Nd
		{
			get { return m_Nd; }
		}

		public string Kod
		{
			get { return m_Kod; }
		}

		public string KodNazev
		{
			get
			{
				string sql = string.Format("SELECT nazevdilu{0} FROM tcskodnicisla WHERE skupina={{0}} AND pozice={{1}} LIMIT 1", (m_Protocol.User.Language.UseEnglishNames ? "_en" : ""));
				return Convert.ToString(m_Protocol.Database.ExecuteScalar(sql, KodSkupina, KodPozice));
			}
		}

		public string KodSkupina
		{
			get
			{
				try
				{
					return m_Kod.Split(".".ToCharArray())[0];
				}
				catch { }
				return "01";
			}
		}

		public string KodPozice
		{
			get
			{
				try
				{
					return m_Kod.Split(".".ToCharArray(), 2)[1];
				}
				catch { }
				return "1";
			}
		}

		public string DZ
		{
			get { return m_DZ; }
			set { m_DZ = value; }
		}

		public bool Schvaleno
		{
			get { return m_Schvaleno; }
		}

		public string DZTitle
		{
			get { return Convert.ToString(m_Protocol.Database.ExecuteScalar("SELECT jmeno FROM tcdruhzavady WHERE id={0}", DZ)); }
		}

		public string Vyrobce
		{
			get { return m_Vyrobce; }
			set { m_Vyrobce = value; }
		}

		public string Nazev
		{
			get { return m_Nazev; }
			set { m_Nazev = value; }
		}

		public string Popis
		{
			get { return m_Popis; }
			set { m_Popis = value; }
		}

		public int CelkemCj
		{
			get
			{
				int cj = 0;
				for (int i = 0; i < Pozice.Count; i++)
					cj += Pozice[i].Cj;
				return cj;
			}
		}

		public decimal CelkemCjCena
		{
			get
			{
				decimal cjCena = 0;
				for (int i = 0; i < Pozice.Count; i++)
					cjCena += Pozice[i].CjCena;
				return cjCena;
			}
		}

		public decimal CelkemCjCenaDPH
		{
			get
			{
				decimal cjCena = 0;
				for (int i = 0; i < Pozice.Count; i++)
					cjCena += Pozice[i].CjCenaDPH;
				return cjCena;
			}
		}

		public decimal CelkemCjSavedCenaDPH
		{
			get
			{
				if (!m_Schvaleno) return 0;
				decimal cjCena = 0;
				for (int i = 0; i < Pozice.Count; i++)
					if (Pozice[i].Schvaleno)
						cjCena += Pozice[i].SavedCenaDPH;
				return cjCena;
			}
		}

		public decimal CelkemCena
		{
			get
			{
				decimal cena = 0.0m;
				for (int i = 0; i < Nd.Count; i++)
				{
					cena += Nd[i].Cena;
				}
				return cena;
			}
		}

		public decimal CelkemCenaDPH
		{
			get
			{
				decimal cena = 0.0m;
				for (int i = 0; i < Nd.Count; i++)
				{
					cena += Nd[i].CenaDPH;
				}
				return cena;
			}
		}

		public decimal CelkemSavedCenaDPH
		{
			get
			{
				if (!m_Schvaleno) return 0;
				decimal cena = 0.0m;
				for (int i = 0; i < Nd.Count; i++)
					if (Nd[i].Schvaleno)
						cena += Nd[i].SavedCenaDPH;
				return cena;
			}
		}

		internal void Save(int poradi, int idprotokol)
		{
			if (m_Protocol.Cislo == null) return;

			int idzavadadealer = Convert.ToInt32(m_Protocol.Database.ExecuteScalar("SELECT IFNULL(MAX(id), 0)+1 FROM tzavada"));

			string sql = m_Protocol.Database.FormatQuery(
				@"INSERT INTO tzavada (cisloprotokol,idzavadadealer,idprotokoldealer,poradi,kod,druh,vyrobce,zavada,popis,prace,material) VALUES
				({0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10})",
				m_Protocol.Cislo, idzavadadealer, idprotokol, poradi, this.Kod.Replace(".", ""), this.DZ, this.Vyrobce, this.Nazev, this.Popis, this.CelkemCjCena, this.CelkemCena
			);
			m_Protocol.Database.Execute(sql);

			for (int i = 0; i < m_Pozice.Count; i++)
				m_Pozice[i].Save(idzavadadealer, this.Kod);

			for (int i = 0; i < m_Nd.Count; i++)
				m_Nd[i].Save(idzavadadealer);

		}

		internal void Load(string cislo, int idzavadadealer)
		{
			Nd.Clear();
			Pozice.Clear();


			// nahradne diely
			MySQLDataReader dr = m_Protocol.Database.ExecuteReader("SELECT iddil,mnozstvi,IFNULL(mnozstviobj,0) AS mnozstviobj,cena,stav FROM tzdil WHERE cisloprotokol={0} AND idzavadadealer={1} ORDER BY id", cislo, idzavadadealer);
			while (dr.Read())
			{
				Nd.Add(
					Convert.ToInt32(m_Protocol.Database.ExecuteScalar("SELECT IFNULL(id,0) FROM tcskodnicisla WHERE sklcislo={0} ORDER BY state ASC LIMIT 1", dr.GetValue("iddil"))),
					Convert.ToInt32(dr.GetValue("mnozstvi")),
					Convert.ToInt32(dr.GetValue("mnozstviobj")),
					(decimal)Convert.ToDecimal(dr.GetValue("cena")),
                    (Convert.ToInt32(dr.GetValue("stav")) == 1 ? true : false)
				);
			}
			// pozice
			dr = m_Protocol.Database.ExecuteReader("SELECT idpozice,cena,stav FROM tzpozice WHERE cisloprotokol={0} AND idzavadadealer={1} ORDER BY id", cislo, idzavadadealer);
			while (dr.Read())
			{
				Pozice.Add(
					dr.GetIntValue(0),
					(decimal)Convert.ToDecimal(dr.GetValue("cena")),
                    (Convert.ToInt32(dr.GetValue("stav")) == 1 ? true : false)
				);
			}
			// volne pozice
			dr = m_Protocol.Database.ExecuteReader("SELECT nazev,operace,cj,cena,stav FROM tzpozicevolna WHERE cisloprotokol={0} AND idzavadadealer={1} ORDER BY id", cislo, idzavadadealer);
			while (dr.Read())
			{
				Pozice.Add(-1,
					Convert.ToString(dr.GetValue("nazev")),
					Convert.ToString(dr.GetValue("operace")),
					Convert.ToInt32(dr.GetValue("cj")),
					(decimal)Convert.ToDecimal(dr.GetValue("cena")),
					(Convert.ToInt32(dr.GetValue("stav")) == 1 ? true : false)
				);
			}
		}

	}

	public class YukiProtocolZavadaList
	{
		private int m_SelectedIndex = -1;
		private ArrayList m_Items = new ArrayList();
		private YukiProtocol m_Protocol;

		public YukiProtocolZavadaList(YukiProtocol protocol)
		{
			m_Protocol = protocol;
		}

		public YukiProtocolZavada this[int index]
		{
			get { return m_Items[index] as YukiProtocolZavada; }
		}

		public int Count
		{
			get { return m_Items.Count; }
		}

		public int SelectedIndex
		{
			get { return m_SelectedIndex; }
			set { m_SelectedIndex = Math.Max(-1, Math.Min(Count - 1, value)); }
		}

		public YukiProtocolZavada Add(string kod, bool schvaleno)
		{
			YukiProtocolZavada ppz = new YukiProtocolZavada(m_Protocol, kod, schvaleno);
			m_Items.Add(ppz);
			return ppz;
		}

		public YukiProtocolZavada Add(int id, string kod, string dz, string vyrobce, string nazev, string popis, bool schvaleno)
		{
			YukiProtocolZavada z = Add(kod, schvaleno);
			z.m_Id = id;
			z.DZ = dz;
			z.Vyrobce = vyrobce;
			z.Nazev = nazev;
			z.Popis = popis;
			return z;
		}

		public YukiProtocolZavada GetById(int id)
		{
			for (int i = 0; i < this.Count; i++)
				if (this[i].Id == id)
					return this[i];
			return null;
		}

		public void Delete(int index)
		{
			m_Items.RemoveAt(index);
		}

		public void Clear()
		{
			m_Items.Clear();
		}
	}

	// yuki protokol
	public class YukiProtocol : IGaranceProtocol
	{
		public readonly static DateTime DATE_NEW_RECOUNT = new DateTime(2007, 5, 30);
        public readonly static DateTime DATE_NEW_RECOUNT_2012 = new DateTime(2012, 6, 4);
        public readonly static DateTime DATE_NEW_RECOUNT_2012_10 = new DateTime(2012, 10, 1);
        public readonly static DateTime DATE_NEW_RECOUNT_2013 = new DateTime(2013, 1, 1);
		public readonly static DateTime DATE_SK_EURO = new DateTime(2009, 1, 1);

		private string m_Cislo = null;

		private DateTime m_DateCreated;		// datum, ked bol protokol vytvoreny
		private DateTime m_DateRecalc;		// datum prepoctu ceny protokolu

		// Vozidlo
		private DateTime m_DateRepair;		// datum opravy
        private bool m_DateRepairOverflow = false;  // bylo datum opravy nastaveno v rozporu s obchodnimi podminkami?
		private string m_VIN;				// id vozidla
		private bool m_PredprodejniServis = false; // pps

		// Majitel
		private int m_JeFO = 0;				// je / neni fyzicka osoba (0 FO, 1 FO ICO, 2 PO ICO)
		private string m_Jmeno;				// meno & priezvisko majitela
		private string m_Telefon;			// telefon
		private string m_Adresa;			// adresa
		private string m_PSC;				// PSC
		private string m_Obec;				// mesto / obec
		private string m_ICO;				// ICO

		// Pocet KM
		private int m_Km = -1;				// pocet najazdenych KM
		private bool m_VymenaTacho = false;	// vymena tachometra		

		// Zavady
		private YukiProtocolZavadaList m_Zavady;
		private MySQLDatabase m_Database;
		private Ka.Garance.User.GaranceUser m_User;

		// Ext info
		private string m_DZ;
		private string m_KO;
        private string m_Resitel = ConfigurationManager.AppSettings["Protocol.Worker"];
        private string m_Tel = ConfigurationManager.AppSettings["Protocol.WorkerPhone"];
        private string m_Fax = ConfigurationManager.AppSettings["Protocol.WorkerFax"];

		private string m_Zprava = "";

		// stav
		private int m_Stav = 0;

		// dph, rabat
		private decimal m_Rabat = 0.0m;
		private decimal m_DPH = 0.0m;

		internal bool RecalcEUR
		{
			get { return this.User.IdZeme == 2 && this.DateCreated >= DATE_SK_EURO; }
		}


		public YukiProtocol(MySQLDatabase db, Ka.Garance.User.GaranceUser user)
		{
			m_Database = db;
			m_User = user;
			m_DateCreated = DateTime.Now;
			m_DateRepair = DateTime.Now;
			m_DateRecalc = DateTime.Now;
			m_Zavady = new YukiProtocolZavadaList(this);
		}

		public YukiProtocol(MySQLDatabase db, Ka.Garance.User.GaranceUser user, string cislo)
			: this(db, user)
		{
			Load(cislo);
		}

		public MySQLDatabase Database
		{
			get { return m_Database; }
		}

		public Ka.Garance.User.GaranceUser User
		{
			get { return m_User; }
		}

		public YukiProtocolZavadaList Zavady
		{
			get { return m_Zavady; }
		}

		public DateTime DateCreated
		{
			get { return m_DateCreated; }
		}

		public DateTime DateRepair
		{
			get { return m_DateRepair; }
			set { m_DateRepair = value; }
		}

        public bool DateRepairOverflow
        {
            get { return m_DateRepairOverflow; }
            set { m_DateRepairOverflow = value; }
        }

		public DateTime DateSold
		{
			get { return Convert.ToDateTime(Database.ExecuteScalar("SELECT CAST(IF(IFNULL(prodej_datum,'')='',NULL,CONCAT('20',MID(prodej_datum,5,2),'-',MID(prodej_datum,3,2),'-',LEFT(prodej_datum,2))) AS DATE) AS datum FROM tmotocykl WHERE ciskar={0}", m_VIN)); }
		}

		public string Cislo
		{
			get { return m_Cislo; }
		}

		public bool JeNovyProtokol
		{
			get { return (m_Cislo == null || m_Cislo.Trim() == ""); }
		}

		public string NazevProtokolu
		{
			get
			{
				if (m_Cislo == null || m_Cislo.Trim() == "")
					return m_User.Language["HEADER_PROTOCOL_ADD"];
				else
					return string.Format("{0}: {1}", m_User.Language["PROTO_NUMBER"], m_Cislo);
			}
		}

		public string VIN
		{
			get { return m_VIN; }
			set	{ if (m_VIN != value) ChangeVIN(value); }
		}

		public string ModelId
		{
			get
			{
				return Convert.ToString(Database.ExecuteScalar("SELECT tmotocykl.model FROM tmotocykl WHERE ciskar={0}", VIN));
			}
		}

		public string ModelIdVzor
		{
			get
			{
				string model = ModelId;
				string vzor = Convert.ToString(Database.ExecuteScalar("SELECT vzor FROM tcmodelklic WHERE id={0}", model));
				if (vzor != null && vzor.Trim() != "")
				{
					if (Convert.ToInt32(Database.ExecuteScalar("SELECT COUNT(*) FROM tcskodnicisla WHERE skupina IS NOT NULL AND typmoto={0}", vzor)) > 0)
						return vzor;
				}
				return model;
			}
		}

        
        public string ModelIdVzorMotor
        {
            get
            {
                string model = ModelId;
                string vzor = Convert.ToString(Database.ExecuteScalar("SELECT vzormotor FROM tcmodelklic WHERE id={0}", model));
                if (vzor != null && vzor.Trim() != "")
                {
                    if (Convert.ToInt32(Database.ExecuteScalar("SELECT COUNT(*) FROM tcskodnicisla WHERE skupina IS NOT NULL AND typmoto={0}", vzor)) > 0)
                        return vzor;
                }
                return model;
            } 
        }

		public string Model
		{
			get
			{
				return Convert.ToString(Database.ExecuteScalar("SELECT CONCAT(tmotocykl.model,' - ',tcmodelklic.jmeno) AS jmeno FROM tmotocykl LEFT JOIN tcmodelklic ON tcmodelklic.id=tmotocykl.model WHERE ciskar={0}", VIN));
			}
		}

		public string ModelNazev
		{
			get
			{
				return Convert.ToString(Database.ExecuteScalar("SELECT tcmodelklic.jmeno AS jmeno FROM tmotocykl LEFT JOIN tcmodelklic ON tcmodelklic.id=tmotocykl.model WHERE ciskar={0}", VIN));
			}
		}

		public string Jmeno
		{
			get { return m_Jmeno; }
			set { m_Jmeno = value; }
		}

		public string Telefon
		{
			get { return m_Telefon; }
			set { m_Telefon = value; }
		}

		public string ICO
		{
			get { return m_ICO; }
			set { m_ICO = value; }
		}

		public string Adresa
		{
			get { return m_Adresa; }
			set { m_Adresa = value; }
		}

		public string PSC
		{
			get { return m_PSC; }
			set { m_PSC = value; }
		}

		public string Obec
		{
			get { return m_Obec; }
			set { m_Obec = value; }
		}

		public int Km
		{
			get { return m_Km; }
			set { m_Km = value; }
		}

		public bool VymenaTachometra
		{
			get { return m_VymenaTacho; }
			set { m_VymenaTacho = value; }
		}

		public int JeFO
		{
			get { return this.m_JeFO; }
			set { this.m_JeFO = value; }
		}

		public string DZ
		{
			get { return m_DZ; }
			set { m_DZ = value; }
		}

		public string DZTitle
		{
			get
			{
				return Convert.ToString(Database.ExecuteScalar(string.Format("SELECT CONCAT(id,' - ',nazev{0}) FROM tcdz WHERE id={{0}}", (User.Language.UseEnglishNames ? "_en" : "")), DZ));
			}
		}

		public string KO
		{
			get { return m_KO; }
			set { m_KO = value; }
		}

		public string KOTitle
		{
			get
			{
				return Convert.ToString(Database.ExecuteScalar(string.Format("SELECT CONCAT(id,' - ',nazev{0}) FROM tcko WHERE id={{0}}", (User.Language.UseEnglishNames ? "_en" : "")), KO));
			}
		}

		public string Resitel
		{
			get { return m_Resitel; }
			set { m_Resitel = value; }
		}

		public string ResitelTel
		{
			get { return m_Tel; }
			set { m_Tel = value; }
		}

		public string ResitelFax
		{
			get { return m_Fax; }
			set { m_Fax = value; }
		}

		public string Zprava
		{
			get { return m_Zprava; }
			set { m_Zprava = value; }
		}

		public int Stav
		{
			get { return m_Stav; }
			set { if (m_Stav == 0 && value == 1) m_Stav = 1; }
		}

		public bool Editovatelny
		{
			get { return (m_Stav <= 2); }
		}

		public bool Rozpracovany
		{
			get { return (m_Stav == 0); }
			set
			{
				if (value && m_Stav <= 2) m_Stav = 0;
				else if (!value && m_Stav == 0) m_Stav = 1;
			}
		}

		public decimal Rabat
		{
			get
			{
				if (m_Cislo == null || m_Stav < 4) return User.Rabat;
				return m_Rabat;
			}
		}

		public decimal DPH
		{
			get
			{
				if (m_Cislo == null || m_Stav < 4) return User.DPH;
				return m_DPH;
			}
		}

		public decimal DPHConst
		{
			get
			{
				return (100 + DPH) / 100;
			}
		}

		public decimal DPHRabatConst
		{
			get
			{
				// old
				// rabat (0.95) * dph (1.19)
				// return (((100 + DPH) / 100) * ((100 - Rabat) / 100));
				if (m_DateRecalc < YukiProtocol.DATE_NEW_RECOUNT)
					return (((100 - Rabat) / 100) * ((100 + DPH) / 100));

				// new
				// sleva (0.75) * rabat (1.05) * dph (1.19)
                if (m_DateRecalc < YukiProtocol.DATE_NEW_RECOUNT_2012)
				    return (0.75m * ((100 + Rabat) / 100) * ((100 + DPH) / 100));

                // new 2012
                // sleva 65% (0.35) * rabat (1.05) * dph (1.19)
                if (m_DateRecalc < YukiProtocol.DATE_NEW_RECOUNT_2012_10)
                    return (0.35m * ((100 + Rabat) / 100) * ((100 + DPH) / 100));

                // new 2012/10
                // sleva 39,5% (0.605) * rabat (1.05) * dph (1.19)
                if (m_DateRecalc < YukiProtocol.DATE_NEW_RECOUNT_2013)
                    return (0.605m * ((100 + Rabat) / 100) * ((100 + DPH) / 100));

                // new 2013
                // sleva 30% (0.7) * rabat (1.05) * dph (1.21)
                return (0.7m * ((100 + Rabat) / 100) * ((100 + DPH) / 100));
			}
		}

		public decimal CelkemCjSavedCenaDPH
		{
			get
			{
				decimal cena = 0;
				for (int i = 0; i < Zavady.Count; i++)
					cena += Zavady[i].CelkemCjSavedCenaDPH;
				return cena;
			}
		}

		public decimal CelkemSavedCenaDPH
		{
			get
			{
				decimal cena = 0;
				for (int i = 0; i < Zavady.Count; i++)
					cena += Zavady[i].CelkemSavedCenaDPH;
				return cena;
			}
		}

		public bool HasValidPrices
		{
			get
			{
				for (int i = 0; i < Zavady.Count; i++)
				{
					for (int j = 0; j < Zavady[i].Nd.Count; j++)
					{
						if (Zavady[i].Nd[j].CenaJednoka <= 0)
							return false;
					}
				}
				return true;
			}
		}

		public bool PredprodejniServis
		{
			get { return m_PredprodejniServis; }
			set { m_PredprodejniServis = value; }
		}

		public bool SendForReview()
		{
			if (Editovatelny && m_Cislo != null && m_Cislo.Trim() != "")
			{
				Database.Execute("UPDATE tprotocol SET stav=1 WHERE cislo={0} AND iddealer={1}", this.Cislo, this.User.Id);
				m_Stav = 1;
				return true;
			}
			return false;
		}

		#region IGaranceProtocol members
		public void Load(object id)
		{
			MySQLDataReader dr = Database.ExecuteReader(@"
SELECT jmeno,telefon,ulice,psc,obec,jeFO,km,karoserie AS vin,CAST(IF(IFNULL(datumoprava,'')='',NULL,
CONCAT('20',MID(datumoprava,5,2),'-',MID(datumoprava,3,2),'-',LEFT(datumoprava,2))) AS DATE) AS datum_opravy,
ko,dz,resitel,tel,fax,stav,timestamp,IFNULL(dphfa,0) AS dph,IFNULL(rabatfa,0) AS rabat,zprava,ico,datumfa FROM tprotocol WHERE cislo={0}", id);
			dr.Read();

			m_Cislo = Convert.ToString(id);

			m_DateCreated = Convert.ToDateTime(dr.GetValue("timestamp"));
			m_DateRepair = Convert.ToDateTime(dr.GetValue("datum_opravy"));
			m_VIN = Convert.ToString(dr.GetValue("vin"));
			m_Jmeno = Convert.ToString(dr.GetValue("jmeno"));
			m_Telefon = Convert.ToString(dr.GetValue("telefon"));
			m_ICO = Convert.ToString(dr.GetValue("ico"));
			m_Adresa = Convert.ToString(dr.GetValue("ulice"));
			m_PSC = Convert.ToString(dr.GetValue("psc"));
			m_Obec = Convert.ToString(dr.GetValue("obec"));
			m_Km = Convert.ToInt32(dr.GetValue("km"));
			m_VymenaTacho = false;
			m_JeFO = Convert.ToInt32(dr.GetValue("jeFO"));
			m_KO = Convert.ToString(dr.GetValue("ko"));
			m_DZ = Convert.ToString(dr.GetValue("dz"));
			m_Resitel = Convert.ToString(dr.GetValue("resitel"));
			m_Tel = Convert.ToString(dr.GetValue("tel"));
			m_Fax = Convert.ToString(dr.GetValue("fax"));
			m_Stav = Convert.ToInt32(dr.GetValue("stav"));
			m_Zprava = Convert.ToString(dr.GetValue("zprava"));

			m_DateRecalc = m_DateCreated;

			object df = dr.GetValue("datumfa");
			if (df != null && !(df is DBNull))
				try { m_DateRecalc = Convert.ToDateTime(df); }
				catch { }

			if (m_Stav == 0)
			{
				m_DPH = User.DPH;
				m_Rabat = User.Rabat;
			}
			else
			{
				m_DPH = (decimal)Convert.ToDecimal(dr.GetValue("dph"));
				m_Rabat = (decimal)Convert.ToDecimal(dr.GetValue("rabat"));
			}

			m_Zavady.Clear();

			// nahraj zavady
			dr = Database.ExecuteReader("SELECT kod,druh,vyrobce,zavada,popis,prace,material,idzavadadealer,id,stav FROM tzavada WHERE cisloprotokol={0} ORDER BY poradi", m_Cislo);
			while (dr.Read())
			{
                bool state = false;
                if (dr.GetValue("stav") is int)
                    if ((int)dr.GetValue("stav") == 1)
                        state = true;
                if (dr.GetValue("stav") is bool)
                    state = (bool)dr.GetValue("stav");
                        

				m_Zavady.Add(
					Convert.ToInt32(dr.GetValue("id")),
					Convert.ToString(dr.GetValue("kod")),
					Convert.ToString(dr.GetValue("druh")),
					Convert.ToString(dr.GetValue("vyrobce")),
					Convert.ToString(dr.GetValue("zavada")),
					Convert.ToString(dr.GetValue("popis")),
					state
				).Load(m_Cislo, Convert.ToInt32(dr.GetValue("idzavadadealer")));
			}

			m_PredprodejniServis = (!IsProdanyVIN);
		}

		public void Delete()
		{
			if (m_Cislo == null || m_Cislo.Trim() == "") return;
			Database.Execute("DELETE FROM tprotocol WHERE cislo={0}", m_Cislo);
			Database.Execute("DELETE FROM tzavada WHERE cisloprotokol={0}", m_Cislo);
			Database.Execute("DELETE FROM tzpozicevolna WHERE cisloprotokol={0}", m_Cislo);
			Database.Execute("DELETE FROM tzpozice WHERE cisloprotokol={0}", m_Cislo);
			Database.Execute("DELETE FROM tzdil WHERE cisloprotokol={0}", m_Cislo);
		}

		public void Save()
		{
			if (!Editovatelny && m_Cislo != null)
				throw new Exception(string.Format("Unable to process protocol - save disabled for state {0}", m_Stav));

			if (m_Cislo == null)
				m_DateCreated = DateTime.Now;

			// zisti, ci mozem ulozit do DB
			bool increment = false;
			if (m_Cislo != null)
			{
				if (Convert.ToInt32(Database.ExecuteScalar("SELECT COUNT(*) FROM tprotocol WHERE cislo={0}", m_Cislo)) != 1)
					return;
				Delete();
			}
			else
			{
				m_Cislo = GetLastNum();
				m_DPH = User.DPH;
				m_Rabat = User.Rabat;
				increment = true;
			}

			if (m_Stav == 2) m_Stav = 1;

			if (m_Zprava == null) m_Zprava = "";
			else m_Zprava = m_Zprava.TrimEnd(null);

			int idd = Convert.ToInt32(Database.ExecuteScalar("SELECT IFNULL(MAX(id),0)+1 FROM tprotocol WHERE iddealer={0}", m_User.Id));
			string sql = Database.FormatQuery(@"
				INSERT INTO tprotocol (cislo,idprotokoldealer,iddealer,jmeno,ulice,psc,obec,km,karoserie,datumoprava,ko,dz,resitel,tel,fax,kulprace,kuldily,kulcizi,kuluvolneni,stav,timestamp,timestamp_sync,zprava,sync,dphfa,rabatfa,mena,koef,datumoprava_d,telefon,jeFO,ico) VALUES
				({0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},0,0,0,0,{15},{18},{18},{22},0,{19},{20},{16},NULL,{17},{21},{23},{24})
			", m_Cislo, idd, m_User.Id, m_Jmeno, Adresa, PSC, Obec, Km, VIN, Garance.User.GaranceUser.DateToUserDate(DateRepair), KO, DZ, Resitel, ResitelTel, ResitelFax, m_Stav, m_User.Currency, DateRepair, m_DateCreated, m_DPH, m_Rabat, m_Telefon, m_Zprava, m_JeFO, m_ICO);
			Database.Execute(sql);

			for (int i = 0; i < m_Zavady.Count; i++)
				m_Zavady[i].Save(i+1, idd);

			if (increment)
				Database.Execute("UPDATE tcprotocol SET LastNum = LastNum + 1");
		}
		#endregion

		private string GetLastNum()
		{
			string year = DateTime.Now.Year.ToString();
			int numBase = Convert.ToInt32(Database.ExecuteScalar("SELECT LastNum FROM tcprotocol"));
			return string.Format("{0}{1:D4}", year.Substring(year.Length-2), numBase + 1);
		}

		public void Clear()
		{
			ActualStep = 1;
			m_DateRepair = DateTime.Now;
			m_VIN = "";
			m_Jmeno = "";
			m_Telefon = "";
			m_ICO = "";
			m_Adresa = "";
			m_PSC = "";
			m_Obec = "";
			m_Km = -1;
			m_VymenaTacho = false;
			m_JeFO = 0;

			m_KO = "";
			m_DZ = "";

            m_Resitel = ConfigurationManager.AppSettings["Protocol.Worker"]; //Zdeněk Kácha
            m_Tel = ConfigurationManager.AppSettings["Protocol.WorkerPhone"]; // "+420 215 159 333";
            m_Fax = ConfigurationManager.AppSettings["Protocol.WorkerFax"]; // "+420 215 159 666";
			m_Zprava = "";

			m_Zavady.Clear();
			m_User.Cache.ClearSub("protocol.[add]");
		}

		private void ChangeVIN(string newVIN)
		{
			m_VIN = newVIN;
			m_Zavady.Clear();
		}

		public bool IsValidVIN
		{
			get
			{
				if (m_PredprodejniServis)
					return (Convert.ToInt32(Database.ExecuteScalar("SELECT COUNT(*) FROM tmotocykl WHERE ciskar={0} AND iddealer={1}", VIN, User.Id)) > 0);
				return (Convert.ToInt32(Database.ExecuteScalar("SELECT COUNT(*) FROM tmotocykl WHERE ciskar={0}", VIN)) > 0);
			}
		}

		public bool IsProdanyVIN
		{
			get
			{
				return (Convert.ToInt32(Database.ExecuteScalar("SELECT COUNT(*) FROM tmotocykl WHERE ciskar={0} AND IFNULL(TRIM(prodej_datum),'')<>''", VIN)) > 0);
			}
		}

		public bool IsValidKm
		{
			get
			{
				if (m_Km < 0) return false;
				if (VymenaTachometra || Stav==2) return true;
				int povodnyStav = Convert.ToInt32(Database.ExecuteScalar("SELECT IFNULL(MAX(km),0) FROM tprotocol WHERE karoserie={0} AND cislo<>{1}", VIN, (m_Cislo == null ? "" : m_Cislo)));
				return (povodnyStav < Km);
			}
		}

		public bool KmMimoZaruky
		{
			get
			{
				bool zrusLimit = (Convert.ToInt32(Database.ExecuteScalar("SELECT zruslimit FROM tmotocykl WHERE ciskar={0}", m_VIN)) == 1);
				if (zrusLimit) return false;
				int zarukaKm = Convert.ToInt32(Database.ExecuteScalar("SELECT zarukakm FROM tcmodelklic WHERE id={0}", this.ModelIdVzor));
				return (Km > zarukaKm);
			}
		}

		public bool ZarukaVyprsela
		{
			get
			{
				return JePoZaruke(Database, m_VIN, this.DateRepair);
			}
		}

		public static bool JePoZaruke(MySQLDatabase db, string vin, DateTime datum)
		{
			MySQLDataReader dr = db.ExecuteReader(@"
					SELECT
						CAST(IF(IFNULL(tmotocykl.prodej_datum,'')='',NULL,CONCAT('20',MID(tmotocykl.prodej_datum,5,2),'-',MID(tmotocykl.prodej_datum,3,2),'-',LEFT(tmotocykl.prodej_datum,2))) AS DATE) AS datum,
						prodlzaruka,
                        majitel_ico
					FROM tmotocykl
					WHERE ciskar={0}
					ORDER BY idzeme DESC
				", vin);
			if (!dr.Read()) return true;
			DateTime prodano = DateTime.Now;
			int zaruka = 0;
            bool isPO = false;
			try
			{
				prodano = dr.GetDateValue(0);
				zaruka = dr.GetIntValue(1);
                isPO = dr.GetStringValue(2).Trim().Length > 0;
			}
			catch
			{
				return false;
			}
			try
			{
                if (zaruka > 0)
                    prodano = prodano.AddDays(zaruka);
                else if (isPO)
                    prodano = prodano.AddMonths(6);
                else
                    prodano = prodano.AddYears(2);
				return (prodano.Ticks < datum.Ticks);
			}
			catch { }
			return false;
		}

		public int ActualStep
		{
			get
			{
				int step = 1;
				try { step = m_User.Cache.GetInt32("protocol.[add][step]"); }
				catch { }
				return Math.Max(1, step);
			}
			set
			{
				m_User.Cache.SetInt32("protocol.[add][step]", value);
			}
		}

		public void LoadFromCache()
		{
			m_Cislo = m_User.Cache.GetString("protocol.[add][Cislo]");
			m_DateRepair = m_User.Cache.GetDateTime("protocol.[add][DR]");
            m_DateRepairOverflow = m_User.Cache.GetBool("protocol.[add][DROverflow]");
			m_VIN = m_User.Cache.GetString("protocol.[add][VIN]");
			m_Jmeno = m_User.Cache.GetString("protocol.[add][Jmeno]");
			m_Telefon = m_User.Cache.GetString("protocol.[add][Telefon]");
			m_ICO = m_User.Cache.GetString("protocol.[add][ICO]");
			m_Adresa = m_User.Cache.GetString("protocol.[add][Adresa]");
			m_PSC = m_User.Cache.GetString("protocol.[add][PSC]");
			m_Obec = m_User.Cache.GetString("protocol.[add][Obec]");
			m_JeFO = m_User.Cache.GetInt32("protocol.[add][JeFO]");
			m_Km = m_User.Cache.GetInt32("protocol.[add][Km]");
			m_VymenaTacho = m_User.Cache.GetBool("protocol.[add][VT]");
			m_KO = m_User.Cache.GetString("protocol.[add][KO]");
			m_DZ = m_User.Cache.GetString("protocol.[add][DZ]");
			m_Resitel = m_User.Cache.GetString("protocol.[add][Resitel]");
			m_Tel = m_User.Cache.GetString("protocol.[add][Tel]");
			m_Fax = m_User.Cache.GetString("protocol.[add][Fax]");
			m_Zprava = m_User.Cache.GetString("protocol.[add][Zprava]");
			m_DPH = m_User.Cache.GetDecimal("protocol.[add][Dph]");
			m_Rabat = m_User.Cache.GetDecimal("protocol.[add][Rabat]");
			m_PredprodejniServis = m_User.Cache.GetBool("protocol.[add][PPS]");

			if (m_Cislo == "") m_Cislo = null;

			m_Zavady.Clear();

			if (m_VIN == null)
			{
				m_DateRepair = DateTime.Now;
				m_Km = -1;
                m_Resitel = ConfigurationManager.AppSettings["Protocol.Worker"];
                m_Tel = ConfigurationManager.AppSettings["Protocol.WorkerPhone"];
                m_Fax = ConfigurationManager.AppSettings["Protocol.WorkerFax"];
			}
			else
			{
				int nZavada = m_User.Cache.GetInt32("protocol.[add][Zavady]");
				for (int i = 0; i < nZavada; i++)
				{
					YukiProtocolZavada z = m_Zavady.Add(
						-1,
						m_User.Cache.GetString(string.Format("protocol.[add][Zavady][{0}]", i)),
						m_User.Cache.GetString(string.Format("protocol.[add][Zavady][{0}][dz]", i)),
						m_User.Cache.GetString(string.Format("protocol.[add][Zavady][{0}][m]", i)),
						m_User.Cache.GetString(string.Format("protocol.[add][Zavady][{0}][n]", i)),
						m_User.Cache.GetString(string.Format("protocol.[add][Zavady][{0}][d]", i)),
						true
					);
					int nPozice = m_User.Cache.GetInt32(string.Format("protocol.[add][Zavady][{0}][pos]", i));
					for (int j = 0; j < nPozice; j++)
					{
						YukiProtocolPozice p = z.Pozice.Add(
							m_User.Cache.GetInt32(string.Format("protocol.[add][Zavady][{0}][pos][{1}][id]", i, j)),
							m_User.Cache.GetString(string.Format("protocol.[add][Zavady][{0}][pos][{1}][q]", i, j)),
							m_User.Cache.GetString(string.Format("protocol.[add][Zavady][{0}][pos][{1}][o]", i, j)),
							m_User.Cache.GetInt32(string.Format("protocol.[add][Zavady][{0}][pos][{1}][cj]", i, j)),
							0, true
						);
					}
					int nNd = m_User.Cache.GetInt32(string.Format("protocol.[add][Zavady][{0}][nd]", i));
					for (int j = 0; j < nNd; j++)
					{
						YukiProtocolND nd = z.Nd.Add(
							m_User.Cache.GetInt32(string.Format("protocol.[add][Zavady][{0}][nd][{1}][id]", i, j)),
							m_User.Cache.GetInt32(string.Format("protocol.[add][Zavady][{0}][nd][{1}][q]", i, j)),
							m_User.Cache.GetInt32(string.Format("protocol.[add][Zavady][{0}][nd][{1}][o]", i, j)),
							0, true
						);
					}
				}
				m_Zavady.SelectedIndex = m_User.Cache.GetInt32("protocol.[add][Zavady][SelectedIndex]");
			}
		}

		public void SaveToCache()
		{
			m_User.Cache.BeginUpdate();
			try
			{
				m_User.Cache.SetString("protocol.[add][Cislo]", m_Cislo);
				m_User.Cache.SetDateTime("protocol.[add][DR]", m_DateRepair);
                m_User.Cache.SetBool("protocol.[add][DROverflow]", m_DateRepairOverflow);
				m_User.Cache.SetString("protocol.[add][VIN]", m_VIN);
				m_User.Cache.SetString("protocol.[add][Jmeno]", m_Jmeno);
				m_User.Cache.SetString("protocol.[add][Telefon]", m_Telefon);
				m_User.Cache.SetString("protocol.[add][ICO]", m_ICO);
				m_User.Cache.SetString("protocol.[add][Adresa]", m_Adresa);
				m_User.Cache.SetString("protocol.[add][PSC]", m_PSC);
				m_User.Cache.SetString("protocol.[add][Obec]", m_Obec);
				m_User.Cache.SetInt32("protocol.[add][JeFO]", m_JeFO);
				m_User.Cache.SetInt32("protocol.[add][Km]", m_Km);
				m_User.Cache.SetBool("protocol.[add][VT]", m_VymenaTacho);
				m_User.Cache.SetString("protocol.[add][KO]", m_KO);
				m_User.Cache.SetString("protocol.[add][DZ]", m_DZ);
				m_User.Cache.SetString("protocol.[add][Resitel]", m_Resitel);
				m_User.Cache.SetString("protocol.[add][Tel]", m_Tel);
				m_User.Cache.SetString("protocol.[add][Fax]", m_Fax);
				m_User.Cache.SetString("protocol.[add][Zprava]", m_Zprava);
				m_User.Cache.SetDouble("protocol.[add][Dph]", (double)m_DPH);
				m_User.Cache.SetDouble("protocol.[add][Rabat]", (double)m_Rabat);
				m_User.Cache.SetBool("protocol.[add][PPS]", m_PredprodejniServis);

				// zavady
				m_User.Cache.SetInt32("protocol.[add][Zavady]", m_Zavady.Count);
				m_User.Cache.SetInt32("protocol.[add][Zavady][SelectedIndex]", m_Zavady.SelectedIndex);
				for (int i = 0; i < m_Zavady.Count; i++)
				{
					m_User.Cache.SetString(string.Format("protocol.[add][Zavady][{0}]", i), m_Zavady[i].Kod);
					m_User.Cache.SetString(string.Format("protocol.[add][Zavady][{0}][DZ]", i), m_Zavady[i].DZ);
					m_User.Cache.SetString(string.Format("protocol.[add][Zavady][{0}][m]", i), m_Zavady[i].Vyrobce);
					m_User.Cache.SetString(string.Format("protocol.[add][Zavady][{0}][n]", i), m_Zavady[i].Nazev);
					m_User.Cache.SetString(string.Format("protocol.[add][Zavady][{0}][d]", i), m_Zavady[i].Popis);

					// pozice
					m_User.Cache.SetInt32(string.Format("protocol.[add][Zavady][{0}][pos]", i), m_Zavady[i].Pozice.Count);
					for (int j = 0; j < m_Zavady[i].Pozice.Count; j++)
					{
						m_User.Cache.SetInt32(string.Format("protocol.[add][Zavady][{0}][pos][{1}][id]", i, j), m_Zavady[i].Pozice[j].Id);
						m_User.Cache.SetString(string.Format("protocol.[add][Zavady][{0}][pos][{1}][q]", i, j), m_Zavady[i].Pozice[j].Nazev);
						m_User.Cache.SetString(string.Format("protocol.[add][Zavady][{0}][pos][{1}][o]", i, j), m_Zavady[i].Pozice[j].Operace);
						m_User.Cache.SetInt32(string.Format("protocol.[add][Zavady][{0}][pos][{1}][cj]", i, j), m_Zavady[i].Pozice[j].Cj);
					}

					// nd
					m_User.Cache.SetInt32(string.Format("protocol.[add][Zavady][{0}][nd]", i), m_Zavady[i].Nd.Count);
					for (int j = 0; j < m_Zavady[i].Nd.Count; j++)
					{
						m_User.Cache.SetInt32(string.Format("protocol.[add][Zavady][{0}][nd][{1}][id]", i, j), m_Zavady[i].Nd[j].Id);
						m_User.Cache.SetInt32(string.Format("protocol.[add][Zavady][{0}][nd][{1}][q]", i, j), m_Zavady[i].Nd[j].Mnozstvi);
						m_User.Cache.SetInt32(string.Format("protocol.[add][Zavady][{0}][nd][{1}][o]", i, j), m_Zavady[i].Nd[j].Objednat);
					}
				}

			}
			finally
			{
				m_User.Cache.EndUpdate();
			}
		}

		// prepocet & update
		// prepocita cenu tzavada
		public static void UpdateZavadaPrice(MySQLDatabase database, string cisloprotocol)
		{
			MySQLDataReader dr = database.ExecuteReader("SELECT idzavadadealer,stav FROM tzavada WHERE cisloprotokol={0}", cisloprotocol);
			while (dr.Read())
			{
				int idzavadadealer = dr.GetIntValue(0);

				try
				{
					// ak nie je schvalena tak 0
					if (dr.GetIntValue(1) == 0)
					{
						database.Execute(
							"UPDATE tzavada SET prace={0},material={1} WHERE cisloprotokol={2} AND idzavadadealer={3}",
							0, 0, cisloprotocol, idzavadadealer
						);
						continue;
					}

					// tzdilCena
					decimal tzDilCena = 0;
					try
					{
						tzDilCena = Convert.ToDecimal(database.ExecuteScalar(
							"SELECT SUM(A.cena) FROM tzdil A WHERE A.cisloprotokol={0} AND A.idzavadadealer={1} AND A.stav=1",
							 cisloprotocol, idzavadadealer
						));
					}
					catch { }

					// tPozice
					decimal tzPoziceCena = 0;
					try
					{
						tzPoziceCena = Convert.ToDecimal(database.ExecuteScalar(
							"SELECT SUM(A.cena) FROM tzpozice A WHERE A.cisloprotokol={0} AND A.idzavadadealer={1} AND A.stav=1",
							cisloprotocol, idzavadadealer
						));
					}
					catch { }

					// tPoziceVolna
					decimal tzPoziceVolnaCena = 0;
					try
					{
						tzPoziceVolnaCena = Convert.ToDecimal(database.ExecuteScalar(
							"SELECT SUM(A.cena) FROM tzpozicevolna A WHERE A.cisloprotokol={0} AND A.idzavadadealer={1} AND A.stav=1",
							cisloprotocol, idzavadadealer
						));
					}
					catch { }

					database.Execute(
						"UPDATE tzavada SET prace={0},material={1} WHERE cisloprotokol={2} AND idzavadadealer={3}",
						tzPoziceCena + tzPoziceVolnaCena, tzDilCena,
						cisloprotocol, idzavadadealer
					);
				}
				catch { }
			}
		}

		public void LoadMajitel()
		{
			// load majitel or last user entry
			MySQLDataReader dr = Database.ExecuteReader(@"
SELECT 0 AS order1,id,jmeno,jeFO AS fo,ico,ulice,psc,obec FROM tprotocol WHERE karoserie={0} AND stav>2

UNION

SELECT
	1 AS order1,
	0 AS id,
	majitel_jmeno AS jmeno,
	majitel_fo AS fo,
	majitel_ico AS ico,
	majitel_ulice AS ulice,
	majitel_psc AS psc,
	majitel_obec AS obec
FROM tmotocykl WHERE ciskar={0}

ORDER BY order1,id DESC", this.VIN);

			if (!dr.Read()) return;

			this.Jmeno = dr.GetStringValue(2);
			this.JeFO = dr.GetIntValue(3);
			this.ICO = dr.GetStringValue(4);
			this.Adresa = dr.GetStringValue(5);
			this.PSC = dr.GetStringValue(6);
			this.Obec = dr.GetStringValue(7);
		}

	}
}

﻿using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.UI;
using Ka.Garance.Web;

namespace Ka.Garance.Web.TabList
{
	[Flags]
	public enum TabItemState
	{
		None = 0,
		First = 1,
		Last = 2,
		Selected = 4,
		Right = 8
	}

	public class TabItem : GaranceControl, INamingContainer
	{
		private string m_Title;
		private string m_Link;
		private string m_LangID;
		internal bool m_Selected = false;

		public string Title
		{
			get
			{
				if (m_LangID != null && m_LangID != "")
					return (Page as GarancePage).Language[m_LangID];
				return m_Title;
			}
			set { m_Title = value; }
		}

		public string LangID
		{
			get { return m_LangID; }
			set { m_LangID = value; }
		}

		public string Link
		{
			get { return m_Link; }
			set { m_Link = value; }
		}

		public bool Selected
		{
			get
			{
				if (Parent != null)
					return ((Parent as TabList).SelectedTab == this);
				return m_Selected;
			}
			set
			{
				m_Selected = value;
				if (value) Select();
			}
		}

		public void Select()
		{
			try
			{
				(Parent as TabList).SelectedTab = this;
				m_Selected = true;
			}
			catch { }
		}

	}

	internal class TabListBuilder : ControlBuilder
	{
		public override Type GetChildControlType(string tagName, IDictionary attributes)
		{
			if (tagName == "TabItem")
				return typeof(TabItem);
			else
				return null;
		}

		public override void AppendLiteralString(string s)
		{
		}
	}


	[ControlBuilderAttribute(typeof(TabListBuilder)), ParseChildren(false)]
	public class TabList : GaranceControl, INamingContainer
	{
		private string m_Width = "";
		private TabItem m_SelectedTab = null;

		public string TabWidth
		{
			get { return m_Width; }
			set { m_Width = value; }
		}

		public TabItem SelectedTab
		{
			get { return m_SelectedTab; }
			set { m_SelectedTab = value; }
		}

		public int SelectedIndex
		{
			get { return Controls.IndexOf(m_SelectedTab); }
			set { try { m_SelectedTab = Controls[value] as TabItem; } catch { } }
		}

		private void RenderItem(System.Web.UI.HtmlTextWriter writer, TabItem item, TabItemState state, TabItemState prevState)
		{
			writer.Write(" ");

			string img = "";

			// add left td
			if ((state & TabItemState.First) == TabItemState.First)
			{
				if ((state & TabItemState.Selected) == TabItemState.Selected)
					img = "tab.lef";
				else
					img = "tab.ldf";
			}
			else
			{
				if ((state & TabItemState.Selected) == TabItemState.Selected)
					img = "tab.leh";
				else if ((prevState & TabItemState.Selected) == TabItemState.Selected)
					img = "tab.reh";
				else if ((state & TabItemState.Right) == TabItemState.Right)
					img = "tab.rdh";
				else
					img = "tab.ldh";
			}

			// write the left border
			writer.Write(
				"<td valign=\"top\" style=\"width:1%;background:url(images/{0}.bk.png)\"><img src=\"images/{0}.png\" alt=\"\" /></td>",
				img
			);

			if ((state & TabItemState.Selected) == TabItemState.Selected)
				// write the selected item
				writer.Write(
					"<td valign=\"top\" align=\"center\"><div class=\"tabSelected\" style=\"width:{0}\">{1}</div></td>",
					m_Width,
					item.Title
				);
			else
				// write the inactive item
				writer.Write(
					"<td valign=\"top\" align=\"center\"><div class=\"tab\" style=\"width:{0}\"><a href=\"{1}\" class=\"tab\" onfocus=\"this.blur();\">{2}</a></div></td>",
					m_Width,
					item.Link,
					item.Title
				);

			// write right tab end if last item
			if ((state & TabItemState.Last) == TabItemState.Last)
			{
				if ((state & TabItemState.Selected) == TabItemState.Selected)
					img = "tab.ref";
				else
					img = "tab.rdf";

				// write the right border
				writer.Write(
					"<td valign=\"top\" style=\"background:url(images/{0}.bk.png)\"><img src=\"images/{0}.png\" alt=\"\" /></td>",
					img
				);
			}

			writer.Write("\n");

		}

		protected override void Render(System.Web.UI.HtmlTextWriter writer)
		{
			if (Controls.Count == 0) return;

			if (m_SelectedTab == null)
				foreach (TabItem item in Controls)
				{
					if (item.m_Selected)
					{
						item.Select();
						break;
					}
				}
			if (m_SelectedTab == null)
				(Controls[0] as TabItem).Select();

			writer.Write("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:1%;padding-bottom:1px\">\n<tr>\n");

			TabItemState state = TabItemState.None;
			TabItemState statePrevious = TabItemState.None;

			bool right = false;

			for (int i = 0; i < Controls.Count; i++)
			{
				state = TabItemState.None;
				if (i == 0) state = state | TabItemState.First;
				if (i == Controls.Count - 1) state = state | TabItemState.Last;
				if (i == SelectedIndex)
				{
					right = true;
					state = state | TabItemState.Selected;
				}
				else if (right)
					state = state | TabItemState.Right;


				RenderItem(writer, Controls[i] as TabItem, state, statePrevious);

				statePrevious = state;
			}

			writer.Write("</tr>\n</table>");
		}
	}
}

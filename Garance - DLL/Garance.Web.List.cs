﻿using System;
using System.Collections;
using System.Text;
using System.Web.UI;
using Ka.Garance.Web;
using Ka.Garance.Data;

namespace Ka.Garance.Web.List
{
	public enum ListHeaderState
	{
		None = 0, Asc = 1, Desc = 2
	}

	public class ValidateEventArgs : EventArgs
	{
		private object value;
		private bool validated;
		private MySQLDataReader reader;

		public ValidateEventArgs(object value, MySQLDataReader reader)
			: base()
		{
			this.value = value;
			this.reader = reader;
			this.validated = false;
		}

		public object Value
		{
			get { return this.value; }
			set { this.value = value; }
		}

		public bool Validated
		{
			get { return this.validated; }
			set { this.validated = value; }
		}

		public MySQLDataReader Reader
		{
			get { return this.reader; }
		}
	}

	public delegate void ValidateListItem(object sender, ValidateEventArgs e);

	public class ListItem : GaranceControl, INamingContainer
	{
		private string m_Title;
		private string m_ColumnName;
		private string m_SortName;
		private string m_LangID;
		private string m_Format = "";
		private bool m_Orderable = true;
		private bool m_Default;
		private ListHeaderState m_DefaultState = ListHeaderState.Asc;
		private ListHeaderState m_State;
		private IFormatProvider m_FormatProvider = null;

		#region properties & events
		public event ValidateListItem Validate;

		public string Title
		{
			get
			{
				if (m_LangID != null && m_LangID != "")
					return (Page as GarancePage).Language[m_LangID];
				return m_Title;
			}
			set { m_Title = value; }
		}

		public string LangID
		{
			get { return m_LangID; }
			set { m_LangID = value; }
		}

		public string ColumnName
		{
			get { return m_ColumnName; }
			set { m_ColumnName = value; }
		}

		public string SortName
		{
			get
			{
				if (m_SortName == null || m_SortName == "") return m_ColumnName;
				return m_SortName;
			}
			set { m_SortName = value; }
		}

		public string Format
		{
			get { return m_Format; }
			set { m_Format = value; }
		}
		public IFormatProvider FormatProvider
		{
			get { return m_FormatProvider; }
			set { m_FormatProvider = value; }
		}
		public bool Orderable
		{
			get { return m_Orderable; }
			set { m_Orderable = value; }
		}
		public bool Default
		{
			get { return m_Default; }
			set { m_Default = value; }
		}
		public ListHeaderState DefaultState
		{
			get { return m_DefaultState; }
			set { m_DefaultState = value; }
		}
		public ListHeaderState State
		{
			get { return m_State; }
			set { m_State = value; }
		}
		#endregion

		protected internal virtual void OnValidate(ValidateEventArgs e)
		{
			ValidateListItem tmp = this.Validate;
			if (tmp != null)
				tmp(this, e);
		}

		internal void RenderHeader(HtmlTextWriter writer)
		{
			string sortMethod = string.Format("{0},{1}", m_ColumnName, "asc");

			writer.Write(" <th");
			//if (m_Align != "" || m_Align != null) writer.Write(" align=\"{0}\"", m_Align);
			foreach (string key in Attributes.Keys)
				if (!key.StartsWith("td_"))
					writer.Write(" {0}=\"{1}\"", key, Attributes[key]);
			
			if (m_Orderable)
			{
				writer.Write(" class=\"");
				switch (m_State)
				{
					case ListHeaderState.Asc:
						writer.Write("header-asc");
						sortMethod = string.Format("{0},{1}", m_ColumnName, "desc");
						break;
					case ListHeaderState.Desc:
						writer.Write("header-desc");
						break;
					default:
						writer.Write("header");
						break;
				}
				writer.Write("\" onmouseover=\"this.style.cursor='pointer';if(!this.h)this.h=this.className;this.className=this.h+'-active';\" onmouseout=\"this.className=this.h;\" ");

				string ajaxId = (Parent as List).AjaxId;
				if (ajaxId != null && ajaxId != "")
					writer.Write("onclick=\"__gwl_ajax('{0}','sort','{1}');\">", GarancePage.JsEncode(ajaxId), GarancePage.JsEncode(sortMethod));
				else
					writer.Write("onclick=\"location='{0}?sort={1}'\">", System.IO.Path.GetFileName(Page.Request.Path), sortMethod);
			}
			else
				writer.Write(">");
			writer.Write("{0}</th>\n", Title);
		}
	}

	internal class ListBuilder : ControlBuilder
	{
		public override Type GetChildControlType(string tagName, IDictionary attributes)
		{
			if (tagName == "ListItem")
				return typeof(ListItem);
			else
				return null;
		}

		public override void AppendLiteralString(string s)
		{
		}
	}

	[ControlBuilderAttribute(typeof(ListBuilder)), ParseChildren(false)]
	public class List : GaranceControl, INamingContainer
	{
		private string m_Name = "";
		private string m_SQL = "";
		private string m_SQLCount = "";
		private string m_Filter = "";
        private string m_FilterEngine = "";
        private string m_FilterFrame = "";
	    private string m_GroupBy = "";
        private string m_GroupByCount = "";
		private string m_Identifier = "";
		private string m_Link = "";
		private string m_CategoryTitle = "";
		private string m_OnClickRow = "";
		private string m_AjaxId = "";
		private string m_InfoText = null;
		private bool m_RowNumbers = true;
		private bool m_RowSelect = false;
		private bool m_DisplayFooter = true;
		private MySQLDataReader m_Reader = null;
		private int m_TotalCount = 0;

		public string SQL
		{
			get { return m_SQL; }
			set { m_SQL = value; }
		}

		public string SQLCount
		{
			get { return m_SQLCount; }
			set { m_SQLCount = value; }
		}

		public string Filter
		{
			get { return m_Filter; }
			set { m_Filter = value; }
		}

        public string FilterEngine
        {
            get { return m_FilterEngine; }
            set { m_FilterEngine = value; }
        }

        public string FilterFrame
        {
            get { return m_FilterFrame; }
            set { m_FilterFrame = value; }
        }

		public string Identifier
		{
			get { return m_Identifier; }
			set { m_Identifier = value; }
		}

		public string Link
		{
			get { return m_Link; }
			set { m_Link = value; }
		}

		public string JsClickRow
		{
			get { return m_OnClickRow; }
			set { m_OnClickRow = value; }
		}

		public string AjaxId
		{
			get { return m_AjaxId; }
			set { m_AjaxId = value; }
		}

		public string Name
		{
			get { return m_Name; }
			set { m_Name = value; }
		}

		public string CategoryTitle
		{
			get { return m_CategoryTitle; }
			set { m_CategoryTitle = value; }
		}

		public bool RowNumbers
		{
			get { return m_RowNumbers; }
			set { m_RowNumbers = value; }
		}

		public bool RowSelect
		{
			get { return m_RowSelect; }
			set { m_RowSelect = value; }
		}

		public bool DisplayFooter
		{
			get { return m_DisplayFooter; }
			set { m_DisplayFooter = value; }
		}

		public string InfoText
		{
			get { return m_InfoText; }
			set { m_InfoText = value; }
		}

		private int PageNo
		{
			get { try { return Math.Max(0, Convert.ToInt32(GetVS("page"))); } catch { return 0; } }
			set { SetVS("page", value); }
		}

		private int PageSize
		{
			get { try { return Math.Max(10, Math.Min(60, (int)(Convert.ToInt32(GetVS("size", 60)) / 5) * 5)); } catch { return 15; } }
			set { SetVS("size", value); }
		}

		private string PageSort
		{
			get { return Convert.ToString(GetVS("sort")); }
			set { SetVS("sort", value); }
		}

	    public string GroupBy
	    {
	        get { return m_GroupBy; }
	        set { m_GroupBy = value; }
	    }

	    public string GroupByCount
	    {
	        get { return m_GroupByCount; }
	        set { m_GroupByCount = value; }
	    }

	    private void SetVS(string key, object value)
		{
			try
			{
				(this.Page as GarancePage).User.Cache.SetString(string.Format("{0}.{1}", Name, key), Convert.ToString(value));
			}
			catch { }
		}

		private object GetVS(string key, object defVal)
		{
			string val = null;
			try
			{
				val = (this.Page as GarancePage).User.Cache.GetString(string.Format("{0}.{1}", Name, key));
			}
			catch
			{
				val = null;
			}
			if (val == null) return defVal;
			return val;
		}

		private object GetVS(string key)
		{
			return GetVS(key, null);
		}

		private void UpdateUserCache()
		{
			GarancePage p = (this.Page as GarancePage);
			string page = Request.QueryString["page"];
			string size = Request.QueryString["size"];
			string sort = Request.QueryString["sort"];

			if (page != null)
				try { PageNo = Convert.ToInt32(page); }
				catch { }
			if (size != null)
				try { PageSize = Convert.ToInt32(size); }
				catch { }
			if (sort != null)
				try { PageSort = sort; }
				catch { }
		}

		private MySQLDataReader GetItems()
		{
			UpdateUserCache();

			var sb = new StringBuilder();
		    string sqlCount = "";

			SqlString(out sb, out sqlCount, m_Filter);
		    

            //TODO repair this
            if (m_TotalCount <= 0 && !(string.IsNullOrEmpty(m_FilterEngine)))
            {
                SqlString(out sb, out sqlCount, m_FilterEngine);
            }
            if (m_TotalCount <= 0 && !(string.IsNullOrEmpty(m_FilterFrame)))
            {
                SqlString(out sb, out sqlCount, m_FilterFrame);
            }

            m_TotalCount = Convert.ToInt32((Page as GarancePage).Database.ExecuteScalar(sqlCount));

		    // append order
			string sortDefault = PageSort; if (sortDefault == null) sortDefault = "";
			string[] sd = sortDefault.ToLower().Split(",".ToCharArray());
			if (sd.Length != 2) sortDefault = "";
			ListItem sortCol = (Controls[0] as ListItem);
			ListItem defaultCol = sortCol;
			foreach (ListItem item in Controls)
			{
				if (item.Default) defaultCol = item;
				if (sortDefault == "" && item.Default)
				{
					sortCol = item;
					sortCol.State = item.DefaultState;
					break;
				}
				else if (sortDefault != "" && sd[0] == item.ColumnName.ToLower())
				{
					sortCol = item;
					switch (sd[1])
					{
						case "asc":
							sortCol.State = ListHeaderState.Asc;
							break;
						default:
							sortCol.State = ListHeaderState.Desc;
							break;
					}
				}
				else
					item.State = ListHeaderState.None;
			}

			int pageNo = Math.Min(PageNo, Math.Max(0, (int)Math.Ceiling((double)m_TotalCount / PageSize) - 1));
			string sortDir = (sortCol.State == ListHeaderState.Desc ? "DESC" : "ASC");

			sb.AppendFormat("\nORDER BY {0} {1},{2} {3}", sortCol.SortName, sortDir, defaultCol.SortName, sortDir);
			sb.AppendFormat("\nLIMIT {0},{1}", pageNo * PageSize, PageSize);

			// perform the query
			return (Page as GarancePage).Database.ExecuteReader(sb.ToString());
		}

        private void SqlString(out StringBuilder sb, out string sqlCount, string filter)
        {
            sb = new StringBuilder();
	        sb.Append(SQL);

	        // append filter
	        if (filter != null && filter != "") 
	            sb.AppendFormat("\nWHERE {0}", filter);

	        sqlCount = m_SQLCount;
	        if (sqlCount == null || sqlCount == "")
	        {
	            sqlCount = sb.ToString();
	            sqlCount = string.Format(
	                "SELECT COUNT(*) {0}",
	                sqlCount.Substring(sqlCount.IndexOf("FROM", StringComparison.InvariantCultureIgnoreCase))
	                );
	        }

            //append group by, must be after sqlCount
            if (!string.IsNullOrEmpty(m_GroupBy))
                sb.AppendFormat("\n {0}", m_GroupBy);
	    }

	    private void RenderHeader(HtmlTextWriter writer)
		{
			// category
			if (m_CategoryTitle != null && m_CategoryTitle != "")
			{
				writer.Write("<tr class=\"category\"><td colspan=\"{0}\" align=\"left\">{1}</td></tr>\n", Controls.Count + (RowNumbers ? 1 : 0) + (RowSelect ? 1 : 0), m_CategoryTitle);
			}
			// header
			writer.Write("<tr class=\"header\">\n");
			if (RowSelect)
			{
				writer.Write(" <th style=\"width:1%\">&nbsp;</th>\n");
			}
			if (RowNumbers)
			{
				writer.Write(" <th style=\"width:1%\">&nbsp;</th>\n");
			}
			foreach (ListItem item in Controls)
			{
				item.RenderHeader(writer);
			}
			writer.Write("</tr>\n");
		}

		private void RenderItems(HtmlTextWriter writer)
		{
			int row = 0;
			while (m_Reader.Read() && row++ <= PageSize)
			{
				writer.Write("<tr class=\"{0}\"", (row%2==0?"odd":"even"));
				// pridaj link ak je definovany
				if (m_Link != null && m_Link != "")
				{
					writer.Write(" onmouseover=\"this.style.cursor='pointer';if(!this.h)this.h=this.className;this.className=this.h+'-active';\"");
					writer.Write(" onmouseout=\"this.className=this.h;\"");
					writer.Write(" onclick=\"location='{0}';\"", string.Format(m_Link, m_Reader.GetValue(m_Identifier)));
				}
				else if (m_OnClickRow != null && m_OnClickRow != "")
				{
					writer.Write(" onmouseover=\"this.style.cursor='pointer';if(!this.h)this.h=this.className;this.className=this.h+'-active';\"");
					writer.Write(" onmouseout=\"this.className=this.h;\"");
					writer.Write(" onclick=\"{0}\"", string.Format(m_OnClickRow, m_Reader.GetValue(m_Identifier)));
				}
				writer.Write(">\n");
				if (RowSelect)
				{
					writer.Write(" <td style=\"width:1%\" class=\"left\">&nbsp;</td>\n");
				}
				if (RowNumbers)
				{
					writer.Write(" <td align=\"left\" style=\"width:1%\">{0}.</td>\n", PageNo * PageSize + row);
				}
				foreach (ListItem item in Controls)
				{
					object val = m_Reader.GetValue(item.ColumnName);
					if (val is string)
					{
						val = Server.HtmlEncode(Convert.ToString(val));
					}

					ValidateEventArgs e = new ValidateEventArgs(val, m_Reader);
					item.OnValidate(e);

					if (e.Validated)
						val = e.Value;
					else if (item.Format != null && item.Format != "")
					{
						IFormatProvider fp = item.FormatProvider;
						if (fp == null) fp = System.Globalization.CultureInfo.GetCultureInfo((Page as GarancePage).Language.Code);
						val = string.Format(fp, item.Format, val);
					}
					writer.Write(" <td");
					foreach (string key in item.Attributes.Keys)
						if (key.StartsWith("td_"))
							writer.Write(" {0}=\"{1}\"", key.Substring(3), item.Attributes[key]);
					writer.Write(">{0}</td>\n", val);
				}
				writer.Write("</tr>\n");
			}
		}

		// vykresli navigaciu
		private void RenderFooter(HtmlTextWriter writer)
		{
			writer.Write("<tr class=\"footer\">\n <td colspan=\"{0}\">\n", Controls.Count + (RowNumbers ? 1 : 0) + (RowSelect ? 1 : 0));

			writer.Write("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:100%\">\n<tr>\n");
			writer.Write(" <td align=\"left\" width=\"25%\" nowrap=\"nowrap\">\n{0}:\n", Language["LIST_PAGE"]);

			string root = System.IO.Path.GetFileName(Request.Path);
			int pageSize = PageSize;
			int pageNo = Math.Min(PageNo, Math.Max(0, (int)Math.Ceiling((double)m_TotalCount / pageSize) - 1));
			int pageSplit = 10;
			int pageStart = (pageNo / pageSplit) * pageSplit;
			if (pageStart > 0) writer.Write("<a href=\"{0}?page={1}\" class=\"page\">&laquo;</a>\n", root, pageStart-1);
			while (pageSplit>0)
			{
				if (pageStart * pageSize >= m_TotalCount) break;
				if (pageStart == pageNo)
					writer.Write("<b class=\"page\">{0}</b>\n", pageStart + 1);
				else
					writer.Write("<a href=\"{0}?page={1}\" class=\"page\">{2}</a>\n", root, pageStart, pageStart + 1);
				pageStart++;
				pageSplit--;
			}

			if (pageStart * pageSize < m_TotalCount) writer.Write("<a href=\"{0}?page={1}\" class=\"page\">&raquo;</a>\n", root, pageStart);

			writer.Write(" </td>\n <td align=\"center\" width=\"50%\">\n");
			writer.Write("<a href=\"{0}?size={1}\"><img src=\"images/size.collapse.png\" alt=\"\" style=\"margin:4px;vertical-align:middle\" /></a>\n", root, PageSize - 10);
			writer.Write("<a href=\"{0}?size={1}\"><img src=\"images/size.expand.png\" alt=\"\" style=\"margin:4px;vertical-align:middle\" /></a>\n", root, PageSize + 10);
			writer.Write(" </td>\n <td align=\"right\" width=\"25%\">{0}: {1} - {2} / {3}&nbsp;</td>\n", Language["LIST_RECORDS"], pageNo * pageSize + 1, Math.Min(m_TotalCount, (pageNo + 1) * pageSize), m_TotalCount);
			writer.Write("</tr>\n</table>");

			writer.Write("\n </td>\n</tr>\n");
		}

		private void RenderFooterAjax(HtmlTextWriter writer)
		{
			writer.Write("<tr class=\"footer\">\n <td colspan=\"{0}\">\n", Controls.Count + (RowNumbers ? 1 : 0) + (RowSelect ? 1 : 0));

			writer.Write("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:100%\">\n<tr>\n");
			writer.Write(" <td align=\"left\" width=\"25%\">\n{0}:\n", Language["LIST_PAGE"]);

			string root = System.IO.Path.GetFileName(Request.Path);
			int pageSize = PageSize;
			int pageNo = Math.Min(PageNo, Math.Max(0, (int)Math.Ceiling((double)m_TotalCount / pageSize) - 1));
			int pageSplit = 10;
			int pageStart = (pageNo / pageSplit) * pageSplit;
			if (pageStart > 0) writer.Write("<a href=\"javascript:void(0);\" class=\"page\" onclick=\"__gwl_ajax('{1}','page',{0});return false;\">&laquo;</a>\n", pageStart - 1, GarancePage.JsEncode(AjaxId));
			while (pageSplit > 0)
			{
				if (pageStart * pageSize >= m_TotalCount) break;
				if (pageStart == pageNo)
					writer.Write("<b class=\"page\">{0}</b>\n", pageStart + 1);
				else
					writer.Write("<a href=\"javascript:void(0);\" class=\"page\" onclick=\"__gwl_ajax('{1}','page',{0});return false;\">{2}</a>\n", pageStart, GarancePage.JsEncode(AjaxId), pageStart + 1);
				pageStart++;
				pageSplit--;
			}

			if (pageStart * pageSize < m_TotalCount) writer.Write("<a href=\"javascript:void(0);\" class=\"page\" onclick=\"__gwl_ajax('{1}','page',{0});return false;\">&raquo;</a>\n", pageStart, GarancePage.JsEncode(AjaxId));

			writer.Write(" </td>\n <td align=\"center\" width=\"50%\">\n");
			writer.Write("<a href=\"javascript:void(0);\" onclick=\"__gwl_ajax('{0}','size',{1});return false;\"><img src=\"images/size.collapse.png\" alt=\"\" style=\"margin:4px;vertical-align:middle\" /></a>\n", GarancePage.JsEncode(AjaxId), PageSize - 10);
			writer.Write("<a href=\"javascript:void(0);\" onclick=\"__gwl_ajax('{0}','size',{1});return false;\"><img src=\"images/size.expand.png\" alt=\"\" style=\"margin:4px;vertical-align:middle\" /></a>\n", GarancePage.JsEncode(AjaxId), PageSize + 10);
			writer.Write(" </td>\n <td align=\"right\" width=\"25%\">{0}: {1} - {2} / {3}&nbsp;</td>\n", Language["LIST_RECORDS"], pageNo * pageSize + 1, Math.Min(m_TotalCount, (pageNo + 1) * pageSize + 1), m_TotalCount);
			writer.Write("</tr>\n</table>");

			writer.Write("\n </td>\n</tr>\n");
		}

		protected override void Render(HtmlTextWriter writer)
		{
			m_Reader = GetItems();

			writer.Write("<table cellpadding=\"4\" cellspacing=\"1\" border=\"0\" class=\"list\">\n");
			RenderHeader(writer);
			if (m_TotalCount == 0)
			{
				writer.Write("<tr class=\"info\"><td colspan=\"{0}\">{1}</td></tr>\n", Controls.Count + (RowNumbers ? 1 : 0) + (RowSelect ? 1 : 0), Language["LIST_NODATA"]);
				writer.Write("<tr class=\"footer\"><td colspan=\"{0}\"></td></tr>\n", Controls.Count + (RowNumbers ? 1 : 0) + (RowSelect ? 1 : 0));
			}
			else
			{
				RenderItems(writer);
				if (m_DisplayFooter)
				{
					if (AjaxId != null && AjaxId != "")
						RenderFooterAjax(writer);
					else
						RenderFooter(writer);
				}

				if (m_InfoText != null && m_InfoText.Trim() != "")
					writer.Write("<tr class=\"infotext\"><td align=\"left\" colspan=\"{0}\">{1}</td></tr>", Controls.Count + (RowNumbers ? 1 : 0) + (RowSelect ? 1 : 0), m_InfoText);
			}
			writer.Write("</table>");
		}

	}




}
